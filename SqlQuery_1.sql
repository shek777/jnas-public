﻿/*
  
   ELMAH - Error Logging Modules and Handlers for ASP.NET
   Copyright (c) 2004-9 Atif Aziz. All rights reserved.
  
    Author(s):
  
        Atif Aziz, http://www.raboof.com
        Phil Haacked, http://haacked.com
  
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
  
      http://www.apache.org/licenses/LICENSE-2.0
  
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  
*/

-- ELMAH DDL script for Microsoft SQL Server 2000 or later.

-- $Id: SQLServer.sql 677 2009-09-29 18:02:39Z azizatif $

DECLARE @DBCompatibilityLevel INT
DECLARE @DBCompatibilityLevelMajor INT
DECLARE @DBCompatibilityLevelMinor INT

SELECT 
    @DBCompatibilityLevel = cmptlevel 
FROM 
    master.dbo.sysdatabases 
WHERE 
    name = DB_NAME()

IF @DBCompatibilityLevel <> 80
BEGIN

    SELECT @DBCompatibilityLevelMajor = @DBCompatibilityLevel / 10, 
           @DBCompatibilityLevelMinor = @DBCompatibilityLevel % 10
           
    PRINT N'
    ===========================================================================
    WARNING! 
    ---------------------------------------------------------------------------
    
    This script is designed for Microsoft SQL Server 2000 (8.0) but your 
    database is set up for compatibility with version ' 
    + CAST(@DBCompatibilityLevelMajor AS NVARCHAR(80)) 
    + N'.' 
    + CAST(@DBCompatibilityLevelMinor AS NVARCHAR(80)) 
    + N'. Although 
    the script should work with later versions of Microsoft SQL Server, 
    you can ensure compatibility by executing the following statement:
    
    ALTER DATABASE [' 
    + DB_NAME() 
    + N'] 
    SET COMPATIBILITY_LEVEL = 80

    If you are hosting ELMAH in the same database as your application 
    database and do not wish to change the compatibility option then you 
    should create a separate database to host ELMAH where you can set the 
    compatibility level more freely.
    
    If you continue with the current setup, please report any compatibility 
    issues you encounter over at:
    
    http://code.google.com/p/elmah/issues/list

    ===========================================================================
'
END

GO

/* ------------------------------------------------------------------------ 
        TABLES
   ------------------------------------------------------------------------ */

CREATE TABLE [dbo].[ELMAH_Error]
(
    [ErrorId]     UNIQUEIDENTIFIER NOT NULL,
    [Application] NVARCHAR(60)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Host]        NVARCHAR(50)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Type]        NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Source]      NVARCHAR(60)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Message]     NVARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [User]        NVARCHAR(50)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [StatusCode]  INT NOT NULL,
    [TimeUtc]     DATETIME NOT NULL,
    [Sequence]    INT IDENTITY (1, 1) NOT NULL,
    [AllXml]      NTEXT COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) 
ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[ELMAH_Error] WITH NOCHECK ADD 
    CONSTRAINT [PK_ELMAH_Error] PRIMARY KEY NONCLUSTERED ([ErrorId]) ON [PRIMARY] 

GO

ALTER TABLE [dbo].[ELMAH_Error] ADD 
    CONSTRAINT [DF_ELMAH_Error_ErrorId] DEFAULT (NEWID()) FOR [ErrorId]

GO

CREATE NONCLUSTERED INDEX [IX_ELMAH_Error_App_Time_Seq] ON [dbo].[ELMAH_Error] 
(
    [Application]   ASC,
    [TimeUtc]       DESC,
    [Sequence]      DESC
) 
ON [PRIMARY]

GO

/* ------------------------------------------------------------------------ 
        STORED PROCEDURES                                                      
   ------------------------------------------------------------------------ */

SET QUOTED_IDENTIFIER ON 

GO

SET ANSI_NULLS ON 

GO

CREATE PROCEDURE [dbo].[ELMAH_GetErrorXml]
(
    @Application NVARCHAR(60),
    @ErrorId UNIQUEIDENTIFIER
)
AS

    SET NOCOUNT ON

    SELECT 
        [AllXml]
    FROM 
        [ELMAH_Error]
    WHERE
        [ErrorId] = @ErrorId
    AND
        [Application] = @Application

GO

SET QUOTED_IDENTIFIER OFF 

GO

SET ANSI_NULLS ON 

GO

SET QUOTED_IDENTIFIER ON 

GO

SET ANSI_NULLS ON 

GO

CREATE PROCEDURE [dbo].[ELMAH_GetErrorsXml]
(
    @Application NVARCHAR(60),
    @PageIndex INT = 0,
    @PageSize INT = 15,
    @TotalCount INT OUTPUT
)
AS 

    SET NOCOUNT ON

    DECLARE @FirstTimeUTC DATETIME
    DECLARE @FirstSequence INT
    DECLARE @StartRow INT
    DECLARE @StartRowIndex INT

    SELECT 
        @TotalCount = COUNT(1) 
    FROM 
        [ELMAH_Error]
    WHERE 
        [Application] = @Application

    -- Get the ID of the first error for the requested page

    SET @StartRowIndex = @PageIndex * @PageSize + 1

    IF @StartRowIndex <= @TotalCount
    BEGIN

        SET ROWCOUNT @StartRowIndex

        SELECT  
            @FirstTimeUTC = [TimeUtc],
            @FirstSequence = [Sequence]
        FROM 
            [ELMAH_Error]
        WHERE   
            [Application] = @Application
        ORDER BY 
            [TimeUtc] DESC, 
            [Sequence] DESC

    END
    ELSE
    BEGIN

        SET @PageSize = 0

    END

    -- Now set the row count to the requested page size and get
    -- all records below it for the pertaining application.

    SET ROWCOUNT @PageSize

    SELECT 
        errorId     = [ErrorId], 
        application = [Application],
        host        = [Host], 
        type        = [Type],
        source      = [Source],
        message     = [Message],
        [user]      = [User],
        statusCode  = [StatusCode], 
        time        = CONVERT(VARCHAR(50), [TimeUtc], 126) + 'Z'
    FROM 
        [ELMAH_Error] error
    WHERE
        [Application] = @Application
    AND
        [TimeUtc] <= @FirstTimeUTC
    AND 
        [Sequence] <= @FirstSequence
    ORDER BY
        [TimeUtc] DESC, 
        [Sequence] DESC
    FOR
        XML AUTO

GO

SET QUOTED_IDENTIFIER OFF 

GO

SET ANSI_NULLS ON 

GO

SET QUOTED_IDENTIFIER ON 

GO

SET ANSI_NULLS ON 

GO

CREATE PROCEDURE [dbo].[ELMAH_LogError]
(
    @ErrorId UNIQUEIDENTIFIER,
    @Application NVARCHAR(60),
    @Host NVARCHAR(30),
    @Type NVARCHAR(100),
    @Source NVARCHAR(60),
    @Message NVARCHAR(500),
    @User NVARCHAR(50),
    @AllXml NTEXT,
    @StatusCode INT,
    @TimeUtc DATETIME
)
AS

    SET NOCOUNT ON

    INSERT
    INTO
        [ELMAH_Error]
        (
            [ErrorId],
            [Application],
            [Host],
            [Type],
            [Source],
            [Message],
            [User],
            [AllXml],
            [StatusCode],
            [TimeUtc]
        )
    VALUES
        (
            @ErrorId,
            @Application,
            @Host,
            @Type,
            @Source,
            @Message,
            @User,
            @AllXml,
            @StatusCode,
            @TimeUtc
        )

GO

SET QUOTED_IDENTIFIER OFF 

GO

SET ANSI_NULLS ON 

CREATE TABLE [dbo].[Addresses] (
    [AddressId] [int] NOT NULL IDENTITY,
    [organizationId] [int] NOT NULL,
    [organization] [nvarchar](max),
    [line1] [nvarchar](max) NOT NULL,
    [line2] [nvarchar](max),
    [city] [nvarchar](max),
    [zipCode] [nvarchar](max),
    [defaultAddress] [bit] NOT NULL,
    CONSTRAINT [PK_dbo.Addresses] PRIMARY KEY ([AddressId])
)
CREATE INDEX [IX_organizationId] ON [dbo].[Addresses]([organizationId])
CREATE TABLE [dbo].[Organizations] (
    [organizationId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max) NOT NULL,
    [organizationTypeId] [int] NOT NULL,
    [organizationType] [nvarchar](max),
    [ParentCompany] [nvarchar](max),
    [ParentCompanyOCN] [nvarchar](max),
    [OCN] [nvarchar](max),
    [OFN] [nvarchar](max),
    [createdDate] [datetime],
    [dateChanged] [datetime],
    [organizationStateId] [int] NOT NULL,
    [state] [nvarchar](max),
    CONSTRAINT [PK_dbo.Organizations] PRIMARY KEY ([organizationId])
)
CREATE INDEX [IX_organizationTypeId] ON [dbo].[Organizations]([organizationTypeId])
CREATE INDEX [IX_organizationStateId] ON [dbo].[Organizations]([organizationStateId])
CREATE TABLE [dbo].[Applications] (
    [applicationId] [int] NOT NULL IDENTITY,
    [organizationId] [int] NOT NULL,
    [application1] [nvarchar](max),
    [applicationReference] [nvarchar](max) NOT NULL,
    [applicant] [nvarchar](max),
    [recipient] [nvarchar](max),
    [UserId] [nvarchar](128) NOT NULL,
    [user] [nvarchar](max),
    [serviceId] [int] NOT NULL,
    [service] [nvarchar](max),
    [resourceTypeId] [int] NOT NULL,
    [resourceType] [nvarchar](max),
    [resourceSubTypeId] [int] NOT NULL,
    [resourceSubType] [nvarchar](max),
    [subscriber] [nvarchar](max),
    [customerContactNumber] [nvarchar](max),
    [localRoutingNumber] [nvarchar](max),
    [FileName] [nvarchar](255),
    [ContentType] [nvarchar](100),
    [additionalDocumentation] [varbinary](max),
    [createdDate] [datetime],
    [dateChanged] [datetime],
    [applicationResourceId] [int] NOT NULL,
    [applicationTypeId] [int] NOT NULL,
    [applicationStateId] [int] NOT NULL,
    [state] [nvarchar](max),
    CONSTRAINT [PK_dbo.Applications] PRIMARY KEY ([applicationId])
)
CREATE INDEX [IX_organizationId] ON [dbo].[Applications]([organizationId])
CREATE INDEX [IX_UserId] ON [dbo].[Applications]([UserId])
CREATE INDEX [IX_resourceTypeId] ON [dbo].[Applications]([resourceTypeId])
CREATE INDEX [IX_resourceSubTypeId] ON [dbo].[Applications]([resourceSubTypeId])
CREATE INDEX [IX_applicationTypeId] ON [dbo].[Applications]([applicationTypeId])
CREATE INDEX [IX_applicationStateId] ON [dbo].[Applications]([applicationStateId])
CREATE TABLE [dbo].[ApplicationNotes] (
    [applicationNotesId] [int] NOT NULL IDENTITY,
    [applicationId] [int] NOT NULL,
    [application] [nvarchar](max),
    [note] [nvarchar](max) NOT NULL,
    [UserId] [nvarchar](max) NOT NULL,
    [user] [nvarchar](max),
    [internalNote] [nvarchar](max),
    [createdDate] [datetime],
    [dateChanged] [datetime],
    CONSTRAINT [PK_dbo.ApplicationNotes] PRIMARY KEY ([applicationNotesId])
)
CREATE INDEX [IX_applicationId] ON [dbo].[ApplicationNotes]([applicationId])
CREATE TABLE [dbo].[ApplicationResources] (
    [applicationResourceId] [int] NOT NULL IDENTITY,
    [applicationId] [int] NOT NULL,
    [application] [nvarchar](max),
    [resourceId] [int] NOT NULL,
    [resource] [nvarchar](max),
    [createdDate] [datetime],
    [dateChanged] [datetime],
    [applicationResourceStateId] [int] NOT NULL,
    [state] [nvarchar](max),
    [Application_applicationId] [int] NOT NULL,
    [ApplicationResourceState_applicationResourceStateId] [int],
    [ApplicationResourceState_applicationResourceStateId1] [int] NOT NULL,
    [Resource_resourceId] [int],
    [Resource_resourceId1] [int] NOT NULL,
    [Application_applicationId1] [int],
    [Organization_organizationId] [int],
    CONSTRAINT [PK_dbo.ApplicationResources] PRIMARY KEY ([applicationResourceId])
)
CREATE INDEX [IX_Application_applicationId] ON [dbo].[ApplicationResources]([Application_applicationId])
CREATE INDEX [IX_ApplicationResourceState_applicationResourceStateId] ON [dbo].[ApplicationResources]([ApplicationResourceState_applicationResourceStateId])
CREATE INDEX [IX_ApplicationResourceState_applicationResourceStateId1] ON [dbo].[ApplicationResources]([ApplicationResourceState_applicationResourceStateId1])
CREATE INDEX [IX_Resource_resourceId] ON [dbo].[ApplicationResources]([Resource_resourceId])
CREATE INDEX [IX_Resource_resourceId1] ON [dbo].[ApplicationResources]([Resource_resourceId1])
CREATE INDEX [IX_Application_applicationId1] ON [dbo].[ApplicationResources]([Application_applicationId1])
CREATE INDEX [IX_Organization_organizationId] ON [dbo].[ApplicationResources]([Organization_organizationId])
CREATE TABLE [dbo].[ApplicationResourceStates] (
    [applicationResourceStateId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max),
    CONSTRAINT [PK_dbo.ApplicationResourceStates] PRIMARY KEY ([applicationResourceStateId])
)
CREATE TABLE [dbo].[Resources] (
    [resourceId] [int] NOT NULL IDENTITY,
    [resource1] [nvarchar](max) NOT NULL,
    [resourceTypeId] [int] NOT NULL,
    [type] [nvarchar](max),
    [resourceSubTypeId] [int] NOT NULL,
    [subtype] [nvarchar](max),
    [binary] [nvarchar](max),
    [createdDate] [datetime],
    [dateChanged] [datetime],
    [resourceLinkId] [int] NOT NULL,
    [resourceStateId] [int] NOT NULL,
    [state] [nvarchar](max),
    [ResourceState_resourceStateId] [int],
    [ResourceState_resourceStateId1] [int] NOT NULL,
    CONSTRAINT [PK_dbo.Resources] PRIMARY KEY ([resourceId])
)
CREATE INDEX [IX_ResourceState_resourceStateId] ON [dbo].[Resources]([ResourceState_resourceStateId])
CREATE INDEX [IX_ResourceState_resourceStateId1] ON [dbo].[Resources]([ResourceState_resourceStateId1])
CREATE TABLE [dbo].[ResourceLinks] (
    [resourceLinkId] [int] NOT NULL IDENTITY,
    [parentResource] [nvarchar](max),
    [childResource] [nvarchar](max),
    [linkedResourceStateId] [int] NOT NULL,
    [state] [nvarchar](max),
    [LinkedResourceState_linkedResourceStateId] [int],
    [LinkedResourceState_linkedResourceStateId1] [int] NOT NULL,
    [Resource_resourceId] [int] NOT NULL,
    [Resource_resourceId1] [int],
    CONSTRAINT [PK_dbo.ResourceLinks] PRIMARY KEY ([resourceLinkId])
)
CREATE INDEX [IX_LinkedResourceState_linkedResourceStateId] ON [dbo].[ResourceLinks]([LinkedResourceState_linkedResourceStateId])
CREATE INDEX [IX_LinkedResourceState_linkedResourceStateId1] ON [dbo].[ResourceLinks]([LinkedResourceState_linkedResourceStateId1])
CREATE INDEX [IX_Resource_resourceId] ON [dbo].[ResourceLinks]([Resource_resourceId])
CREATE INDEX [IX_Resource_resourceId1] ON [dbo].[ResourceLinks]([Resource_resourceId1])
CREATE TABLE [dbo].[LinkedResourceStates] (
    [linkedResourceStateId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max),
    CONSTRAINT [PK_dbo.LinkedResourceStates] PRIMARY KEY ([linkedResourceStateId])
)
CREATE TABLE [dbo].[NotificationDetails] (
    [notificationDetailId] [bigint] NOT NULL IDENTITY,
    [notificationId] [int] NOT NULL,
    [notification] [nvarchar](max),
    [userTypeId] [int] NOT NULL,
    [userType] [nvarchar](max),
    [resourceTypeId] [int] NOT NULL,
    [resourceType] [nvarchar](max),
    [resourceSubTypeId] [int] NOT NULL,
    [resourceSubType] [nvarchar](max),
    [linkedResourceStateId] [int] NOT NULL,
    [state] [nvarchar](max),
    CONSTRAINT [PK_dbo.NotificationDetails] PRIMARY KEY ([notificationDetailId])
)
CREATE INDEX [IX_notificationId] ON [dbo].[NotificationDetails]([notificationId])
CREATE INDEX [IX_userTypeId] ON [dbo].[NotificationDetails]([userTypeId])
CREATE INDEX [IX_resourceTypeId] ON [dbo].[NotificationDetails]([resourceTypeId])
CREATE INDEX [IX_resourceSubTypeId] ON [dbo].[NotificationDetails]([resourceSubTypeId])
CREATE INDEX [IX_linkedResourceStateId] ON [dbo].[NotificationDetails]([linkedResourceStateId])
CREATE TABLE [dbo].[Notifications] (
    [notificationId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max) NOT NULL,
    [mandatory] [bit] NOT NULL,
    CONSTRAINT [PK_dbo.Notifications] PRIMARY KEY ([notificationId])
)
CREATE TABLE [dbo].[UserNotifications] (
    [userNotificationId] [int] NOT NULL IDENTITY,
    [UserId] [nvarchar](128) NOT NULL,
    [user] [nvarchar](max),
    [notificationId] [int] NOT NULL,
    [notification] [nvarchar](max),
    [assigned] [bit] NOT NULL,
    CONSTRAINT [PK_dbo.UserNotifications] PRIMARY KEY ([userNotificationId])
)
CREATE INDEX [IX_UserId] ON [dbo].[UserNotifications]([UserId])
CREATE INDEX [IX_notificationId] ON [dbo].[UserNotifications]([notificationId])
CREATE TABLE [dbo].[Users] (
    [UserId] [nvarchar](128) NOT NULL,
    [name] [nvarchar](max) NOT NULL,
    [userNo] [int] NOT NULL IDENTITY,
    [organizationId] [int] NOT NULL,
    [organization] [nvarchar](max),
    [userTypeId] [int] NOT NULL,
    [userType] [nvarchar](max),
    [userProfileId] [int] NOT NULL,
    [userProfile] [nvarchar](max),
    [primaryContact] [bit] NOT NULL,
    [securityQuestion] [nvarchar](max) NOT NULL,
    [securityAnswer] [nvarchar](max) NOT NULL,
    [createdDate] [datetime],
    [dateChanged] [datetime],
    [lastConnectionDate] [datetime],
    [userStateId] [int] NOT NULL,
    [state] [nvarchar](max),
    [Email] [nvarchar](256),
    [EmailConfirmed] [bit] NOT NULL,
    [PasswordHash] [nvarchar](max),
    [SecurityStamp] [nvarchar](max),
    [PhoneNumber] [nvarchar](max),
    [PhoneNumberConfirmed] [bit] NOT NULL,
    [TwoFactorEnabled] [bit] NOT NULL,
    [LockoutEndDateUtc] [datetime],
    [LockoutEnabled] [bit] NOT NULL,
    [AccessFailedCount] [int] NOT NULL,
    [UserName] [nvarchar](256) NOT NULL,
    CONSTRAINT [PK_dbo.Users] PRIMARY KEY ([UserId])
)
CREATE INDEX [IX_organizationId] ON [dbo].[Users]([organizationId])
CREATE INDEX [IX_userTypeId] ON [dbo].[Users]([userTypeId])
CREATE INDEX [IX_userStateId] ON [dbo].[Users]([userStateId])
CREATE UNIQUE INDEX [UserNameIndex] ON [dbo].[Users]([UserName])
CREATE TABLE [dbo].[UserClaims] (
    [UserClaimId] [int] NOT NULL IDENTITY,
    [UserId] [nvarchar](128) NOT NULL,
    [ClaimType] [nvarchar](max),
    [ClaimValue] [nvarchar](max),
    CONSTRAINT [PK_dbo.UserClaims] PRIMARY KEY ([UserClaimId])
)
CREATE INDEX [IX_UserId] ON [dbo].[UserClaims]([UserId])
CREATE TABLE [dbo].[UserLogins] (
    [LoginProvider] [nvarchar](128) NOT NULL,
    [ProviderKey] [nvarchar](128) NOT NULL,
    [UserId] [nvarchar](128) NOT NULL,
    CONSTRAINT [PK_dbo.UserLogins] PRIMARY KEY ([LoginProvider], [ProviderKey], [UserId])
)
CREATE INDEX [IX_UserId] ON [dbo].[UserLogins]([UserId])
CREATE TABLE [dbo].[UserRoles] (
    [UserId] [nvarchar](128) NOT NULL,
    [RoleId] [nvarchar](128) NOT NULL,
    [Discriminator] [nvarchar](128) NOT NULL,
    CONSTRAINT [PK_dbo.UserRoles] PRIMARY KEY ([UserId], [RoleId])
)
CREATE INDEX [IX_UserId] ON [dbo].[UserRoles]([UserId])
CREATE INDEX [IX_RoleId] ON [dbo].[UserRoles]([RoleId])
CREATE TABLE [dbo].[UserStates] (
    [userStateId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max),
    CONSTRAINT [PK_dbo.UserStates] PRIMARY KEY ([userStateId])
)
CREATE TABLE [dbo].[UserTypes] (
    [userTypeId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max),
    CONSTRAINT [PK_dbo.UserTypes] PRIMARY KEY ([userTypeId])
)
CREATE TABLE [dbo].[OrganizationUserTypes] (
    [organizationUserTypeId] [int] NOT NULL IDENTITY,
    [organizationTypeId] [int] NOT NULL,
    [organizationType] [nvarchar](max),
    [userTypeId] [int] NOT NULL,
    [userType] [nvarchar](max),
    CONSTRAINT [PK_dbo.OrganizationUserTypes] PRIMARY KEY ([organizationUserTypeId])
)
CREATE INDEX [IX_organizationTypeId] ON [dbo].[OrganizationUserTypes]([organizationTypeId])
CREATE INDEX [IX_userTypeId] ON [dbo].[OrganizationUserTypes]([userTypeId])
CREATE TABLE [dbo].[OrganizationTypes] (
    [organizationTypeId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max),
    CONSTRAINT [PK_dbo.OrganizationTypes] PRIMARY KEY ([organizationTypeId])
)
CREATE TABLE [dbo].[UserProfileDetails] (
    [userProfileDetailId] [int] NOT NULL IDENTITY,
    [userProfileId] [int] NOT NULL,
    [userProfile] [nvarchar](max),
    [serviceId] [int] NOT NULL,
    [service] [nvarchar](max),
    [userTypeId] [int] NOT NULL,
    [userType] [nvarchar](max),
    [assigned] [bit] NOT NULL,
    CONSTRAINT [PK_dbo.UserProfileDetails] PRIMARY KEY ([userProfileDetailId])
)
CREATE INDEX [IX_userProfileId] ON [dbo].[UserProfileDetails]([userProfileId])
CREATE INDEX [IX_serviceId] ON [dbo].[UserProfileDetails]([serviceId])
CREATE INDEX [IX_userTypeId] ON [dbo].[UserProfileDetails]([userTypeId])
CREATE TABLE [dbo].[Services] (
    [serviceId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max) NOT NULL,
    [applicationYN] [bit] NOT NULL,
    CONSTRAINT [PK_dbo.Services] PRIMARY KEY ([serviceId])
)
CREATE TABLE [dbo].[ServiceProfiles] (
    [serviceProfileId] [int] NOT NULL IDENTITY,
    [serviceId] [int] NOT NULL,
    [service] [nvarchar](max),
    [resourceTypeId] [int] NOT NULL,
    [resourceType] [nvarchar](max),
    [resourceSubTypeId] [int] NOT NULL,
    [resourceSubType] [nvarchar](max),
    [providerServiceId] [int] NOT NULL,
    [providerService] [nvarchar](max),
    [assigned] [bit] NOT NULL,
    CONSTRAINT [PK_dbo.ServiceProfiles] PRIMARY KEY ([serviceProfileId])
)
CREATE INDEX [IX_serviceId] ON [dbo].[ServiceProfiles]([serviceId])
CREATE INDEX [IX_resourceTypeId] ON [dbo].[ServiceProfiles]([resourceTypeId])
CREATE INDEX [IX_resourceSubTypeId] ON [dbo].[ServiceProfiles]([resourceSubTypeId])
CREATE INDEX [IX_providerServiceId] ON [dbo].[ServiceProfiles]([providerServiceId])
CREATE TABLE [dbo].[ProviderServices] (
    [ProviderServiceId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max) NOT NULL,
    CONSTRAINT [PK_dbo.ProviderServices] PRIMARY KEY ([ProviderServiceId])
)
CREATE TABLE [dbo].[NrufCompanyInfoes] (
    [NrufCompanyInfoId] [int] NOT NULL IDENTITY,
    [organizationId] [int] NOT NULL,
    [UserId] [nvarchar](max),
    [applicationReference] [nvarchar](max),
    [ProviderServiceId] [int] NOT NULL,
    [name] [nvarchar](max),
    [ParentCompany] [nvarchar](max),
    [ParentCompanyOCN] [nvarchar](max),
    [OCN] [nvarchar](max),
    [OFN] [nvarchar](max),
    [line1] [nvarchar](max),
    [line2] [nvarchar](max),
    [line3] [nvarchar](max),
    [ContactName] [nvarchar](max),
    [ContactTelNo] [nvarchar](max),
    [ContactFaxNo] [nvarchar](max),
    [ContactEmail] [nvarchar](max),
    [Status] [nvarchar](max),
    [expiryDate] [datetime],
    [createdDate] [datetime],
    [dateChanged] [datetime],
    CONSTRAINT [PK_dbo.NrufCompanyInfoes] PRIMARY KEY ([NrufCompanyInfoId])
)
CREATE INDEX [IX_ProviderServiceId] ON [dbo].[NrufCompanyInfoes]([ProviderServiceId])
CREATE TABLE [dbo].[NrufFormF3a] (
    [FormF3aId] [int] NOT NULL IDENTITY,
    [NrufCompanyInfoId] [int] NOT NULL,
    [Npa] [nvarchar](max),
    [Year1] [int] NOT NULL,
    [Year2] [int] NOT NULL,
    [Year3] [int] NOT NULL,
    [Year4] [int] NOT NULL,
    [Year5] [int] NOT NULL,
    [Total] [int] NOT NULL,
    [createdDate] [datetime],
    [dateChanged] [datetime],
    CONSTRAINT [PK_dbo.NrufFormF3a] PRIMARY KEY ([FormF3aId])
)
CREATE INDEX [IX_NrufCompanyInfoId] ON [dbo].[NrufFormF3a]([NrufCompanyInfoId])
CREATE TABLE [dbo].[NrufFormF3b] (
    [FormF3bId] [int] NOT NULL IDENTITY,
    [NrufCompanyInfoId] [int] NOT NULL,
    [Npa] [nvarchar](max),
    [Year1] [int] NOT NULL,
    [Year2] [int] NOT NULL,
    [Year3] [int] NOT NULL,
    [Year4] [int] NOT NULL,
    [Year5] [int] NOT NULL,
    [Total] [int] NOT NULL,
    [createdDate] [datetime],
    [dateChanged] [datetime],
    CONSTRAINT [PK_dbo.NrufFormF3b] PRIMARY KEY ([FormF3bId])
)
CREATE INDEX [IX_NrufCompanyInfoId] ON [dbo].[NrufFormF3b]([NrufCompanyInfoId])
CREATE TABLE [dbo].[NrufFormU1] (
    [FormU1Id] [int] NOT NULL IDENTITY,
    [NrufCompanyInfoId] [int] NOT NULL,
    [Npa] [nvarchar](max),
    [Nxx] [nvarchar](max),
    [X] [int] NOT NULL,
    [Rate] [nvarchar](max),
    [Assigned] [int] NOT NULL,
    [Intermediate] [int] NOT NULL,
    [Reserved] [int] NOT NULL,
    [Aging] [int] NOT NULL,
    [Admin] [int] NOT NULL,
    [PortedOut] [int] NOT NULL,
    [Notes] [nvarchar](max),
    [Available] [int] NOT NULL,
    [Utilization] [nvarchar](max),
    [createdDate] [datetime],
    [dateChanged] [datetime],
    CONSTRAINT [PK_dbo.NrufFormU1] PRIMARY KEY ([FormU1Id])
)
CREATE INDEX [IX_NrufCompanyInfoId] ON [dbo].[NrufFormU1]([NrufCompanyInfoId])
CREATE TABLE [dbo].[NrufFormU2] (
    [FormU2Id] [int] NOT NULL IDENTITY,
    [NrufCompanyInfoId] [int] NOT NULL,
    [Npa] [nvarchar](max),
    [Nxx] [nvarchar](max),
    [Rate] [nvarchar](max),
    [Assigned] [int] NOT NULL,
    [Intermediate] [int] NOT NULL,
    [Reserved] [int] NOT NULL,
    [Aging] [int] NOT NULL,
    [Admin] [int] NOT NULL,
    [PortedOut] [int] NOT NULL,
    [Notes] [nvarchar](max),
    [Available] [int] NOT NULL,
    [Utilization] [nvarchar](max),
    [createdDate] [datetime],
    [dateChanged] [datetime],
    CONSTRAINT [PK_dbo.NrufFormU2] PRIMARY KEY ([FormU2Id])
)
CREATE INDEX [IX_NrufCompanyInfoId] ON [dbo].[NrufFormU2]([NrufCompanyInfoId])
CREATE TABLE [dbo].[NrufFormU3] (
    [FormU3Id] [int] NOT NULL IDENTITY,
    [NrufCompanyInfoId] [int] NOT NULL,
    [Npa] [nvarchar](max),
    [Nxx] [nvarchar](max),
    [X] [int] NOT NULL,
    [Rate] [nvarchar](max),
    [Assigned] [int] NOT NULL,
    [NumbersReceived] [int] NOT NULL,
    [Reserved] [int] NOT NULL,
    [Aging] [int] NOT NULL,
    [Admin] [int] NOT NULL,
    [PortedOut] [int] NOT NULL,
    [Notes] [nvarchar](max),
    [Available] [int] NOT NULL,
    [Utilization] [nvarchar](max),
    [createdDate] [datetime],
    [dateChanged] [datetime],
    CONSTRAINT [PK_dbo.NrufFormU3] PRIMARY KEY ([FormU3Id])
)
CREATE INDEX [IX_NrufCompanyInfoId] ON [dbo].[NrufFormU3]([NrufCompanyInfoId])
CREATE TABLE [dbo].[NrufFormU4] (
    [FormU4Id] [int] NOT NULL IDENTITY,
    [NrufCompanyInfoId] [int] NOT NULL,
    [Npa] [nvarchar](max),
    [Nxx] [nvarchar](max),
    [Rate] [nvarchar](max),
    [Assigned] [int] NOT NULL,
    [Aging] [int] NOT NULL,
    [PortedIn] [int] NOT NULL,
    [Notes] [nvarchar](max),
    [Available] [int] NOT NULL,
    [Utilization] [nvarchar](max),
    [createdDate] [datetime],
    [dateChanged] [datetime],
    CONSTRAINT [PK_dbo.NrufFormU4] PRIMARY KEY ([FormU4Id])
)
CREATE INDEX [IX_NrufCompanyInfoId] ON [dbo].[NrufFormU4]([NrufCompanyInfoId])
CREATE TABLE [dbo].[OrganizationServices] (
    [organizationServicesId] [int] NOT NULL IDENTITY,
    [AddressId] [int] NOT NULL,
    [ProviderServiceId] [int] NOT NULL,
    [organization] [nvarchar](max),
    [providerService] [nvarchar](max),
    [createdDate] [datetime],
    [dateChanged] [datetime],
    CONSTRAINT [PK_dbo.OrganizationServices] PRIMARY KEY ([organizationServicesId])
)
CREATE INDEX [IX_AddressId] ON [dbo].[OrganizationServices]([AddressId])
CREATE INDEX [IX_ProviderServiceId] ON [dbo].[OrganizationServices]([ProviderServiceId])
CREATE TABLE [dbo].[ResourceSubTypes] (
    [resourceSubTypeId] [int] NOT NULL IDENTITY,
    [resourceTypeId] [int] NOT NULL,
    [name] [nvarchar](max) NOT NULL,
    [subType] [nvarchar](max),
    [format] [nvarchar](max) NOT NULL,
    [formatRegex] [nvarchar](max) NOT NULL,
    CONSTRAINT [PK_dbo.ResourceSubTypes] PRIMARY KEY ([resourceSubTypeId])
)
CREATE INDEX [IX_resourceTypeId] ON [dbo].[ResourceSubTypes]([resourceTypeId])
CREATE TABLE [dbo].[OrganizationResourceTypes] (
    [organizationResourceTypeId] [int] NOT NULL IDENTITY,
    [organizationId] [int] NOT NULL,
    [organization] [nvarchar](max),
    [resourceTypeId] [int] NOT NULL,
    [resourceType] [nvarchar](max),
    [resourceSubTypeId] [int] NOT NULL,
    [resourceSubType] [nvarchar](max),
    [linkedResourceStateId] [int] NOT NULL,
    [state] [nvarchar](max),
    CONSTRAINT [PK_dbo.OrganizationResourceTypes] PRIMARY KEY ([organizationResourceTypeId])
)
CREATE INDEX [IX_organizationId] ON [dbo].[OrganizationResourceTypes]([organizationId])
CREATE INDEX [IX_resourceTypeId] ON [dbo].[OrganizationResourceTypes]([resourceTypeId])
CREATE INDEX [IX_resourceSubTypeId] ON [dbo].[OrganizationResourceTypes]([resourceSubTypeId])
CREATE INDEX [IX_linkedResourceStateId] ON [dbo].[OrganizationResourceTypes]([linkedResourceStateId])
CREATE TABLE [dbo].[ResourceTypes] (
    [resourceTypeId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max) NOT NULL,
    [format] [nvarchar](max) NOT NULL,
    [formatRegex] [nvarchar](max) NOT NULL,
    CONSTRAINT [PK_dbo.ResourceTypes] PRIMARY KEY ([resourceTypeId])
)
CREATE TABLE [dbo].[UserProfiles] (
    [userProfileId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max) NOT NULL,
    [defaultProfile] [bit] NOT NULL,
    CONSTRAINT [PK_dbo.UserProfiles] PRIMARY KEY ([userProfileId])
)
CREATE TABLE [dbo].[ResourceStates] (
    [resourceStateId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max) NOT NULL,
    CONSTRAINT [PK_dbo.ResourceStates] PRIMARY KEY ([resourceStateId])
)
CREATE TABLE [dbo].[ApplicationStates] (
    [applicationStateId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max) NOT NULL,
    CONSTRAINT [PK_dbo.ApplicationStates] PRIMARY KEY ([applicationStateId])
)
CREATE TABLE [dbo].[ApplicationTypes] (
    [applicationTypeId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max) NOT NULL,
    [abbreviation] [nvarchar](max) NOT NULL,
    CONSTRAINT [PK_dbo.ApplicationTypes] PRIMARY KEY ([applicationTypeId])
)
CREATE TABLE [dbo].[CentralOfficeCodeAssignments] (
    [CentralOfficeCodeAssignmentsId] [int] NOT NULL IDENTITY,
    [organizationId] [int] NOT NULL,
    [NPA] [nvarchar](max),
    [NXX] [nvarchar](max),
    [DateCreated] [datetime],
    [DateChanged] [datetime],
    CONSTRAINT [PK_dbo.CentralOfficeCodeAssignments] PRIMARY KEY ([CentralOfficeCodeAssignmentsId])
)
CREATE INDEX [IX_organizationId] ON [dbo].[CentralOfficeCodeAssignments]([organizationId])
CREATE TABLE [dbo].[OrganizationStates] (
    [organizationStateId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max),
    CONSTRAINT [PK_dbo.OrganizationStates] PRIMARY KEY ([organizationStateId])
)
CREATE TABLE [dbo].[Mails] (
    [mailId] [bigint] NOT NULL IDENTITY,
    [title] [nvarchar](max) NOT NULL,
    [toAddress] [nvarchar](max) NOT NULL,
    [fromAddress] [nvarchar](max) NOT NULL,
    [message] [nvarchar](max) NOT NULL,
    [attachment] [varbinary](max),
    [createdDate] [datetime],
    [dateChanged] [datetime],
    CONSTRAINT [PK_dbo.Mails] PRIMARY KEY ([mailId])
)
CREATE TABLE [dbo].[Reports] (
    [ReportId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max) NOT NULL,
    CONSTRAINT [PK_dbo.Reports] PRIMARY KEY ([ReportId])
)
CREATE TABLE [dbo].[ReportOnNXXAndNPACodes] (
    [ReportOnNXXAndNPACodesId] [int] NOT NULL IDENTITY,
    [NPA] [int] NOT NULL,
    [NX] [int] NOT NULL,
    [ServiceAllocation] [nvarchar](max),
    [X_0] [nvarchar](max),
    [X_0_CellStyle] [nvarchar](max),
    [X_1] [nvarchar](max),
    [X_1_CellStyle] [nvarchar](max),
    [X_2] [nvarchar](max),
    [X_2_CellStyle] [nvarchar](max),
    [X_3] [nvarchar](max),
    [X_3_CellStyle] [nvarchar](max),
    [X_4] [nvarchar](max),
    [X_4_CellStyle] [nvarchar](max),
    [X_5] [nvarchar](max),
    [X_5_CellStyle] [nvarchar](max),
    [X_6] [nvarchar](max),
    [X_6_CellStyle] [nvarchar](max),
    [X_7] [nvarchar](max),
    [X_7_CellStyle] [nvarchar](max),
    [X_8] [nvarchar](max),
    [X_8_CellStyle] [nvarchar](max),
    [X_9] [nvarchar](max),
    [X_9_CellStyle] [nvarchar](max),
    [DateCreated] [datetime],
    [DateChanged] [datetime],
    CONSTRAINT [PK_dbo.ReportOnNXXAndNPACodes] PRIMARY KEY ([ReportOnNXXAndNPACodesId])
)
CREATE TABLE [dbo].[ReportOnNXXAndNPACodesKeys] (
    [ReportOnNXXAndNPACodesKeyId] [int] NOT NULL IDENTITY,
    [Key] [nvarchar](max),
    [CellText] [nvarchar](max),
    [CellStyle] [nvarchar](max),
    [DateCreated] [datetime],
    [DateChanged] [datetime],
    CONSTRAINT [PK_dbo.ReportOnNXXAndNPACodesKeys] PRIMARY KEY ([ReportOnNXXAndNPACodesKeyId])
)
CREATE TABLE [dbo].[Roles] (
    [RoleId] [nvarchar](128) NOT NULL,
    [Name] [nvarchar](256) NOT NULL,
    CONSTRAINT [PK_dbo.Roles] PRIMARY KEY ([RoleId])
)
CREATE UNIQUE INDEX [RoleNameIndex] ON [dbo].[Roles]([Name])
CREATE TABLE [dbo].[SystemMessages] (
    [systemMessageId] [bigint] NOT NULL IDENTITY,
    [title] [nvarchar](max) NOT NULL,
    [message] [nvarchar](max) NOT NULL,
    [expiryDate] [datetime],
    [createdDate] [datetime],
    [dateChanged] [datetime],
    CONSTRAINT [PK_dbo.SystemMessages] PRIMARY KEY ([systemMessageId])
)
CREATE TABLE [dbo].[SystemParameters] (
    [systemParameterId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max) NOT NULL,
    [value] [nvarchar](max) NOT NULL,
    [UserId] [nvarchar](max),
    [createdDate] [datetime],
    [dateChanged] [datetime],
    CONSTRAINT [PK_dbo.SystemParameters] PRIMARY KEY ([systemParameterId])
)
CREATE TABLE [dbo].[UserResourceTypes] (
    [userResourceTypeId] [int] NOT NULL IDENTITY,
    [userTypeId] [int] NOT NULL,
    [userType] [nvarchar](max),
    [resourceTypeId] [int] NOT NULL,
    [resourceType] [nvarchar](max),
    [resourceSubTypeId] [int] NOT NULL,
    [resourceSubType] [nvarchar](max),
    [linkedResourceStateId] [int] NOT NULL,
    [state] [nvarchar](max),
    CONSTRAINT [PK_dbo.UserResourceTypes] PRIMARY KEY ([userResourceTypeId])
)
ALTER TABLE [dbo].[Addresses] ADD CONSTRAINT [FK_dbo.Addresses_dbo.Organizations_organizationId] FOREIGN KEY ([organizationId]) REFERENCES [dbo].[Organizations] ([organizationId])
ALTER TABLE [dbo].[Organizations] ADD CONSTRAINT [FK_dbo.Organizations_dbo.OrganizationTypes_organizationTypeId] FOREIGN KEY ([organizationTypeId]) REFERENCES [dbo].[OrganizationTypes] ([organizationTypeId])
ALTER TABLE [dbo].[Organizations] ADD CONSTRAINT [FK_dbo.Organizations_dbo.OrganizationStates_organizationStateId] FOREIGN KEY ([organizationStateId]) REFERENCES [dbo].[OrganizationStates] ([organizationStateId])
ALTER TABLE [dbo].[Applications] ADD CONSTRAINT [FK_dbo.Applications_dbo.Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserId])
ALTER TABLE [dbo].[Applications] ADD CONSTRAINT [FK_dbo.Applications_dbo.ResourceSubTypes_resourceSubTypeId] FOREIGN KEY ([resourceSubTypeId]) REFERENCES [dbo].[ResourceSubTypes] ([resourceSubTypeId])
ALTER TABLE [dbo].[Applications] ADD CONSTRAINT [FK_dbo.Applications_dbo.ResourceTypes_resourceTypeId] FOREIGN KEY ([resourceTypeId]) REFERENCES [dbo].[ResourceTypes] ([resourceTypeId])
ALTER TABLE [dbo].[Applications] ADD CONSTRAINT [FK_dbo.Applications_dbo.ApplicationStates_applicationStateId] FOREIGN KEY ([applicationStateId]) REFERENCES [dbo].[ApplicationStates] ([applicationStateId])
ALTER TABLE [dbo].[Applications] ADD CONSTRAINT [FK_dbo.Applications_dbo.ApplicationTypes_applicationTypeId] FOREIGN KEY ([applicationTypeId]) REFERENCES [dbo].[ApplicationTypes] ([applicationTypeId])
ALTER TABLE [dbo].[Applications] ADD CONSTRAINT [FK_dbo.Applications_dbo.Organizations_organizationId] FOREIGN KEY ([organizationId]) REFERENCES [dbo].[Organizations] ([organizationId])
ALTER TABLE [dbo].[ApplicationNotes] ADD CONSTRAINT [FK_dbo.ApplicationNotes_dbo.Applications_applicationId] FOREIGN KEY ([applicationId]) REFERENCES [dbo].[Applications] ([applicationId])
ALTER TABLE [dbo].[ApplicationResources] ADD CONSTRAINT [FK_dbo.ApplicationResources_dbo.Applications_Application_applicationId] FOREIGN KEY ([Application_applicationId]) REFERENCES [dbo].[Applications] ([applicationId])
ALTER TABLE [dbo].[ApplicationResources] ADD CONSTRAINT [FK_dbo.ApplicationResources_dbo.ApplicationResourceStates_ApplicationResourceState_applicationResourceStateId] FOREIGN KEY ([ApplicationResourceState_applicationResourceStateId]) REFERENCES [dbo].[ApplicationResourceStates] ([applicationResourceStateId])
ALTER TABLE [dbo].[ApplicationResources] ADD CONSTRAINT [FK_dbo.ApplicationResources_dbo.ApplicationResourceStates_ApplicationResourceState_applicationResourceStateId1] FOREIGN KEY ([ApplicationResourceState_applicationResourceStateId1]) REFERENCES [dbo].[ApplicationResourceStates] ([applicationResourceStateId])
ALTER TABLE [dbo].[ApplicationResources] ADD CONSTRAINT [FK_dbo.ApplicationResources_dbo.Resources_Resource_resourceId] FOREIGN KEY ([Resource_resourceId]) REFERENCES [dbo].[Resources] ([resourceId])
ALTER TABLE [dbo].[ApplicationResources] ADD CONSTRAINT [FK_dbo.ApplicationResources_dbo.Resources_Resource_resourceId1] FOREIGN KEY ([Resource_resourceId1]) REFERENCES [dbo].[Resources] ([resourceId])
ALTER TABLE [dbo].[ApplicationResources] ADD CONSTRAINT [FK_dbo.ApplicationResources_dbo.Applications_Application_applicationId1] FOREIGN KEY ([Application_applicationId1]) REFERENCES [dbo].[Applications] ([applicationId])
ALTER TABLE [dbo].[ApplicationResources] ADD CONSTRAINT [FK_dbo.ApplicationResources_dbo.Organizations_Organization_organizationId] FOREIGN KEY ([Organization_organizationId]) REFERENCES [dbo].[Organizations] ([organizationId])
ALTER TABLE [dbo].[Resources] ADD CONSTRAINT [FK_dbo.Resources_dbo.ResourceStates_ResourceState_resourceStateId] FOREIGN KEY ([ResourceState_resourceStateId]) REFERENCES [dbo].[ResourceStates] ([resourceStateId])
ALTER TABLE [dbo].[Resources] ADD CONSTRAINT [FK_dbo.Resources_dbo.ResourceStates_ResourceState_resourceStateId1] FOREIGN KEY ([ResourceState_resourceStateId1]) REFERENCES [dbo].[ResourceStates] ([resourceStateId])
ALTER TABLE [dbo].[ResourceLinks] ADD CONSTRAINT [FK_dbo.ResourceLinks_dbo.LinkedResourceStates_LinkedResourceState_linkedResourceStateId] FOREIGN KEY ([LinkedResourceState_linkedResourceStateId]) REFERENCES [dbo].[LinkedResourceStates] ([linkedResourceStateId])
ALTER TABLE [dbo].[ResourceLinks] ADD CONSTRAINT [FK_dbo.ResourceLinks_dbo.LinkedResourceStates_LinkedResourceState_linkedResourceStateId1] FOREIGN KEY ([LinkedResourceState_linkedResourceStateId1]) REFERENCES [dbo].[LinkedResourceStates] ([linkedResourceStateId])
ALTER TABLE [dbo].[ResourceLinks] ADD CONSTRAINT [FK_dbo.ResourceLinks_dbo.Resources_Resource_resourceId] FOREIGN KEY ([Resource_resourceId]) REFERENCES [dbo].[Resources] ([resourceId])
ALTER TABLE [dbo].[ResourceLinks] ADD CONSTRAINT [FK_dbo.ResourceLinks_dbo.Resources_Resource_resourceId1] FOREIGN KEY ([Resource_resourceId1]) REFERENCES [dbo].[Resources] ([resourceId])
ALTER TABLE [dbo].[NotificationDetails] ADD CONSTRAINT [FK_dbo.NotificationDetails_dbo.LinkedResourceStates_linkedResourceStateId] FOREIGN KEY ([linkedResourceStateId]) REFERENCES [dbo].[LinkedResourceStates] ([linkedResourceStateId])
ALTER TABLE [dbo].[NotificationDetails] ADD CONSTRAINT [FK_dbo.NotificationDetails_dbo.Notifications_notificationId] FOREIGN KEY ([notificationId]) REFERENCES [dbo].[Notifications] ([notificationId])
ALTER TABLE [dbo].[NotificationDetails] ADD CONSTRAINT [FK_dbo.NotificationDetails_dbo.UserTypes_userTypeId] FOREIGN KEY ([userTypeId]) REFERENCES [dbo].[UserTypes] ([userTypeId])
ALTER TABLE [dbo].[NotificationDetails] ADD CONSTRAINT [FK_dbo.NotificationDetails_dbo.ResourceSubTypes_resourceSubTypeId] FOREIGN KEY ([resourceSubTypeId]) REFERENCES [dbo].[ResourceSubTypes] ([resourceSubTypeId])
ALTER TABLE [dbo].[NotificationDetails] ADD CONSTRAINT [FK_dbo.NotificationDetails_dbo.ResourceTypes_resourceTypeId] FOREIGN KEY ([resourceTypeId]) REFERENCES [dbo].[ResourceTypes] ([resourceTypeId])
ALTER TABLE [dbo].[UserNotifications] ADD CONSTRAINT [FK_dbo.UserNotifications_dbo.Notifications_notificationId] FOREIGN KEY ([notificationId]) REFERENCES [dbo].[Notifications] ([notificationId])
ALTER TABLE [dbo].[UserNotifications] ADD CONSTRAINT [FK_dbo.UserNotifications_dbo.Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserId])
ALTER TABLE [dbo].[Users] ADD CONSTRAINT [FK_dbo.Users_dbo.Organizations_organizationId] FOREIGN KEY ([organizationId]) REFERENCES [dbo].[Organizations] ([organizationId])
ALTER TABLE [dbo].[Users] ADD CONSTRAINT [FK_dbo.Users_dbo.UserStates_userStateId] FOREIGN KEY ([userStateId]) REFERENCES [dbo].[UserStates] ([userStateId])
ALTER TABLE [dbo].[Users] ADD CONSTRAINT [FK_dbo.Users_dbo.UserTypes_userTypeId] FOREIGN KEY ([userTypeId]) REFERENCES [dbo].[UserTypes] ([userTypeId])
ALTER TABLE [dbo].[UserClaims] ADD CONSTRAINT [FK_dbo.UserClaims_dbo.Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserId])
ALTER TABLE [dbo].[UserLogins] ADD CONSTRAINT [FK_dbo.UserLogins_dbo.Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserId])
ALTER TABLE [dbo].[UserRoles] ADD CONSTRAINT [FK_dbo.UserRoles_dbo.Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserId])
ALTER TABLE [dbo].[UserRoles] ADD CONSTRAINT [FK_dbo.UserRoles_dbo.Roles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Roles] ([RoleId])
ALTER TABLE [dbo].[OrganizationUserTypes] ADD CONSTRAINT [FK_dbo.OrganizationUserTypes_dbo.OrganizationTypes_organizationTypeId] FOREIGN KEY ([organizationTypeId]) REFERENCES [dbo].[OrganizationTypes] ([organizationTypeId])
ALTER TABLE [dbo].[OrganizationUserTypes] ADD CONSTRAINT [FK_dbo.OrganizationUserTypes_dbo.UserTypes_userTypeId] FOREIGN KEY ([userTypeId]) REFERENCES [dbo].[UserTypes] ([userTypeId])
ALTER TABLE [dbo].[UserProfileDetails] ADD CONSTRAINT [FK_dbo.UserProfileDetails_dbo.Services_serviceId] FOREIGN KEY ([serviceId]) REFERENCES [dbo].[Services] ([serviceId])
ALTER TABLE [dbo].[UserProfileDetails] ADD CONSTRAINT [FK_dbo.UserProfileDetails_dbo.UserProfiles_userProfileId] FOREIGN KEY ([userProfileId]) REFERENCES [dbo].[UserProfiles] ([userProfileId])
ALTER TABLE [dbo].[UserProfileDetails] ADD CONSTRAINT [FK_dbo.UserProfileDetails_dbo.UserTypes_userTypeId] FOREIGN KEY ([userTypeId]) REFERENCES [dbo].[UserTypes] ([userTypeId])
ALTER TABLE [dbo].[ServiceProfiles] ADD CONSTRAINT [FK_dbo.ServiceProfiles_dbo.ProviderServices_providerServiceId] FOREIGN KEY ([providerServiceId]) REFERENCES [dbo].[ProviderServices] ([ProviderServiceId])
ALTER TABLE [dbo].[ServiceProfiles] ADD CONSTRAINT [FK_dbo.ServiceProfiles_dbo.ResourceTypes_resourceTypeId] FOREIGN KEY ([resourceTypeId]) REFERENCES [dbo].[ResourceTypes] ([resourceTypeId])
ALTER TABLE [dbo].[ServiceProfiles] ADD CONSTRAINT [FK_dbo.ServiceProfiles_dbo.ResourceSubTypes_resourceSubTypeId] FOREIGN KEY ([resourceSubTypeId]) REFERENCES [dbo].[ResourceSubTypes] ([resourceSubTypeId])
ALTER TABLE [dbo].[ServiceProfiles] ADD CONSTRAINT [FK_dbo.ServiceProfiles_dbo.Services_serviceId] FOREIGN KEY ([serviceId]) REFERENCES [dbo].[Services] ([serviceId])
ALTER TABLE [dbo].[NrufCompanyInfoes] ADD CONSTRAINT [FK_dbo.NrufCompanyInfoes_dbo.ProviderServices_ProviderServiceId] FOREIGN KEY ([ProviderServiceId]) REFERENCES [dbo].[ProviderServices] ([ProviderServiceId])
ALTER TABLE [dbo].[NrufFormF3a] ADD CONSTRAINT [FK_dbo.NrufFormF3a_dbo.NrufCompanyInfoes_NrufCompanyInfoId] FOREIGN KEY ([NrufCompanyInfoId]) REFERENCES [dbo].[NrufCompanyInfoes] ([NrufCompanyInfoId])
ALTER TABLE [dbo].[NrufFormF3b] ADD CONSTRAINT [FK_dbo.NrufFormF3b_dbo.NrufCompanyInfoes_NrufCompanyInfoId] FOREIGN KEY ([NrufCompanyInfoId]) REFERENCES [dbo].[NrufCompanyInfoes] ([NrufCompanyInfoId])
ALTER TABLE [dbo].[NrufFormU1] ADD CONSTRAINT [FK_dbo.NrufFormU1_dbo.NrufCompanyInfoes_NrufCompanyInfoId] FOREIGN KEY ([NrufCompanyInfoId]) REFERENCES [dbo].[NrufCompanyInfoes] ([NrufCompanyInfoId])
ALTER TABLE [dbo].[NrufFormU2] ADD CONSTRAINT [FK_dbo.NrufFormU2_dbo.NrufCompanyInfoes_NrufCompanyInfoId] FOREIGN KEY ([NrufCompanyInfoId]) REFERENCES [dbo].[NrufCompanyInfoes] ([NrufCompanyInfoId])
ALTER TABLE [dbo].[NrufFormU3] ADD CONSTRAINT [FK_dbo.NrufFormU3_dbo.NrufCompanyInfoes_NrufCompanyInfoId] FOREIGN KEY ([NrufCompanyInfoId]) REFERENCES [dbo].[NrufCompanyInfoes] ([NrufCompanyInfoId])
ALTER TABLE [dbo].[NrufFormU4] ADD CONSTRAINT [FK_dbo.NrufFormU4_dbo.NrufCompanyInfoes_NrufCompanyInfoId] FOREIGN KEY ([NrufCompanyInfoId]) REFERENCES [dbo].[NrufCompanyInfoes] ([NrufCompanyInfoId])
ALTER TABLE [dbo].[OrganizationServices] ADD CONSTRAINT [FK_dbo.OrganizationServices_dbo.Addresses_AddressId] FOREIGN KEY ([AddressId]) REFERENCES [dbo].[Addresses] ([AddressId])
ALTER TABLE [dbo].[OrganizationServices] ADD CONSTRAINT [FK_dbo.OrganizationServices_dbo.ProviderServices_ProviderServiceId] FOREIGN KEY ([ProviderServiceId]) REFERENCES [dbo].[ProviderServices] ([ProviderServiceId])
ALTER TABLE [dbo].[ResourceSubTypes] ADD CONSTRAINT [FK_dbo.ResourceSubTypes_dbo.ResourceTypes_resourceTypeId] FOREIGN KEY ([resourceTypeId]) REFERENCES [dbo].[ResourceTypes] ([resourceTypeId])
ALTER TABLE [dbo].[OrganizationResourceTypes] ADD CONSTRAINT [FK_dbo.OrganizationResourceTypes_dbo.LinkedResourceStates_linkedResourceStateId] FOREIGN KEY ([linkedResourceStateId]) REFERENCES [dbo].[LinkedResourceStates] ([linkedResourceStateId])
ALTER TABLE [dbo].[OrganizationResourceTypes] ADD CONSTRAINT [FK_dbo.OrganizationResourceTypes_dbo.Organizations_organizationId] FOREIGN KEY ([organizationId]) REFERENCES [dbo].[Organizations] ([organizationId])
ALTER TABLE [dbo].[OrganizationResourceTypes] ADD CONSTRAINT [FK_dbo.OrganizationResourceTypes_dbo.ResourceSubTypes_resourceSubTypeId] FOREIGN KEY ([resourceSubTypeId]) REFERENCES [dbo].[ResourceSubTypes] ([resourceSubTypeId])
ALTER TABLE [dbo].[OrganizationResourceTypes] ADD CONSTRAINT [FK_dbo.OrganizationResourceTypes_dbo.ResourceTypes_resourceTypeId] FOREIGN KEY ([resourceTypeId]) REFERENCES [dbo].[ResourceTypes] ([resourceTypeId])
ALTER TABLE [dbo].[CentralOfficeCodeAssignments] ADD CONSTRAINT [FK_dbo.CentralOfficeCodeAssignments_dbo.Organizations_organizationId] FOREIGN KEY ([organizationId]) REFERENCES [dbo].[Organizations] ([organizationId])
CREATE TABLE [dbo].[__MigrationHistory] (
    [MigrationId] [nvarchar](150) NOT NULL,
    [ContextKey] [nvarchar](300) NOT NULL,
    [Model] [varbinary](max) NOT NULL,
    [ProductVersion] [nvarchar](32) NOT NULL,
    CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY ([MigrationId], [ContextKey])
)
INSERT [dbo].[__MigrationHistory]([MigrationId], [ContextKey], [Model], [ProductVersion])
VALUES (N'201701180100544_InitialCreate', N'jnas.Migrations.Configuration',  0x1F8B0800000000000400ED7DDB721C3992E5FB9AED3FD0F8343356238A17A9ABCAA419635362B7AC24924D8A6DEA275A646624195399113911912AB2D7F6CBF6613F697F6111D78C00DC01C7252E994A6BB36A31C3E170771C381C8003F87FFFE7FFBEFBCFE7E5E2E0BB1F274114BE3F3C7EF5FAF0C00FA7D12C081FDF1FAED3F9BFFF7CF89FFFF13FFFC7BB8FB3E5F3C1DF2BBAD38C8E950C93F7874F69BAFAF5E828993EF94B2F79B50CA6719444F3F4D5345A1E79B3E8E8E4F5EB5F8E8E8F8F7CC6E290F13A387877BB0ED360E9E77FB03F2FA270EAAFD2B5B7F812CDFC4552FECEBEDCE55C0FAEBCA59FACBCA9FFFEF0BF42564941767870BE083C26C29DBF981F1E786118A55ECA04FCF53EF1EFD2380A1FEF56EC076FF1F565E533BAB9B748FC52F05F37E4541D5E9F643A1C6D0A56ACA6EB248D969A0C8F4F4BA31CF1C58D4C7B581B8D99ED23336FFA92699D9BEEFDE1F96C16FB09B3185FD7AF178B38A36B19F65549FED341F6E34F75BB337864FFFBE9E062BD48D7B1FF3EF4D769EC2D7E3AB8594F16C1F437FFE56BF4BB1FBE0FD78B4553202612FBD6FA81FD7413472B3F4E5F6EFD795BCC4FB3C383A376F123BE7C5D5A2C5AE8F3294C4F4F0E0FAE9828DE64E1D7ADDFD0FD2E8D62FF2F7EE8C75EEACF6EBC34F563D6789F667E6E3F4108AECA287EF4C2E09F392F75BD745E15270661D60D0F0FBE78CF9FFDF0317D7A7FC8FE797870193CFBB3EA9792FB7D18B05ECB0AA5F15A59D92208FD6307B568EA98557BD2B972D3BCE53AAEE49FC1EA8211775ECFCC9F7BACAFD5DDB7A8EECF51B4F0BD50DD0057DEF7E031C714C7F7BA05B75B7F91FF2B790A5685377DD52478A8ABBF8CA3E56DB4E01854DF1FBE7AF1A39F32292309D15DB48EA786A2DEF9F1F760EA274A912B4295E83C9D5405811852E5DDD1C6F94A5D72BB09687EB959A677E7CCBB3B5D0FADE72EDDB9E990FDB77F67D7D436ABDBE50091F1E9DCF5DC7831B3EF45B45C7961F70EB555DBF5C555E715F652C765F7754C633FEB151FD87FAABAB27F7F0D96EAC185D15D3C79E1A33FD32DDA44E31DEBA6D6F04ED28602EE8C858E2AF5B8E06AECABC602E9D8578D2E6429572BE6B9F151BAF1FDA13D9E6CA4C56884A10E25D41DB11B8C6EFDA42CACB4335408B3B948ABB03F5040B72D2E987F62C3E9F57CCE62802CF83B4F92E0315CB29F13242291156999F8B8A92ABD94D0841A456DC2B0CA86C538A468D926310A52520169708697B20A380BB7A48A36332A926E22A53CE214C96DB421B517B99DF4DAC7BE5DEE133F06A5CF3EA0120B1F0529450AABB8BEE5B389CB2D9B22BD47F5DEA66E93A09E2BBE8D4B2F0D155C2C8A902B636DE0B3A0773AC0E4A494224C3BD737F6A7C12AF07BA829EBC61B2800D51C9FFCDC852DD7B95BEA58B9A4587AB08EB20B363DB4FA6630B695396E051B3D097EB79EB894BD64D73D4CD693641A07931E10598C6C7E7C1185A9374DAFD6CB3E6A5D44536F711BAD53C6BEA72A2F83857F255F423A79F3C64145992599A35400E5F8F56B17C3D06C16646390B7F8104DD7D9FCA0B5F3F0E720F4E2179A31C7B83CE189F33D8751820BCFD060B79DEB271BF9AFA21499FFF2440FADD8185CA210E864CB142271574B15CD3A142B150A5261A142456FB16684CF5E792242CB0874B29611892D5A069DB6723404257832990E02ADCDD4DBFDF29D0C47E07C972AF62D1FB40092733498E5256482E565B4BA96572E50B5568B14C29325B7165BBEC28188C97F83D7375462992C6F948E5F7B8D232F37E442472E80E56A47CDA3AF250FAD951632ABCEC367A6FE000B1CCAC5808EEAED6531200833E4788B2B37A61D4500EF620BCC3CC6948CA578406AEB3E37C1A3B607AD8A0EE9449B132B0B3F4A9F9FFDF0AE347634958D6BE0ED86EF50836BDBA7D82ACF574F16D5CE0F2295F93F90DE624EDA6A1213BD380664255BE574356E17369D5191D555AC2B08742475A42B0A360358D912C6A3585E7E0C4359ED261C8C67449733BA34C70E57CEDA9D88BE8CA62C47813F5ED8AA3BE8067183456ECDE04117DEF4C0C31D9CAB3A0738A7E072F732DDC25DCB643DE945EC49B9CBB4935168D5269F83F07767DBC85B1EC64A070A59D8052DCB92060229B169189735A95C853625207B930017BA4565BC8C8F06DA423D42480D53A805C6C364ED81B530A1DEE09A95196C80AD3ABCE9204B7318EE06DA557EF262D3D3BAF6C64FC162D65B6D0B664C7F76BBCDFEF3B3A882B437E7CE022C24F66B8C16EDE16801A753E2566DB231A0452017DA4DB80FDA95E69C80A2BDFB28A43BE8BA2AA35EB5B533DDAB280DE665ECF0C14FBD600182562453F5425A09613F9758CC265BC1FC808742659D72F4E31E2ED45706775025589CA7A2155C95B28095D782104C735A62C9DE7D56288860E2B2602EB5C77A7BD6ADC76AD46E1B7F3479751E3F65FBD92E96142A3EFBCC7355655B9A79FE0305DA9D0DF4FCA0A0191F98C4334A051FA0B103D60C2095C62E10BDE9A8AD93AB28D7484DADCC5C74A8172D8191A691AE3AAE74B9AF7D3F92D948D0414206E63BEAC86E144699045083864EB641D3164EF034C71BD696338F095EEF48D85FB3643075B474B8B23144D679743AB37200E1891EDA25DADD1A25043B364E6DD5B545B568DD9B2FD77B175F730298747388475F5D7DA74FED6EEDB4CFCBAF0DD96C7ABAF5845DFA0CDEFFD13C8C9B031AA213E14E6908C2804735042A6BCFA6E3CD7AF760261E6B088F314C5052F8E6EDBC58A3D73B4DB76EA52AAB88FD340F164E642E59752EF62A0E965EFC521EFBD71E20840B29A6EB98C1F26F6B3F7104134309CEC3E40F27218166FDC364272DBC24654D18FAD37C2E60507986B9312FF371B57C5CE61329B49693376F5DD5C2EC3A0FE2A541F4C471BB6161D81F513CFBAB973C756EA0BBB21FB0365DAE3AAFEDE6290AFD9E2EF068D4E5AC69BEFE115D32F717C51FC3AC9435BFCFD1F4F7689D7E0C7347709F4E75BB63CDC08938E7D3A99F24970CCCFEEC225A6F6E6F32EBE279442B0F9FA8FDCF7802A23A4DA375C41A9A65581D90B95878C112BE4423E75D7DE7642A7E86C529BFE94AF2397A0C428924D5774E92E2675892F29BAE24CAEB1334EFFD0365B3BB2981F19198AAFCCCC994FF0A0B537CEA6C49CE60820A8A894E637524C6F7D7EAAF0FC5DCB52DE3E60B387B6E7CEE669F0292A9FE80EF45A0129127F2D50C2EE394F76DD9ACFE4BFD54C879B2BAF2D35755E95705DFCB98F164B1C5EFAF04B63F1D900B6F56094EA8AB04A7C793F9E9CF6FDE7AB3D3B767FEE99BFE570C76774D336FBE5EE6AE794D7FF7166BD75519F5867C7C71DF1B72B6E3EF0DB998ECE7EFC12CF34C47EA121531634FA2AFF0ACDBE738C9FAEE0E2D35FBAEBC1F1F60D45DB291C97D6FC9B88EBFB3C050064933854C503F94F7AFE41D09E22A4C902F88AA0BD8ED401CFCD94BFCD20659E02622FF4829B7565E7F5D6090BD5FE31C7E8DF5BBD1257638D9AE234D31A0F90F300331EE2145B446075AF6EB2038ABB6564C6046DB96D95A9411937C1C64BA41685465C599ACB64867C210E1C3A6047CD64320941EEE10A94DE6F236F378D4D268B7574953EECF2900D2A202AD8A53818B0F08A9D54A040C15FD17EC067368112084ED8B76F70338BAAD7F636EFC9BF61DBEABA3F2996801BD7776AC7C68D7A300EF67696386B5D732F558837B2B179E6AC7C331ADA76D85DE427FEC4A065D91BA9770CCA18BD1D2CE4D0705A224FAC4A855709019524B02D3A992C0A4AF4EBAA5D968C976BD8B34FE78A7F344F0BBCAD4802F2BBF3D009E60E3BF5022212CC2294D42A11AD2F2A99B5C7829A16C02E74809E9FEB6D5E4139A312BE6A946A3448D1FDAD85092F73E22341C93EE3840F669A30BD1CCEFCEFEC795730F23EBAF6D92871A538297E1283017C39359F817C90291A57BE47BA8DA91DA74D0DAFC5AFDB42C3554776D444086BD961C43B9EBBC5B1603ED6FE800D9F57070A34887B87303178EDD1646A237BC0680CBE5681E78E7B6F1BB724A61E05090F7710F06AE8B9C5279FF85235D948A10B5D051C185FC84198E7EE0810CDE587C6234720B1D82367473C57A1FBB6F44C7A63B78032C762CF4C677AFE3F5FC225AAEBCF0E553388FE0FB29DA340F02501AF753C849C50B8114F436B718963CE00C7D8850A618855EBA31831632F42FB2F98DB3618B773BC451CEC8FD084824DEB0D32ED6BBFBE1EA37713F008B21B6906D434265E6A3A348B07995FBDC8FFDB087F0537B8418624F8C9739BFD9BB0456BFB55D5F5C755E612F755C765FC722089DBC72A2AEE5A4975A4EBB3F7E525C3EA03852EAB4AEAFFE62730D47D7955D7ACFFD55A63A19EFE878391BAED649E7D5F8CFAB207E31B9CF60E8774EB318E0328A9797A71E1A8397DF1F8448A91D7F236460EC8DD1EA06A51B5E1385F8139AF80299447C91D654FCFB63A9F4F7C714E1452A547680D458F413B9E82724D1052A5C7491D458F453B9E8A724D1052A5C7491D458F433B9E86724D1052A5C7491545774CABAA7C319BF7023257185C0780E597B51FAFCB12CD2FBDCB1ACD764CED828DAD75C517B9AAA60B7F23A8F07FEE17BF1B19D98198B137B16A7F62CCEEC59BCB163F195216961C7620C711665BDD332D682BC9E2A2EB3F478137D8F3719C8E34DCC3DDE64EFF1F61E6FEFF13AF378C6D333DCE3E153392B8F97CDD5F41CDEFDF120FEEEFED8D4DD1525F7DE0EAFE3F9B9F33ABED999E1B68FEB2DCFB9EC18335159293FBB1D3168886CA8B49FA593D9CA73FE985BCB8AC56C1984762C6EA298F5A6EBB5E525885751EA77BF007AFEDD0B72A92CB7F2D260D1D7CDCDDB343E1A2F0062A3A364ADD06E703CD11E1C4F86191C4F8C07C793FDE038F8E0B81FDDF6A3DB7E74DB95D1CD748F081DDDF0ED24BBD1ED547B743B1D66743B351EDD4EF7A3DBE0A3DB8F33F52BEEC74F6EFDA91F588F6CFBF1713F3EEEE4F8689A88808E8F78CE82DDF878A63D3E9E0D333E9E198F8F67FBF171F0F1718B063807A34931147CB21C50F6230152D7568D04A6795DE84880A780198D04F03928FD1BCFAAB283DE7A5609617BF359934F5F23C7F96C16FB09A1CA5ECF9FF4FA34E3AAE7A3E2433B92B2C9E9C70EEB028AE386259DF472368158F7E6394A7A68278727495A39CD16154ECDD3FC2357AC77D7085C44A1EB15B5EFB270E7105DDEFF117A43DC5B94F47463C79CC5055EDABF7E45BDB7FEA3EF6206D1D98B7DFCB513C8E37D1232C1E5C86875FD28F15A78BE4AF9EDF06A6AA54E8EEF8A57DED681113F08DE171E2F6465A407EEA505BBBD9444A29A963A4E5520DC1BE0EC8A1815085DDE1B80A3517FBAD32C3FE894E7961B256DA63D3CAFBE46FAAD7D4B7E7F4519C8AE73F11741F8BB3FAB7DC5885F17C79FCF1555D01B15410684911128279D51290B3B7FAE17AD9D72FF3A5A80AEA4DD53BF840BD63A8D74C86A62314537910E3EF822210F2AB734F6414B593D6463122D0C1A21F00393E9B4BFEF48609879FA7EFE4C9A3F5326CFE49973DFD3668D39B3EE847990D97247CE55AAA3D225BB1C249DCC93A5EAD80E801AF364E224596786EC7C7ADC7ABB40FB4195219F52B17C4465F707B8993FF75833D4AD5BD46F7F33B2F67B85368F5EF01D81F64486DD7E8FCEFBBEAD42C3EDF598BEF32B30D8B12EA11C8CE40B9DF95477430A2C70B628F085CD3699154C1BE1931652F972BD83D5E30430C12BC4E34781AC2A62E71B188BDA65740282A5C4AE70AC33C3E68A0D8962D37936C062C730CC2B3C99C4FEF7C0D5F27C1F3D4836ED9590C9FA0F69F24BEE3E17ACC119AEAFE76CFEE95F30EB1489B04BF63339954FC6A3F78E2513C6A497A9F86DE33ED7D5CD79F799E1DFBE755E4796357751E4E1E9E6E07D709183D75CD380EFF494A1E7812BBEF10CF45282A3D028EA2EF557277E140A0E9BF46B1A41824CB66CF8ED64D3AD08EF28BB6D22A53C3B5324B742F0178DC77FBF0CF1DEEFD2F089DFAA5C8DC6B7679DA291D13879845773104BA33AA3B9F7BD1786E7C12A5FB25ABDC721C2EF34F5A64FD948522F0206A117BFD0EA1E3E9B5E63056F15C5297DE92EA3EEDD3B14D59AF8874DC92D1BAFA478D56CDDEB9005A8E7E18C85C25994449EEBC0A5076A7D5E0C7334409CFA4247633662389DB1BC41A1DC083B5F2CA26A96DEF1C4E5DBC3EB3EEA78B8F0178BBBF4C5C9F0ACAAADFBD765581DBD6AD4FD4B36AC8E5E35EAFED51C5647AF1A9DF55147AF1ABDE9A38E5E357ADB471DBD6AF4A73EEAE855A39FFBA8A3578D7EE9A38E1E35EA63B1D3328866B1A65D1CCD188C24946645DD45D325B3BE02EABC193A466386FBAFFEB38BC45775453F6007ABDA3A5B0795F5A92FC1348E92689EBE3A4F56577EFAAA2AF8AA60791933767F44F1EFAF9A1C7F3A2097DBF4C4136A4F3C3D9ECC4F7F7EF3D69B9DBE3DF34FDF98F44A93CE277D16F6F8E4E72E16C5AEE44B1B276FDE76BB179D25AFC1974034DBFBA124DB2CC48B5F85C57780C46AC1FDEE2549FDE5976AF992364EB40AF53E3624CDDA4D202930D8F145F9C116A7C7FD26A7661FB9F132EFCB1A5CAF97D4C506EA2775FDE63DA5C5629796A7C55ABF7B8BF500D5F6F47EFAA87A55A6B3C9313CBEDC20E7146C0FE9433CFAEA5959DDB43A697CF687F25595ED0FE57779281FF338E749124D8BC4542067A6BE7AAD2DEFC7707620A6B988F7AF150AF079364C17E674822C0D9409C4E637823D14FCEBA3D01BFE754E439BF5BF09AC9987F2E3CC05788B0B36E9603E2F0853D19D05E13458790B82965C5965DE96E808B3C6A96BE4BF7CF0577E98F92C8241DC8852D7C8796B95E5DE1D35B0248758230738BFEEB595678C41415608821C4FAFC606B12E087ECD3C6915BA8D20489188D2FE8DD47B4B2452DAC38944FD02B23EA3A48949A89C02969B735546C8046BB402A79DC584E331B57E1ACAA04C88B62C337ADB6ABE7EF54A36C86889213730B149DD41B3ADB7216A906B71FA022D7CB18E413B9BC35926166A555921C88A98E914F09456D3371C810B1FA02B99305D68C521E3419758D01148AC17B02678E1542743BC9E8C94A115999B580CFA7AEDE754C61EC280A676D0AD29147449AF5081E1AC332152550520D8A4E758035862060A2AC2466987909558CC8D543D80345BDA437542212A2D0501942FA0E36DE5952920DA917725C9D43334498DB255C024CD9A78420C7E3A7E51E0A9371172E40131CD286DE8004BA68B02D5CECA70B0B95878C1125F666CD038034BC90EC049B5479091E5545DA2A5AD555F40692BBF1D18F91C3D06A10223058D338C94EC1418C9A9BAC4485BABBE30D2567E3B30D2DA6790362D7C7A578E1755CC039FF135DE0631070C2447CF1B15A891B7669722D7205346E1737212672EA7E0A6F038456E65770EA7A5525FFEA6A5F976B81B6192246D5A21DC7785198131001FFDF99C397C3045FB4212668FAD0055B18296E340D6E81B320C46C4B54A942D02A20E8123EA4469B17555D2017444F51D48D01370B276D259B59494C1205564D4E8210AAB62C8954A82EAD4768793DBB481A7B092BD343D80B019EDE5FA90A2716929552E912E20E5956946EC1D6417A106D08D9A1DC092642B77720D0DD0DABF1961A7EEC37D02B6AE54015CCC75F70160DE306303326FC3AD0574AD88169085522A00D3B144AD108933693DC51AB0A83C3D0600A406D98A10A025BD3AF2944D5F8C63CD81262F824243449026339741017315AFE79751BCBC3CF51EB27F5F44CB9517BE7C0AE7119E6E819701532D36E45A2943925AA0B90B277C4739426AA128CDCE15B5CDB050B78733A17A85E4C4009242193924276690146B19012451A1868424DA1EDB08C9FB636D448A456480BC3F36C12350C7E070C4651A0E8D785B6C25184FF4C128149182F1C4088C621DC3831195694030A26DB195603CD507A350440AC65323308A750C0F4654A601C188B6C55682F14C1F8C42112918CF8CC028D6313C185199060423DA16DB06C6A60A4CD2EFC1CC8FCB3B83A5889494C36029818C0A9BB2DA0080F28A7407508260144470451DA094D03ECE04EB7919BB9425D1BA7A802FA45AC4AEE84D17B185FAE8571274B0848D4943814059C4E10236D616D6D20C0544AAE3A414EE0D98A3F1A33AD2F5EF4C75DA6C9B3CAAA042FB116E0C4AF2621078B54144AE0C402CA745377B39341BF40F559AB92872AD460252FE257ACAD9314919D991FBFA36283A3C65350D72B88CA03AA5F581FBB62C5049B09233A10680A4463AA5BA68C7001D598E25DD1EC3A3D62EF1722CE06D0633CD7B13B56EE8D061A28A66DB37579A45B44A1906BDB3C344520AA4DCDFDC61D2AE4E251DB203682722A3A57B87FC088E146AC946C14C47F7212A1BCD8D6C4302990F17B4E1C48F7C7DC399AF1F40B47648E416D48884FD0729BA6DB84DA14A4B1F9D799FCEA4CF34A01ED774CFC55CCF61C8EC6296371EE819CCEF0C267756401CE9B4CEE19CCE353ADDCCE6C60352744026E1071D46BA022C5AA1227AD58B35ECC1AB32CC901056D9703B814C8D5E75235627B0B58F4CBB00A98358CF35361D04C8E3812471634C52A633408E643F8CA0FA906834DF061B1518EBFEAD8747B858C71B0D2303A6DC06C36F2ED82374E8D93A6F5945220C4C0E81521727CA3A702C76B48C249783D2C88993EC00B9CDADE5E80F64F911D6520DC58C1C2D21819A8EDFC3F923A78BDB6277E3F5944AF78E39DC3EDB00BB86F01AD09396C24EB4236E4E75A81DAF674018920C4069FFF5A6B08343F04A5B3911A95F58960A28AFF5C08B2800A9FF9C86A4AAFE6FF3500B436D7407C19FBA11EC85E9017E505240F557F60D05A1AA20044542BA86E2891C65AD92C59C5C1D67EFE3B46AD5C9BB511594CDE9082AE8D5E524A7C6D46AD51F3453557F74669FBA0209863A78888AD4DB40EA2E9E9E1AB633B53BB47A51A4452E5D0C31F035482D1AE8B037C8839E4F79207B12935EF2A0F41D1D790DE83D34254864857A7AFAAE5F8FD2DCAB377C9CD1E03D46F3271847F1CC5D83B9F01424C562422185B9B4EF6A96D63548BE0A457B4AECEB717CDC3D538B1ACA9D58FDBE554BCE9E929451005377AF4056D3D0B0B4C9A4F2DA6CDC81D2269F8A2454BF90A4A55A6305340715E2C03B861C6A953894E6769836AD6A0037E2F49C29AD15DEA80AAA32A335031C65757D473817CCFEB1B7B89ECF83A97F11CD7C46123C864BF673D282C4316A423A0BC898B2D23A1D5D438A61BABEBE803D3B03FD76DC4AF750847FDA4781C462CA4B1C7483794575C33F4B80DB401709AE8FB2E1E67228590F40AD5EF7CA34CCB70CF08B6F445208904D2A1D2C02DC877B8C0C5795D2BA9630C32D41A93C2BD5139A3EE662B232292BE1C76250FE61927DF49F591B4ED7491A2DBD308CD2FCD3AF4CB18B45BED994BC3F4CE3B5183165CCEFFCB47D45CFE141F1A119AE545F046CB539B47D97C0A6FD59C1AB353910256A7EA573BA8A521F545020A1F3DC047032B61B2A7DCEE5A843615F922AEA90884C96B3BD2F80F2293E2B78813B50024B904AC1193A3C2330868834F82A382A7989EFF709FC4412024F08E8C54B098AC2C22BC91027E029650DB6E58BBF72B6E5EBBB1A6C33D7ACE25A0C6A04FB61386C7C23702996D94026C5270D9F2AE1079369F026F025F104B24F40E5391A05D73AC74FE0557FA171A833B530463581829F703D96C050A050F914FEAE4DD1ADF014048EF5FB1820B7FA2B99D344CA6942E694DD478F32BA87E25884CF898CCF099DCFA98CCF299DCF998CCF99569FDCDC1928ED971B32E2985D9F144087ED9A4243DAF6D118A9C46D52A2D40A9175BD93DC2FD16D898C127A718AB861288BF6B47922A61328141C65CB49204AE52B813A3D01310A40A3E0FA051E92BE5046A15B7F15C529D8D8C50752F9EBF0EADBB7F3707675739E9904B61C4C6A58C36FFE0BBD0E46ACAE4611679162ACBB9724F5975FD8C4D27B0407E4F67712B71B2F667FA559908BF06B50107C85C2F788241CCFC6BC1E06767D19F241831400377C69B2642382BB2EB9568E9FA50BAB180A86D592518321B63C70D4D69E60197E1EDEDA02072C24A5C7159315832CA65A1EA03387AC2759CAB0B260BD014537225884A42A5452614A6CA941AB8A210C2A649B6C342159172FAF6507940DD1EEE078695EA9BC25D07525E710E7D4D3C6BB2401530799706266773D014ED5344080418BC8E4815A404A8F5B40560CB230C9AC52A6BD611AB8D8065CEE146D492C891B80C600B2AF7ADDD4A422C0E694955F3B9B43D745298CADBC610A555E76C714625EB26165974A69359D8141F955EA87B606A239E5057085A5E52053AAD6CF35B82B8CE9C88CAA984DA091ABA38ACACAED028551FA0DBCF21AABED07C402E56785D8059585DE2503406568ABC48DE6D50E09A279F95921784165A179C940A17925AB1BCDDBF353447F3CD9475402CCF0D1B70598B9439C579BDAA25C68418C507C55889D1359A85D945720A094D38DD2A2B746F4170815AA089EDDDC2A022BC040CA5D5B33FB146152B1930B1BA641215763438899423D49E4F8207670058E3CA99E16ACC9C8E5EA20A5301B412B7F649E7D0567FC46AED2CDCA0BD0560D8572AAF548A529E5DCBBF7CAD2FA3760D034277C03045D71E13A884ECC2BDC0481D482E61FD899BBAE9E6A66B1004D71A19CCAAC242720AF01719B8ECCD8AA4FEA2B0983464D67EF0FBB1E311A290EC2BBBDD0F45E422E997EE3A5C0693D9E7641660B0D1A8ADC101BE34DF48C279253B4144AC98D27649A90D9F66ABCFB631DDB01D46A1DC54232CB89A93554A6FDDA4D78C35E6A37919AA0A250486A37219588CAB45FBB09CFAD4BED26521354445F7407ED26A44E5199F66B37F1617799DD14CFC0C32AE20FC143761352C5A84C7BB39BEC0D72C478E467CB0565290F97D335D6620F185495DB6917E956097DD4E410819E1685F2C554612E966748AFA0D3E411D213CF544BD2C1A9F538B473CB0E8255C51BC4808D755E2D6E294D7CB798AEAE0E77C0988A2C71F3BD73E8A95CC9D6B9F2655D70935BF6B62EB0718E6401935977BE47C4574E5B182494A2EB4B5B26B4B1E848160D9B798ED4E404F3774B51CFA762A372B5B2744DF34A7B4B5AA03D84A9D31406CBBA68F9EE8D3FE0726F4B0EA1436B589C2F6B6006DE47746E77BE42C0F4AAB323E6EE5D6760341815758644B2F71E6E30341809CD8741833150CF84038E7EA8E3696BA2302BED8D37DC00CA57DEAC4DAC7CD6CDC0ABD89A9BE05F0D7DAAAE1F35B3E9F0FE523D29939113D5544FC7CCCC37828918DD7E94179EA4113EDD8A26F387016CC92B842FBE2094B8767001C86672AD284C7153B9B311F034096E26D53B26905292574CD47A1119225BB8F233FE66DBD8F88B2EC8B636F1091861835AFD080CACAD72E75BFDEA4B7F66E45F2091DB50FA5E09A627F662898E9254DEDD6651289FCE00ACA7F7DC464B4FF2831B7A8B0E7A7548E216F06A1FF371177D574332F2D2DEE2004747E56B1C448DF598F7B64ED4928472164BF24007AE22E5F4959ED16447AF1C9EB7AAAB51F55F9850AD8CB2A7EA1D4E1BA84F72CF73C86260C9431E70740A3FE501C5BC4A2F86F0EC19424A6F257BDB43DEEC4ACFA48722D417B9F642D2773C884773099822BDFCE1F8E86D4F2853BDF12137A2F921738373E554138EE6F4B8F8FE87DC9A8AF742307DF117436065B5CEE5E34F84F471230265619EFCAA05A62265795E71931199759FC6536ED9D11E5F40BB996A63CEA4EB0EB8FD46757F7AEF06A0BB5F540768789BCE902E50E38100C0BEA6CF0BB48C61F0C040C32CAA7BC824AD60F0A6403FF006EEB057E5AA296EBDC793C7F07BEFB1FC34E59044BCE8BE3B5B02D7AC03F6535DC6DED249721D7B33D8838EBEAAF8383E4EFBEEA86054DF145E7F7B7774377DF2975EF9C3BB234632F557E9DA5B7C61E85F24D5872FDE6A15848FC9A664F9CBC1DDCA9B663DE7DFEF0E0F9E978B30797FF894A6AB5F8F8E929C75F26A194CE32889E6E9AB69B43CF266D1D1C9EBD7BF1C1D1F1F2D0B1E47D3969DDF71D2D635A551EC3DFADC57563593F4328893F483977A132F610D70315B0A64E0BDE86D13D6F6AEAA6C5F7D2EB661469E4509157DF6EFA2CC7F854CF3C286AF90F21B235E32BD321F93ABE803E9AF424956F66EEA2DBCB8BA80BE2D6A7601FD45B4582F43E1671E89382FFE218F2643D5231F34AE384F1D8E0BD6BEC76D56E54F7A3C4E441EC0A90E9C47F602429B45F10B9DC33F835506E63693FA473A9F993FF7D68BB44E9E6EB2E3BF895CDF1D71B0E4717F24009FF3447C3F22F532C55045EB6A322684FE262FDE6F4709D97FDBBC8A5FCCBA5A662E996CD57773EE72DE3A9C6FBCEC398CF2CC459B2DF7C990E7F5C595846DFE95CE5960A65BFE922F7FA9557E1AFB2CA09A7DC863B096F7697ED0701E8CFEE2C90B1F7D0E2EAD0F6638A9DFDAC1A0823EC683F34F5241F3040E4807736DF2450D621081F3A00412B2D29865BD4D21BECDB84F4307150D718E5141B50202AF39B7CEDFE799FA28E70685760D610AB20D85FBA265BC627F1AAC029FE7D5F899CE2B9B61F02D53FD46E7B2CEAF1268F2287ED1E8D7453E092F4AE3676D5E20273D3B6FB2D078C1F86F665C719E261CCB242F4CD4C66763DE52CE5A6DB49E24D33898F0B869FEAE312AE6EF5DF9713E019CA657EBA5C01821D1983444ECD7DB689D32CF0B55007DA773BF0C16F930D0E6B9F995CE299FEC86A9D85AAD0F1A9E6B360B32B7E72D3E44D37536DE00B339946877221B4F5C7F958C934D12A33AA09E0C7C36E20D0666D0F75D8DCB8A9BDB1D04672023BD080D614168C6BCA4A419EBEF46207116047ACDBD2584A3D65439E29156FCB27D414FC01C72CC7CE695A051FBCBF639D1E1BBB8648B4FBB9763BCF43A3ACE652443CE687B7C8C281F1B691CD7BBA710AF6DEC6D1AF8510DFD02DDAE8600AAFC3B6327013234F31408AB7134377DC57AA0C6B61E012CDCBEBEAFEFC6C71DC3CCB416C7BA59064985F9693AA2658F643D1105AC7FA4F39904A11773BB1AD56FBB33CE54A6CE32CEB166A8BE19B42FE4C3848F3B354E290E18E8B92F8889860B838BF70B8555BE63770B066EFC378D6EF5142C663053EE93566A017F8287370342B25300A69DC3A2E198C08B00671297215B75F4F114E94E105A93AA59115A94C244B286C495E5DB13A6D05AA5AACBCB789B73C579EAAE6241E153F3777D6E30AFE1A3CEFDE6DB7E90021D9A135766E9C4CCDC9743E7421E81300E4B2F645388889FF1347E1E4DE313DE0BA20140C5880002350B99BFBD928001FABE7DFB24DB32967AF9F9117E52BDF97554E037DFFF84DE0521A29C8E6C7BDCD9BBB3A2F7403D6AF82438F799F5638D04D79B3B5E20D11A9F8C78A21CB51628E260E9C52F657211B740C17DD349649BAE63D67DFEB6F613B19DC5AFFA9CCFC3E40F21058BFBB63BEB940B2F49593384FE349FD5096242DFF53005C6D2AD0FDD44D0188F8FCBFC06A5268FF2274D1ECC2CF3205EF20DC17FD349D14F923FA278F6572F79E2D3F39B5FE81CEF4ADC32532F576D96DC270D299FA2D087B2FD5A1F8CF821168529E8357CFD23BA646E268A3F86DE64C17317BFD2397F8EA6BF47EBF46398F7E3FB74DA660D7C36E00DC8CC7FA3733D9F4EFD24B96410F56717D19A4F98063EEBC5C6574284B1F97534B11EF02CB169E027B0328802093C7442C29CC510B39ABC6220C976F3B326AFBF7B8B35C4ACFC7D9478425F9ED6C753CECA124F080FDCE930F29BF2D90FDEE7B43E6938F8B2CC6F3E7F96ADF9A12BB08E0012D83BD438223E25D9BFAFE7FF82412363F9AF96D0C878D091E1C6456475F25CAADFDCB79DE6041E6CA79D332AE3C2A2A8E26CC241EE498B3ACB4B159A4C3E04D9C1936510662B95033790DAC83F403B091AF4DB5C164B6B76FBDD2807E21A9BE6DEB6EB692B7DD96DC01642AE2CA63710C480D83E70D17E56E546DF38C497D0F5EF87B068341A1BCA42ED3DD298188DD972F036DDFE30F4A2F30860EE0CE20EE06D0E6DF7A01BBDAFA2BC22401F51A49C88438B82076137044E860209766B07678C17100CED1CB77E9FFB0E7D4286D62F91F284DE8896EC037F74CF89B6F0E688D03FB80B8BB84F636BEBAAEB5B3639C286DEF228030500103F297EDD6ED7B64FEFEC2EBD73D57EC99A9719F86CCC5BCA792787941BE563F0343FA3E04370344A0EAA3D0A041FC0E71D0ADAAFE2F5BCBCFAEF53388FCCF37EE57C08EDA7E480D9982BC8B71FF079E86435374BCAAD73CB9DDCD2368E9E814AB7BF2953EF90C3282E0ACE0A9C8A3C4EB51204CA4BCB0408B53E68F3FBEA2FF8F4D6F6176D8E97DE33C2B1FCA2CD11C8606B7FD148104BBD74CDEDFA55BFD1B9F8CFAB207E1193079BBF6F5FCAE480A3F165142F2F4F3DAB9118E1411C85D1D29895CB02FCF8D0F899DE625D8EE6572B8FE396FD402FFF0FDF8B391F5AFEA4C7E344E4A1E543B302A7220F2D1F9A153813799CE9F27823F278A395A019A51EE7D0CA9FF65E43DB6B4C1C780D818796D7004ACBBDC604F61A93BDD7D87B0D9CC7DE6B88DF0DBDC63DF418959ED3105968F80CA8B0CC65DC1F431EA3F875371CC6D5F333573EFB815EFE5BBBF437AD3C35A137DC6A76837370E9F25CB2748971FA94DDFAB9F467812053FB8B86767EB636CFCBB6F95543CB47866D4EC5E2270D1EB365C01D622B7FD258B08862E6AAAED7DC598EC6CF1AB8CBAFDE6D23AFF84943A7EF6C169A9D4CE1F4DAFCACB14C96060BF04867EBC3DEE1EB3AFC137B872FB0D071F84061A9C33F011DFEC9DEE1EF9DF6DE69EF9DF68FE1B44FED9DB6C042C7690385A54EFB1474DAA77BA7BD6B517A71383BB9F5A77E207869E1E3DEEDEFDD3E85DFDEED176EF7CCDEED0B2C74DC3E5058EAF6CF40B77FB677FB6374DD2E1C63E1BD3E85904FFBA4D5E5F72E6DA75D5AF35C4D9931647E55028519C1CDD1D8600D1001A56549624D1A9DC8C3DDABE65D2674B9BF81ADABE4DA1FBC23DE72A9D4A67D50C187D0FD941C308BF79186EE36153FF46CD31D13282D3ED14F879FB380CCE3A620D56FBA5C6EFD47FF1962557E180DE69B8EFEB679F6C1C5002463A83908C959515CEFAD04BE32BAA173A2DD0F1FFB3335FB2BD337DF3B1D4A9D8CA39683A8D9083AB651EE871C9D1A67D55D9C99B73B2D6F744EDED939767B04CDFCB9B75EA437D06178FEDB6810E0E66920DB47810C9F03EAEA65323A16867F3FD3D9BB990EDECB347F27B38397D0B7A911AD8671051FBD26D41BCCBD764149030E31A47B9349EC7F0FA0F78F5B5F4683880B5653EC2DAEE7F360EA5FB0D62DD6D63301128B354B195B03B4E8B1C31A47C685C7918A76E829E4D5CD39B77D90FDA051FE1BB7279FFF402F9FAD1B5E144B886D3EAD0F9AFCA075C50FA35C576C2DA65B0D874A4EBA4BFB7A0362C417952EEAEFE090F8C5E6F22EA830A1BDE062980997C0AD5C4BED8BB858E5FC0CA1FC49834754EEC4707C363F6B4C5BE36809726B7DA0F35B327AEF91D3B0FE5163044F536FFA94351B377E377EDF6FBF90A77AAB284E2DE6785071D2E40E2E8899B5A017EE2CAE7FDD217F5728751DB2F1FE3C9CB1A8210B6E6C423D98A171A3A919C91B912F0F372A44D567F0C5C75E3AA5CB5DE0F3C522825EB0033E6B246A3EBCE65235B31FB4CA3F5CF88BC55DFAC28F36DC271D9EC73C27ADF3A48C1C97E9D854A6139E93D6F954468ECB74622AD329CF49EBBC2B23C7653A3595E98CE7A4757E9691E3329D99CAF486E7A4751E9791E332BD3195E92DCFE9AD66795CA6B7A632FD89E7F427CDF2B84C7F3295E9679ED3CF9AE571997E3695E9179ED32F9AE571997E3193E9079F9EC383FC6FFE8BEB30277BD2C755A403F2D20B76180B5ABC5312D201203C6AA4F9985186E2AFFE3337A9DAFCAAC709E82AFB6E62D04DB41F6C825EAAB17CA546EF851AAD7765E0E05B98A15D8D6B8676F792A4FEF24BB97461DA3A522E84E65194C7AC9B348B09D70EF31FFB5DBE72B544B4BF06CE39DA6FBC98FD958AAF846BE21DE54346BC84831CF3754118F5ADCF7D6E497E171F97FC8EBD2B89F1707389EB0F8EF4FC31331749742A4604ACAB5960465F7325A16C28F37CDBB13E7BB1CFAFDDE7D76EBEDBB987F32489A641794A0A4FD0A9B47D68FC76A8CEC7818B49D26FAA02803B9AF1C7B3E4D53C7CF5E2471FDAAF5165F20807BE1A4C3932BEBD326BD772DAAB7007DA82A042C54D9290D424A1ABF1EE08048C15A6F21EF40081800E30098FCED086D669DF6EA0DF41459031708F505C6DE31ED7195C0F9ACFB97E4AAED68BC5FBC3B9B748F863CAF6A6EDA26740562E526ACC5C2FC7A3173FDCB6A735445CF50C60D3CEADF36E0BB2839E5CA634864F7919377894D5A1D90E3162FC9A5D6CEA9534C5DE5ECF4A309535123F8B01F943F557F60D43A3BA1C764E23FFAA6E5355059A7024CC4DA01A89F3154DBC2A75D3C36CDC288A4D14AB6FEE516A6C36675E3437192086CA93E2E52CB1ABAA40B37D5D62D77EEC562A67364EB805AF5B6C5187668ED8258A0C87B32146E1B6BC3B8506D2D088103B4283CD20A88106777E627B47368A599C41AB1D05A8B0C55363E0D200569BA521B2C0C1A95D8340EA08679CFC6EFCA4B17BD40218C930EE9D182948E2A91D20ADCDD2B0A5F491D6814BB30978DC40AD93A53395179297E96E69ACFB20A89BA52D43A7BA154B59CD3D17B37D889EB61EDCEE369037BA9C226AC716B8E846B38669F364AD0E4ED5E5DC0055558F26526567D55B55A90EAE6B6255A9C6F68255C36C6AB456A905F933B841E8C73C499DBB50FE52FF9D543F64B8F31EFD02A79B7277D3277FE9E5F64B56DED4CF9EDA9DF997419CA41FBCD49B78895F901C1EDC94B791561973AF32825777FFBDB85804F9F1D88AE00BD375EE27E9D7E8773F7C7F78F2FAF8E4F0E07C117849764A6D313F3C785E2EC2E4D7E93A49A3A51786519AABFEFEF0294D57BF1E1D25798DC9AB65308DA3249AA7AFA6D1F2C89B45478CD7E9D1F1F1913F5B1EF1C54BB6242EAF7FA9B824C9AC7526BB918D5539D4F25072BBCD7EF3055C5478B9F5E7EDA25093F3E5DFF16E7C533493E5FD61909938F71E7FF11902B2C4B91B2F4DFD38DCE4541F1E6478CC2E81AE317924AD850766A32ADEBCBF7E0A67FEF3FBC3FF9517FDF5E0D3370ED63F1D5CC7ACF57F3D787DF0BFAD04A9C408BF7BF1F4C98BFF65E93DFF6B93631A8B698C3CC3F2E16F0527926CE5FBDF0E849AE66DE480D13F8355D64F9DF02A2F0CABCFDE172C2741AA3054331F4ADA819AAED0A417A9BCA7AA2B4940EEBA3F85DE52DD28DA1DA2CAD933EF9D0507673DB448F57380BD1B2F66962E1F3A70CFF1FAE2CA0953677C2EDDF069254E17FCB2DCE734C8D0A7D9FB9B39D376ACC02B63CC315BB2B0026D92364CA46172B26F6B665B1AB8366992A3DAB371C57F8440A1A1B27A78A7A0B6352398FBCC7B4C1DB9F0927366230772C6FE34581581B6036ED5298E362B52631645DB8DF8C57BFEEC878FE9D3FBC3E3939FB52D95A5E63B512BD9BCEAD040A8AED3A89E5B70D26AC524D36C086F97B6EA374D564E356B9C293053AE66E044BFFA20820B34AD27C9340E268EC059D8C48FF359FC342DDE8D7333D989A6DEE2365AA76CC6EF90ED65B0C8D7B84437D1E8EF276FDEE8F2CD0CC01C19D44E6D57F2FAB5B6379F154B33DEE243345D67A7385A534A56CB2408BDF86587C22E6449CBDCFF35189AF56C8181AB11DF30AA14396C4D5059BC0E661759E63C2CC3CB9A478731261ECDEAB5B13BBC39F1A24C684701251CB499F172167205D993D7CCDF5E51D4DC162F6BD255E1ED1BEDDE8A6F4B6875586428E8AFCF0EDCEB62274361DCDAEFDB01609330030DB33D8D91C046087E26546F844019598D1646E7D98C05A75681A9A469706A75C73DA8746CD54C6006B39ECC000B1B33C3E944D622D999499662E1A8B7A1029216D065BBEA7A024A58D125B48913801BDB8D83053409D4206280BCD420FB6416E6B689C5F0CC4AB53911EFE2DA7C55358E36B8258B949A9CD27E5617758391F5C49960C5F2D10EC783FC4109FBE07954E1A42289DE6CFC0599B9080EB0CC748762EA040CDAFE573CADA4E783E1D33A743F0C80D8B52F5EE50907EDC4505BC7F0142C664E39226742C7D021350E04EBA19ECCD8A6A3EA9CA4ED48FC6D9A18998AD4E9CCC822CE848E831BB83BE2017895D753779B6D8BE3AFA2349897F3930F7E2ABC9F44336F287031B12ECCA5CA967CECC6BE8D3AF53B66BBB4559F6CB272E2F69B373BEA69B52969A5D1E63AC87D9AC78F9DE6E124DAD08A2CC6B1CDDC74AEB66ED5D6A18E619C227162286033EB68B304E030513EDBDFB56D9435C7C3A461201E1D36CE8EE622EEEED8EDE56FE2FAB38EBA8009ECE15BC9D550C7B067092277FEA6E88B3F466EB7F34360BB156466CCD84FF36061B978D360E444B0551C2CBDF8A5CC6BA57B0520E6F1A7EB9861F86F6B3F21A1408BEB7998FC4170EE249EE35CBC5F7849CA9A21F4A7F92CD5817019560C43E246D141026191D3C7ECFD644536F35B23A6CCE8F3205EEA8C8922A71B36B0FE11C5B3BF7AC993137DEF4AE0B35658AEDC1CAF7B8A42DF618A79839F13137EFD23BA643E288A3F8659292B5E9FA3E9EFD13AFD18E6BDFC3E9DDAF6A59AA1B568E7D3A99F24970C78FEEC225A6F0E05990C07F9B4030858D49DBC2A99FFFCD3C1A7E43E0CFE7BCD3E7C65D6E06271AE67D9048955A493557FB1F082A569C49817360D1BEBC23FCCD428D7D859389373FB7BF12451778B1C4DAC7C8E1E03A349755EF0A6BCDE82D70B2C511183CF253A9CBF7092399EC6B4B470CC7B48681BA1477CB0D0666A0A92C2AF1A9ACF62877113951606D214459D4AF321C8CEF02D83305BB5B344B1D63286F16E602BE43659BBDB859DBFFB6AE66C68C06ADA6F623F60C960DBCCD7CC6EB5316504F0B1BD83E5BE1F13FF1837A86CF31297119A5D20D9058A77C049DC6FD601CD932BD63C1353AF2B30E9D0B2F8522AB1E3D485ADFB8ECB8558F8FA0B824A7541BB853A8717676CB35FEB75C7ECAE34BA41D76DC045B7C3C2481BEFB67DE39CCB3FAEBA6B85AA339B3746C32B19B609ECD75C37CD0EB99A7DF2D616276FADCA25AA3B533C0A0CAC54E4B88D7DF0B8E1C435F05B376203E83A2E80C50807157A5E5DBC9E9717507E0AE79189513916264605580C9343E2645DB8BFABFD48BB96B64E47606097E065364F8436A0F7D7B17676D73495939BABA6334EA76EF6E9CAEBEA5C81ACE4F7D55F6CF2DA5C30BCF49EDD3204B345CCF230982358274E58F9CFAB207E7191D933860C26AD31F5328A9797A79EC9785A163519471B453B1C3FE5A33561441118588D28572BCF095AFFE17BF1B14D34903138B165706ACBE0CC96C11B1B065F59AB2F6C186C694F9F98F7F489794F9FEC7BFABEA7EF7B7A8F3DFDFED8B4A3DF1F9BF6F3A2E4BE9BEBA2E2EAF9D9099F6F3628BF7576B720B7B26622CCA7ECFED3A53F0B1A421929E5676BDA76B29C3F06E1A31583D932086D18DC4431EB3ED76BAB84E4E2D263270DFC9D4DE13272AB15A93458B83C95B49D4EFAC4D8499F183BE993BD931ED249EFFD2CA6CFDECFEEFD6C477EF6D4D8CF9E1AFBD9D3BD9FDD07C32E9C7471922FB9F5A77E60E963F77E7AEFA7C7ECA7CF8CFDF499B19F3EDBFBE97D3CECCAB715AEE993957BDB7BA6F178A6E6218932AFC4E8E5A808E0637B58A2C9A743FF053F9A4E79ADA02A68E5AF469616E4FC3A9D5507C98D5BD5C56EB9045683DE05A4E4EA762C79566F576F0D0055F59B1D1D521290A8EF01E4CDE702BF73169779A91BB90A5EB7FEA3AF8E576C12549B23C56D33E7DC72B4B8E5A0623362F0BC3A44F8EEDE7F369ABEBB3FD9B0BF96567790B51D616D87D71E3C8FBB216D1B87A1C6F163CB83C796478EB7A69567FEDC5B2FD29BF6916187E781AC5F32101ED4310E6EC77287898D391BAFBEB978EDCDC52B6FBB6657D3510278E4DCC2AA5B3556789349EC7F0F6861A64D335D3055636F713D9F0753FF229AF9C5BAE792FD6CB43225E367D2802A7E3FC29CE3EAE6DCCD1AFBB76F4EF8640B4117C59A90ED7AD0877E975C4D1D7CC433B15E6C1D8B8BB7B0EC17C3DB68968617D02C7B79CE871522DCF4426315956BD68E62FE385A3A65B8649CBC475723569A7AD3A7CC2F57FC18BBE225CE1F61D17915C5A94967284A9A74874DC9A17D88DD6C26D3E23A6463D3793863235D36C61B451E302773CB429CBADCD7DF0CF246BBBC56094AE56ED1F96211397C83E4DBC36B577C1E2EFCC5E22E7D717409D7B7073767C1191FE792B9395BCEF83897CCCD5975C6C7B96467AEF83897EC8D2B3ECE257BEB8A8F73C9FEE48A8F73C97E76C5C7B964BFB8E2E358B2AD9A2BC2037F76B3BAB378841575179294CC3A8C4A805BE5CDAEEB60A0FAEA3FAB773FA8CC7E5084560D697AD1BDE9EDF5D87DF19617BF5F01D30BF5EA5A264CF6AF9E9F34B97B49527FF9A59C341B183F693230BA3D9267B0354B214E971A7ED0FB750AFCDD7831FB2B357B823169B330C7608BC508D71F444EDF49AFD8F47DFDDD5601307F61C532F363CDF1304D0BE831F70CB9E6DA904BD74963E3CEFADA8A9CAD81B3ADCE93249A165BBFC01E569507CF75BE8FE12C8F8BDAC4955477FE62FEAAFDE1CB7A9106D9E6389380056F82921B86DC9E43CEABFEADCDE6DF0436E5A59E69E065EF2F2669EC31138B6E2208A7C1CA5B402A70C4CAFD39F01DA7A3BA0AFECB077FE587996BE0B475536DCD9D73712AB3BC3B6AA0400E8E46A2457ED4E7A1F1030A92264DAB5D9BBF6B40849301E3597EEC0434A8DA48E33552436C21C32BE8A4FA7EA153F9C381D1538981B1DD7CDF5D0CD53A522468947B1829A4F221F641A9A01C1077CD4116434549D486C6EB57AFB60E7C921C4F351285D0665858B661A027E7F03E50966DDB1160F768DD02B40A8DD4135C651845E1094205C3C7D6F84BAD26AE66920321AC6E36B91C3D008845A8C1BC54A178C7EDE1B3382346A104D1369B1FFC4EF76FA2742DEED0E74EB0A53409D2CEC8E28205E200959D0AD233E41E54FA20604061B08DE86AC94F69CDB05160183CA925E80148D902340AA6FE60C48BD1622A7EDC3908092A6E1580482B1C19A1D0AC86813A7589C411327249292D823E7A6F140E5956D957E35F2CBC60892F9E5BB77BB5CB9495CBEB6A7103BE6E3D06449DB603099FA3C720EC0909795D2812CAAF3B858442A7ED4002BEDDD4E5B61A09603BB0A14646E02876D372406482F7E419F2443ECC31141F77CA2F88898BDAF5F685027998EB1210639850F40908A309C4E080289684453B710D29AE89357E1DE7F0B01190D210EB8ABAAF814151614FCD9F3585CEEA55554668C4E2C7ED5BB5AA65A7B6199CDFD7C76A95BCF61E00D30CB872E068C7980274C48F74081905AF1DC49E64F8445C214B1859C5BFE30512DC23FB0614E8E860821F02605A7E729440AB873B25C09C0D726302D410039D3180061FEA5A60E91E287DC7C57D03412B2A1EB4E11BEF7DF38F4AE07B711C5D2B90E5BF6984C88DA7C77996F5EFDDECC3C914475A0E78C2C32644C69E5DB7A9BD57F84CC6039F09029FC90F009FC956C2E7FE782CE8B93F06C193FDBCE3D8E15F87DE16E89C8C063A2730744E761F3A27DB099DD3D140E71486CEE9EE43E7743BA173361AE89CC1D039DB7DE89C6D1D741A72F06F50A1F8E1E99AAD2D7CD3C38F212C1D21496A00A4316FC497BF2C91A48B6592043D2FF555AFBE298F956B9C02A7AEF3D52FE061EB7C1B826E8E30691CF56E3C5BE768950F7EFFCFA4FAA12033B01B1A1394867748C6D01A8B57E2E47828FFFF067C7DA6737071B537B9F29F76145032FB2302AC460225EE0D4852C63FFF6E247454B2FEB60DE700A40F61220DA87CF9520B41BAA70348B50F001F8DA49F8E50349614A0E13165981034166861CF7A8EE90C2EFAF428165BB58976FC3C2EED61565B718684E230C721C688BA218F4D58E16C1447295080C99FE8EE7C301D23D2861F58ADF03696E1B505339DA81FC5D7F6C5FBDA8D3660A43F1EB018C4F8CE3033B6E87E080459C6F5E30112CD8F7609A7318F6F4340CBC9C8361E80E9464FCE7065118EED008A6C22B4F18087B8A8EE1C3B432FA70F891C8385F45101A75A06D5C48EE359DB581034DC0CCD024743CFCBDAA23FA8368CA1BDBC2DDCC3D3D9BB4B86DAB39356DC1F34F2D343A5DC8AF99723748815F2A78AB8AF3B821185A1470A9386D41A50695062ADBB7D90690A4F69BAF5867E10E828EBEF173EE5A5B7FD1D6D1D0F6AC8818BC3B38C667819345A01361BEB5977F6ADCB8D59C5EDDB2D31A0C8BAF8B0E37BB078632022402D3AA6BDD8A642634A05D8C3AD5BB80DFD8A408E37B7CF078C154E43BC16A00D9CF13C13504B421AF8B41032E2216E5B4132B41F69C748EAA541619C72111391C0E77A35903C2CC56DCFEF04325A7029DA8820C610DE451EE518E2660F9A6E403394B7819EEAEA2F78D93F7CE4E8E1A311C0C7F435426A4AD7D6BCA1B5430F5E8E0056C24B97144C491F0AB47920B0E7FC43416A4D3C397E00D061F5FD82889CBBCA95C19A7B7B325879A1355B70803C5652ED3D0F6DC31CDF180E35833E16AF8997D19DCBD00A848C40B335A1D04870A4150CB59A7214E0BA606C626F713D9F0753FF229AF98C24780C97ECE7A4E59A8EFBF34D32995A15C809770E755275B7D29D1501B8F6082886E0C0D7EDBB6D9F1E86477CA921EFDB1F3612AF1E2BCA14C99344F02B919AA4E01B4EC587ED7A18AA253B698527B2CE2B327A200AAFD7394A3EE6D2B132292BE1C7D550C73CE6651027E9072FF5265E22064E59A93B3F6DDFE17478507C680639D597BBE993BFF4DE1FCE26D9AD6ADE64B129965D5D2400A75D41DBBD08B5B43F43553529D4D5B5427B51A7E65750AF0D81565D5751EA8346144814B59654F49A3701A8ACF20D95A2FE8AD048867250A208529212A5C9A9D522496C2137005DEBF67E2F5A4DF159565546A1AE0E4C95116A05A9A0CA0142B50CD0514F410488089240A4D3134051B5BA527575E2DB7642952209542D4F45AB1AA90EAF42CD1678E157A803A0C12A2C9F2BD6A9B57C4D565A6B4983D55A3E8DAB536B1191482B2D48B03AB3AFB456C3BA66E31B5609B11B6EB27CC14A8A4F581DD957BDB15A521D4CA61ABBCDC4208840AB9E563590F80C5A9BA3C1CCDE2253D75E1F1811EAACBF40356DAE91A4F1AF0F1960D5D40492DA4A1A75A5C27D8742AD0205542D47441835F84B7FC58183A700C78E3611B1E2FA7D17B0D2FA2B56614D40AE6A22AD6AA2AA6A42AE2A7B3902AD29FB28ABE81E9A6822F59CC8EA3951D47342AFE75456CFA9A29E537A3D67B27ACE14F59C6939CD4D1F913ACE0D99CA79923B9D7096168D906B0A59905C12E90D19EDE3E0520BB44955566852D34DA1B083DA08DA23977CCC528E561AAD8C043D849988E61C44CC3E904D3269934BFDAA91A614281415D31A54BEEE2F08212787249297D07137488B00344A47436B932F7064F6050DC6BE90E2AF5B7F15C52908E8E2038CE4EC1B95F97578F5EDDB7938BBBA39CF4C0EB5244688572ED21AC9F29BFF421627A7A54BC4C8E95337C5B40D9FB2D1A66B772F49EA2FBFF849E23D824170FB3B18033749A835DE7831FB2B0567F802055E6B4D441B1314C3904882CE85A5C35163911A7612F55B0E070D52C051C06F3E48B6B26AA5F90FC21A7C6383BFBDEA9D17AE7FE3343B6AAB46509B5FCE6D254E01EA4BE9254A884BDD8522CDDF65464096AF7926F092B49559EA045CBA65C022BD18875F55E6F96CBE776122217D73531DC95E78792DE55B633C668192681BCC0A59A4145F1B86D223143B6C56992D20334AE971E5408D3035466016718B033C3D0D188858125758B25D942B0C7E971810DFFAC9D9419F9D9A0FBA01556137E5A5A9A882A86AA33311BFB1F4D096563490BC4097E6C136D3722EE24727A6514515028D5C7C4164AACFA1061EA66A56FB708886E56797CAA11B897971E0AB1B45ABAD3F44D1F273578AB6F62E0545CBAF6E146D4F60107565796C4E674924B399AA5ACED5111D8BAF5DB56873814168D0E2A31B25450787E82B10BA547D081F5C4C3B0AD1609D1B1472D1C530A6F1EB40F0CDCF43D1821319B95CF8E6924DAD40F1E3C802127EA35DE9C8E405684E4D308FF851622623F7E8DA349B06D5341176715AE7A60271091338355DDD8BA826130B74D1D986324D4B3BA9D721385833D5BB72AF8DFC07E14D7268EE27219738493839A4F090FC3799B315733D6A1E5812878D49267A2611C9FB34C9043189986C626E92FB631D8B00D4FD19649339D3B207901363618E132D7388D43D9AE304368798BA63618E532D7388D43D9AE3143687986164618E332D7388D43D9AE30C3687980865668E863CC293E1884D6445708590F4C75C2BE19BC2308606B60CBDAAE42FEA0EAE40EF641F160DBBF8643721EC4293D71C9985001FFD97EA3BC1D0506653BCA20E184CE7DDF54E4C05A74AE76CF84FCEF6D1A097C125DB68CA87C4C15D342E33B3B599567F1B7C7D9BD791B6D44228D5BD81865E7F69E6E950371ECD5F80EE74FB5195D52BF8AC36517766D559DEC20B76B9763F5AD3095D49C37A7CD9EE3BF3D0666CA94F1C170C0605D446231B0E0CC602F381C0DC28430C01A89B694BAD3014F5594AA7E61A552723F8274B9F646E280BAF666B1675C82E23EFD2284346EA74AB50DE2BEC6404EBD33C6D860F92093142A95403927F6CEA034F81E11650BD1BE6C408E8A9E67ADF86FBEA647F0A7F190DD9AF223EA586A98629355ED3F0AF7EC9ED227D23CCE1A65EBFE650BE6B051845EF2DAC4EA7A8D07D302D1F5D7C7036F2A08F3149C61EDA034EBB67264A2EBAE4B1215001FD24F4BE5557F61E98701754E75E7C91856392B761E0304CE802FAE02719D045CB2B3D82EC8993AD535FFA0207F134CF0E1E4351BD2C21B78CF989B1ED3C6E27BE9220B78FE255054C39E9792FAD735E1DAEB6C9EEFD975B4563E911B956815767240B90E85DF68A5ED4DB7A7F3F6650DED2AE3CDCADE5559C9AA547BFA271DF386030D3DBCA1D9B8E72FF48CE51EB1E11CB9C0BF12A6D55B685E2F26D3C3F42F0D4C0D711E4970357430326515D20DD121DBA87E3AE794C063A220372707DD8E6DD51C1A7BEF5B8FEF6EEA8B8B7A2FC81FD9946B1F7E87F61785C24F9AFEF8E6ED7ACF4D22FFEFAE06728AD59BC633C437F9AD5B9615AD1645955D565CF9C441549F5B9BAA1C64FBD99977AE7711ACCBD69CA3E4F7DD62FC2C7C383BF7B8B3523F9B89CF8B34FE1F53A5DAD53A6B2BF9C2C5E9AC6C82E8D96D5FFEE4890F9DDF52AFB2B71A1021333602AF8D7E19FD7C16256CB7DE92D12AED130169937F88BCF7E2F3B0DFB7FFFF1A5E67415854446A5F9EA4BB4BFFACBD522BB2AE83ABCF3BEFB26B231F87DF61FBDE94B95168433513744DBECEF3E04DE63EC2D9392C7A63CFB936178B67CFE8FFF0F4A7E71B84D660400 , N'6.1.3-40302')

