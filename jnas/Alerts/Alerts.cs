﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TableDependency.Enums;
using TableDependency.EventArgs;
using TableDependency;
using TableDependency.SqlClient;
using TableDependency.SqlClient;
using Quartz;
using Quartz.Impl;
using System.Net;
using System.Net.Mail;
using jnas.Models.Jnasdb;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using System.Data.SqlClient;
using jnas.Configuration;
using TableDependency.Abstracts;
using TableDependency.SqlClient.Where;
using System.Linq.Expressions;

namespace jnas.Alerts
{
    public class Alerts
    {

        // Database 
        private UserManager _userManager;
        private Jnasdb db = new Jnasdb();
        private Jnasdb db1 = new Jnasdb();


        public void Start()
        {
            // The mappar is use to link model properties with table columns name in case name do not match
            // var mapper = new ModelToTableMapper<Resource>();
            //mapper.AddMapping(c => c.Surname, "Second Name");
            //mapper.AddMapping(c => c.Name, "First Name");
            // var assigned = db.ResourceStates.Where(rs => rs.name == "Assigned").FirstOrDefault();
            // Define WHERE filter specifing the WHERE condition
            // We also pass the mapper defined above as last contructor's parameter
            //  Expression<Func<Resource, ResourceState, bool>> expression = (r, rs) =>
            //    (r.resourceStateId == rs.name == "Assigned" );

            Expression<Func<Resource, bool>> expression = r => r.resourceStateId == getAssignedId();

            ITableDependencyFilter whereCondition = new SqlTableDependencyFilter<Resource>(
                expression);

            // Here - as second parameter - we pass table name: this is necessary because the model name is 
            // different from table name (Customer vs Customers)
            using (var dep = new SqlTableDependency<Resource>(Config.ConnectionString, "Resources", null, null, filter: whereCondition))
            {
                dep.OnChanged += Changed;
                dep.Start();

              //  Console.WriteLine("Press a key to exit");
              //  Console.ReadKey();

              //  dep.Stop();
            }

        }

       void Changed(object sender, RecordChangedEventArgs<Resource> e)
        {
            var changedEntity = e.Entity;
            //Console.WriteLine("DML operation: " + e.ChangeType);
            //Console.WriteLine("ID: " + changedEntity.Id);
            //Console.WriteLine("Name: " + changedEntity.Name);
            //Console.WriteLine("Surame: " + changedEntity.Surname);

            if(e.ChangeType == ChangeType.Update)
            {
                var resourceId = changedEntity.resourceId;

                var appRes = db.ApplicationResources.Where(a => a.resourceId == resourceId).FirstOrDefault();

                var appId = 0;

                if(appRes == null)
                    return;


                appId = (int)appRes.applicationId;
                
                  
             

                var app = db.Applications.Where(a => a.applicationId == appId).FirstOrDefault();

                var orgId = 0;

                if (app == null)
                    return;

                orgId = (int)app.organizationId;
                

                var orgstate = db.OrganizationStates.Where(os => os.name == "Validated").FirstOrDefault();

                var orgstateid = 3;

                if (orgstate != null)
                {
                    orgstateid = orgstate.organizationStateId;
                }

                var org = db.Organizations.Where(o => o.organizationId == orgId && o.organizationStateId == orgstateid).FirstOrDefault();

                if (org == null)
                    return;

                var users = db.Users.Where(u => u.organizationId == orgId).ToList();

                List<string> user_id = new List<string>();

                foreach (var item in users)
                {
                    user_id.Add(item.UserId);
                }
                // Complex part. Get the resource type/ subtype and get all the users within the org who are subscribed to it. Send them an email.
                //resourcetype, resourcesubtype, notifications, usernotification

                var resourcetype = db.ResourceTypes.Where(r => r.resourceTypeId == changedEntity.resourceTypeId).FirstOrDefault();
                var resourcesubtype = db.ResourceSubTypes.Where(r => r.resourceSubTypeId == changedEntity.resourceSubTypeId).FirstOrDefault();

                var listOfSubscribedUserNotifications = db.UserNotifications.Where(un => user_id.Contains(un.UserId) && un.notification == "Yes" && un.assigned == true).ToList();

                foreach(var item in listOfSubscribedUserNotifications)
                {
                    var notid = item.notificationId;

                    var notification_name = db.Notifications.Where(n => n.notificationId == notid).FirstOrDefault();

                    if (notification_name == null)
                        continue;


                    if(resourcetype.name == notification_name.name)
                    {
                        var userstate = db.UserStates.Where(us => us.name == "Validated").FirstOrDefault();

                        var userstateid = 3;

                        if(userstate != null)
                        {
                            userstateid = userstate.userStateId;
                        }

                        var user = db.Users.Where(u => u.UserId == item.UserId && u.userStateId == userstateid).FirstOrDefault();

                        if (user == null)
                            continue;

                        // Send Email

                        sendEmail(user, org, resourcetype.name, resourcesubtype.name, changedEntity.resource1, notification_name.name);

                    }

                }


            }


        }


       public void Changed(Resource e)
        {
            var changedEntity = e;
            //Console.WriteLine("DML operation: " + e.ChangeType);
            //Console.WriteLine("ID: " + changedEntity.Id);
            //Console.WriteLine("Name: " + changedEntity.Name);
            //Console.WriteLine("Surame: " + changedEntity.Surname);

           
                var resourceId = changedEntity.resourceId;

                var appRes = db.ApplicationResources.Where(a => a.resourceId == resourceId).FirstOrDefault();

                var appId = 0;

                if (appRes == null)
                    return;


                appId = (int)appRes.applicationId;




                var app = db.Applications.Where(a => a.applicationId == appId).FirstOrDefault();

                var orgId = 0;

                if (app == null)
                    return;

                orgId = (int)app.organizationId;


                var orgstate = db.OrganizationStates.Where(os => os.name == "Validated").FirstOrDefault();

                var orgstateid = 3;

                if (orgstate != null)
                {
                    orgstateid = orgstate.organizationStateId;
                }

                var org = db.Organizations.Where(o => o.organizationId == orgId && o.organizationStateId == orgstateid).FirstOrDefault();

                if (org == null)
                    return;

                var users = db.Users.Where(u => u.organizationId == orgId).ToList();

                List<string> user_id = new List<string>();

                foreach (var item in users)
                {
                    user_id.Add(item.UserId);
                }
                // Complex part. Get the resource type/ subtype and get all the users within the org who are subscribed to it. Send them an email.
                //resourcetype, resourcesubtype, notifications, usernotification

                var resourcetype = db.ResourceTypes.Where(r => r.resourceTypeId == changedEntity.resourceTypeId).FirstOrDefault();
                var resourcesubtype = db.ResourceSubTypes.Where(r => r.resourceSubTypeId == changedEntity.resourceSubTypeId).FirstOrDefault();

                var listOfSubscribedUserNotifications = db.UserNotifications.Where(un => user_id.Contains(un.UserId) && un.notification == "Yes" && un.assigned == true).ToList();

                foreach (var item in listOfSubscribedUserNotifications)
                {
                    var notid = item.notificationId;

                    var notification_name = db.Notifications.Where(n => n.notificationId == notid).FirstOrDefault();

                    if (notification_name == null)
                        continue;


                    if (resourcetype.name == notification_name.name)
                    {
                        var userstate = db.UserStates.Where(us => us.name == "Validated").FirstOrDefault();

                        var userstateid = 3;

                        if (userstate != null)
                        {
                            userstateid = userstate.userStateId;
                        }

                        var user = db.Users.Where(u => u.UserId == item.UserId && u.userStateId == userstateid).FirstOrDefault();

                        if (user == null)
                            continue;

                        // Send Email

                        sendEmail(user, org, resourcetype.name, resourcesubtype.name, changedEntity.resource1, notification_name.name);

                    }

                }


            


        }


        public int getAssignedId()
        {
            var assigned = db.ResourceStates.Where(rs => rs.name == "Assigned").FirstOrDefault();
            if(assigned != null)
            {
                return assigned.resourceStateId;
            }
            else
            {
                return 4;
            }

           
        }


        private bool sendEmail(User user, Organization org, string resourcetype, string resourcesubtype, string resource, string notification)
        {
            try
            {

                //Loop all external users who are on the notiication list, and send them an email or send it to their organisation.
                
               

                var email = db.SystemParameters.Where(p => p.name.Equals("JNAS email")).FirstOrDefault();
                var password = db.SystemParameters.Where(p => p.name.Equals("JNAS email password")).FirstOrDefault();
                var host = db.SystemParameters.Where(p => p.name.Equals("JNAS email host")).FirstOrDefault();
                var port = db.SystemParameters.Where(p => p.name.Equals("JNAS email port")).FirstOrDefault();
                var subject = db.SystemParameters.Where(p => p.name.Equals("Resource Alert email subject")).FirstOrDefault();
                var body = db.SystemParameters.Where(p => p.name.Equals("Resource Alert email body")).FirstOrDefault();

            

                    if (!IsValidEmail(user.Email))
                        return false;

                    using (var message = new MailMessage(email.value, user.Email))
                    {

                        // var htmlBody = @"<html><body><p> You have received mail.</p>";

                        //                    htmlBody = @"<h2 style='color: #5e9ca0;'>This is a gentle reminder to submit your <span style='color: #2b2301;'>N.R.U.F.</span> Report via the J.N.A.S. website</h2>
                        //<h3> INSTRUCTIONS </h3><h5><span style= 'font-weight: bold;'> When to Submit Reports:</span></h5><p style = 'color: #5e9ca0;'> Reporting carriers shall submit utililization and forecast reports semi-annually, on or before February 1 for the preceding 6 month reporting period ending December 31, and on or before August 1 for the preceding 6 month reporting period ending June 30. <span style = 'font-weight: bold;'> Reporting is mandatory </span>.</p>  <p style= 'color: #5e9ca0;'> Contact Us for Help at <abbr title='Telephone'> Tel:</abbr> 876.968.6053 or email us at <a href = 'mailto:consumer@our.org.jm' > consumer@our.org.jm </a></p><p style = 'color: #5e9ca0;'> &nbsp;</p><p style = 'color: #6E2C00;'> Thank you for your cooperation.</p>";

                        //                    htmlBody = htmlBody + @"</body></html>";
                        // message.Bcc
                        var htmlBody = body.value.Replace("{{resource}}",resource);


                        message.Subject = subject.value.Replace("{{resource}}", resource);
                        message.Body = htmlBody;
                        // message.Body = "Test at " + DateTime.Now;
                        var plainView = AlternateView.CreateAlternateViewFromString(Regex.Replace(htmlBody, @"<(.|\n)*?>", string.Empty), null, "text/plain");
                        var htmlView = AlternateView.CreateAlternateViewFromString(htmlBody, null, "text/html");

                        message.AlternateViews.Add(plainView);
                        message.AlternateViews.Add(htmlView);
                        using (SmtpClient client = new SmtpClient
                        {
                            EnableSsl = true,
                            Host = host.value,
                            Port = Convert.ToInt32(port.value),
                            Credentials = new NetworkCredential(email.value, password.value)
                        })
                        {
                            client.Send(message);
                        }
                    }

                return true;

            }
            catch { }

            return false;
        }

        public static bool IsValidEmail(String email)
        {
            return !string.IsNullOrEmpty(email) && Regex.IsMatch(email, @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
        }
    }
}


//https://github.com/christiandelbianco/monitor-table-change-with-mvc-signalR-knockoutjs-sqltabledependency/blob/master/FlightBooking/FlightBookingService.cs

//https://github.com/christiandelbianco/monitor-table-change-with-sqltabledependency

//https://github.com/christiandelbianco/monitor-table-change-with-mvc-signalR-jquery-sqltabledependency

//https://sqltabledependencywhere.codeplex.com/

//https://youtu.be/hBwGTTfTs3g

//SELECT Users.Email, Users.name, Users.userNo, Users.userProfileId, Users.UserName, Users.organizationId
//FROM            Resources INNER JOIN
//                         ApplicationResources ON Resources.resourceId = ApplicationResources.Resource_resourceId AND Resources.resourceId = ApplicationResources.Resource_resourceId1 INNER JOIN
//                         Applications ON ApplicationResources.Application_applicationId = Applications.applicationId AND ApplicationResources.Application_applicationId1 = Applications.applicationId INNER JOIN
//                         Users ON Applications.UserId = Users.UserId