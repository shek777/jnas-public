﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(jnas.Startup))]
namespace jnas
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
            ConfigureAuth(app);

        }
    }
}
