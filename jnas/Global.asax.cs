﻿using jnas.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using jnas.ScheduledTasks;


namespace jnas
{
    public class MvcApplication : System.Web.HttpApplication
    {
        
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            // Database.SetInitializer<ApplicationDbContext>(new jnasInitializer()); // Seed Database

            // Start Job Schedule
            var job = new JobScheduler();
            job.Start();
            //  JobScheduler.Start();
            //Start SQL Dependency when application initializes
            //   System.Data.SqlClient.SqlDependency.Start(jnas.Configuration.Config.ConnectionString);

          //  var alert = new jnas.Alerts.Alerts();
          //  alert.Start();

        }

        protected void Application_End()
        {
            //Stop SQL Dependency 
          //  System.Data.SqlClient.SqlDependency.Stop(jnas.Configuration.Config.ConnectionString);
        }
    }
}
