﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Jamaica Numbering Administration System")]
[assembly: AssemblyDescription("JNAS provides a number of services to Internal, External and General Public Users. JNAS will be deployed across an Application and Web server and will provide access to all User groups. The scope of this document focuses on the interaction between JNAS, the JNAS DB, the web pages used to present the data and the Internal and External Users.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("OUR")]
[assembly: AssemblyProduct("jnas")]
[assembly: AssemblyCopyright("Copyright ©  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("0e34a8f7-bc47-435e-a847-31613ba739de")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.1")]
[assembly: AssemblyFileVersion("1.0.0.1")]
