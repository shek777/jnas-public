﻿using jnas.CustomFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace jnas.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Reports()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Jamaica Numbering Administration Website.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "JNAS contact page.";

            return View();
        }

        [HandleError]
       // [ExceptionHandler]
        public ActionResult ThrowError()
        {
            /*
            https://msdn.microsoft.com/en-us/library/system.timezoneinfo.converttimefromutc(v=vs.110).aspx
                                DateTime timeUtc = DateTime.UtcNow;
                    try
                    {
                       TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
                       DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, cstZone);
                       Console.WriteLine("The date and time are {0} {1}.", 
                                         cstTime, 
                                         cstZone.IsDaylightSavingTime(cstTime) ?
                                                 cstZone.DaylightName : cstZone.StandardName);
                    }
                    catch (TimeZoneNotFoundException)
                    {
                       Console.WriteLine("The registry does not define the Central Standard Time zone.");
                    }                           
                    catch (InvalidTimeZoneException)
                    {
                       Console.WriteLine("Registry data on the Central Standard Time zone has been corrupted.");
                    }
            */
            // TODO: Store all DateTime as utc
            
            throw new Exception("This is a bad error!");
        }

    }
}