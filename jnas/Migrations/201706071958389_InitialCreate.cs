namespace jnas.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using System.IO;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            // FROM FILE (AppDomain.CurrentDomain.BaseDirectory = E:\projects\jnas\jnas\)
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"TSQL\ELMAH-1.2-db-SQLServer.sql");
            Sql(File.ReadAllText(sqlFile));

            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        AddressId = c.Int(nullable: false, identity: true),
                        organizationId = c.Int(nullable: false),
                        organization = c.String(),
                        line1 = c.String(nullable: false),
                        line2 = c.String(),
                        city = c.String(),
                        zipCode = c.String(),
                        defaultAddress = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.AddressId)
                .ForeignKey("dbo.Organizations", t => t.organizationId)
                .Index(t => t.organizationId);
            
            CreateTable(
                "dbo.Organizations",
                c => new
                    {
                        organizationId = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                        organizationTypeId = c.Int(nullable: false),
                        organizationType = c.String(),
                        ParentCompany = c.String(),
                        ParentCompanyOCN = c.String(),
                        OCN = c.String(),
                        OFN = c.String(),
                        createdDate = c.DateTime(),
                        dateChanged = c.DateTime(),
                        organizationStateId = c.Int(nullable: false),
                        state = c.String(),
                    })
                .PrimaryKey(t => t.organizationId)
                .ForeignKey("dbo.OrganizationTypes", t => t.organizationTypeId)
                .ForeignKey("dbo.OrganizationStates", t => t.organizationStateId)
                .Index(t => t.organizationTypeId)
                .Index(t => t.organizationStateId);
            
            CreateTable(
                "dbo.Applications",
                c => new
                    {
                        applicationId = c.Int(nullable: false, identity: true),
                        organizationId = c.Int(nullable: false),
                        application1 = c.String(),
                        applicationReference = c.String(nullable: false),
                        applicant = c.String(),
                        recipient = c.String(),
                        UserId = c.String(nullable: false, maxLength: 128),
                        user = c.String(),
                        serviceId = c.Int(nullable: false),
                        service = c.String(),
                        resourceTypeId = c.Int(nullable: false),
                        resourceType = c.String(),
                        resourceSubTypeId = c.Int(nullable: false),
                        resourceSubType = c.String(),
                        subscriber = c.String(),
                        customerContactNumber = c.String(),
                        localRoutingNumber = c.String(),
                        FileName = c.String(maxLength: 255),
                        ContentType = c.String(maxLength: 100),
                        additionalDocumentation = c.Binary(),
                        createdDate = c.DateTime(),
                        dateChanged = c.DateTime(),
                        applicationResourceId = c.Int(nullable: false),
                        applicationTypeId = c.Int(nullable: false),
                        applicationStateId = c.Int(nullable: false),
                        state = c.String(),
                    })
                .PrimaryKey(t => t.applicationId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .ForeignKey("dbo.ResourceSubTypes", t => t.resourceSubTypeId)
                .ForeignKey("dbo.ResourceTypes", t => t.resourceTypeId)
                .ForeignKey("dbo.ApplicationStates", t => t.applicationStateId)
                .ForeignKey("dbo.ApplicationTypes", t => t.applicationTypeId)
                .ForeignKey("dbo.Organizations", t => t.organizationId)
                .Index(t => t.organizationId)
                .Index(t => t.UserId)
                .Index(t => t.resourceTypeId)
                .Index(t => t.resourceSubTypeId)
                .Index(t => t.applicationTypeId)
                .Index(t => t.applicationStateId);
            
            CreateTable(
                "dbo.ApplicationNotes",
                c => new
                    {
                        applicationNotesId = c.Int(nullable: false, identity: true),
                        applicationId = c.Int(nullable: false),
                        application = c.String(),
                        note = c.String(nullable: false),
                        UserId = c.String(nullable: false),
                        user = c.String(),
                        internalNote = c.String(),
                        createdDate = c.DateTime(),
                        dateChanged = c.DateTime(),
                    })
                .PrimaryKey(t => t.applicationNotesId)
                .ForeignKey("dbo.Applications", t => t.applicationId)
                .Index(t => t.applicationId);
            
            CreateTable(
                "dbo.ApplicationResources",
                c => new
                    {
                        applicationResourceId = c.Int(nullable: false, identity: true),
                        applicationId = c.Int(nullable: true),
                        application = c.String(),
                        resourceId = c.Int(nullable: true),
                        resource = c.String(),
                        createdDate = c.DateTime(),
                        dateChanged = c.DateTime(),
                        applicationResourceStateId = c.Int(nullable: true),
                        state = c.String(),
                        Organization_organizationId = c.Int(nullable: true),
                    })
                .PrimaryKey(t => t.applicationResourceId)
                .Index(t => t.Organization_organizationId);
            
            CreateTable(
                "dbo.ApplicationResourceStates",
                c => new
                    {
                        applicationResourceStateId = c.Int(nullable: false, identity: true),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.applicationResourceStateId);

            CreateTable(
                "dbo.Resources",
                c => new
                {
                    resourceId = c.Int(nullable: false, identity: true),
                    resource1 = c.String(nullable: true),
                    resourceTypeId = c.Int(nullable: true),
                    type = c.String(),
                    resourceSubTypeId = c.Int(nullable: true),
                    subtype = c.String(),
                    binary = c.String(),
                    createdDate = c.DateTime(),
                    dateChanged = c.DateTime(),
                    resourceLinkId = c.Int(nullable: true),
                    resourceStateId = c.Int(nullable: true),
                    state = c.String(),

                })
                .PrimaryKey(t => t.resourceId);
               
            
            CreateTable(
                "dbo.ResourceLinks",
                c => new
                    {
                        resourceLinkId = c.Int(nullable: false, identity: true),
                        parentResource = c.String(),
                        childResource = c.String(),
                        linkedResourceStateId = c.Int(nullable: true),
                        state = c.String(),
                        
                    })
                .PrimaryKey(t => t.resourceLinkId);
            
            CreateTable(
                "dbo.LinkedResourceStates",
                c => new
                    {
                        linkedResourceStateId = c.Int(nullable: false, identity: true),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.linkedResourceStateId);
            
            CreateTable(
                "dbo.NotificationDetails",
                c => new
                    {
                        notificationDetailId = c.Long(nullable: false, identity: true),
                        notificationId = c.Int(nullable: false),
                        notification = c.String(),
                        userTypeId = c.Int(nullable: false),
                        userType = c.String(),
                        resourceTypeId = c.Int(nullable: false),
                        resourceType = c.String(),
                        resourceSubTypeId = c.Int(nullable: false),
                        resourceSubType = c.String(),
                        linkedResourceStateId = c.Int(nullable: false),
                        state = c.String(),
                    })
                .PrimaryKey(t => t.notificationDetailId)
                .ForeignKey("dbo.LinkedResourceStates", t => t.linkedResourceStateId)
                .ForeignKey("dbo.Notifications", t => t.notificationId)
                .ForeignKey("dbo.UserTypes", t => t.userTypeId)
                .ForeignKey("dbo.ResourceSubTypes", t => t.resourceSubTypeId)
                .ForeignKey("dbo.ResourceTypes", t => t.resourceTypeId)
                .Index(t => t.notificationId)
                .Index(t => t.userTypeId)
                .Index(t => t.resourceTypeId)
                .Index(t => t.resourceSubTypeId)
                .Index(t => t.linkedResourceStateId);
            
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        notificationId = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                        mandatory = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.notificationId);
            
            CreateTable(
                "dbo.UserNotifications",
                c => new
                    {
                        userNotificationId = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        user = c.String(),
                        notificationId = c.Int(nullable: false),
                        notification = c.String(),
                        assigned = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.userNotificationId)
                .ForeignKey("dbo.Notifications", t => t.notificationId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.notificationId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        name = c.String(nullable: false),
                        userNo = c.Int(nullable: false, identity: true),
                        organizationId = c.Int(nullable: false),
                        organization = c.String(),
                        userTypeId = c.Int(nullable: false),
                        userType = c.String(),
                        userProfileId = c.Int(nullable: false),
                        userProfile = c.String(),
                        primaryContact = c.Boolean(nullable: false),
                        securityQuestion = c.String(nullable: false),
                        securityAnswer = c.String(nullable: false),
                        createdDate = c.DateTime(),
                        dateChanged = c.DateTime(),
                        lastConnectionDate = c.DateTime(),
                        userStateId = c.Int(nullable: false),
                        state = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Organizations", t => t.organizationId)
                .ForeignKey("dbo.UserStates", t => t.userStateId)
                .ForeignKey("dbo.UserTypes", t => t.userTypeId)
                .Index(t => t.organizationId)
                .Index(t => t.userTypeId)
                .Index(t => t.userStateId)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.UserClaims",
                c => new
                    {
                        UserClaimId = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.UserClaimId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.Users", t => t.UserId)
                .ForeignKey("dbo.Roles", t => t.RoleId)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.UserStates",
                c => new
                    {
                        userStateId = c.Int(nullable: false, identity: true),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.userStateId);
            
            CreateTable(
                "dbo.UserTypes",
                c => new
                    {
                        userTypeId = c.Int(nullable: false, identity: true),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.userTypeId);
            
            CreateTable(
                "dbo.OrganizationUserTypes",
                c => new
                    {
                        organizationUserTypeId = c.Int(nullable: false, identity: true),
                        organizationTypeId = c.Int(nullable: false),
                        organizationType = c.String(),
                        userTypeId = c.Int(nullable: false),
                        userType = c.String(),
                    })
                .PrimaryKey(t => t.organizationUserTypeId)
                .ForeignKey("dbo.OrganizationTypes", t => t.organizationTypeId)
                .ForeignKey("dbo.UserTypes", t => t.userTypeId)
                .Index(t => t.organizationTypeId)
                .Index(t => t.userTypeId);
            
            CreateTable(
                "dbo.OrganizationTypes",
                c => new
                    {
                        organizationTypeId = c.Int(nullable: false, identity: true),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.organizationTypeId);
            
            CreateTable(
                "dbo.UserProfileDetails",
                c => new
                    {
                        userProfileDetailId = c.Int(nullable: false, identity: true),
                        userProfileId = c.Int(nullable: false),
                        userProfile = c.String(),
                        serviceId = c.Int(nullable: false),
                        service = c.String(),
                        userTypeId = c.Int(nullable: false),
                        userType = c.String(),
                        assigned = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.userProfileDetailId)
                .ForeignKey("dbo.Services", t => t.serviceId)
                .ForeignKey("dbo.UserProfiles", t => t.userProfileId)
                .ForeignKey("dbo.UserTypes", t => t.userTypeId)
                .Index(t => t.userProfileId)
                .Index(t => t.serviceId)
                .Index(t => t.userTypeId);
            
            CreateTable(
                "dbo.Services",
                c => new
                    {
                        serviceId = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                        applicationYN = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.serviceId);
            
            CreateTable(
                "dbo.ServiceProfiles",
                c => new
                    {
                        serviceProfileId = c.Int(nullable: false, identity: true),
                        serviceId = c.Int(nullable: false),
                        service = c.String(),
                        resourceTypeId = c.Int(nullable: false),
                        resourceType = c.String(),
                        resourceSubTypeId = c.Int(nullable: false),
                        resourceSubType = c.String(),
                        providerServiceId = c.Int(nullable: false),
                        providerService = c.String(),
                        assigned = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.serviceProfileId)
                .ForeignKey("dbo.ProviderServices", t => t.providerServiceId)
                .ForeignKey("dbo.ResourceTypes", t => t.resourceTypeId)
                .ForeignKey("dbo.ResourceSubTypes", t => t.resourceSubTypeId)
                .ForeignKey("dbo.Services", t => t.serviceId)
                .Index(t => t.serviceId)
                .Index(t => t.resourceTypeId)
                .Index(t => t.resourceSubTypeId)
                .Index(t => t.providerServiceId);
            
            CreateTable(
                "dbo.ProviderServices",
                c => new
                    {
                        ProviderServiceId = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ProviderServiceId);
            
            CreateTable(
                "dbo.NrufCompanyInfoes",
                c => new
                    {
                        NrufCompanyInfoId = c.Int(nullable: false, identity: true),
                        organizationId = c.Int(nullable: false),
                        UserId = c.String(),
                        applicationReference = c.String(),
                        ProviderServiceId = c.Int(nullable: false),
                        name = c.String(),
                        ParentCompany = c.String(),
                        ParentCompanyOCN = c.String(),
                        OCN = c.String(),
                        OFN = c.String(),
                        line1 = c.String(),
                        line2 = c.String(),
                        line3 = c.String(),
                        ContactName = c.String(),
                        ContactTelNo = c.String(),
                        ContactFaxNo = c.String(),
                        ContactEmail = c.String(),
                        Status = c.String(),
                        expiryDate = c.DateTime(),
                        createdDate = c.DateTime(),
                        dateChanged = c.DateTime(),
                    })
                .PrimaryKey(t => t.NrufCompanyInfoId)
                .ForeignKey("dbo.ProviderServices", t => t.ProviderServiceId)
                .Index(t => t.ProviderServiceId);
            
            CreateTable(
                "dbo.NrufFormF3a",
                c => new
                    {
                        FormF3aId = c.Int(nullable: false, identity: true),
                        NrufCompanyInfoId = c.Int(nullable: false),
                        Npa = c.String(),
                        Year1 = c.Int(nullable: false),
                        Year2 = c.Int(nullable: false),
                        Year3 = c.Int(nullable: false),
                        Year4 = c.Int(nullable: false),
                        Year5 = c.Int(nullable: false),
                        Total = c.Int(nullable: false),
                        createdDate = c.DateTime(),
                        dateChanged = c.DateTime(),
                    })
                .PrimaryKey(t => t.FormF3aId)
                .ForeignKey("dbo.NrufCompanyInfoes", t => t.NrufCompanyInfoId)
                .Index(t => t.NrufCompanyInfoId);
            
            CreateTable(
                "dbo.NrufFormF3b",
                c => new
                    {
                        FormF3bId = c.Int(nullable: false, identity: true),
                        NrufCompanyInfoId = c.Int(nullable: false),
                        Npa = c.String(),
                        Year1 = c.Int(nullable: false),
                        Year2 = c.Int(nullable: false),
                        Year3 = c.Int(nullable: false),
                        Year4 = c.Int(nullable: false),
                        Year5 = c.Int(nullable: false),
                        Total = c.Int(nullable: false),
                        createdDate = c.DateTime(),
                        dateChanged = c.DateTime(),
                    })
                .PrimaryKey(t => t.FormF3bId)
                .ForeignKey("dbo.NrufCompanyInfoes", t => t.NrufCompanyInfoId)
                .Index(t => t.NrufCompanyInfoId);
            
            CreateTable(
                "dbo.NrufFormU1",
                c => new
                    {
                        FormU1Id = c.Int(nullable: false, identity: true),
                        NrufCompanyInfoId = c.Int(nullable: false),
                        Npa = c.String(),
                        Nxx = c.String(),
                        X = c.Int(nullable: false),
                        Rate = c.String(),
                        Assigned = c.Int(nullable: false),
                        Intermediate = c.Int(nullable: false),
                        Reserved = c.Int(nullable: false),
                        Aging = c.Int(nullable: false),
                        Admin = c.Int(nullable: false),
                        PortedOut = c.Int(nullable: false),
                        Notes = c.String(),
                        Available = c.Int(nullable: false),
                        Utilization = c.String(),
                        createdDate = c.DateTime(),
                        dateChanged = c.DateTime(),
                    })
                .PrimaryKey(t => t.FormU1Id)
                .ForeignKey("dbo.NrufCompanyInfoes", t => t.NrufCompanyInfoId)
                .Index(t => t.NrufCompanyInfoId);
            
            CreateTable(
                "dbo.NrufFormU2",
                c => new
                    {
                        FormU2Id = c.Int(nullable: false, identity: true),
                        NrufCompanyInfoId = c.Int(nullable: false),
                        Npa = c.String(),
                        Nxx = c.String(),
                        Rate = c.String(),
                        Assigned = c.Int(nullable: false),
                        Intermediate = c.Int(nullable: false),
                        Reserved = c.Int(nullable: false),
                        Aging = c.Int(nullable: false),
                        Admin = c.Int(nullable: false),
                        PortedOut = c.Int(nullable: false),
                        Notes = c.String(),
                        Available = c.Int(nullable: false),
                        Utilization = c.String(),
                        createdDate = c.DateTime(),
                        dateChanged = c.DateTime(),
                    })
                .PrimaryKey(t => t.FormU2Id)
                .ForeignKey("dbo.NrufCompanyInfoes", t => t.NrufCompanyInfoId)
                .Index(t => t.NrufCompanyInfoId);
            
            CreateTable(
                "dbo.NrufFormU3",
                c => new
                    {
                        FormU3Id = c.Int(nullable: false, identity: true),
                        NrufCompanyInfoId = c.Int(nullable: false),
                        Npa = c.String(),
                        Nxx = c.String(),
                        X = c.Int(nullable: false),
                        Rate = c.String(),
                        Assigned = c.Int(nullable: false),
                        NumbersReceived = c.Int(nullable: false),
                        Reserved = c.Int(nullable: false),
                        Aging = c.Int(nullable: false),
                        Admin = c.Int(nullable: false),
                        PortedOut = c.Int(nullable: false),
                        Notes = c.String(),
                        Available = c.Int(nullable: false),
                        Utilization = c.String(),
                        createdDate = c.DateTime(),
                        dateChanged = c.DateTime(),
                    })
                .PrimaryKey(t => t.FormU3Id)
                .ForeignKey("dbo.NrufCompanyInfoes", t => t.NrufCompanyInfoId)
                .Index(t => t.NrufCompanyInfoId);
            
            CreateTable(
                "dbo.NrufFormU4",
                c => new
                    {
                        FormU4Id = c.Int(nullable: false, identity: true),
                        NrufCompanyInfoId = c.Int(nullable: false),
                        Npa = c.String(),
                        Nxx = c.String(),
                        Rate = c.String(),
                        Assigned = c.Int(nullable: false),
                        Aging = c.Int(nullable: false),
                        PortedIn = c.Int(nullable: false),
                        Notes = c.String(),
                        Available = c.Int(nullable: false),
                        Utilization = c.String(),
                        createdDate = c.DateTime(),
                        dateChanged = c.DateTime(),
                    })
                .PrimaryKey(t => t.FormU4Id)
                .ForeignKey("dbo.NrufCompanyInfoes", t => t.NrufCompanyInfoId)
                .Index(t => t.NrufCompanyInfoId);
            
            CreateTable(
                "dbo.OrganizationServices",
                c => new
                    {
                        organizationServicesId = c.Int(nullable: false, identity: true),
                        AddressId = c.Int(nullable: false),
                        ProviderServiceId = c.Int(nullable: false),
                        organization = c.String(),
                        providerService = c.String(),
                        createdDate = c.DateTime(),
                        dateChanged = c.DateTime(),
                    })
                .PrimaryKey(t => t.organizationServicesId)
                .ForeignKey("dbo.Addresses", t => t.AddressId)
                .ForeignKey("dbo.ProviderServices", t => t.ProviderServiceId)
                .Index(t => t.AddressId)
                .Index(t => t.ProviderServiceId);
            
            CreateTable(
                "dbo.ResourceSubTypes",
                c => new
                    {
                        resourceSubTypeId = c.Int(nullable: false, identity: true),
                        resourceTypeId = c.Int(nullable: false),
                        name = c.String(nullable: false),
                        subType = c.String(),
                        format = c.String(nullable: false),
                        formatRegex = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.resourceSubTypeId)
                .ForeignKey("dbo.ResourceTypes", t => t.resourceTypeId)
                .Index(t => t.resourceTypeId);
            
            CreateTable(
                "dbo.OrganizationResourceTypes",
                c => new
                    {
                        organizationResourceTypeId = c.Int(nullable: false, identity: true),
                        organizationId = c.Int(nullable: false),
                        organization = c.String(),
                        resourceTypeId = c.Int(nullable: false),
                        resourceType = c.String(),
                        resourceSubTypeId = c.Int(nullable: false),
                        resourceSubType = c.String(),
                        linkedResourceStateId = c.Int(nullable: false),
                        state = c.String(),
                    })
                .PrimaryKey(t => t.organizationResourceTypeId)
                .ForeignKey("dbo.LinkedResourceStates", t => t.linkedResourceStateId)
                .ForeignKey("dbo.Organizations", t => t.organizationId)
                .ForeignKey("dbo.ResourceSubTypes", t => t.resourceSubTypeId)
                .ForeignKey("dbo.ResourceTypes", t => t.resourceTypeId)
                .Index(t => t.organizationId)
                .Index(t => t.resourceTypeId)
                .Index(t => t.resourceSubTypeId)
                .Index(t => t.linkedResourceStateId);
            
            CreateTable(
                "dbo.ResourceTypes",
                c => new
                    {
                        resourceTypeId = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                        format = c.String(nullable: false),
                        formatRegex = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.resourceTypeId);
            
            CreateTable(
                "dbo.UserProfiles",
                c => new
                    {
                        userProfileId = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                        defaultProfile = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.userProfileId);
            
            CreateTable(
                "dbo.ResourceStates",
                c => new
                    {
                        resourceStateId = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.resourceStateId);
            
            CreateTable(
                "dbo.ApplicationStates",
                c => new
                    {
                        applicationStateId = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.applicationStateId);
            
            CreateTable(
                "dbo.ApplicationTypes",
                c => new
                    {
                        applicationTypeId = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                        abbreviation = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.applicationTypeId);
            
            CreateTable(
                "dbo.OrganizationStates",
                c => new
                    {
                        organizationStateId = c.Int(nullable: false, identity: true),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.organizationStateId);
            
            CreateTable(
                "dbo.CentralOfficeCodeAssignments",
                c => new
                    {
                        CentralOfficeCodeAssignmentsId = c.Int(nullable: false, identity: true),
                        organizationId = c.Int(nullable: false),
                        NPA = c.String(),
                        NXX = c.String(),
                        DateCreated = c.DateTime(),
                        DateChanged = c.DateTime(),
                    })
                .PrimaryKey(t => t.CentralOfficeCodeAssignmentsId)
                .ForeignKey("dbo.Organizations", t => t.organizationId)
                .Index(t => t.organizationId);
            
            CreateTable(
                "dbo.Mails",
                c => new
                    {
                        mailId = c.Long(nullable: false, identity: true),
                        title = c.String(nullable: false),
                        toAddress = c.String(nullable: false),
                        fromAddress = c.String(nullable: false),
                        message = c.String(nullable: false),
                        attachment = c.Binary(),
                        createdDate = c.DateTime(),
                        dateChanged = c.DateTime(),
                    })
                .PrimaryKey(t => t.mailId);
            
            CreateTable(
                "dbo.Reports",
                c => new
                    {
                        ReportId = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ReportId);
            
            CreateTable(
                "dbo.ReportOnNXXAndNPACodes",
                c => new
                    {
                        ReportOnNXXAndNPACodesId = c.Int(nullable: false, identity: true),
                        NPA = c.Int(nullable: false),
                        NX = c.Int(nullable: false),
                        ServiceAllocation = c.String(),
                        X_0 = c.String(),
                        X_0_CellStyle = c.String(),
                        X_1 = c.String(),
                        X_1_CellStyle = c.String(),
                        X_2 = c.String(),
                        X_2_CellStyle = c.String(),
                        X_3 = c.String(),
                        X_3_CellStyle = c.String(),
                        X_4 = c.String(),
                        X_4_CellStyle = c.String(),
                        X_5 = c.String(),
                        X_5_CellStyle = c.String(),
                        X_6 = c.String(),
                        X_6_CellStyle = c.String(),
                        X_7 = c.String(),
                        X_7_CellStyle = c.String(),
                        X_8 = c.String(),
                        X_8_CellStyle = c.String(),
                        X_9 = c.String(),
                        X_9_CellStyle = c.String(),
                        DateCreated = c.DateTime(),
                        DateChanged = c.DateTime(),
                    })
                .PrimaryKey(t => t.ReportOnNXXAndNPACodesId);
            
            CreateTable(
                "dbo.ReportOnNXXAndNPACodesKeys",
                c => new
                    {
                        ReportOnNXXAndNPACodesKeyId = c.Int(nullable: false, identity: true),
                        Key = c.String(),
                        CellText = c.String(),
                        CellStyle = c.String(),
                        DateCreated = c.DateTime(),
                        DateChanged = c.DateTime(),
                    })
                .PrimaryKey(t => t.ReportOnNXXAndNPACodesKeyId);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        RoleId = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.RoleId)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.SystemMessages",
                c => new
                    {
                        systemMessageId = c.Long(nullable: false, identity: true),
                        title = c.String(nullable: false),
                        message = c.String(nullable: false),
                        expiryDate = c.DateTime(),
                        createdDate = c.DateTime(),
                        dateChanged = c.DateTime(),
                    })
                .PrimaryKey(t => t.systemMessageId);
            
            CreateTable(
                "dbo.SystemParameters",
                c => new
                    {
                        systemParameterId = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                        value = c.String(nullable: false),
                        UserId = c.String(),
                        createdDate = c.DateTime(),
                        dateChanged = c.DateTime(),
                    })
                .PrimaryKey(t => t.systemParameterId);
            
            CreateTable(
                "dbo.UserResourceTypes",
                c => new
                    {
                        userResourceTypeId = c.Int(nullable: false, identity: true),
                        userTypeId = c.Int(nullable: false),
                        userType = c.String(),
                        resourceTypeId = c.Int(nullable: false),
                        resourceType = c.String(),
                        resourceSubTypeId = c.Int(nullable: false),
                        resourceSubType = c.String(),
                        linkedResourceStateId = c.Int(nullable: false),
                        state = c.String(),
                    })
                .PrimaryKey(t => t.userResourceTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserRoles", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.CentralOfficeCodeAssignments", "organizationId", "dbo.Organizations");
            DropForeignKey("dbo.Organizations", "organizationStateId", "dbo.OrganizationStates");
            DropForeignKey("dbo.ApplicationResources", "Organization_organizationId", "dbo.Organizations");
            DropForeignKey("dbo.Applications", "organizationId", "dbo.Organizations");
            DropForeignKey("dbo.Applications", "applicationTypeId", "dbo.ApplicationTypes");
            DropForeignKey("dbo.Applications", "applicationStateId", "dbo.ApplicationStates");
            DropForeignKey("dbo.ApplicationResources", "Application_applicationId1", "dbo.Applications");
            DropForeignKey("dbo.ApplicationResources", "Resource_resourceId1", "dbo.Resources");
            DropForeignKey("dbo.Resources", "ResourceState_resourceStateId1", "dbo.ResourceStates");
            DropForeignKey("dbo.Resources", "ResourceState_resourceStateId", "dbo.ResourceStates");
            DropForeignKey("dbo.ResourceLinks", "Resource_resourceId1", "dbo.Resources");
            DropForeignKey("dbo.ResourceLinks", "Resource_resourceId", "dbo.Resources");
            DropForeignKey("dbo.ResourceLinks", "LinkedResourceState_linkedResourceStateId1", "dbo.LinkedResourceStates");
            DropForeignKey("dbo.ResourceLinks", "LinkedResourceState_linkedResourceStateId", "dbo.LinkedResourceStates");
            DropForeignKey("dbo.UserProfileDetails", "userTypeId", "dbo.UserTypes");
            DropForeignKey("dbo.UserProfileDetails", "userProfileId", "dbo.UserProfiles");
            DropForeignKey("dbo.UserProfileDetails", "serviceId", "dbo.Services");
            DropForeignKey("dbo.ServiceProfiles", "serviceId", "dbo.Services");
            DropForeignKey("dbo.ServiceProfiles", "resourceSubTypeId", "dbo.ResourceSubTypes");
            DropForeignKey("dbo.ServiceProfiles", "resourceTypeId", "dbo.ResourceTypes");
            DropForeignKey("dbo.ResourceSubTypes", "resourceTypeId", "dbo.ResourceTypes");
            DropForeignKey("dbo.OrganizationResourceTypes", "resourceTypeId", "dbo.ResourceTypes");
            DropForeignKey("dbo.NotificationDetails", "resourceTypeId", "dbo.ResourceTypes");
            DropForeignKey("dbo.Applications", "resourceTypeId", "dbo.ResourceTypes");
            DropForeignKey("dbo.OrganizationResourceTypes", "resourceSubTypeId", "dbo.ResourceSubTypes");
            DropForeignKey("dbo.OrganizationResourceTypes", "organizationId", "dbo.Organizations");
            DropForeignKey("dbo.OrganizationResourceTypes", "linkedResourceStateId", "dbo.LinkedResourceStates");
            DropForeignKey("dbo.NotificationDetails", "resourceSubTypeId", "dbo.ResourceSubTypes");
            DropForeignKey("dbo.Applications", "resourceSubTypeId", "dbo.ResourceSubTypes");
            DropForeignKey("dbo.ServiceProfiles", "providerServiceId", "dbo.ProviderServices");
            DropForeignKey("dbo.OrganizationServices", "ProviderServiceId", "dbo.ProviderServices");
            DropForeignKey("dbo.OrganizationServices", "AddressId", "dbo.Addresses");
            DropForeignKey("dbo.NrufCompanyInfoes", "ProviderServiceId", "dbo.ProviderServices");
            DropForeignKey("dbo.NrufFormU4", "NrufCompanyInfoId", "dbo.NrufCompanyInfoes");
            DropForeignKey("dbo.NrufFormU3", "NrufCompanyInfoId", "dbo.NrufCompanyInfoes");
            DropForeignKey("dbo.NrufFormU2", "NrufCompanyInfoId", "dbo.NrufCompanyInfoes");
            DropForeignKey("dbo.NrufFormU1", "NrufCompanyInfoId", "dbo.NrufCompanyInfoes");
            DropForeignKey("dbo.NrufFormF3b", "NrufCompanyInfoId", "dbo.NrufCompanyInfoes");
            DropForeignKey("dbo.NrufFormF3a", "NrufCompanyInfoId", "dbo.NrufCompanyInfoes");
            DropForeignKey("dbo.Users", "userTypeId", "dbo.UserTypes");
            DropForeignKey("dbo.OrganizationUserTypes", "userTypeId", "dbo.UserTypes");
            DropForeignKey("dbo.OrganizationUserTypes", "organizationTypeId", "dbo.OrganizationTypes");
            DropForeignKey("dbo.Organizations", "organizationTypeId", "dbo.OrganizationTypes");
            DropForeignKey("dbo.NotificationDetails", "userTypeId", "dbo.UserTypes");
            DropForeignKey("dbo.Users", "userStateId", "dbo.UserStates");
            DropForeignKey("dbo.UserNotifications", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserRoles", "UserId", "dbo.Users");
            DropForeignKey("dbo.Users", "organizationId", "dbo.Organizations");
            DropForeignKey("dbo.UserLogins", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserClaims", "UserId", "dbo.Users");
            DropForeignKey("dbo.Applications", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserNotifications", "notificationId", "dbo.Notifications");
            DropForeignKey("dbo.NotificationDetails", "notificationId", "dbo.Notifications");
            DropForeignKey("dbo.NotificationDetails", "linkedResourceStateId", "dbo.LinkedResourceStates");
            DropForeignKey("dbo.ApplicationResources", "Resource_resourceId", "dbo.Resources");
            DropForeignKey("dbo.ApplicationResources", "ApplicationResourceState_applicationResourceStateId1", "dbo.ApplicationResourceStates");
            DropForeignKey("dbo.ApplicationResources", "ApplicationResourceState_applicationResourceStateId", "dbo.ApplicationResourceStates");
            DropForeignKey("dbo.ApplicationResources", "Application_applicationId", "dbo.Applications");
            DropForeignKey("dbo.ApplicationNotes", "applicationId", "dbo.Applications");
            DropForeignKey("dbo.Addresses", "organizationId", "dbo.Organizations");
            DropIndex("dbo.Roles", "RoleNameIndex");
            DropIndex("dbo.CentralOfficeCodeAssignments", new[] { "organizationId" });
            DropIndex("dbo.OrganizationResourceTypes", new[] { "linkedResourceStateId" });
            DropIndex("dbo.OrganizationResourceTypes", new[] { "resourceSubTypeId" });
            DropIndex("dbo.OrganizationResourceTypes", new[] { "resourceTypeId" });
            DropIndex("dbo.OrganizationResourceTypes", new[] { "organizationId" });
            DropIndex("dbo.ResourceSubTypes", new[] { "resourceTypeId" });
            DropIndex("dbo.OrganizationServices", new[] { "ProviderServiceId" });
            DropIndex("dbo.OrganizationServices", new[] { "AddressId" });
            DropIndex("dbo.NrufFormU4", new[] { "NrufCompanyInfoId" });
            DropIndex("dbo.NrufFormU3", new[] { "NrufCompanyInfoId" });
            DropIndex("dbo.NrufFormU2", new[] { "NrufCompanyInfoId" });
            DropIndex("dbo.NrufFormU1", new[] { "NrufCompanyInfoId" });
            DropIndex("dbo.NrufFormF3b", new[] { "NrufCompanyInfoId" });
            DropIndex("dbo.NrufFormF3a", new[] { "NrufCompanyInfoId" });
            DropIndex("dbo.NrufCompanyInfoes", new[] { "ProviderServiceId" });
            DropIndex("dbo.ServiceProfiles", new[] { "providerServiceId" });
            DropIndex("dbo.ServiceProfiles", new[] { "resourceSubTypeId" });
            DropIndex("dbo.ServiceProfiles", new[] { "resourceTypeId" });
            DropIndex("dbo.ServiceProfiles", new[] { "serviceId" });
            DropIndex("dbo.UserProfileDetails", new[] { "userTypeId" });
            DropIndex("dbo.UserProfileDetails", new[] { "serviceId" });
            DropIndex("dbo.UserProfileDetails", new[] { "userProfileId" });
            DropIndex("dbo.OrganizationUserTypes", new[] { "userTypeId" });
            DropIndex("dbo.OrganizationUserTypes", new[] { "organizationTypeId" });
            DropIndex("dbo.UserRoles", new[] { "RoleId" });
            DropIndex("dbo.UserRoles", new[] { "UserId" });
            DropIndex("dbo.UserLogins", new[] { "UserId" });
            DropIndex("dbo.UserClaims", new[] { "UserId" });
            DropIndex("dbo.Users", "UserNameIndex");
            DropIndex("dbo.Users", new[] { "userStateId" });
            DropIndex("dbo.Users", new[] { "userTypeId" });
            DropIndex("dbo.Users", new[] { "organizationId" });
            DropIndex("dbo.UserNotifications", new[] { "notificationId" });
            DropIndex("dbo.UserNotifications", new[] { "UserId" });
            DropIndex("dbo.NotificationDetails", new[] { "linkedResourceStateId" });
            DropIndex("dbo.NotificationDetails", new[] { "resourceSubTypeId" });
            DropIndex("dbo.NotificationDetails", new[] { "resourceTypeId" });
            DropIndex("dbo.NotificationDetails", new[] { "userTypeId" });
            DropIndex("dbo.NotificationDetails", new[] { "notificationId" });
            DropIndex("dbo.ResourceLinks", new[] { "Resource_resourceId1" });
            DropIndex("dbo.ResourceLinks", new[] { "Resource_resourceId" });
            DropIndex("dbo.ResourceLinks", new[] { "LinkedResourceState_linkedResourceStateId1" });
            DropIndex("dbo.ResourceLinks", new[] { "LinkedResourceState_linkedResourceStateId" });
            DropIndex("dbo.Resources", new[] { "ResourceState_resourceStateId1" });
            DropIndex("dbo.Resources", new[] { "ResourceState_resourceStateId" });
            DropIndex("dbo.ApplicationResources", new[] { "Organization_organizationId" });
            DropIndex("dbo.ApplicationResources", new[] { "Application_applicationId1" });
            DropIndex("dbo.ApplicationResources", new[] { "Resource_resourceId1" });
            DropIndex("dbo.ApplicationResources", new[] { "Resource_resourceId" });
            DropIndex("dbo.ApplicationResources", new[] { "ApplicationResourceState_applicationResourceStateId1" });
            DropIndex("dbo.ApplicationResources", new[] { "ApplicationResourceState_applicationResourceStateId" });
            DropIndex("dbo.ApplicationResources", new[] { "Application_applicationId" });
            DropIndex("dbo.ApplicationNotes", new[] { "applicationId" });
            DropIndex("dbo.Applications", new[] { "applicationStateId" });
            DropIndex("dbo.Applications", new[] { "applicationTypeId" });
            DropIndex("dbo.Applications", new[] { "resourceSubTypeId" });
            DropIndex("dbo.Applications", new[] { "resourceTypeId" });
            DropIndex("dbo.Applications", new[] { "UserId" });
            DropIndex("dbo.Applications", new[] { "organizationId" });
            DropIndex("dbo.Organizations", new[] { "organizationStateId" });
            DropIndex("dbo.Organizations", new[] { "organizationTypeId" });
            DropIndex("dbo.Addresses", new[] { "organizationId" });
            DropTable("dbo.UserResourceTypes");
            DropTable("dbo.SystemParameters");
            DropTable("dbo.SystemMessages");
            DropTable("dbo.Roles");
            DropTable("dbo.ReportOnNXXAndNPACodesKeys");
            DropTable("dbo.ReportOnNXXAndNPACodes");
            DropTable("dbo.Reports");
            DropTable("dbo.Mails");
            DropTable("dbo.CentralOfficeCodeAssignments");
            DropTable("dbo.OrganizationStates");
            DropTable("dbo.ApplicationTypes");
            DropTable("dbo.ApplicationStates");
            DropTable("dbo.ResourceStates");
            DropTable("dbo.UserProfiles");
            DropTable("dbo.ResourceTypes");
            DropTable("dbo.OrganizationResourceTypes");
            DropTable("dbo.ResourceSubTypes");
            DropTable("dbo.OrganizationServices");
            DropTable("dbo.NrufFormU4");
            DropTable("dbo.NrufFormU3");
            DropTable("dbo.NrufFormU2");
            DropTable("dbo.NrufFormU1");
            DropTable("dbo.NrufFormF3b");
            DropTable("dbo.NrufFormF3a");
            DropTable("dbo.NrufCompanyInfoes");
            DropTable("dbo.ProviderServices");
            DropTable("dbo.ServiceProfiles");
            DropTable("dbo.Services");
            DropTable("dbo.UserProfileDetails");
            DropTable("dbo.OrganizationTypes");
            DropTable("dbo.OrganizationUserTypes");
            DropTable("dbo.UserTypes");
            DropTable("dbo.UserStates");
            DropTable("dbo.UserRoles");
            DropTable("dbo.UserLogins");
            DropTable("dbo.UserClaims");
            DropTable("dbo.Users");
            DropTable("dbo.UserNotifications");
            DropTable("dbo.Notifications");
            DropTable("dbo.NotificationDetails");
            DropTable("dbo.LinkedResourceStates");
            DropTable("dbo.ResourceLinks");
            DropTable("dbo.Resources");
            DropTable("dbo.ApplicationResourceStates");
            DropTable("dbo.ApplicationResources");
            DropTable("dbo.ApplicationNotes");
            DropTable("dbo.Applications");
            DropTable("dbo.Organizations");
            DropTable("dbo.Addresses");
        }
    }
}
