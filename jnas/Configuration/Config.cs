﻿// TODO: Rem to use using in the dbcontext and transactions for db.
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace jnas.Configuration
{
    public static class Config
    {

        //    name=DefaultConnection "Data Source=.\SQLEXPRESS;AttachDbFilename=|DataDirectory|\aspnet-jnas-20160211092324.mdf;Initial Catalog=aspnet-jnas-20160211092324;Integrated Security=True;User Instance=True"
        //name=DefaultConnection1 connectionString="Server=.\SQLEXPRESS;Database=jnasdb2;Integrated Security=True;MultipleActiveResultSets=true;"
        //name=DefaultConnection2 "Server=.\SQL2012;Database=jnasdb;Integrated Security=True;MultipleActiveResultSets=true;"
        //name=DefaultConnection3 "Server=.\SQL2012;Database=jnasdb;persist security info=True;user id=jnasuser;password=P@55w0rd;"
        //name=DefaultConnection4 "Server=.\SQLEXPRESS;Database=jnasdb;persist security info=True;user id=jnasuser;password=P@55w0rd;"
        //name=Jnasdb "data source=.\SQLEXPRESS;initial catalog=jnasdb2;integrated security=True;multipleactiveresultsets=True;App=EntityFramework"
        //  AppSettingsWrapper appSettings = new AppSettingsWrapper(); http://www.codeproject.com/Articles/203483/YouGrade-Asp-NET-MVC-Multimedia-Exam-Suite

        // http://stackoverflow.com/questions/4595288/reading-a-key-from-the-web-config-using-configurationmanager

        //Server=.\SQLEXPRESS;Database=jnasdb2;Integrated Security = True; MultipleActiveResultSets=true;
        //Server=.\SQL2012;Database=jnasdb;persist security info=True;user id=jnasuser;password=P@55w0rd;
        //Server=database-svr;Database=jnasdb2;persist security info=True;user id=jnasuser;password=P@55w0rd;

       // private static string conStr = @"Server=database-svr;Database=jnasdb2;persist security info=True;user id=jnasuser;password=P@55w0rd;";

        private static string conStr = @"Server=.\SQL2012;Database=jnasdb;persist security info=True;user id=jnasuser;password=P@55w0rd;";
        //  private static string conStrWebConfig = WebConfigurationManager.AppSettings["DefaultConnection3"].ToString();


        private static string conStrName1 = @"name=DefaultConnection3";


        private static string conStrName2 = @"DefaultConnection3";


        public static string ConnectionString { get { return conStr; } set { conStr = value; } }

        //   public static string ConnectionStringWebConfig { get { return conStrWebConfig; } set { conStrWebConfig = value; } }

        public static string ConnectionStringName1 { get { return conStrName1; } set { conStrName1 = value; } }

        public static string ConnectionStringName2 { get { return conStrName2; } set { conStrName2 = value; } }
    }

    //public static class AppSettingsWrapper : DynamicObject
    //{
    //    private NameValueCollection _items;

    //    public AppSettingsWrapper()
    //    {
    //        _items = ConfigurationManager.AppSettings;
    //    }

    //    public override bool TryGetMember(GetMemberBinder binder, out object result)
    //    {
    //        result = _items[binder.Name];
    //        return result != null;
    //    }
    //}
}