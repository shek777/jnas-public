﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using Quartz.Impl;
using System.Net;
using System.Net.Mail;
using jnas.Models.Jnasdb;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using System.Data.SqlClient;
using jnas.Configuration;

namespace jnas.ScheduledTasks
{
    public class EmailJob : IJob
    {
        // Database 
        private UserManager _userManager;
        private Jnasdb db = new Jnasdb();
        private Jnasdb db1 = new Jnasdb();

        public static bool IsValidEmail(String email)
        {
            return !string.IsNullOrEmpty(email) && Regex.IsMatch(email, @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {

                //Loop all external users who are on the notiication list, and send them an email or send it to their organisation.
                var users = db.Users.Include(u => u.UserType1).Include(s => s.UserState);
                var usertypes = new string[] { "Primary External User", "Secondary External User", "Read-only User" };
                var us = users.Where(e => usertypes.Contains(e.UserType1.name)).ToList();
                var listOfEmails = us.Where(s => s.UserState.name.Equals("Validated")).ToList();

                var email = db.SystemParameters.Where(p => p.name.Equals("JNAS email")).FirstOrDefault();
                var password = db.SystemParameters.Where(p => p.name.Equals("JNAS email password")).FirstOrDefault();
                var host = db.SystemParameters.Where(p => p.name.Equals("JNAS email host")).FirstOrDefault();
                var port = db.SystemParameters.Where(p => p.name.Equals("JNAS email port")).FirstOrDefault();
                var subject = db.SystemParameters.Where(p => p.name.Equals("NRUF email subject")).FirstOrDefault();
                var body = db.SystemParameters.Where(p => p.name.Equals("NRUF email body")).FirstOrDefault();

                foreach (var item in listOfEmails)
                {

                    if (!IsValidEmail(item.Email))
                        continue;

                    using (var message = new MailMessage(email.value, item.Email))
                    {

                        // var htmlBody = @"<html><body><p> You have received mail.</p>";

                        //                    htmlBody = @"<h2 style='color: #5e9ca0;'>This is a gentle reminder to submit your <span style='color: #2b2301;'>N.R.U.F.</span> Report via the J.N.A.S. website</h2>
                        //<h3> INSTRUCTIONS </h3><h5><span style= 'font-weight: bold;'> When to Submit Reports:</span></h5><p style = 'color: #5e9ca0;'> Reporting carriers shall submit utililization and forecast reports semi-annually, on or before February 1 for the preceding 6 month reporting period ending December 31, and on or before August 1 for the preceding 6 month reporting period ending June 30. <span style = 'font-weight: bold;'> Reporting is mandatory </span>.</p>  <p style= 'color: #5e9ca0;'> Contact Us for Help at <abbr title='Telephone'> Tel:</abbr> 876.968.6053 or email us at <a href = 'mailto:consumer@our.org.jm' > consumer@our.org.jm </a></p><p style = 'color: #5e9ca0;'> &nbsp;</p><p style = 'color: #6E2C00;'> Thank you for your cooperation.</p>";

                        //                    htmlBody = htmlBody + @"</body></html>";
                        // message.Bcc
                        var htmlBody = body.value;


                        message.Subject = subject.value;
                        message.Body = htmlBody;
                        // message.Body = "Test at " + DateTime.Now;
                        var plainView = AlternateView.CreateAlternateViewFromString(Regex.Replace(htmlBody, @"<(.|\n)*?>", string.Empty), null, "text/plain");
                        var htmlView = AlternateView.CreateAlternateViewFromString(htmlBody, null, "text/html");

                        message.AlternateViews.Add(plainView);
                        message.AlternateViews.Add(htmlView);
                        using (SmtpClient client = new SmtpClient
                        {
                            EnableSsl = true,
                            Host = host.value,
                            Port = Convert.ToInt32(port.value),
                            Credentials = new NetworkCredential(email.value, password.value)
                        })
                        {
                            client.Send(message);
                        }
                    }

                } //loop ends

            }
            catch { }
        }
    }

    public class ReminderJob : IJob
    {
        // Database 
        private UserManager _userManager;
        private Jnasdb db = new Jnasdb();
        private Jnasdb db1 = new Jnasdb();

        public void Execute(IJobExecutionContext context)
        {

        }
    }

    public class changeAgedResourceJob : IJob
    {
        // Database 
        private UserManager _userManager;
        private Jnasdb db = new Jnasdb();
        private Jnasdb db1 = new Jnasdb();

        private int getAgedResourceState()
        {
            var rs = 7;

            var result = db.ResourceStates.Where(res => res.name == "Aging").Select(d => d.resourceStateId).FirstOrDefault();

            return result != 0 ? result : rs;

        }

        private int getBlockedResourceState()
        {
            var rs = 2;

            var result = db.ResourceStates.Where(res => res.name == "Blocked").Select(d => d.resourceStateId).FirstOrDefault();

            return result != 0 ? result : rs;

        }

        public void Execute(IJobExecutionContext context)
        {

            var time = DateTime.Now;

            var blocked = getBlockedResourceState();

            var AgingPeriod = db.SystemParameters.Where(p => p.name.Equals("Aging Period (days)")).Select(d => d.value).FirstOrDefault();

            var agedResources = db.Resources.Where(r => r.resourceStateId == getAgedResourceState()).ToList();

            foreach (var item in agedResources)
            {
                var expiredTime = item.dateChanged.Value.AddDays(Convert.ToDouble(AgingPeriod));

                if(expiredTime < time)
                {
                    item.resourceStateId = blocked;

                    db.SaveChanges();
                }
            }

        }
    }




    public class JobScheduler
    {
        // Database 
        private UserManager _userManager;
        private Jnasdb db = new Jnasdb();
        private Jnasdb db1 = new Jnasdb();

        //  public static void Start() {}
        public void Start()
        {

            var count = 0;
            var NRUFreminderCollection = db.SystemParameters.Where(r => r.name.Equals("NRUF Report reminder")).ToList();

            // get a scheduler
            ISchedulerFactory schedulerFactory = new StdSchedulerFactory();

            IScheduler scheduler = schedulerFactory.GetScheduler();
            scheduler.Start();

            //Loop starts
            foreach (var nruf in NRUFreminderCollection)
            {
                var myJob = @"myJob" + count;
                var trigger1 = @"trigger" + count;
                var cron = nruf.value;

                IJobDetail job = JobBuilder.Create<EmailJob>()
                                    .WithIdentity(myJob)
                                    .Build();

                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity(trigger1)
                    .WithCronSchedule(cron, x => x
                        .WithMisfireHandlingInstructionFireAndProceed())
                    .ForJob(myJob)
                    .Build();

                scheduler.ScheduleJob(job, trigger);

                count++;
            }


            IJobDetail job1 = JobBuilder.Create<EmailJob>()
                                  .WithIdentity("blockResourcesJob")
                                  .Build();

            ITrigger trigger2 = TriggerBuilder.Create()
                .WithIdentity("triggerblocked")
                .WithCronSchedule("0 0 0 1/1 * ? *", x => x
                    .WithMisfireHandlingInstructionFireAndProceed())
                .ForJob("blockResourcesJob")
                .Build();

            scheduler.ScheduleJob(job1, trigger2);

            //IJobDetail job = JobBuilder.Create<EmailJob>()
            //                  .WithIdentity("myJob")
            //                  .Build();

            //ITrigger trigger = TriggerBuilder.Create()
            //    .WithIdentity("trigger1")
            //    .WithCronSchedule("0 0/1 * 1/1 * ? *", x => x
            //        .WithMisfireHandlingInstructionFireAndProceed())
            //    .ForJob("myJob")
            //    .Build();

            //scheduler.ScheduleJob(job, trigger);
            //Loop ends - same Job but different triggers from system parameters db | Jobs - send emails and signalr n vue update alerts on client website. Later sql dependency, signalr and quartz for resources changes notifications to select clients.
        }


        //public static void Stop()
        //{
        //    scheduler
        //}
    }
}


/* 
 
gmail
deveedition@gmail.com
Qu@rt5d0tN3t


http://www.cronmaker.com/

https://www.mikesdotnetting.com/article/254/scheduled-tasks-in-asp-net-with-quartz-net



notification reminder

use quartz.net
use cron triggers
send email ---- create a jnas gmail account -- configure gmail to accept mails from external sources
send signalR message

https://stackoverflow.com/questions/28711919/how-can-i-send-message-to-specific-user-with-signalr
https://github.com/venkatbaggu/signalrdatabasenotifications/blob/master/SignalRDbUpdates/Views/Shared/_MessagesList.cshtml

Less secure email setting
https://myaccount.google.com/lesssecureapps?pli=1

https://stackoverflow.com/questions/25895516/quartz-scheduler-trigger-does-not-reference-given-job
     
https://youtu.be/_mdZyH41ULs
     
https://www.mikesdotnetting.com/article/254/scheduled-tasks-in-asp-net-with-quartz-net

http://www.venkatbaggu.com/signalr-database-update-notifications-asp-net-mvc-usiing-sql-dependency/

https://github.com/venkatbaggu/signalrdatabasenotifications

http://codetunnel.io/cannot-have-many-tabs-open-with-signalr/

https://docs.microsoft.com/en-us/aspnet/signalr/overview/guide-to-the-api/mapping-users-to-connections

https://www.youtube.com/watch?v=RlW4vUsoxEY

https://github.com/Leftyx/AspNetQuartSignalR/blob/master/AspNetQuartSignalR/Scheduler/Jobs/HelloJob.cs

https://www.quartz-scheduler.net/documentation/quartz-3.x/tutorial/crontriggers.html

http://nodatime.org/


     */
