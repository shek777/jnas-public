﻿using System.Web.Mvc;

namespace jnas.Areas.External
{
    public class ExternalAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "External";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
           // context.Routes.MapMvcAttributeRoutes();

            context.MapRoute(
                name: "External_default",
                url: "External/{controller}/{action}/{id}",
                defaults: new { area = "External", controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "jnas.Areas.External.Controllers" }
                );
        }
    }
}