﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.External.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "PrimaryExternalUser, SecondaryExternalUser, ReadOnlyUser, Administrator")]
    public class UsersController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: External/Users
        public async Task<ActionResult> Index()
        {
            // Get User Id


            var user = UserManager.FindById(User.Identity.GetUserId());

            if (Session["UserId"] == null)
            {

                Session["UserId"] = user.Id;
            }

            var users = db.Users.Where(u => u.organizationId.Equals(user.organizationId)).Include(u => u.Organization1).Include(u => u.UserState).Include(u => u.UserType1);
            return View(await users.ToListAsync());
        }

        // GET: External/Users/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: External/Users/Create
        //public ActionResult Create()
        //{
        //    ViewBag.organizationId = new SelectList(db.Organizations, "organizationId", "name");
        //    ViewBag.userStateId = new SelectList(db.UserStates, "userStateId", "name");
        //    ViewBag.userTypeId = new SelectList(db.UserTypes, "userTypeId", "name");
        //    return View();
        //}

        // POST: External/Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "UserId,name,userNo,organizationId,organization,userTypeId,userType,userProfileId,userProfile,primaryContact,securityQuestion,securityAnswer,createdDate,dateChanged,lastConnectionDate,userStateId,state,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName")] User user)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Users.Add(user);
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.organizationId = new SelectList(db.Organizations, "organizationId", "name", user.organizationId);
        //    ViewBag.userStateId = new SelectList(db.UserStates, "userStateId", "name", user.userStateId);
        //    ViewBag.userTypeId = new SelectList(db.UserTypes, "userTypeId", "name", user.userTypeId);
        //    return View(user);
        //}

        // GET: External/Users/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);

            if (user == null)
            {
                return HttpNotFound();
            }

            UserViewModel usr = new UserViewModel();

            usr.name = user.name;
            usr.primaryContact = user.primaryContact;
            usr.securityQuestion = user.securityQuestion;
            usr.securityAnswer = user.securityAnswer;
            usr.Email = user.Email;
            usr.PhoneNumber = user.PhoneNumber;
            usr.UserName = user.UserName;
            usr.UserId = user.UserId;

            //ViewBag.organizationId = new SelectList(db.Organizations, "organizationId", "name", user.organizationId);
            //ViewBag.userStateId = new SelectList(db.UserStates, "userStateId", "name", user.userStateId);
            //ViewBag.userTypeId = new SelectList(db.UserTypes, "userTypeId", "name", user.userTypeId);
            return View(usr);
        }

        // POST: External/Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "UserId,name,primaryContact,securityQuestion,securityAnswer,Email,PhoneNumber,UserName")] UserViewModel user)
        {
            if (ModelState.IsValid)
            {

                var usr = db.Users.Find(user.UserId);

                if (user.primaryContact == true)
                {
                    var listofusers = db.Users.Where(d => d.UserId != user.UserId && d.organizationId == usr.organizationId);
                    foreach(var item in listofusers)
                    {
                        item.primaryContact = false;
                        db.Entry(item).State = EntityState.Modified;
                        db.SaveChanges();
                    }


                }else
                {
                    if (db.Users.Where(d => d.organizationId == usr.organizationId).Count() == 1)
                    {
                        user.primaryContact = true;
                    }
                    else {

                        var chosenusers = db.Users.Where(d => d.UserId != user.UserId && d.primaryContact == false && d.organizationId == usr.organizationId).FirstOrDefault();

                        if (chosenusers != null)
                        {
                            chosenusers.primaryContact = true;
                            db.Entry(chosenusers).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                      }
                }

                usr.name = user.name;
                usr.primaryContact = user.primaryContact;
                usr.securityQuestion = user.securityQuestion;
                usr.securityAnswer = user.securityAnswer;
                usr.Email = user.Email;
                usr.PhoneNumber = user.PhoneNumber;
                usr.UserName = user.UserName;
                usr.dateChanged = DateTime.Now;

                db.Entry(usr).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            //ViewBag.organizationId = new SelectList(db.Organizations, "organizationId", "name", user.organizationId);
            //ViewBag.userStateId = new SelectList(db.UserStates, "userStateId", "name", user.userStateId);
            //ViewBag.userTypeId = new SelectList(db.UserTypes, "userTypeId", "name", user.userTypeId);
            return View(user);
        }

        // GET: External/Users/Delete/5
        //public async Task<ActionResult> Delete(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    User user = await db.Users.FindAsync(id);
        //    if (user == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(user);
        //}

        // POST: External/Users/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(string id)
        //{
        //    User user = await db.Users.FindAsync(id);
        //    db.Users.Remove(user);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
