﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using System.Data.SqlClient;
using jnas.Configuration;

namespace jnas.Areas.External.Controllers
{
    public class ApplicationResourcesController : Controller
    {
        private Jnasdb db = new Jnasdb();

        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: External/ApplicationResources
        public async Task<ActionResult> Index()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());

            // Get User Id
            if (Session["UserId"] == null)
            {
             //   var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }


            var applicationResources = db.ApplicationResources.Include(a => a.Application1).Include(a => a.Resource1);
           
            return View(await applicationResources.Where(a => a.Application1.organizationId == user.organizationId).ToListAsync());
        }

        // GET: External/ApplicationResources/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = UserManager.FindById(User.Identity.GetUserId());

            var applicationResource = await db.ApplicationResources.Include(a => a.Application1).Include(a => a.Resource1).Where(ar => ar.applicationResourceId == id && ar.Application1.organizationId == user.organizationId).FirstOrDefaultAsync();
           // ApplicationResource applicationResource = await db.ApplicationResources.FindAsync(id);
            if (applicationResource == null)
            {
                return HttpNotFound();
            }
            return View(applicationResource);
        }

        // GET: External/ApplicationResources/Create
        public ActionResult Create()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            var orgId = user.organizationId.ToString();
            
            var results1 = (from tabledata in db.Applications.Where(a => a.organizationId == user.organizationId) select new { tabledata.applicationId, name = tabledata.applicationReference }).ToList();
            ViewBag.applicationId = new SelectList(results1.AsEnumerable(), "applicationId", "name");
            var results = (from tabledata in db.Resources.Where(r => (r.state == orgId || r.state.Equals(null)) && r.resourceStateId == getVacantResourceState()) select new { tabledata.resourceId, name = tabledata.type + " " + tabledata.subtype }).ToList();
           // var results = (from tabledata in db.Resources select new { tabledata.resourceId, name = tabledata.type + " " + tabledata.subtype }).ToList();
            ViewBag.resourceId = new SelectList(results.AsEnumerable(), "resourceId", "name");
         
            return View();
        }

        private int getVacantResourceState()
        {
           var rs = 3;

           var result = db.ResourceStates.Where(res => res.name == "Vacant").Select(d => d.resourceStateId).FirstOrDefault();

           return result != 0 ? result : rs; 

        }

        private int getReservedResourceState()
        {
            var rs = 1;

            var result = db.ResourceStates.Where(res => res.name == "Reserved").Select(d => d.resourceStateId).FirstOrDefault();

            return result != 0 ? result : rs;

        }

        // POST: External/ApplicationResources/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "applicationResourceId,applicationId,application,resourceId,resource,createdDate,dateChanged,applicationResourceStateId,state")] ApplicationResource applicationResource)
        {
            if (ModelState.IsValid)
            {
                db.ApplicationResources.Add(applicationResource);
                await db.SaveChangesAsync();

                //Chamge Resource State to Reserved

                var original = db.Resources.Find(applicationResource.resourceId);

                if (original != null)
                {
                    original.resourceStateId = getReservedResourceState();
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }

            var user = UserManager.FindById(User.Identity.GetUserId());
            var orgId = user.organizationId.ToString();

            var results1 = (from tabledata in db.Applications.Where(a => a.organizationId == user.organizationId) select new { tabledata.applicationId, name = tabledata.applicationReference }).ToList();
            ViewBag.applicationId = new SelectList(results1.AsEnumerable(), "applicationId", "name", applicationResource.applicationId);
            var results = (from tabledata in db.Resources.Where(r => (r.state == orgId || r.state.Equals(null)) && r.resourceStateId == getVacantResourceState()) select new { tabledata.resourceId, name = tabledata.type + " " + tabledata.subtype }).ToList();
           // var results = (from tabledata in db.Resources select new { tabledata.resourceId, name = tabledata.type + " " + tabledata.subtype }).ToList();
            ViewBag.resourceId = new SelectList(results.AsEnumerable(), "resourceId", "name", Convert.ToInt32(applicationResource.resourceId));
          
            return View(applicationResource);
        }

        // GET: External/ApplicationResources/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

           

            ApplicationResource applicationResource = await db.ApplicationResources.FindAsync(id);
            if (applicationResource == null)
            {
                return HttpNotFound();
            }

            var user = UserManager.FindById(User.Identity.GetUserId());
            var orgId = user.organizationId.ToString();

            var results1 = (from tabledata in db.Applications.Where(a => a.organizationId == user.organizationId) select new { tabledata.applicationId, name = tabledata.applicationReference }).ToList();
            ViewBag.applicationId = new SelectList(results1.AsEnumerable(), "applicationId", "name", applicationResource.applicationId);
             var results = (from tabledata in db.Resources.Where(r => (r.state == orgId || r.state.Equals(null)) && r.resourceStateId == getVacantResourceState()) select new { tabledata.resourceId, name = tabledata.type + " " + tabledata.subtype }).ToList();
           // var results = (from tabledata in db.Resources select new { tabledata.resourceId, name = tabledata.type + " " + tabledata.subtype }).ToList();
            ViewBag.resourceId = new SelectList(results.AsEnumerable(), "resourceId", "name", Convert.ToInt32(applicationResource.resourceId));
         
            return View(applicationResource);
        }

        // POST: External/ApplicationResources/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "applicationResourceId,applicationId,application,resourceId,resource,createdDate,dateChanged,applicationResourceStateId,state")] ApplicationResource applicationResource)
        {
            if (ModelState.IsValid)
            {
                db.Entry(applicationResource).State = EntityState.Modified;
                await db.SaveChangesAsync();


                //Chamge Resource State to Reserved

                var original = db.Resources.Find(applicationResource.resourceId);

                if (original != null)
                {
                    if (original.resourceStateId == getVacantResourceState())
                    {
                        original.resourceStateId = getReservedResourceState();
                        db.SaveChanges();
                    }
                }


                return RedirectToAction("Index");
            }

            var user = UserManager.FindById(User.Identity.GetUserId());
            var orgId = user.organizationId.ToString();

            var results1 = (from tabledata in db.Applications.Where(a => a.organizationId == user.organizationId) select new { tabledata.applicationId, name = tabledata.applicationReference }).ToList();
            ViewBag.applicationId = new SelectList(results1.AsEnumerable(), "applicationId", "name", applicationResource.applicationId);
            var results = (from tabledata in db.Resources.Where(r => (r.state == orgId || r.state.Equals(null)) && r.resourceStateId == getVacantResourceState()) select new { tabledata.resourceId, name = tabledata.type + " " + tabledata.subtype }).ToList();
           // var results = (from tabledata in db.Resources select new { tabledata.resourceId, name = tabledata.type + " " + tabledata.subtype }).ToList();
            ViewBag.resourceId = new SelectList(results.AsEnumerable(), "resourceId", "name", Convert.ToInt32(applicationResource.resourceId));
            
            return View(applicationResource);
        }

        // GET: External/ApplicationResources/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = UserManager.FindById(User.Identity.GetUserId());

            var applicationResource = await db.ApplicationResources.Include(a => a.Application1).Include(a => a.Resource1).Where(ar => ar.applicationResourceId == id && ar.Application1.organizationId == user.organizationId).FirstOrDefaultAsync();
            if (applicationResource == null)
            {
                return HttpNotFound();
            }
            return View(applicationResource);
        }

        // POST: External/ApplicationResources/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            var user = UserManager.FindById(User.Identity.GetUserId());

            var applicationResource = await db.ApplicationResources.Include(a => a.Application1).Include(a => a.Resource1).Where(ar => ar.applicationResourceId == id && ar.Application1.organizationId == user.organizationId).FirstOrDefaultAsync();
            if (applicationResource == null)
            {
                return HttpNotFound();
            }
            db.ApplicationResources.Remove(applicationResource);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
