﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.External.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "PrimaryExternalUser, SecondaryExternalUser, ReadOnlyUser, Administrator")]
    public class SystemMessagesController : Controller
    {
        private Jnasdb db = new Jnasdb();

        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: External/SystemMessages
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            return View(await db.SystemMessages.OrderByDescending(s => s.createdDate).ToListAsync());
        }

        // GET: External/SystemMessages/Details/5
        public async Task<ActionResult> Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SystemMessage systemMessage = await db.SystemMessages.FindAsync(id);
            if (systemMessage == null)
            {
                return HttpNotFound();
            }
            return View(systemMessage);
        }

        // GET: External/SystemMessages/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        // POST: External/SystemMessages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "systemMessageId,title,message,expiryDate,createdDate,dateChanged")] SystemMessage systemMessage)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.SystemMessages.Add(systemMessage);
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }

        //    return View(systemMessage);
        //}

        // GET: External/SystemMessages/Edit/5
        //public async Task<ActionResult> Edit(long? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    SystemMessage systemMessage = await db.SystemMessages.FindAsync(id);
        //    if (systemMessage == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(systemMessage);
        //}

        // POST: External/SystemMessages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "systemMessageId,title,message,expiryDate,createdDate,dateChanged")] SystemMessage systemMessage)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(systemMessage).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    return View(systemMessage);
        //}

        // GET: External/SystemMessages/Delete/5
        //public async Task<ActionResult> Delete(long? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    SystemMessage systemMessage = await db.SystemMessages.FindAsync(id);
        //    if (systemMessage == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(systemMessage);
        //}

        // POST: External/SystemMessages/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(long id)
        //{
        //    SystemMessage systemMessage = await db.SystemMessages.FindAsync(id);
        //    db.SystemMessages.Remove(systemMessage);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
