﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.External.Controllers
{
    public class UserNotificationsController : Controller
    {
        private Jnasdb db = new Jnasdb();

        private UserManager _userManager;
        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: External/UserNotifications
        public async Task<ActionResult> Index()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());

            // Get User Id
            if (Session["UserId"] == null)
            {
               // var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }


            var userNotifications = db.UserNotifications.Where(un => un.assigned == true && un.UserId == user.Id).Include(u => u.Notification1).Include(u => u.User1);
            return View(await userNotifications.ToListAsync());
        }

        // GET: External/UserNotifications/Details/5
        //public async Task<ActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    UserNotification userNotification = await db.UserNotifications.FindAsync(id);
        //    if (userNotification == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(userNotification);
        //}

        // GET: External/UserNotifications/Create
        //public ActionResult Create()
        //{
        //    ViewBag.notificationId = new SelectList(db.Notifications, "notificationId", "name");
        //    ViewBag.UserId = new SelectList(db.Users, "UserId", "name");
        //    return View();
        //}

        // POST: External/UserNotifications/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "userNotificationId,UserId,user,notificationId,notification,assigned")] UserNotification userNotification)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.UserNotifications.Add(userNotification);
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.notificationId = new SelectList(db.Notifications, "notificationId", "name", userNotification.notificationId);
        //    ViewBag.UserId = new SelectList(db.Users, "UserId", "name", userNotification.UserId);
        //    return View(userNotification);
        //}

        // GET: External/UserNotifications/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = UserManager.FindById(User.Identity.GetUserId());

            UserNotification userNotification = await db.UserNotifications.Where(un => un.userNotificationId == id && un.assigned == true && un.UserId == user.Id).FirstOrDefaultAsync();
            if (userNotification == null)
            {
                return HttpNotFound();
            }
            var notification = new List<Subscribe>
   { new Subscribe{Text="No", Value="No"},
     new Subscribe{Text="Yes", Value="Yes"}};
            ViewBag.notification = new SelectList(notification, "Value", "Text", userNotification.notification);
            // ViewBag.notificationId = new SelectList(db.Notifications, "notificationId", "name", userNotification.notificationId);
            // ViewBag.UserId = new SelectList(db.Users, "UserId", "name", userNotification.UserId);
            TempData["UserNotification"] = userNotification;
            return View(userNotification);
        }

        // POST: External/UserNotifications/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        // public async Task<ActionResult> Edit([Bind(Include = "userNotificationId,UserId,user,notificationId,notification,assigned")] UserNotification userNotification)
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "userNotificationId,UserId,user,notificationId,notification,assigned")] UserNotification userNotification)
        {
            var unTemp = TempData["UserNotification"] as UserNotification;

            if(userNotification.notification == "Yes" || userNotification.notification == "No")
                 unTemp.notification = userNotification.notification;

            if (ModelState.IsValid)
            {
                userNotification = unTemp;
                db.Entry(userNotification).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            var notification = new List<Subscribe>
   { new Subscribe{Text="No", Value="No"},
     new Subscribe{Text="Yes", Value="Yes"}};
            ViewBag.notification = new SelectList(notification, "Value", "Text", unTemp.notification);
            // ViewBag.notificationId = new SelectList(db.Notifications, "notificationId", "name", userNotification.notificationId);
            // ViewBag.UserId = new SelectList(db.Users, "UserId", "name", userNotification.UserId);
            TempData["UserNotification"] = unTemp;
            return View(userNotification);
        }

        // GET: External/UserNotifications/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    UserNotification userNotification = await db.UserNotifications.FindAsync(id);
        //    if (userNotification == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(userNotification);
        //}

        // POST: External/UserNotifications/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    UserNotification userNotification = await db.UserNotifications.FindAsync(id);
        //    db.UserNotifications.Remove(userNotification);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }

    public class Subscribe
    {
        public string Text{ get; set; }
        public string Value { get; set; }
    }
}
