﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using System.Data.SqlClient;
using jnas.Configuration;


namespace jnas.Areas.External.Controllers
{
    public class ResourcesController : Controller
    {
        private Jnasdb db = new Jnasdb();

        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: External/Resources
        public async Task<ActionResult> Index()
        {

            var user = UserManager.FindById(User.Identity.GetUserId());
            var orgId = user.organizationId.ToString();

            // Get User Id
            if (Session["UserId"] == null)
            {
               // var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }



            var resources = db.Resources.Include(r => r.ResourceState);
            return View(await resources.Where(r => r.state == orgId).ToListAsync());
        }

        // GET: External/Resources/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = UserManager.FindById(User.Identity.GetUserId());
            var orgId = user.organizationId.ToString();

            var resource = await db.Resources.Where(rl => rl.state == orgId && rl.resourceId == id).FirstOrDefaultAsync();

           // Resource resource = await db.Resources.FindAsync(id);
            if (resource == null)
            {
                return HttpNotFound();
            }
            return View(resource);
        }

        // GET: External/Resources/Create
        public ActionResult Create()
        {

            // Filter
            var user = UserManager.FindById(User.Identity.GetUserId());
            var serviceProfile = db.Database.SqlQuery<ServiceProfile>(string.Format("SELECT ServiceProfiles.* FROM Organizations INNER JOIN Addresses ON Organizations.organizationId = Addresses.organizationId INNER JOIN OrganizationServices ON Addresses.AddressId = OrganizationServices.AddressId INNER JOIN ProviderServices ON OrganizationServices.ProviderServiceId = ProviderServices.ProviderServiceId INNER JOIN ServiceProfiles ON ProviderServices.ProviderServiceId = ServiceProfiles.providerServiceId WHERE(Organizations.organizationId = {0}) AND (ServiceProfiles.assigned = 1)", user.organizationId)).ToList();

            var userProfileServices = db.Database.SqlQuery<Service>(string.Format("SELECT DISTINCT Services.serviceId, Services.name, Services.applicationYN, UserProfiles.userProfileId FROM UserProfiles INNER JOIN UserProfileDetails ON UserProfiles.userProfileId = UserProfileDetails.userProfileId INNER JOIN Services ON UserProfileDetails.serviceId = Services.serviceId WHERE(UserProfiles.userProfileId = {0})", user.userProfileId)).ToList();

            List<ResourceType> restypes = new List<ResourceType>();
            List<ResourceSubType> ressubtypes = new List<ResourceSubType>();
           // List<ApplicationType> apptypes = new List<ApplicationType>();

            foreach (var sp in serviceProfile)
            {
                var restype = db.ResourceTypes.Where(r => r.resourceTypeId.Equals(sp.resourceTypeId)).FirstOrDefault();
                var ressubtype = db.ResourceSubTypes.Where(r => r.resourceSubTypeId.Equals(sp.resourceSubTypeId)).FirstOrDefault();

                restypes.Add(restype);
                ressubtypes.Add(ressubtype);

            }

            //foreach (var at in userProfileServices)
            //{

            //    var apptype = db.ApplicationTypes.Where(r => r.name.Equals(at.name)).FirstOrDefault();

            //    apptypes.Add(apptype);

            //}

            ViewBag.resourceSubTypeId = new SelectList(ressubtypes.Distinct().AsEnumerable(), "resourceSubTypeId", "name");
            ViewBag.resourceTypeId = new SelectList(restypes.Distinct().AsEnumerable(), "resourceTypeId", "name");

            //ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name");
            //ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name");
            //ViewBag.ResourceState_resourceStateId = new SelectList(db.ResourceStates, "resourceStateId", "name");
            //ViewBag.ResourceState_resourceStateId1 = new SelectList(db.ResourceStates, "resourceStateId", "name");
            var query = @"SELECT resourceLinkId, (SELECT resource1 FROM Resources WHERE(resourceId = CONVERT(int, ISNULL(ResourceLinks.parentResource, 0)))) AS parentresourcename, parentResource, (SELECT resource1 FROM Resources WHERE(resourceId = CONVERT(int, ISNULL(ResourceLinks.childResource, 0)))) AS childresourcename, childResource FROM ResourceLinks";


            List<ResourceState> resouceLink = new List<ResourceState>();

            // 1. Instantiate the connection
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(jnas.Configuration.Config.ConnectionString);
            System.Data.SqlClient.SqlDataReader rdr = null;
            try
            {
                // 2. Open the connection
                conn.Open();

                // 3. Pass the connection to a command object
                System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(query, conn);

                //
                // 4. Use the connection
                //

                // get query results
                rdr = cmd.ExecuteReader();

                // print the CustomerID of each record
                while (rdr.Read())
                {
                    // Get all Codes

                    resouceLink.Add(new ResourceState { resourceStateId = Convert.ToInt32(rdr[0].ToString()), name = "Parent: " + rdr[1].ToString() + " Child: " + rdr[3].ToString() });
                }
            }
            finally
            {
                // close the reader
                if (rdr != null)
                {
                    rdr.Close();
                }

                // 5. Close the connection
                if (conn != null)
                {
                    conn.Close();
                }
            }



            ViewBag.resourceLinkId = new SelectList(resouceLink.AsEnumerable(), "resourceStateId", "name");

            return View();
        }

        // POST: External/Resources/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.

        // public async Task<ActionResult> Create([Bind(Include = "resourceId,resource1,resourceTypeId,type,resourceSubTypeId,subtype,binary,createdDate,dateChanged,resourceLinkId,resourceStateId,state")] 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "resourceId,resource1,resourceTypeId,resourceSubTypeId,binary,resourceLinkId")] Resource resource)
        {
            var user = UserManager.FindById(User.Identity.GetUserId());

            if (ModelState.IsValid)
            {
                var type = db.ResourceTypes.Where(rt => rt.resourceTypeId.Equals(resource.resourceTypeId)).FirstOrDefault();
                if (type != null)
                    resource.type = type.name;
                var subtype = db.ResourceSubTypes.Where(rt => rt.resourceSubTypeId.Equals(resource.resourceSubTypeId)).FirstOrDefault();
                if (subtype != null)
                    resource.subtype = subtype.name;

                var resourcestate = db.ResourceStates.Where(rs => rs.name == "Blocked").FirstOrDefault();
                if (resourcestate != null) {
                    resource.resourceStateId = resourcestate.resourceStateId;
                }
                else
                {
                    resource.resourceStateId = 2;
                }
                resource.state = user.organizationId.ToString();
                resource.createdDate = DateTime.Now;
                resource.dateChanged = DateTime.Now;
                db.Resources.Add(resource);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }


            // Filter
            
            var serviceProfile = db.Database.SqlQuery<ServiceProfile>(string.Format("SELECT ServiceProfiles.* FROM Organizations INNER JOIN Addresses ON Organizations.organizationId = Addresses.organizationId INNER JOIN OrganizationServices ON Addresses.AddressId = OrganizationServices.AddressId INNER JOIN ProviderServices ON OrganizationServices.ProviderServiceId = ProviderServices.ProviderServiceId INNER JOIN ServiceProfiles ON ProviderServices.ProviderServiceId = ServiceProfiles.providerServiceId WHERE(Organizations.organizationId = {0}) AND (ServiceProfiles.assigned = 1)", user.organizationId)).ToList();

            var userProfileServices = db.Database.SqlQuery<Service>(string.Format("SELECT DISTINCT Services.serviceId, Services.name, Services.applicationYN, UserProfiles.userProfileId FROM UserProfiles INNER JOIN UserProfileDetails ON UserProfiles.userProfileId = UserProfileDetails.userProfileId INNER JOIN Services ON UserProfileDetails.serviceId = Services.serviceId WHERE(UserProfiles.userProfileId = {0})", user.userProfileId)).ToList();

            List<ResourceType> restypes = new List<ResourceType>();
            List<ResourceSubType> ressubtypes = new List<ResourceSubType>();
            //List<ApplicationType> apptypes = new List<ApplicationType>();

            foreach (var sp in serviceProfile)
            {
                var restype = db.ResourceTypes.Where(r => r.resourceTypeId.Equals(sp.resourceTypeId)).FirstOrDefault();
                var ressubtype = db.ResourceSubTypes.Where(r => r.resourceSubTypeId.Equals(sp.resourceSubTypeId)).FirstOrDefault();

                restypes.Add(restype);
                ressubtypes.Add(ressubtype);

            }

            //ViewBag.resourceSubTypeId = new SelectList(ressubtypes.Distinct().AsEnumerable(), "resourceSubTypeId", "name");
            //ViewBag.resourceTypeId = new SelectList(restypes.Distinct().AsEnumerable(), "resourceTypeId", "name");

            ViewBag.resourceSubTypeId = new SelectList(ressubtypes.Distinct().AsEnumerable(), "resourceSubTypeId", "name", resource.resourceSubTypeId);
            ViewBag.resourceTypeId = new SelectList(restypes.Distinct().AsEnumerable(), "resourceTypeId", "name", resource.resourceTypeId);
            //ViewBag.ResourceState_resourceStateId = new SelectList(db.ResourceStates, "resourceStateId", "name", resource.ResourceState_resourceStateId);
            //ViewBag.ResourceState_resourceStateId1 = new SelectList(db.ResourceStates, "resourceStateId", "name", resource.ResourceState_resourceStateId1);
               var query = @"SELECT resourceLinkId, (SELECT resource1 FROM Resources WHERE(resourceId = CONVERT(int, ISNULL(ResourceLinks.parentResource, 0)))) AS parentresourcename, parentResource, (SELECT resource1 FROM Resources WHERE(resourceId = CONVERT(int, ISNULL(ResourceLinks.childResource, 0)))) AS childresourcename, childResource FROM ResourceLinks";


            List<ResourceState> resouceLink = new List<ResourceState>();

            // 1. Instantiate the connection
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(jnas.Configuration.Config.ConnectionString);
            System.Data.SqlClient.SqlDataReader rdr = null;
            try
            {
                // 2. Open the connection
                conn.Open();

                // 3. Pass the connection to a command object
                System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(query, conn);

                //
                // 4. Use the connection
                //

                // get query results
                rdr = cmd.ExecuteReader();

                // print the CustomerID of each record
                while (rdr.Read())
                {
                    // Get all Codes

                    resouceLink.Add(new ResourceState { resourceStateId = Convert.ToInt32(rdr[0].ToString()), name = "Parent: " + rdr[1].ToString() + " Child: " + rdr[3].ToString() });
                }
            }
            finally
            {
                // close the reader
                if (rdr != null)
                {
                    rdr.Close();
                }

                // 5. Close the connection
                if (conn != null)
                {
                    conn.Close();
                }
            }



            ViewBag.resourceLinkId = new SelectList(resouceLink.AsEnumerable(), "resourceStateId", "name", resource.resourceLinkId);


            return View(resource);
        }

        // GET: External/Resources/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = UserManager.FindById(User.Identity.GetUserId());
            var orgId = user.organizationId.ToString();

            var resource = await db.Resources.Where(rl => rl.state == orgId && rl.resourceId == id).FirstOrDefaultAsync();


            //Resource resource = await db.Resources.FindAsync(id);
            if (resource == null)
            {
                return HttpNotFound();
            }

            // Filter
           // var user = UserManager.FindById(User.Identity.GetUserId());
            var serviceProfile = db.Database.SqlQuery<ServiceProfile>(string.Format("SELECT ServiceProfiles.* FROM Organizations INNER JOIN Addresses ON Organizations.organizationId = Addresses.organizationId INNER JOIN OrganizationServices ON Addresses.AddressId = OrganizationServices.AddressId INNER JOIN ProviderServices ON OrganizationServices.ProviderServiceId = ProviderServices.ProviderServiceId INNER JOIN ServiceProfiles ON ProviderServices.ProviderServiceId = ServiceProfiles.providerServiceId WHERE(Organizations.organizationId = {0}) AND (ServiceProfiles.assigned = 1)", user.organizationId)).ToList();

            var userProfileServices = db.Database.SqlQuery<Service>(string.Format("SELECT DISTINCT Services.serviceId, Services.name, Services.applicationYN, UserProfiles.userProfileId FROM UserProfiles INNER JOIN UserProfileDetails ON UserProfiles.userProfileId = UserProfileDetails.userProfileId INNER JOIN Services ON UserProfileDetails.serviceId = Services.serviceId WHERE(UserProfiles.userProfileId = {0})", user.userProfileId)).ToList();

            List<ResourceType> restypes = new List<ResourceType>();
            List<ResourceSubType> ressubtypes = new List<ResourceSubType>();
           // List<ApplicationType> apptypes = new List<ApplicationType>();

            foreach (var sp in serviceProfile)
            {
                var restype = db.ResourceTypes.Where(r => r.resourceTypeId.Equals(sp.resourceTypeId)).FirstOrDefault();
                var ressubtype = db.ResourceSubTypes.Where(r => r.resourceSubTypeId.Equals(sp.resourceSubTypeId)).FirstOrDefault();

                restypes.Add(restype);
                ressubtypes.Add(ressubtype);

            }

           
            ViewBag.resourceSubTypeId = new SelectList(ressubtypes.Distinct().AsEnumerable(), "resourceSubTypeId", "name", resource.resourceSubTypeId);
            ViewBag.resourceTypeId = new SelectList(restypes.Distinct().AsEnumerable(), "resourceTypeId", "name", resource.resourceTypeId);
            //ViewBag.ResourceState_resourceStateId = new SelectList(db.ResourceStates, "resourceStateId", "name", resource.ResourceState_resourceStateId);
            //ViewBag.ResourceState_resourceStateId1 = new SelectList(db.ResourceStates, "resourceStateId", "name", resource.ResourceState_resourceStateId1);
            var query = @"SELECT resourceLinkId, (SELECT resource1 FROM Resources WHERE(resourceId = CONVERT(int, ISNULL(ResourceLinks.parentResource, 0)))) AS parentresourcename, parentResource, (SELECT resource1 FROM Resources WHERE(resourceId = CONVERT(int, ISNULL(ResourceLinks.childResource, 0)))) AS childresourcename, childResource FROM ResourceLinks";


            List<ResourceState> resouceLink = new List<ResourceState>();

            // 1. Instantiate the connection
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(jnas.Configuration.Config.ConnectionString);
            System.Data.SqlClient.SqlDataReader rdr = null;
            try
            {
                // 2. Open the connection
                conn.Open();

                // 3. Pass the connection to a command object
                System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(query, conn);

                //
                // 4. Use the connection
                //

                // get query results
                rdr = cmd.ExecuteReader();

                // print the CustomerID of each record
                while (rdr.Read())
                {
                    // Get all Codes

                    resouceLink.Add(new ResourceState { resourceStateId = Convert.ToInt32(rdr[0].ToString()), name = "Parent: " + rdr[1].ToString() + " Child: " + rdr[3].ToString() });
                }
            }
            finally
            {
                // close the reader
                if (rdr != null)
                {
                    rdr.Close();
                }

                // 5. Close the connection
                if (conn != null)
                {
                    conn.Close();
                }
            }

            //Set the resourcestates
           // var resourceStates = getResourceStates(resource.resourceStateId);
           // ViewBag.resourceStateId = new SelectList(resourceStates, "resourceStateId", "name", resource.resourceStateId);


            ViewBag.resourceLinkId = new SelectList(resouceLink.AsEnumerable(), "resourceStateId", "name", resource.resourceLinkId);
            TempData["resource"] = resource;
            return View(resource);
        }

        private List<ResourceState> getResourceStates(int resourceStateId)
        {
            List<ResourceState> rs = new List<ResourceState>();

            var name = db.ResourceStates.Where(res => res.resourceStateId == resourceStateId).Select(d => d.name).FirstOrDefault();

            switch (name)
            {
                case "Blocked":
                    rs = db.ResourceStates.Where(r => r.name == "Vacant" || r.name == "Discontinued" || r.resourceStateId == resourceStateId).ToList();
                    break;

                case "Vacant":
                    rs = db.ResourceStates.Where(r => r.name == "Blocked" || r.name == "Reserved" || r.resourceStateId == resourceStateId).ToList();
                    break;

                case "Reserved":
                    rs = db.ResourceStates.Where(r => r.name == "Assigned" || r.name == "Suspended" || r.resourceStateId == resourceStateId).ToList();
                    break;

                case "Assigned":
                    rs = db.ResourceStates.Where(r => r.name == "Active" || r.name == "Suspended" || r.resourceStateId == resourceStateId).ToList();
                    break;

                case "Active":
                    rs = db.ResourceStates.Where(r => r.name == "Aging" || r.name == "Suspended" || r.resourceStateId == resourceStateId).ToList();
                    break;

                case "Suspended":
                    rs = db.ResourceStates.Where(r => r.name == "Blocked" || r.name == "Discontinued" || r.name == "Reserved" || r.name == "Assigned" || r.name == "Aging" || r.name == "Assigned" || r.resourceStateId == resourceStateId).ToList();
                    break;

                case "Aging":
                    rs = db.ResourceStates.Where(r => r.name == "Blocked" || r.resourceStateId == resourceStateId).ToList();
                    break;


                default:
                    rs = db.ResourceStates.Where(r => r.name == "Vacant" || r.name == "Discontinued" || r.resourceStateId == resourceStateId).ToList();
                    break;
            }

            return rs;
        }

        // POST: External/Resources/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "resourceId,resource1,resourceTypeId,resourceSubTypeId,binary,resourceLinkId")] Resource resource)
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            var resource_original = TempData["resource"] as Resource;

            if (ModelState.IsValid)
            {

                //Don't Edit when resourcestate is set to aging 
                if (resource_original.resourceStateId == getAgingResourceState())
                {
                    return RedirectToAction("Index");
                }

                var type = db.ResourceTypes.Where(rt => rt.resourceTypeId.Equals(resource.resourceTypeId)).FirstOrDefault();
                if (type != null)
                    resource.type = type.name;
                var subtype = db.ResourceSubTypes.Where(rt => rt.resourceSubTypeId.Equals(resource.resourceSubTypeId)).FirstOrDefault();
                if (subtype != null)
                    resource.subtype = subtype.name;
                resource.state = user.organizationId.ToString();
                resource.dateChanged = DateTime.Now;
                db.Entry(resource).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }


            // Filter
            
            var serviceProfile = db.Database.SqlQuery<ServiceProfile>(string.Format("SELECT ServiceProfiles.* FROM Organizations INNER JOIN Addresses ON Organizations.organizationId = Addresses.organizationId INNER JOIN OrganizationServices ON Addresses.AddressId = OrganizationServices.AddressId INNER JOIN ProviderServices ON OrganizationServices.ProviderServiceId = ProviderServices.ProviderServiceId INNER JOIN ServiceProfiles ON ProviderServices.ProviderServiceId = ServiceProfiles.providerServiceId WHERE(Organizations.organizationId = {0}) AND (ServiceProfiles.assigned = 1)", user.organizationId)).ToList();

            var userProfileServices = db.Database.SqlQuery<Service>(string.Format("SELECT DISTINCT Services.serviceId, Services.name, Services.applicationYN, UserProfiles.userProfileId FROM UserProfiles INNER JOIN UserProfileDetails ON UserProfiles.userProfileId = UserProfileDetails.userProfileId INNER JOIN Services ON UserProfileDetails.serviceId = Services.serviceId WHERE(UserProfiles.userProfileId = {0})", user.userProfileId)).ToList();

            List<ResourceType> restypes = new List<ResourceType>();
            List<ResourceSubType> ressubtypes = new List<ResourceSubType>();
            // List<ApplicationType> apptypes = new List<ApplicationType>();

            foreach (var sp in serviceProfile)
            {
                var restype = db.ResourceTypes.Where(r => r.resourceTypeId.Equals(sp.resourceTypeId)).FirstOrDefault();
                var ressubtype = db.ResourceSubTypes.Where(r => r.resourceSubTypeId.Equals(sp.resourceSubTypeId)).FirstOrDefault();

                restypes.Add(restype);
                ressubtypes.Add(ressubtype);

            }


            ViewBag.resourceSubTypeId = new SelectList(ressubtypes.Distinct().AsEnumerable(), "resourceSubTypeId", "name", resource.resourceSubTypeId);
            ViewBag.resourceTypeId = new SelectList(restypes.Distinct().AsEnumerable(), "resourceTypeId", "name", resource.resourceTypeId);

            //ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", resource.resourceSubTypeId);
            //ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", resource.resourceTypeId);
            //ViewBag.ResourceState_resourceStateId = new SelectList(db.ResourceStates, "resourceStateId", "name", resource.ResourceState_resourceStateId);
            //ViewBag.ResourceState_resourceStateId1 = new SelectList(db.ResourceStates, "resourceStateId", "name", resource.ResourceState_resourceStateId1);

            var query = @"SELECT resourceLinkId, (SELECT resource1 FROM Resources WHERE(resourceId = CONVERT(int, ISNULL(ResourceLinks.parentResource, 0)))) AS parentresourcename, parentResource, (SELECT resource1 FROM Resources WHERE(resourceId = CONVERT(int, ISNULL(ResourceLinks.childResource, 0)))) AS childresourcename, childResource FROM ResourceLinks";


            List<ResourceState> resouceLink = new List<ResourceState>();

            // 1. Instantiate the connection
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(jnas.Configuration.Config.ConnectionString);
            System.Data.SqlClient.SqlDataReader rdr = null;
            try
            {
                // 2. Open the connection
                conn.Open();

                // 3. Pass the connection to a command object
                System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(query, conn);

                //
                // 4. Use the connection
                //

                // get query results
                rdr = cmd.ExecuteReader();

                // print the CustomerID of each record
                while (rdr.Read())
                {
                    // Get all Codes

                    resouceLink.Add(new ResourceState { resourceStateId = Convert.ToInt32(rdr[0].ToString()), name = "Parent: " + rdr[1].ToString() + " Child: " + rdr[3].ToString() });
                }
            }
            finally
            {
                // close the reader
                if (rdr != null)
                {
                    rdr.Close();
                }

                // 5. Close the connection
                if (conn != null)
                {
                    conn.Close();
                }
            }



            ViewBag.resourceLinkId = new SelectList(resouceLink.AsEnumerable(), "resourceStateId", "name", resource.resourceLinkId);
            TempData["resource"] = resource_original;
            return View(resource);
        }

        private int getAgingResourceState()
        {
            var rs = 7;

            var result = db.ResourceStates.Where(res => res.name == "Aging").Select(d => d.resourceStateId).FirstOrDefault();

            return result != 0 ? result : rs;

        }

        // GET: External/Resources/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = UserManager.FindById(User.Identity.GetUserId());
            var orgId = user.organizationId.ToString();

            var resource = await db.Resources.Where(rl => rl.state == orgId && rl.resourceId == id).FirstOrDefaultAsync();


           // Resource resource = await db.Resources.FindAsync(id);
            if (resource == null)
            {
                return HttpNotFound();
            }
            return View(resource);
        }

        // POST: External/Resources/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {

            var user = UserManager.FindById(User.Identity.GetUserId());
            var orgId = user.organizationId.ToString();

            var resource = await db.Resources.Where(rl => rl.state == orgId && rl.resourceId == id).FirstOrDefaultAsync();


            //  Resource resource = await db.Resources.FindAsync(id);
            if (resource == null)
            {
                return HttpNotFound();
            }
            db.Resources.Remove(resource);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
