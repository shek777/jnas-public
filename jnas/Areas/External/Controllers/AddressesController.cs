﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;

namespace jnas.Areas.External.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "PrimaryExternalUser, SecondaryExternalUser, ReadOnlyUser, Administrator")]
    public class AddressesController : Controller
    {
        private UserManager _userManager;
        private Jnasdb db = new Jnasdb();
        private Jnasdb db1 = new Jnasdb();

        // GET: Administration/Addresses
        public async Task<ActionResult> Index(int? id)
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            if (id == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());

                var addresses1 = db.Addresses.Where(o => o.organizationId.Equals(user.organizationId)).Include(a => a.Organization1);
                return View(await addresses1.ToListAsync());
            }
            else {

                var user1 = UserManager.FindById(User.Identity.GetUserId());

                if (id == user1.organizationId)
                {

                    var addresses2 = db.Addresses.Where(o => o.organizationId.Equals(user1.organizationId)).Include(a => a.Organization1);
                    return View(await addresses2.ToListAsync());
                }

            }

            return View();

        }

        // GET: External/Addresses/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = UserManager.FindById(User.Identity.GetUserId());

            Address address = await (from d in db.Addresses where d.AddressId == id && d.organizationId == user.organizationId select d).FirstOrDefaultAsync();
            if (address == null)
            {
                return HttpNotFound();
            }
            return View(address);
        }

        // GET: External/Addresses/Create
        public ActionResult Create()
        {

            var user = UserManager.FindById(User.Identity.GetUserId());
            var result = db.Organizations.ToList();
            var result1 = result.Where(d => d.organizationId == user.organizationId);
            ViewBag.organizationId = new SelectList(result1, "organizationId", "name");
            return View();
        }

        // POST: External/Addresses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "AddressId,organizationId,line1,line2,city,zipCode,defaultAddress")] Address address)
        {
            var user = UserManager.FindById(User.Identity.GetUserId());

            if (ModelState.IsValid)
            {
                // chk org
                if(user.organizationId != address.organizationId)
                {
                    var resulta = db.Organizations.ToList();
                    var result1a = resulta.Where(d => d.organizationId == user.organizationId);
                    ViewBag.organizationId = new SelectList(result1a, "organizationId", "name");
                    return View(address);
                }


                // set default address
                if (address.defaultAddress == true)
                {
                    var listofOrgAddresses = db1.Addresses.Where(d => d.organizationId == address.organizationId).ToList();
                    listofOrgAddresses.ForEach(item =>
                    {
                        item.defaultAddress = false;
                        //  db.Entry(item).State = EntityState.Modified;

                    });

                    db1.SaveChanges();

                }
                else
               if (address.defaultAddress == false)
                {
                    if (db1.Addresses.Where(d => d.organizationId == address.organizationId).Count() == 0)
                    {
                        address.defaultAddress = true;
                    }

                }

                // Get Org Name
                var orgname = (from d in db.Organizations where d.organizationId == address.organizationId select d.name).FirstOrDefault().ToString();

                address.organization = orgname + " - " + address.line1;

                db.Addresses.Add(address);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            //var user = UserManager.FindById(User.Identity.GetUserId());
            var result = db.Organizations.ToList();
            var result1 = result.Where(d => d.organizationId == user.organizationId);
            ViewBag.organizationId = new SelectList(result1, "organizationId", "name", address.organizationId);
            return View(address);
        }

        // GET: External/Addresses/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = UserManager.FindById(User.Identity.GetUserId());

            Address address = await (from d in db.Addresses where d.AddressId == id && d.organizationId == user.organizationId select d).FirstOrDefaultAsync();
            if (address == null)
            {
                return HttpNotFound();
            }

           // var user = UserManager.FindById(User.Identity.GetUserId());
            var result = db.Organizations.ToList();
            var result1 = result.Where(d => d.organizationId == user.organizationId);
            ViewBag.organizationId = new SelectList(result1, "organizationId", "name", address.organizationId);
            return View(address);
        }

        // POST: External/Addresses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AddressId,organizationId,line1,line2,city,zipCode,defaultAddress")] Address address)
        {
            var user = UserManager.FindById(User.Identity.GetUserId());


            if (ModelState.IsValid)
            {

                // chk org
                if (user.organizationId != address.organizationId)
                {
                    var resulta = db.Organizations.ToList();
                    var result1a = resulta.Where(d => d.organizationId == user.organizationId);
                    ViewBag.organizationId = new SelectList(result1a, "organizationId", "name");
                    return View(address);
                }

                //chk address
                Address address1 = await (from d in db.Addresses where d.AddressId == address.AddressId && d.organizationId == user.organizationId select d).FirstOrDefaultAsync();
                if (address1 == null)
                {
                    var resulta = db.Organizations.ToList();
                    var result1a = resulta.Where(d => d.organizationId == user.organizationId);
                    ViewBag.organizationId = new SelectList(result1a, "organizationId", "name", address.organizationId);
                    return View(address);
                }

                // set default address
                if (address.defaultAddress == true)
                {
                    var listofOrgAddresses = db1.Addresses.Where(d => d.organizationId == address.organizationId).ToList();
                    listofOrgAddresses.ForEach(item =>
                    {
                        item.defaultAddress = false;
                        //  db.Entry(item).State = EntityState.Modified;

                    });

                    db1.SaveChanges();

                }
                else
               if (address.defaultAddress == false)
                {
                    if (db1.Addresses.Where(d => d.organizationId == address.organizationId).Count() == 0)
                    {
                        address.defaultAddress = true;
                    }

                }

                // Get Org Name
                var orgname = (from d in db.Organizations where d.organizationId == address.organizationId select d.name).FirstOrDefault().ToString();

                address.organization = orgname + " - " + address.line1;


                db.Entry(address).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
           // var user = UserManager.FindById(User.Identity.GetUserId());
            var result = db.Organizations.ToList();
            var result1 = result.Where(d => d.organizationId == user.organizationId);
            ViewBag.organizationId = new SelectList(result1, "organizationId", "name", address.organizationId);
            return View(address);
        }

        // GET: External/Addresses/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Address address = await db.Addresses.FindAsync(id);
        //    if (address == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(address);
        //}

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // POST: External/Addresses/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    Address address = await db.Addresses.FindAsync(id);
        //    db.Addresses.Remove(address);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
