﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.External.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "PrimaryExternalUser, SecondaryExternalUser, ReadOnlyUser, Administrator")]
    public class MailsController : Controller
    {
        private Jnasdb db = new Jnasdb();

        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: External/Mails
        public async Task<ActionResult> Index()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            var orgId = user.organizationId.ToString();

            // Get User Id
            if (Session["UserId"] == null)
            {
               Session["UserId"] = user.Id;
            }

           // ViewBag.user = user;
            return View(await db.Mails.Where(m => m.toAddress == orgId || m.fromAddress == orgId).OrderByDescending(d => d.createdDate).ToListAsync());
        }

        // GET: External/Mails/Details/5
        public async Task<ActionResult> Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = UserManager.FindById(User.Identity.GetUserId());
            var orgId = user.organizationId.ToString();

            var mail = await db.Mails.Where(m => m.mailId == id && (m.toAddress == orgId || m.fromAddress == orgId)).FirstOrDefaultAsync();

           // Mail mail = await db.Mails.FindAsync(id);
            if (mail == null)
            {
                return HttpNotFound();
            }


            ViewBag.To = "";
            try
            {
                var filter = Convert.ToInt32(mail.toAddress);
                var orgTo = db.Organizations.Where(o => o.organizationId == filter).FirstOrDefault();
                if (orgTo != null)
                {
                    ViewBag.To = orgTo.name;
                }
            }
            catch { }

            //var orgInt = db.Organizations.Where(o => o.OrganizationType1.name == "Internal").FirstOrDefault();
            //if (orgInt != null)
            //{
            //    ViewBag.To = orgInt.name;
            //}
            //else
            //{
            //    ViewBag.To = "Office of Utilities Regulation";
            //}


            ViewBag.From = "";
            try
            {
                var filter1 = Convert.ToInt32(mail.fromAddress);
                var orgFrom = db.Organizations.Where(o => o.organizationId == filter1).FirstOrDefault();
                if (orgFrom != null)
                {
                    ViewBag.From = orgFrom.name;
                }
            }
            catch { }

            //var org = db.Organizations.Where(o => o.organizationId == user.organizationId).FirstOrDefault();
            //if (org != null)
            //{
            //    ViewBag.From = org.name;
            //}

            return View(mail);
        }

        // GET: External/Mails/Create
        public ActionResult Create()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());

            var orgInt = db.Organizations.Where(o => o.OrganizationType1.name == "Internal").FirstOrDefault();
            if (orgInt != null)
            {
                ViewBag.To = orgInt.name;
            }
            else
            {
                ViewBag.To = "Office of Utilities Regulation";
            }

            var org = db.Organizations.Where(o => o.organizationId == user.organizationId).FirstOrDefault();
            if (org != null)
            {
                ViewBag.From = org.name;
            }
           
            return View();
        }

        // POST: External/Mails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "mailId,title,toAddress,fromAddress,message,attachment,createdDate,dateChanged")] Mail mail)
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            var orgId = user.organizationId.ToString();


            if (ModelState.IsValid)
            {
                var currentDate = DateTime.Now;

                var org = db.Organizations.Where(o => o.OrganizationType1.name == "Internal").FirstOrDefault();
                if(org != null)
                mail.toAddress = org.organizationId.ToString();

                mail.fromAddress = orgId;
                mail.createdDate = currentDate;
                mail.dateChanged = currentDate;
                db.Mails.Add(mail);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            var orgInt = db.Organizations.Where(o => o.OrganizationType1.name == "Internal").FirstOrDefault();
            if (orgInt != null)
            {
                ViewBag.To = orgInt.name;
            }
            else
            {
                ViewBag.To = "Office of Utilities Regulation";
            }

            var org1 = db.Organizations.Where(o => o.organizationId == user.organizationId).FirstOrDefault();
            if (org1 != null)
            {
                ViewBag.From = org1.name;
            }


            return View(mail);
        }

        // GET: External/Mails/Edit/5
        //public async Task<ActionResult> Edit(long? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Mail mail = await db.Mails.FindAsync(id);
        //    if (mail == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(mail);
        //}

        // POST: External/Mails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "mailId,title,toAddress,fromAddress,message,attachment,createdDate,dateChanged")] Mail mail)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(mail).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    return View(mail);
        //}

        // GET: External/Mails/Delete/5
        //public async Task<ActionResult> Delete(long? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Mail mail = await db.Mails.FindAsync(id);
        //    if (mail == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(mail);
        //}

        // POST: External/Mails/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(long id)
        //{
        //    Mail mail = await db.Mails.FindAsync(id);
        //    db.Mails.Remove(mail);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
