﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using jnas.Models.Jnasdb;
using FluentDateTime; // TODO: http://stackoverflow.com/questions/1044688/add-business-days-and-getbusinessdays http://codereview.stackexchange.com/questions/66432/find-the-number-of-working-days-between-two-dates-inclusively https://github.com/nodatime/nodatime/issues/6
using System.IO;
using System.Runtime.InteropServices;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Packaging;
using System.Text.RegularExpressions;

namespace jnas.Areas.External.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "PrimaryExternalUser, SecondaryExternalUser, ReadOnlyUser, Administrator")]
    public class NrufCompanyInfoesController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        private Random _random = new Random((int)DateTime.Now.Ticks);

        /// <summary>
        /// Store the Application object we can use in the member functions.
        /// </summary>
       // Microsoft.Office.Interop.Excel.Application _excelApp = new Microsoft.Office.Interop.Excel.Application();

        ///// <summary>
        ///// Initialize a new Excel reader. Must be integrated
        ///// with an Excel interface object.
        ///// </summary>
        //public NrufCompanyInfoesController()
        //{
        //    _excelApp = new Microsoft.Office.Interop.Excel.Application();
        //}
        // http://www.dotnetperls.com/excel  interop requires Office to be installed on the Webserver
        // http://www.codeproject.com/Questions/599641/createplusexcelplusfilepluswithoutplusinteropplusr

        //private void ProcessObjects(object[,] valueArray)
        //{
        //   // valueArray.
        //    throw new NotImplementedException();
        //}

        /// <summary>
        /// Scan the selected Excel workbook and store the information in the cells
        /// for this workbook in an object[,] array. Then, call another method
        /// to process the data.
        /// </summary>
        //private void ExcelScanIntenal(Workbook workBookIn)
        //{
        //    //
        //    // Get sheet Count and store the number of sheets.
        //    //
        //    int numSheets = workBookIn.Sheets.Count;

        //    //
        //    // Iterate through the sheets. They are indexed starting at 1.
        //    //
        //    for (int sheetNum = 1; sheetNum < numSheets + 1; sheetNum++)
        //    {
        //        Worksheet sheet = (Worksheet)workBookIn.Sheets[sheetNum];

        //        //
        //        // Take the used range of the sheet. Finally, get an object array of all
        //        // of the cells in the sheet (their values). You can do things with those
        //        // values. See notes about compatibility.
        //        //
        //        Range excelRange = sheet.UsedRange;
        //        object[,] valueArray = (object[,])excelRange.get_Value(
        //            XlRangeValueDataType.xlRangeValueDefault);

        //        //
        //        // Do something with the data in the array with a custom method.
        //        //
        //        ProcessObjects(valueArray);
        //    }
        //}



        /// <summary>
        /// Open the file path received in Excel. Then, open the workbook
        /// within the file. Send the workbook to the next function, the internal scan
        /// function. Will throw an exception if a file cannot be found or opened.
        /// </summary>
        //public void ExcelOpenSpreadsheets(string thisFileName)
        //{
        //    try
        //    {
        //        //
        //        // This mess of code opens an Excel workbook. I don't know what all
        //        // those arguments do, but they can be changed to influence behavior.
        //        //
        //        Workbook workBook = _excelApp.Workbooks.Open(thisFileName,
        //            Type.Missing, Type.Missing, Type.Missing, Type.Missing,
        //            Type.Missing, Type.Missing, Type.Missing, Type.Missing,
        //            Type.Missing, Type.Missing, Type.Missing, Type.Missing,
        //            Type.Missing, Type.Missing);

        //        //
        //        // Pass the workbook to a separate function. This new function
        //        // will iterate through the worksheets in the workbook.
        //        //
        //        ExcelScanIntenal(workBook);

        //        //
        //        // Clean up.
        //        //
        //        workBook.Close(false, thisFileName, null);
        //        Marshal.ReleaseComObject(workBook);
        //    }
        //    catch
        //    {
        //        //
        //        // Deal with exceptions.
        //        //
        //    }
        //}

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: External/NrufCompanyInfoes/GeneralInfo
        public ActionResult GeneralInfo()
        {

            return View();
        }

        // GET: External/NrufCompanyInfoes/Instructions
        public ActionResult Instructions()
        {

            return View();
        }

        // GET: External/NrufCompanyInfoes/ExportToExcel
        public async Task<ActionResult> ExportToExcel(int id)
        {


            // Only view my NRUF forms
            List<NrufCompanyInfo> nrufCompanyIf = db.NrufCompanyInfo.Include(n => n.ProviderService).ToList();

            var nrufCompanyInfo = nrufCompanyIf.Find(d => d.NrufCompanyInfoId == id);

            var user = UserManager.FindById(User.Identity.GetUserId());


            if (nrufCompanyInfo == null)
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }
            if (nrufCompanyInfo.organizationId != user.organizationId)
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            var message = "";
            try
            {

                var output = string.Format("{0}-{1}", nrufCompanyInfo.applicationReference, DateTime.Now.ToString("yyyyMMddhhss"));
                var excelFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Documents\NRUFFormOUR_Mar2016.xlsx");
                var outputexcelFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Heap\" + output + ".xlsx");
                System.IO.File.Copy(excelFile, outputexcelFile, true);
                // Stream stream = System.IO.File.Open(outputexcelFile, FileMode.Open);
                // ExcelOpenSpreadsheets(stream, 10, 13);
                //string result = ExcelAddRows(outputexcelFile);
                //stream.Close();
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.ParentCompany, 8, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.name, 9, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.line1, 10, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.line2, 11, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.line3, 12, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.ContactName, 13, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.ContactTelNo, 14, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.ContactFaxNo, 15, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.ContactEmail, 16, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.ParentCompanyOCN, 8, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.OCN, 9, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.OFN, 10, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.ProviderService.name, 11, "K", CellValues.String);

                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.ParentCompany, 8, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.name, 9, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.line1, 10, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.line2, 11, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.line3, 12, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.ContactName, 13, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.ContactTelNo, 14, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.ContactFaxNo, 15, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.ContactEmail, 16, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.ParentCompanyOCN, 8, "J", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.OCN, 9, "J", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.OFN, 10, "J", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.ProviderService.name, 11, "J", CellValues.String);

                var formf3a = await db.NrufFormF3a.Where(p => p.NrufCompanyInfoId == id).ToListAsync();

                uint count = 22;
                foreach(var item in formf3a)
                {
                    UpdateCell(outputexcelFile, "Form F-3a", item.Npa, count, "B", CellValues.String);
                    UpdateCell(outputexcelFile, "Form F-3a", item.Year1.ToString(), count, "D", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form F-3a", item.Year2.ToString(), count, "E", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form F-3a", item.Year3.ToString(), count, "F", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form F-3a", item.Year4.ToString(), count, "G", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form F-3a", item.Year5.ToString(), count, "H", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form F-3a", item.Total.ToString(), count, "I", CellValues.Number);

                    count++;
                }

                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.ParentCompany, 8, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.name, 9, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.line1, 10, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.line2, 11, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.line3, 12, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.ContactName, 13, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.ContactTelNo, 14, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.ContactFaxNo, 15, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.ContactEmail, 16, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.ParentCompanyOCN, 8, "J", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.OCN, 9, "J", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.OFN, 10, "J", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.ProviderService.name, 11, "J", CellValues.String);

                var formf3b = await db.NrufFormF3b.Where(p => p.NrufCompanyInfoId == id).ToListAsync();

                count = 22;
                foreach (var item in formf3b)
                {
                    UpdateCell(outputexcelFile, "Form F-3b", item.Npa, count, "B", CellValues.String);
                    UpdateCell(outputexcelFile, "Form F-3b", item.Year1.ToString(), count, "D", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form F-3b", item.Year2.ToString(), count, "E", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form F-3b", item.Year3.ToString(), count, "F", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form F-3b", item.Year4.ToString(), count, "G", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form F-3b", item.Year5.ToString(), count, "H", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form F-3b", item.Total.ToString(), count, "I", CellValues.Number);

                    count++;
                }

                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.ParentCompany, 7, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.name, 8, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.line1, 9, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.line2, 10, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.line3, 11, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.ContactName, 12, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.ContactTelNo, 13, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.ContactFaxNo, 14, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.ContactEmail, 15, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.ParentCompanyOCN, 7, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.OCN, 8, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.OFN, 9, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.ProviderService.name, 10, "K", CellValues.String);

                var formU1 = await db.NrufFormU1.Where(p => p.NrufCompanyInfoId == id).ToListAsync();

                count = 21;
                foreach (var item in formU1)
                {
                    UpdateCell(outputexcelFile, "Form U1", item.Npa + "-" + item.Nxx, count, "A", CellValues.String);
                    UpdateCell(outputexcelFile, "Form U1", item.X.ToString(), count, "B", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U1", item.Rate.ToString(), count, "C", CellValues.String);
                    UpdateCell(outputexcelFile, "Form U1", item.Assigned.ToString(), count, "D", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U1", item.Intermediate.ToString(), count, "E", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U1", item.Reserved.ToString(), count, "F", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U1", item.Aging.ToString(), count, "G", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U1", item.Admin.ToString(), count, "H", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U1", item.PortedOut.ToString(), count, "I", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U1", item.Notes.ToString(), count, "J", CellValues.String);
                    UpdateCell(outputexcelFile, "Form U1", item.Available.ToString(), count, "K", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U1", item.Utilization.ToString(), count, "L", CellValues.Number);

                    count++;
                }

                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.ParentCompany, 7, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.name, 8, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.line1, 9, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.line2, 10, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.line3, 11, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.ContactName, 12, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.ContactTelNo, 13, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.ContactFaxNo, 14, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.ContactEmail, 15, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.ParentCompanyOCN, 7, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.OCN, 8, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.OFN, 9, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.ProviderService.name, 10, "K", CellValues.String);

                var formU2 = await db.NrufFormU2.Where(p => p.NrufCompanyInfoId == id).ToListAsync();

                count = 21;
                foreach (var item in formU2)
                {
                    UpdateCell(outputexcelFile, "Form U2", item.Npa + "-" + item.Nxx, count, "A", CellValues.String);
                    UpdateCell(outputexcelFile, "Form U2", item.Rate.ToString(), count, "B", CellValues.String);
                    UpdateCell(outputexcelFile, "Form U2", item.Assigned.ToString(), count, "D", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U2", item.Intermediate.ToString(), count, "E", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U2", item.Reserved.ToString(), count, "F", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U2", item.Aging.ToString(), count, "G", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U2", item.Admin.ToString(), count, "H", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U2", item.PortedOut.ToString(), count, "I", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U2", item.Notes.ToString(), count, "J", CellValues.String);
                    UpdateCell(outputexcelFile, "Form U2", item.Available.ToString(), count, "K", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U2", item.Utilization.ToString(), count, "L", CellValues.Number);


                    count++;
                }


                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.ParentCompany, 7, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.name, 8, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.line1, 9, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.line2, 10, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.line3, 11, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.ContactName, 12, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.ContactTelNo, 13, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.ContactFaxNo, 14, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.ContactEmail, 15, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.ParentCompanyOCN, 7, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.OCN, 8, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.OFN, 9, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.ProviderService.name, 10, "K", CellValues.String);

                var formU3 = await db.NrufFormU3.Where(p => p.NrufCompanyInfoId == id).ToListAsync();

                count = 21;
                foreach (var item in formU3)
                {
                    UpdateCell(outputexcelFile, "Form U3", item.Npa + "-" + item.Nxx, count, "A", CellValues.String);
                    UpdateCell(outputexcelFile, "Form U3", item.X.ToString(), count, "B", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U3", item.Rate.ToString(), count, "C", CellValues.String);
                    UpdateCell(outputexcelFile, "Form U3", item.Assigned.ToString(), count, "D", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U3", item.PortedOut.ToString(), count, "E", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U3", item.Reserved.ToString(), count, "F", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U3", item.Aging.ToString(), count, "G", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U3", item.Admin.ToString(), count, "H", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U3", item.NumbersReceived.ToString(), count, "I", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U3", item.Notes.ToString(), count, "J", CellValues.String);
                    UpdateCell(outputexcelFile, "Form U3", item.Available.ToString(), count, "K", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U3", item.Utilization.ToString(), count, "L", CellValues.Number);

                    count++;
                }

                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.ParentCompany, 7, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.name, 8, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.line1, 9, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.line2, 10, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.line3, 11, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.ContactName, 12, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.ContactTelNo, 13, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.ContactFaxNo, 14, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.ContactEmail, 15, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.ParentCompanyOCN, 7, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.OCN, 8, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.OFN, 9, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.ProviderService.name, 10, "K", CellValues.String);

                var formU4 = await db.NrufFormU4.Where(p => p.NrufCompanyInfoId == id).ToListAsync();

                count = 21;
                foreach (var item in formU4)
                {
                    UpdateCell(outputexcelFile, "Form U4", item.Npa + "-" + item.Nxx, count, "A", CellValues.String);
                    UpdateCell(outputexcelFile, "Form U4", item.Rate.ToString(), count, "B", CellValues.String);
                    UpdateCell(outputexcelFile, "Form U4", item.Assigned.ToString(), count, "D", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U4", item.Aging.ToString(), count, "G", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U4", item.PortedIn.ToString(), count, "I", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U4", item.Notes.ToString(), count, "J", CellValues.String);
                    UpdateCell(outputexcelFile, "Form U4", item.Available.ToString(), count, "K", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U4", item.Utilization.ToString(), count, "L", CellValues.Number);

                    count++;
                }



                // Send File
                string filename = output + ".xlsx";
                byte[] filedata = System.IO.File.ReadAllBytes(outputexcelFile);
                string contentType = MimeMapping.GetMimeMapping(outputexcelFile);

                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = filename,
                    Inline = true,
                };

                Response.AppendHeader("Content-Disposition", cd.ToString());

                return File(filedata, contentType);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            TempData["ErrorMessageExportToExcel"] = message;
            return RedirectToAction("Index", "NrufCompanyInfoes");
        }



        // private string ExcelAddRows(string outputexcelFile)
        // {
        //     using (SpreadsheetDocument document =
        //SpreadsheetDocument.Open(outputexcelFile, false))
        //     {
        //         WorkbookPart workbookPart = document.WorkbookPart;
        //         Workbook workbook = document.WorkbookPart.Workbook;
        //         string sheetName = workbookPart.Workbook.Descendants<Sheet>().ElementAt(1).Name;
        //         IEnumerable<Sheet> sheets = document.WorkbookPart.Workbook.Descendants<Sheet>().Where(s => s.Name == "Form U1");
        //         if (sheets.Count() == 0)
        //         {
        //             // The specified worksheet does not exist.
        //             return "Null";
        //         }
        //         WorksheetPart worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheets.First().Id);
        //         SheetData sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();
        //         var excelRows = sheetData.Descendants<DocumentFormat.OpenXml.Spreadsheet.Row>().ToList();
        //         int rowindex = 10;
        //         foreach (var item in excelRows)
        //         {
        //             //how to write the data in cell

        //             rowindex++;
        //         }



        //         worksheetPart.Worksheet.Save();
        //         workbookPart.Workbook.Save();
        //         document.Close();
        //         //worksheetPart.Worksheet.Save();
        //     }

        //     return "Success";

        // }

        public static void UpdateCell(string docName, string sheetname, string text,
           uint rowIndex, string columnName, CellValues number)
        {
            // Open the document for editing.
            using (SpreadsheetDocument spreadSheet =
                     SpreadsheetDocument.Open(docName, true))
            {
                WorksheetPart worksheetPart =
                      GetWorksheetPartByName(spreadSheet, sheetname);

                if (worksheetPart != null)
                {
                    Cell cell = GetCell(worksheetPart.Worksheet,
                                             columnName, rowIndex);

                    cell.CellValue = new CellValue(text);
                    cell.DataType =
                        new EnumValue<CellValues>(number);

                    // Save the worksheet.
                    worksheetPart.Worksheet.Save();
                }
            }

        }

        private static WorksheetPart
             GetWorksheetPartByName(SpreadsheetDocument document,
             string sheetName)
        {
            IEnumerable<Sheet> sheets =
               document.WorkbookPart.Workbook.GetFirstChild<Sheets>().
               Elements<Sheet>().Where(s => s.Name == sheetName);

            if (sheets.Count() == 0)
            {
                // The specified worksheet does not exist.

                return null;
            }

            string relationshipId = sheets.First().Id.Value;
            WorksheetPart worksheetPart = (WorksheetPart)
                 document.WorkbookPart.GetPartById(relationshipId);
            return worksheetPart;

        }



        // Given a worksheet, a column name, and a row index,
        // gets the cell at the specified column and
        private static Cell GetCell(Worksheet worksheet,
                  string columnName, uint rowIndex)
        {
            Row row = GetRow(worksheet, rowIndex);

            if (row == null)
                return null;

            return row.Elements<Cell>().Where(c => string.Compare
                   (c.CellReference.Value, columnName +
                   rowIndex, true) == 0).First();
        }


        // Given a worksheet and a row index, return the row.
        private static Row GetRow(Worksheet worksheet, uint rowIndex)
        {
            return worksheet.GetFirstChild<SheetData>().
              Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
        }



        //private void ExcelOpenSpreadsheets(Stream stream, int numRows, int numCols)
        //{

        //    // http://forums.asp.net/t/1984834.aspx?Add+data+to+Existing+OpenXML+Spreadsheets
        //    // http://www.codeproject.com/Questions/321691/How-to-add-rows-in-excel-sheet-using-OpenXML
        //    // http://technet.weblineindia.com/web/export-data-to-excel-using-openxml-sdk/2/
        //    // http://stackoverflow.com/questions/32308023/add-cell-and-row-in-openxml

        //    using (SpreadsheetDocument myDoc = SpreadsheetDocument.Open(stream, true))
        //    {
        //        WorkbookPart workbookPart = myDoc.WorkbookPart;
        //        WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
        //        string originalSheetId = workbookPart.GetIdOfPart(worksheetPart);

        //        WorksheetPart replacementPart = workbookPart.AddNewPart<WorksheetPart>();
        //        string replacementPartId = workbookPart.GetIdOfPart(replacementPart);

        //        OpenXmlReader reader = OpenXmlReader.Create(worksheetPart);
        //        OpenXmlWriter writer = OpenXmlWriter.Create(replacementPart);

        //        Row r = new Row();
        //        Cell c = new Cell();
        //        CellFormula f = new CellFormula();
        //        f.CalculateCell = true;
        //        f.Text = "RAND()";
        //        c.Append(f);
        //        CellValue v = new CellValue();
        //        c.Append(v);

        //        while(reader.Read())
        //        {
        //            if(reader.ElementType == typeof(SheetData))
        //            {
        //                if (reader.IsEndElement)
        //                    continue;
        //                writer.WriteStartElement(new SheetData());

        //                for(int row = 0; row < numRows; row++)
        //                {
        //                    writer.WriteStartElement(r);
        //                    for(int col = 0; col < numCols; col++)
        //                    {
        //                        writer.WriteElement(c);
        //                    }
        //                    writer.WriteEndElement();
        //                }

        //                writer.WriteEndElement();
        //            }
        //            else
        //            {
        //                if (reader.IsStartElement)
        //                {
        //                    writer.WriteStartElement(reader);
        //                }
        //                else if (reader.IsEndElement)
        //                {
        //                    writer.WriteEndElement();
        //                }
        //            }
        //        }
        //        reader.Close();
        //        writer.Close();

        //        Sheet sheet = workbookPart.Workbook.Descendants<Sheet>().Where(s => s.Id.Value.Equals(originalSheetId)).First();
        //        sheet.Id.Value = replacementPartId;
        //        workbookPart.DeletePart(worksheetPart);
        //    }
        //}


        public string GenerateRandomNo()
        {
            return _random.Next(0, 9999).ToString("D4");
        }

        public string GenerateDateForApplRef()
        {
            DateTime _now = DateTime.Now;
            return _now.ToString("yyyyMMdd");
        }


        // GET: External/NrufCompanyInfoes
        public async Task<ActionResult> Index()
        {

            // Get User Id


            ViewBag.message = TempData["ErrorMessageExportToExcel"];
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (Session["UserId"] == null)
            {

                Session["UserId"] = user.Id;
            }
            var nrufCompanyInfo = db.NrufCompanyInfo.Include(n => n.ProviderService);
            var nruf = nrufCompanyInfo.Where(p => p.organizationId == user.organizationId);
            return View(await nruf.ToListAsync());
        }

        // GET: External/NrufCompanyInfoes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NrufCompanyInfo nrufCompanyInfo = await db.NrufCompanyInfo.FindAsync(id);
            if (nrufCompanyInfo == null)
            {
                return HttpNotFound();
            }

           var user = UserManager.FindById(User.Identity.GetUserId());

            if (nrufCompanyInfo.organizationId != user.organizationId)
            {
                return HttpNotFound();
            }

            return View(nrufCompanyInfo);
        }

        // GET: External/NrufCompanyInfoes/Create
        public ActionResult Create()
        {
            // add default data to Company Information
            var user = UserManager.FindById(User.Identity.GetUserId());

            var organization = db.Organizations.Where(o => o.organizationId.Equals(user.organizationId)).FirstOrDefault();
            var defaultAddress = db.Addresses.Where(d => d.organizationId == user.organizationId && d.defaultAddress == true).FirstOrDefault();
            var primaryContact = db.Users.Where(d => d.organizationId == user.organizationId && d.primaryContact == true).FirstOrDefault();

            var companyInfo = new NrufCompanyInfo();

            companyInfo.name = organization.name;
            companyInfo.ParentCompany = organization.ParentCompany;
            companyInfo.ParentCompanyOCN = organization.ParentCompanyOCN;
            companyInfo.OCN = organization.OCN;
            companyInfo.OFN = organization.OFN;
            companyInfo.ContactName = primaryContact.name;
            companyInfo.ContactEmail = primaryContact.Email;
            companyInfo.ContactTelNo = primaryContact.PhoneNumber;
            companyInfo.line1 = defaultAddress.line1;
            companyInfo.line2 = defaultAddress.line2;
            companyInfo.line3 = defaultAddress.city;

            if(!string.IsNullOrEmpty(defaultAddress.zipCode))
                companyInfo.line3 += " " + defaultAddress.zipCode;

            ViewBag.ProviderServiceId = new SelectList(db.ProviderServices, "ProviderServiceId", "name");
            ViewBag.appRef = string.Format("NRUF-{0}-{1}", GenerateDateForApplRef(), GenerateRandomNo());
            return View(companyInfo);
        }

        // POST: External/NrufCompanyInfoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "NrufCompanyInfoId,applicationReference,ProviderServiceId,name,ParentCompany,ParentCompanyOCN,OCN,OFN,line1,line2,line3,ContactName,ContactTelNo,ContactFaxNo,ContactEmail,Status")] NrufCompanyInfo nrufCompanyInfo)
        {
            if (ModelState.IsValid)
            {



                var time = DateTime.Now;

                var user = UserManager.FindById(User.Identity.GetUserId());
                nrufCompanyInfo.organizationId = user.organizationId;
                nrufCompanyInfo.UserId = user.Id;

                nrufCompanyInfo.createdDate = time;
                nrufCompanyInfo.dateChanged = time;

                var AmtExpDays = db.SystemParameters.Where(d => d.name == "NRUF Edit Time Frame (Days)").FirstOrDefault();
                if (AmtExpDays.value.All(char.IsDigit))
                {
                    nrufCompanyInfo.expiryDate = time.AddBusinessDays(Convert.ToInt32(AmtExpDays.value));
                }
                else
                {
                    nrufCompanyInfo.expiryDate = time.AddBusinessDays(5);
                }
                nrufCompanyInfo.Status = "In Progress";

                var regex = new Regex(@"^NRUF-[0-9]{8}-[0-9]{4}$");
                var match = regex.Match(nrufCompanyInfo.applicationReference);
                if (!match.Success)
                {
                    nrufCompanyInfo.applicationReference = string.Format("NRUF-{0}-{1}", GenerateDateForApplRef(), GenerateRandomNo());
                }



                db.NrufCompanyInfo.Add(nrufCompanyInfo);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.ProviderServiceId = new SelectList(db.ProviderServices, "ProviderServiceId", "name", nrufCompanyInfo.ProviderServiceId);
            return View(nrufCompanyInfo);
        }

        // GET: External/NrufCompanyInfoes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // Only view my NRUF forms
            NrufCompanyInfo nrufCompanyInfo = await db.NrufCompanyInfo.FindAsync(id);

            var user = UserManager.FindById(User.Identity.GetUserId());


            if (nrufCompanyInfo == null)
            {
                return HttpNotFound();
            }

            if (nrufCompanyInfo.organizationId != user.organizationId)
            {
                return HttpNotFound();
            }

            ViewBag.ProviderServiceId = new SelectList(db.ProviderServices, "ProviderServiceId", "name", nrufCompanyInfo.ProviderServiceId);
            return View(nrufCompanyInfo);
        }

        // POST: External/NrufCompanyInfoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "NrufCompanyInfoId,organizationId,UserId,ProviderServiceId,name,ParentCompany,ParentCompanyOCN,OCN,OFN,line1,line2,line3,ContactName,ContactTelNo,ContactFaxNo,ContactEmail,Status")] NrufCompanyInfo nrufCompanyInfo)
        {

            // Check it

            if (nrufCompanyInfo == null)
            {
                ViewBag.ProviderServiceId = new SelectList(db.ProviderServices, "ProviderServiceId", "name", nrufCompanyInfo.ProviderServiceId);
                return View(nrufCompanyInfo);
            }

            NrufCompanyInfo nrufCompanyInfoVal = await db.NrufCompanyInfo.FindAsync(nrufCompanyInfo.NrufCompanyInfoId);
            if (nrufCompanyInfoVal != null)
            {
                if (nrufCompanyInfoVal.expiryDate < DateTime.Now)
                {
                    return RedirectToAction("Index");
                }
            }
            else
            {
                return RedirectToAction("Index");
            }

            // Only view my NRUF forms



            var user = UserManager.FindById(User.Identity.GetUserId());

            if (nrufCompanyInfo.organizationId != user.organizationId || nrufCompanyInfo.organizationId == 0)
            {
                ViewBag.ProviderServiceId = new SelectList(db.ProviderServices, "ProviderServiceId", "name", nrufCompanyInfo.ProviderServiceId);
                return View(nrufCompanyInfo);
            }


            if (ModelState.IsValid)
            {
                var time = DateTime.Now;

                nrufCompanyInfo.organizationId = nrufCompanyInfoVal.organizationId;
                nrufCompanyInfo.UserId = nrufCompanyInfoVal.UserId;
                nrufCompanyInfo.Status = nrufCompanyInfoVal.Status;
                nrufCompanyInfo.createdDate = nrufCompanyInfoVal.createdDate;
                nrufCompanyInfo.dateChanged = time;
                nrufCompanyInfo.expiryDate = nrufCompanyInfoVal.expiryDate;
                nrufCompanyInfo.applicationReference = nrufCompanyInfoVal.applicationReference;

                Detach(nrufCompanyInfoVal);
                //  Update(nrufCompanyInfo);
                db.Entry(nrufCompanyInfo).State = EntityState.Modified;
                await db.SaveChangesAsync();

                return RedirectToAction("Index");
            }
            ViewBag.ProviderServiceId = new SelectList(db.ProviderServices, "ProviderServiceId", "name", nrufCompanyInfo.ProviderServiceId);
            return View(nrufCompanyInfo);
        }

        // GET: External/NrufCompanyInfoes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // Only view my NRUF forms
            NrufCompanyInfo nrufCompanyInfo = await db.NrufCompanyInfo.FindAsync(id);

            if (nrufCompanyInfo == null)
            {
                return HttpNotFound();
            }

            var user = UserManager.FindById(User.Identity.GetUserId());

            if (nrufCompanyInfo.organizationId != user.organizationId)
            {
                return HttpNotFound();
            }



            return View(nrufCompanyInfo);
        }

        // POST: External/NrufCompanyInfoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {


            // Only view my NRUF forms
            NrufCompanyInfo nrufCompanyInfo = await db.NrufCompanyInfo.FindAsync(id);

            if (nrufCompanyInfo == null)
            {
                return HttpNotFound();
            }

            if (nrufCompanyInfo.expiryDate < DateTime.Now)
            {
                return RedirectToAction("Index");
            }

            var user = UserManager.FindById(User.Identity.GetUserId());

            if (nrufCompanyInfo.organizationId != user.organizationId)
            {
                return HttpNotFound();
            }


            db.NrufCompanyInfo.Remove(nrufCompanyInfo);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //async void Update(NrufCompanyInfo obj)
        //{
        //    db.Entry(obj).State = EntityState.Modified;
        //    await db.SaveChangesAsync();
        //}

        void Update(NrufCompanyInfo obj)
        {
            db.Entry(obj).State = EntityState.Modified;
            db.SaveChanges();
        }

        void Detach(NrufCompanyInfo obj)
        {
            db.Entry(obj).State = EntityState.Detached;
        }
    }
}
