﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;

namespace jnas.Areas.External.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "PrimaryExternalUser, SecondaryExternalUser, ReadOnlyUser, Administrator")]
    public class NrufFormU4Controller : Controller
    {
        private UserManager _userManager;
        private Jnasdb db = new Jnasdb();

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: External/NrufFormU4
        public async Task<ActionResult> Index(int? id)
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            if (id != null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == id && d.organizationId == user.organizationId).FirstOrDefaultAsync();

                if (nrufcompinfo != null)
                {
                    Session["NrufCompanyInfo"] = id;
                    Session["AppRef"] = nrufcompinfo.applicationReference;

                    if (!CheckExpiryDate())
                    {
                        return RedirectToAction("Index", "NrufCompanyInfoes");
                    }
                    var nrufFormU4 = await (from d in db.NrufFormU4 where d.NrufCompanyInfoId == id select d).ToListAsync();
                    return View(nrufFormU4);

                }
            }



            // return View("Index", "NrufCompanyInfoes"); view viewName and masterLayout
            return RedirectToAction("Index", "NrufCompanyInfoes");
        }

        // GET: External/NrufFormU4/Details/5
        public async Task<ActionResult> Details(int? id)
        {

            if (Session["NrufCompanyInfo"] == null)
                return RedirectToAction("Index", "NrufCompanyInfoes");

            var compInfo = (int)Session["NrufCompanyInfo"];

            if (!CheckExpiryDate())
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            if (id != null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());

                var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo && d.organizationId == user.organizationId).FirstOrDefaultAsync();

                if (nrufcompinfo == null)
                    return RedirectToAction("Index", "NrufCompanyInfoes");

                NrufFormU4 nrufFormU4 = await db.NrufFormU4.FindAsync(id);
                if (nrufFormU4 == null)
                {
                    return HttpNotFound();
                }
                return View(nrufFormU4);

            }



            // return View("Index", "NrufCompanyInfoes"); view viewName and masterLayout
            return RedirectToAction("Index", "NrufCompanyInfoes");


        }

        // GET: External/NrufFormU4/Create
        public ActionResult Create()
        {
            if (Session["NrufCompanyInfo"] == null)
                return RedirectToAction("Index", "NrufCompanyInfoes");

            if (!CheckExpiryDate())
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            // ViewBag.NrufCompanyInfoId = new SelectList(db.NrufCompanyInfo, "NrufCompanyInfoId", "UserId");
            return View();
        }

        // POST: External/NrufFormU4/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "FormU4Id,NrufCompanyInfoId,Npa,Nxx,Rate,Assigned,Aging,PortedIn,Notes")] NrufFormU4 nrufFormU4)
        {

            try
            {

                if (Session["NrufCompanyInfo"] == null)
                    return RedirectToAction("Index", "NrufCompanyInfoes");

                var compInfo = (int)Session["NrufCompanyInfo"];

                if (!CheckExpiryDate())
                {
                    return RedirectToAction("Index", "NrufCompanyInfoes");
                }

                if (ModelState.IsValid)
                {
                    var user = UserManager.FindById(User.Identity.GetUserId());

                    var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo && d.organizationId == user.organizationId).FirstOrDefaultAsync();

                    if (nrufcompinfo == null)
                        return RedirectToAction("Index", "NrufCompanyInfoes");

                    string message;
                    var error = nrufFormU4.Calculate(out message);
                    if (error == 0)
                    {
                        throw new Exception(message);
                    }
                    nrufFormU4.NrufCompanyInfoId = compInfo;

                    var time = DateTime.Now;
                    nrufFormU4.createdDate = time;
                    nrufFormU4.dateChanged = time;

                    db.NrufFormU4.Add(nrufFormU4);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index", new { id = compInfo });
                }

            }
            catch (Exception dex)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator. " + dex.Message);
            }
            // ViewBag.NrufCompanyInfoId = new SelectList(db.NrufCompanyInfo, "NrufCompanyInfoId", "UserId", nrufFormU4.NrufCompanyInfoId);
            return View(nrufFormU4);
        }

        // GET: External/NrufFormU4/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {

            if (Session["NrufCompanyInfo"] == null)
                return RedirectToAction("Index", "NrufCompanyInfoes");

            var compInfo = (int)Session["NrufCompanyInfo"];

            if (!CheckExpiryDate())
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            if (id != null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());

                var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo && d.organizationId == user.organizationId).FirstOrDefaultAsync();

                if (nrufcompinfo == null)
                    return RedirectToAction("Index", "NrufCompanyInfoes");

                NrufFormU4 nrufFormU4 = await db.NrufFormU4.FindAsync(id);
                if (nrufFormU4 == null)
                {
                    return HttpNotFound();
                }
                return View(nrufFormU4);

            }



            // return View("Index", "NrufCompanyInfoes"); view viewName and masterLayout
            return RedirectToAction("Index", "NrufCompanyInfoes");
        }

        // POST: External/NrufFormU4/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = " FormU4Id,NrufCompanyInfoId,Npa,Nxx,Rate,Assigned,Aging,PortedIn,Notes")] NrufFormU4 nrufFormU4)
        {

            try
            {

                if (Session["NrufCompanyInfo"] == null)
                    return RedirectToAction("Index", "NrufCompanyInfoes");

                var compInfo = (int)Session["NrufCompanyInfo"];

                if (!CheckExpiryDate())
                {
                    return RedirectToAction("Index", "NrufCompanyInfoes");
                }

                if (ModelState.IsValid)
                {
                    var user = UserManager.FindById(User.Identity.GetUserId());

                    var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo && d.organizationId == user.organizationId).FirstOrDefaultAsync();

                    if (nrufcompinfo == null)
                        return RedirectToAction("Index", "NrufCompanyInfoes");

                    NrufFormU4 nrufFormU4Val = await db.NrufFormU4.FindAsync(nrufFormU4.FormU4Id);

                    string message;
                    var error = nrufFormU4.Calculate(out message);
                    if (error == 0)
                    {
                        throw new Exception(message);
                    }

                    if (compInfo != nrufFormU4.NrufCompanyInfoId)
                    {
                        if (compInfo != nrufFormU4Val.NrufCompanyInfoId)
                        {
                            nrufFormU4.NrufCompanyInfoId = nrufFormU4Val.NrufCompanyInfoId;
                            Session["NrufCompanyInfo"] = nrufFormU4Val.NrufCompanyInfoId;
                        }
                        else
                        {
                            nrufFormU4.NrufCompanyInfoId = compInfo;
                        }
                    }


                    var time = DateTime.Now;
                    nrufFormU4.createdDate = nrufFormU4Val.createdDate;
                    nrufFormU4.dateChanged = time;

                    Detach(nrufFormU4Val);
                    db.Entry(nrufFormU4).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index", new { id = compInfo });
                }

            }
            catch (Exception dex)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator. " + dex.Message);
            }

            return View(nrufFormU4);


        }

        // GET: External/NrufFormU4/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {

            if (Session["NrufCompanyInfo"] == null)
                return RedirectToAction("Index", "NrufCompanyInfoes");

            var compInfo = (int)Session["NrufCompanyInfo"];

            if (!CheckExpiryDate())
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            if (id != null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());

                var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo && d.organizationId == user.organizationId).FirstOrDefaultAsync();

                if (nrufcompinfo == null)
                    return RedirectToAction("Index", "NrufCompanyInfoes");

                NrufFormU4 nrufFormU4 = await db.NrufFormU4.FindAsync(id);
                if (nrufFormU4 == null)
                {
                    return HttpNotFound();
                }
                return View(nrufFormU4);

            }



            // return View("Index", "NrufCompanyInfoes"); view viewName and masterLayout
            return RedirectToAction("Index", "NrufCompanyInfoes");
        }

        // POST: External/NrufFormU4/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            // Only view my NRUF forms
            NrufFormU4 nrufFormU4 = await db.NrufFormU4.FindAsync(id);

            if (nrufFormU4 == null)
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            if ((int)Session["NrufCompanyInfo"] != nrufFormU4.NrufCompanyInfoId)
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            if (!CheckExpiryDate())
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            db.NrufFormU4.Remove(nrufFormU4);
            await db.SaveChangesAsync();
            var compInfo = (int)Session["NrufCompanyInfo"];
            return RedirectToAction("Index", new { id = compInfo });
        }

        bool CheckExpiryDate()
        {
            // CHeck the Expiry Date
            NrufCompanyInfo nrufCompanyInfo = db.NrufCompanyInfo.Find((int)Session["NrufCompanyInfo"]);

            if (nrufCompanyInfo != null)
            {
                if (nrufCompanyInfo.expiryDate < DateTime.Now)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

            return true;
        }

        void Update(NrufFormU4 obj)
        {
            db.Entry(obj).State = EntityState.Modified;
            db.SaveChanges();
        }

        void Detach(NrufFormU4 obj)
        {
            db.Entry(obj).State = EntityState.Detached;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
