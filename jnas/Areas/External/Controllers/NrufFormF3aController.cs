﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;


namespace jnas.Areas.External.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "PrimaryExternalUser, SecondaryExternalUser, ReadOnlyUser, Administrator")]
    public class NrufFormF3aController : Controller
    {
        private UserManager _userManager;
        private Jnasdb db = new Jnasdb();

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: External/NrufFormF3a
        public async Task<ActionResult> Index(int? id)
        {

            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            if (id != null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == id && d.organizationId == user.organizationId).FirstOrDefaultAsync();

                if (nrufcompinfo != null)
                {
                    Session["NrufCompanyInfo"] = id;
                    Session["AppRef"] = nrufcompinfo.applicationReference;

                    if (!CheckExpiryDate())
                    {
                        return RedirectToAction("Index", "NrufCompanyInfoes");
                    }

                    var nrufFormF3a = await (from d in db.NrufFormF3a where d.NrufCompanyInfoId == id select d).ToListAsync();
                    return View(nrufFormF3a);
                }
            }



            // return View("Index", "NrufCompanyInfoes"); view viewName and masterLayout
            return RedirectToAction("Index", "NrufCompanyInfoes");
        }

        // GET: External/NrufFormF3a/Details/5
        public async Task<ActionResult> Details(int? id)
        {

            if(Session["NrufCompanyInfo"] == null)
                return RedirectToAction("Index", "NrufCompanyInfoes");

            var compInfo = (int)Session["NrufCompanyInfo"];

            if (!CheckExpiryDate())
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            if (id != null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());

                var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo && d.organizationId == user.organizationId).FirstOrDefaultAsync();

                if (nrufcompinfo == null)
                    return RedirectToAction("Index", "NrufCompanyInfoes");

                NrufFormF3a nrufFormF3a = await db.NrufFormF3a.FindAsync(id);
                if (nrufFormF3a == null)
                {
                    return HttpNotFound();
                }
                return View(nrufFormF3a);

            }



            // return View("Index", "NrufCompanyInfoes"); view viewName and masterLayout
            return RedirectToAction("Index", "NrufCompanyInfoes");


        }

        // GET: External/NrufFormF3a/Create
        public ActionResult Create()
        {
            if (Session["NrufCompanyInfo"] == null)
                return RedirectToAction("Index", "NrufCompanyInfoes");

            var compInfo = (int)Session["NrufCompanyInfo"];

            if (!CheckExpiryDate())
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            // ViewBag.NrufCompanyInfoId = new SelectList(db.NrufCompanyInfo, "NrufCompanyInfoId", "UserId");
            return View();
        }

        // POST: External/NrufFormF3a/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Npa,Year1,Year2,Year3,Year4,Year5")] NrufFormF3a nrufFormF3a)
        {

            try {

                        if (Session["NrufCompanyInfo"] == null)
                            return RedirectToAction("Index", "NrufCompanyInfoes");

                var compInfo = (int)Session["NrufCompanyInfo"];

                if (!CheckExpiryDate())
                {
                    return RedirectToAction("Index", "NrufCompanyInfoes");
                }

                if (ModelState.IsValid)
                        {
                            var user = UserManager.FindById(User.Identity.GetUserId());

                            var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo && d.organizationId == user.organizationId).FirstOrDefaultAsync();

                            if (nrufcompinfo == null)
                                return RedirectToAction("Index", "NrufCompanyInfoes");

                                string message;
                                var error = nrufFormF3a.Calculate(out message);
                                if (error == 0)
                                {
                                    throw new Exception(message);
                                }
                                nrufFormF3a.NrufCompanyInfoId = compInfo;

                                var time = DateTime.Now;
                                nrufFormF3a.createdDate = time;
                                nrufFormF3a.dateChanged = time;

                            db.NrufFormF3a.Add(nrufFormF3a);
                            await db.SaveChangesAsync();
                            return RedirectToAction("Index", new { id = compInfo });
                }

            }
            catch (Exception dex)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator. " + dex.Message);
            }
            // ViewBag.NrufCompanyInfoId = new SelectList(db.NrufCompanyInfo, "NrufCompanyInfoId", "UserId", nrufFormF3a.NrufCompanyInfoId);
            return View(nrufFormF3a);
        }

        // GET: External/NrufFormF3a/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {

            if (Session["NrufCompanyInfo"] == null)
                return RedirectToAction("Index", "NrufCompanyInfoes");

            var compInfo = (int)Session["NrufCompanyInfo"];

            if (!CheckExpiryDate())
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            if (id != null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());

                var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo && d.organizationId == user.organizationId).FirstOrDefaultAsync();

                if (nrufcompinfo == null)
                    return RedirectToAction("Index", "NrufCompanyInfoes");

                NrufFormF3a nrufFormF3a = await db.NrufFormF3a.FindAsync(id);
                if (nrufFormF3a == null)
                {
                    return HttpNotFound();
                }
                return View(nrufFormF3a);

            }



            // return View("Index", "NrufCompanyInfoes"); view viewName and masterLayout
            return RedirectToAction("Index", "NrufCompanyInfoes");
         }

        // POST: External/NrufFormF3a/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "FormF3aId,NrufCompanyInfoId,Npa,Year1,Year2,Year3,Year4,Year5")] NrufFormF3a nrufFormF3a)
        {

            try
            {

                if (Session["NrufCompanyInfo"] == null)
                    return RedirectToAction("Index", "NrufCompanyInfoes");

                var compInfo = (int)Session["NrufCompanyInfo"];

                if (!CheckExpiryDate())
                {
                    return RedirectToAction("Index", "NrufCompanyInfoes");
                }

                if (ModelState.IsValid)
                {
                    var user = UserManager.FindById(User.Identity.GetUserId());

                    var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo && d.organizationId == user.organizationId).FirstOrDefaultAsync();

                    if (nrufcompinfo == null)
                        return RedirectToAction("Index", "NrufCompanyInfoes");

                    NrufFormF3a nrufFormF3aVal = await db.NrufFormF3a.FindAsync(nrufFormF3a.FormF3aId);

                    string message;
                    var error = nrufFormF3a.Calculate(out message);
                    if (error == 0)
                    {
                        throw new Exception(message);
                    }

                    if (compInfo != nrufFormF3a.NrufCompanyInfoId)
                    {
                        if (compInfo != nrufFormF3aVal.NrufCompanyInfoId)
                        {
                            nrufFormF3a.NrufCompanyInfoId = nrufFormF3aVal.NrufCompanyInfoId;
                            Session["NrufCompanyInfo"] = nrufFormF3aVal.NrufCompanyInfoId;
                        }
                        else
                        {
                            nrufFormF3a.NrufCompanyInfoId = compInfo;
                        }
                    }


                    var time = DateTime.Now;
                    nrufFormF3a.createdDate = nrufFormF3aVal.createdDate;
                    nrufFormF3a.dateChanged = time;

                    Detach(nrufFormF3aVal);
                    db.Entry(nrufFormF3a).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index", new { id = compInfo });
                }

            }
            catch (Exception dex)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator. " + dex.Message);
            }

            return View(nrufFormF3a);


        }

        // GET: External/NrufFormF3a/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {

            if (Session["NrufCompanyInfo"] == null)
                return RedirectToAction("Index", "NrufCompanyInfoes");

            var compInfo = (int)Session["NrufCompanyInfo"];

            if (!CheckExpiryDate())
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            if (id != null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());

                var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo && d.organizationId == user.organizationId).FirstOrDefaultAsync();

                if (nrufcompinfo == null)
                    return RedirectToAction("Index", "NrufCompanyInfoes");

                NrufFormF3a nrufFormF3a = await db.NrufFormF3a.FindAsync(id);
                if (nrufFormF3a == null)
                {
                    return HttpNotFound();
                }
                return View(nrufFormF3a);

            }



            // return View("Index", "NrufCompanyInfoes"); view viewName and masterLayout
            return RedirectToAction("Index", "NrufCompanyInfoes");
        }

        // POST: External/NrufFormF3a/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            // Only view my NRUF forms
            NrufFormF3a nrufFormF3a = await db.NrufFormF3a.FindAsync(id);

            if (nrufFormF3a == null)
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            if ((int)Session["NrufCompanyInfo"] != nrufFormF3a.NrufCompanyInfoId)
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            if (!CheckExpiryDate()) {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            db.NrufFormF3a.Remove(nrufFormF3a);
            await db.SaveChangesAsync();
            var compInfo = (int)Session["NrufCompanyInfo"];
            return RedirectToAction("Index", new { id = compInfo });
        }

       bool CheckExpiryDate()
        {
            // CHeck the Expiry Date
            NrufCompanyInfo nrufCompanyInfo = db.NrufCompanyInfo.Find((int)Session["NrufCompanyInfo"]);

            if (nrufCompanyInfo != null)
            {
                if (nrufCompanyInfo.expiryDate < DateTime.Now)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

            return true;
        }

        void Update(NrufFormF3a obj)
        {
            db.Entry(obj).State = EntityState.Modified;
            db.SaveChanges();
        }

        void Detach(NrufFormF3a obj)
        {
            db.Entry(obj).State = EntityState.Detached;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
