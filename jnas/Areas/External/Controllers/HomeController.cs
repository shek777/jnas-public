﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.External.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "PrimaryExternalUser, SecondaryExternalUser, ReadOnlyUser, Administrator")]
    public class HomeController : Controller
    {

        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public HomeController()
        {
            // Get User Id
            // var user = UserManager.FindById(User.Identity.GetUserId());
            // Session["UserId"] = user.Id;


        }

        // GET: External/Home
        public ActionResult Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            return View();
        }

        // GET: External/Organization
        public ActionResult Organization()
        {
            return View();
        }

        // GET: External/Users
        public ActionResult Users()
        {
            return View();
        }

        // GET: External/Notifications
        public ActionResult Notifications()
        {
            return View();
        }

        // GET: External/NRUF
        public ActionResult NRUF()
        {
            return View();
        }

        // GET: External/Applications
        public ActionResult Applications()
        {
            return View();
        }

        // GET: External/Reports
        public ActionResult Reports()
        {
            return View();
        }

        // GET: External/SystemMessages
        public ActionResult SystemMessages()
        {
            return View();
        }

        // GET: External/Mails
        public ActionResult Mail()
        {
            return View();
        }
    }
}