﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;

namespace jnas.Areas.External.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "PrimaryExternalUser, SecondaryExternalUser, ReadOnlyUser, Administrator")]
    public class NrufFormU1Controller : Controller
    {
        private UserManager _userManager;
        private Jnasdb db = new Jnasdb();

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: External/NrufFormU1
        public async Task<ActionResult> Index(int? id)
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            if (id != null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == id && d.organizationId == user.organizationId).FirstOrDefaultAsync();

                if (nrufcompinfo != null)
                {
                    Session["NrufCompanyInfo"] = id;
                    Session["AppRef"] = nrufcompinfo.applicationReference;

                    if (!CheckExpiryDate())
                    {
                        return RedirectToAction("Index", "NrufCompanyInfoes");
                    }
                    var nrufFormU1 = await (from d in db.NrufFormU1 where d.NrufCompanyInfoId == id select d).ToListAsync();
                    return View(nrufFormU1);
                }
            }



            // return View("Index", "NrufCompanyInfoes"); view viewName and masterLayout
            return RedirectToAction("Index", "NrufCompanyInfoes");
        }

        // GET: External/NrufFormU1/Details/5
        public async Task<ActionResult> Details(int? id)
        {

            if (Session["NrufCompanyInfo"] == null)
                return RedirectToAction("Index", "NrufCompanyInfoes");

            var compInfo = (int)Session["NrufCompanyInfo"];

            if (!CheckExpiryDate())
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            if (id != null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());

                var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo && d.organizationId == user.organizationId).FirstOrDefaultAsync();

                if (nrufcompinfo == null)
                    return RedirectToAction("Index", "NrufCompanyInfoes");

                NrufFormU1 nrufFormU1 = await db.NrufFormU1.FindAsync(id);
                if (nrufFormU1 == null)
                {
                    return HttpNotFound();
                }
                return View(nrufFormU1);

            }



            // return View("Index", "NrufCompanyInfoes"); view viewName and masterLayout
            return RedirectToAction("Index", "NrufCompanyInfoes");


        }

        // GET: External/NrufFormU1/Create
        public ActionResult Create()
        {
            if (Session["NrufCompanyInfo"] == null)
                return RedirectToAction("Index", "NrufCompanyInfoes");

            if (!CheckExpiryDate())
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            // ViewBag.NrufCompanyInfoId = new SelectList(db.NrufCompanyInfo, "NrufCompanyInfoId", "UserId");
            return View();
        }

        // POST: External/NrufFormU1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "FormU1Id,NrufCompanyInfoId,Npa,Nxx,X,Rate,Assigned,Intermediate,Reserved,Aging,Admin,PortedOut,Notes")] NrufFormU1 nrufFormU1)
        {

            try
            {

                if (Session["NrufCompanyInfo"] == null)
                    return RedirectToAction("Index", "NrufCompanyInfoes");

                var compInfo = (int)Session["NrufCompanyInfo"];

                if (!CheckExpiryDate())
                {
                    return RedirectToAction("Index", "NrufCompanyInfoes");
                }

                if (ModelState.IsValid)
                {
                    var user = UserManager.FindById(User.Identity.GetUserId());

                    var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo && d.organizationId == user.organizationId).FirstOrDefaultAsync();

                    if (nrufcompinfo == null)
                        return RedirectToAction("Index", "NrufCompanyInfoes");

                    string message;
                    var error = nrufFormU1.Calculate(out message);
                    if (error == 0)
                    {
                        throw new Exception(message);
                    }
                    nrufFormU1.NrufCompanyInfoId = compInfo;

                    var time = DateTime.Now;
                    nrufFormU1.createdDate = time;
                    nrufFormU1.dateChanged = time;

                    db.NrufFormU1.Add(nrufFormU1);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index", new { id = compInfo });
                }

            }
            catch (Exception dex)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator. " + dex.Message);
            }
            // ViewBag.NrufCompanyInfoId = new SelectList(db.NrufCompanyInfo, "NrufCompanyInfoId", "UserId", nrufFormU1.NrufCompanyInfoId);
            return View(nrufFormU1);
        }

        // GET: External/NrufFormU1/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {

            if (Session["NrufCompanyInfo"] == null)
                return RedirectToAction("Index", "NrufCompanyInfoes");

            var compInfo = (int)Session["NrufCompanyInfo"];

            if (!CheckExpiryDate())
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            if (id != null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());

                var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo && d.organizationId == user.organizationId).FirstOrDefaultAsync();

                if (nrufcompinfo == null)
                    return RedirectToAction("Index", "NrufCompanyInfoes");

                NrufFormU1 nrufFormU1 = await db.NrufFormU1.FindAsync(id);
                if (nrufFormU1 == null)
                {
                    return HttpNotFound();
                }
                return View(nrufFormU1);

            }



            // return View("Index", "NrufCompanyInfoes"); view viewName and masterLayout
            return RedirectToAction("Index", "NrufCompanyInfoes");
        }

        // POST: External/NrufFormU1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "FormU1Id,NrufCompanyInfoId,Npa,Nxx,X,Rate,Assigned,Intermediate,Reserved,Aging,Admin,PortedOut,Notes")] NrufFormU1 nrufFormU1)
        {

            try
            {

                if (Session["NrufCompanyInfo"] == null)
                    return RedirectToAction("Index", "NrufCompanyInfoes");

                var compInfo = (int)Session["NrufCompanyInfo"];

                if (!CheckExpiryDate())
                {
                    return RedirectToAction("Index", "NrufCompanyInfoes");
                }

                if (ModelState.IsValid)
                {
                    var user = UserManager.FindById(User.Identity.GetUserId());

                    var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo && d.organizationId == user.organizationId).FirstOrDefaultAsync();

                    if (nrufcompinfo == null)
                        return RedirectToAction("Index", "NrufCompanyInfoes");

                    NrufFormU1 nrufFormU1Val = await db.NrufFormU1.FindAsync(nrufFormU1.FormU1Id);

                    string message;
                    var error = nrufFormU1.Calculate(out message);
                    if (error == 0)
                    {
                        throw new Exception(message);
                    }

                    if (compInfo != nrufFormU1.NrufCompanyInfoId)
                    {
                        if (compInfo != nrufFormU1Val.NrufCompanyInfoId)
                        {
                            nrufFormU1.NrufCompanyInfoId = nrufFormU1Val.NrufCompanyInfoId;
                            Session["NrufCompanyInfo"] = nrufFormU1Val.NrufCompanyInfoId;
                        }
                        else
                        {
                            nrufFormU1.NrufCompanyInfoId = (int)Session["NrufCompanyInfo"];
                        }
                    }


                    var time = DateTime.Now;
                    nrufFormU1.createdDate = nrufFormU1Val.createdDate;
                    nrufFormU1.dateChanged = time;

                    Detach(nrufFormU1Val);
                    db.Entry(nrufFormU1).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index", new { id = compInfo });
                }

            }
            catch (Exception dex)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator. " + dex.Message);
            }

            return View(nrufFormU1);


        }

        // GET: External/NrufFormU1/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {

            if (Session["NrufCompanyInfo"] == null)
                return RedirectToAction("Index", "NrufCompanyInfoes");

            var compInfo = (int)Session["NrufCompanyInfo"];

            if (!CheckExpiryDate())
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            if (id != null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());

                var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo && d.organizationId == user.organizationId).FirstOrDefaultAsync();

                if (nrufcompinfo == null)
                    return RedirectToAction("Index", "NrufCompanyInfoes");

                NrufFormU1 nrufFormU1 = await db.NrufFormU1.FindAsync(id);
                if (nrufFormU1 == null)
                {
                    return HttpNotFound();
                }
                return View(nrufFormU1);

            }



            // return View("Index", "NrufCompanyInfoes"); view viewName and masterLayout
            return RedirectToAction("Index", "NrufCompanyInfoes");
        }

        // POST: External/NrufFormU1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            // Only view my NRUF forms
            NrufFormU1 nrufFormU1 = await db.NrufFormU1.FindAsync(id);

            if (nrufFormU1 == null)
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            if ((int)Session["NrufCompanyInfo"] != nrufFormU1.NrufCompanyInfoId)
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            if (!CheckExpiryDate())
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            db.NrufFormU1.Remove(nrufFormU1);
            await db.SaveChangesAsync();
            var compInfo = (int)Session["NrufCompanyInfo"];
            return RedirectToAction("Index", new { id = compInfo });
        }

        bool CheckExpiryDate()
        {
            // CHeck the Expiry Date
            NrufCompanyInfo nrufCompanyInfo = db.NrufCompanyInfo.Find((int)Session["NrufCompanyInfo"]);

            if (nrufCompanyInfo != null)
            {
                if (nrufCompanyInfo.expiryDate < DateTime.Now)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

            return true;
        }

        void Update(NrufFormU1 obj)
        {
            db.Entry(obj).State = EntityState.Modified;
            db.SaveChanges();
        }

        void Detach(NrufFormU1 obj)
        {
            db.Entry(obj).State = EntityState.Detached;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
