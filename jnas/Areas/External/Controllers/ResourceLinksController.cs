﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using System.Data.SqlClient;
using jnas.Configuration;


namespace jnas.Areas.External.Controllers
{
    public class ResourceLinksController : Controller
    {
        private Jnasdb db = new Jnasdb();

        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: External/ResourceLinks
        public async Task<ActionResult> Index()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            var orgId = user.organizationId.ToString();

            // Get User Id
            if (Session["UserId"] == null)
            {
               // var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }


            var resourceLinks = db.ResourceLinks.Include(r => r.LinkedResourceState).Include(r => r.Resource);
            return View(await resourceLinks.Where(rl => rl.state == orgId).ToListAsync());
        }

        // GET: External/ResourceLinks/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = UserManager.FindById(User.Identity.GetUserId());
            var orgId = user.organizationId.ToString();

            var resourceLink = await db.ResourceLinks.Where(rl => rl.state == orgId && rl.resourceLinkId == id).FirstOrDefaultAsync();
          //  ResourceLink resourceLink = await db.ResourceLinks.FindAsync(id);
            if (resourceLink == null)
            {
                return HttpNotFound();
            }
            int pr = Convert.ToInt32(resourceLink.parentResource);
            ViewBag.parentResource = (from tabledata in db.Resources where tabledata.resourceId == pr select tabledata.resource1).FirstOrDefault();
            int cr = Convert.ToInt32(resourceLink.childResource);
            ViewBag.childResource = (from tabledata in db.Resources where tabledata.resourceId == cr select tabledata.resource1).FirstOrDefault();
            return View(resourceLink);
        }

        // GET: External/ResourceLinks/Create
        public ActionResult Create()
        {
            var results = (from tabledata in db.Resources select new { tabledata.resourceId, name = tabledata.resource1 }).ToList();
            ViewBag.parentResource = new SelectList(results.AsEnumerable(), "resourceId", "name");
            ViewBag.childResource = new SelectList(results.AsEnumerable(), "resourceId", "name");
            ViewBag.linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name");
            //ViewBag.LinkedResourceState_linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name");
            //ViewBag.LinkedResourceState_linkedResourceStateId1 = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name");
            //ViewBag.Resource_resourceId = new SelectList(db.Resources, "resourceId", "resource1");
            //ViewBag.Resource_resourceId1 = new SelectList(db.Resources, "resourceId", "resource1");
            return View();
        }

        // POST: External/ResourceLinks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "resourceLinkId,parentResource,childResource,linkedResourceStateId,state")] ResourceLink resourceLink)
        {
            var user = UserManager.FindById(User.Identity.GetUserId());

            if (ModelState.IsValid)
            {
                //resourceLink.LinkedResourceState_linkedResourceStateId1 = resourceLink.linkedResourceStateId;
                // resourceLink.Resource_resourceId = resourceLink.linkedResourceStateId;

                resourceLink.state = user.organizationId.ToString();
                db.ResourceLinks.Add(resourceLink);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            var results = (from tabledata in db.Resources select new { tabledata.resourceId, name = tabledata.resource1 }).ToList();
            ViewBag.parentResource = new SelectList(results.AsEnumerable(), "resourceId", "name", Convert.ToInt32(resourceLink.parentResource));
            ViewBag.childResource = new SelectList(results.AsEnumerable(), "resourceId", "name", Convert.ToInt32(resourceLink.childResource));
            ViewBag.linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", resourceLink.linkedResourceStateId);
            //ViewBag.LinkedResourceState_linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", resourceLink.LinkedResourceState_linkedResourceStateId);
            //ViewBag.LinkedResourceState_linkedResourceStateId1 = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", resourceLink.LinkedResourceState_linkedResourceStateId1);
            //ViewBag.Resource_resourceId = new SelectList(db.Resources, "resourceId", "resource1", resourceLink.Resource_resourceId);
            //ViewBag.Resource_resourceId1 = new SelectList(db.Resources, "resourceId", "resource1", resourceLink.Resource_resourceId1);
            return View(resourceLink);
        }

        // GET: External/ResourceLinks/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = UserManager.FindById(User.Identity.GetUserId());
            var orgId = user.organizationId.ToString();

            var resourceLink = await db.ResourceLinks.Where(rl => rl.state == orgId && rl.resourceLinkId == id).FirstOrDefaultAsync();

            // ResourceLink resourceLink = await db.ResourceLinks.FindAsync(id);
            if (resourceLink == null)
            {
                return HttpNotFound();
            }
            var results = (from tabledata in db.Resources select new { tabledata.resourceId, name = tabledata.resource1 }).ToList();
            ViewBag.parentResource = new SelectList(results.AsEnumerable(), "resourceId", "name", Convert.ToInt32(resourceLink.parentResource));
            ViewBag.childResource = new SelectList(results.AsEnumerable(), "resourceId", "name", Convert.ToInt32(resourceLink.childResource));
            ViewBag.linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", resourceLink.linkedResourceStateId);
            //ViewBag.LinkedResourceState_linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", resourceLink.LinkedResourceState_linkedResourceStateId);
            //ViewBag.LinkedResourceState_linkedResourceStateId1 = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", resourceLink.LinkedResourceState_linkedResourceStateId1);
            //ViewBag.Resource_resourceId = new SelectList(db.Resources, "resourceId", "resource1", resourceLink.Resource_resourceId);
            //ViewBag.Resource_resourceId1 = new SelectList(db.Resources, "resourceId", "resource1", resourceLink.Resource_resourceId1);
            return View(resourceLink);
        }

        // POST: External/ResourceLinks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "resourceLinkId,parentResource,childResource,linkedResourceStateId,state")] ResourceLink resourceLink)
        {
            var user = UserManager.FindById(User.Identity.GetUserId());

            if (ModelState.IsValid)
            {
                //resourceLink.LinkedResourceState_linkedResourceStateId1 = resourceLink.linkedResourceStateId;
                resourceLink.state = user.organizationId.ToString();
                db.Entry(resourceLink).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            var results = (from tabledata in db.Resources select new { tabledata.resourceId, name = tabledata.resource1 }).ToList();
            ViewBag.parentResource = new SelectList(results.AsEnumerable(), "resourceId", "name", Convert.ToInt32(resourceLink.parentResource));
            ViewBag.childResource = new SelectList(results.AsEnumerable(), "resourceId", "name", Convert.ToInt32(resourceLink.childResource));
            ViewBag.linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", resourceLink.linkedResourceStateId);
            //ViewBag.LinkedResourceState_linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", resourceLink.LinkedResourceState_linkedResourceStateId);
            //ViewBag.LinkedResourceState_linkedResourceStateId1 = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", resourceLink.LinkedResourceState_linkedResourceStateId1);
            //ViewBag.Resource_resourceId = new SelectList(db.Resources, "resourceId", "resource1", resourceLink.Resource_resourceId);
            //ViewBag.Resource_resourceId1 = new SelectList(db.Resources, "resourceId", "resource1", resourceLink.Resource_resourceId1);
            return View(resourceLink);
        }

        // GET: External/ResourceLinks/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = UserManager.FindById(User.Identity.GetUserId());
            var orgId = user.organizationId.ToString();

            var resourceLink = await db.ResourceLinks.Where(rl => rl.state == orgId && rl.resourceLinkId == id).FirstOrDefaultAsync();

           // ResourceLink resourceLink = await db.ResourceLinks.FindAsync(id);
            if (resourceLink == null)
            {
                return HttpNotFound();
            }
            int pr = Convert.ToInt32(resourceLink.parentResource);
            ViewBag.parentResource = (from tabledata in db.Resources where tabledata.resourceId == pr select tabledata.resource1).FirstOrDefault();
            int cr = Convert.ToInt32(resourceLink.childResource);
            ViewBag.childResource = (from tabledata in db.Resources where tabledata.resourceId == cr select tabledata.resource1).FirstOrDefault();
            return View(resourceLink);
        }

        // POST: External/ResourceLinks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {

            var user = UserManager.FindById(User.Identity.GetUserId());
            var orgId = user.organizationId.ToString();

            var resourceLink = await db.ResourceLinks.Where(rl => rl.state == orgId && rl.resourceLinkId == id).FirstOrDefaultAsync();

            //  ResourceLink resourceLink = await db.ResourceLinks.FindAsync(id);
            if (resourceLink == null)
            {
                return HttpNotFound();
            }
            db.ResourceLinks.Remove(resourceLink);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
