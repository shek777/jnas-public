﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using System.Data.SqlClient;
using jnas.Configuration;

namespace jnas.Areas.External.Controllers
{
    public class ApplicationNotesController : Controller
    {
        private Jnasdb db = new Jnasdb();

        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: External/ApplicationNotes
        public async Task<ActionResult> Index()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());

            // Get User Id
            if (Session["UserId"] == null)
            {
               // var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }



            var applicationNotes = db.ApplicationNotes.Include(a => a.Application1);
            return View(await applicationNotes.Where(an => an.Application1.organizationId == user.organizationId).ToListAsync());
        }

        // GET: External/ApplicationNotes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = UserManager.FindById(User.Identity.GetUserId());

            var applicationNote = await db.ApplicationNotes.Include(a => a.Application1).Where(ar => ar.applicationNotesId == id && ar.Application1.organizationId == user.organizationId).FirstOrDefaultAsync();

           // ApplicationNote applicationNote = await db.ApplicationNotes.FindAsync(id);
            if (applicationNote == null)
            {
                return HttpNotFound();
            }
            return View(applicationNote);
        }

        // GET: External/ApplicationNotes/Create
        //public ActionResult Create()
        //{
        //    ViewBag.applicationId = new SelectList(db.Applications, "applicationId", "application1");
        //    return View();
        //}

        // POST: External/ApplicationNotes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "applicationNotesId,applicationId,application,note,UserId,user,internalNote,createdDate,dateChanged")] ApplicationNote applicationNote)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.ApplicationNotes.Add(applicationNote);
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.applicationId = new SelectList(db.Applications, "applicationId", "application1", applicationNote.applicationId);
        //    return View(applicationNote);
        //}

        // GET: External/ApplicationNotes/Edit/5
        //public async Task<ActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ApplicationNote applicationNote = await db.ApplicationNotes.FindAsync(id);
        //    if (applicationNote == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.applicationId = new SelectList(db.Applications, "applicationId", "application1", applicationNote.applicationId);
        //    return View(applicationNote);
        //}

        // POST: External/ApplicationNotes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "applicationNotesId,applicationId,application,note,UserId,user,internalNote,createdDate,dateChanged")] ApplicationNote applicationNote)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(applicationNote).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.applicationId = new SelectList(db.Applications, "applicationId", "application1", applicationNote.applicationId);
        //    return View(applicationNote);
        //}

        // GET: External/ApplicationNotes/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ApplicationNote applicationNote = await db.ApplicationNotes.FindAsync(id);
        //    if (applicationNote == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(applicationNote);
        //}

        // POST: External/ApplicationNotes/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    ApplicationNote applicationNote = await db.ApplicationNotes.FindAsync(id);
        //    db.ApplicationNotes.Remove(applicationNote);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
