﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace jnas.Areas.External.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "PrimaryExternalUser, SecondaryExternalUser, ReadOnlyUser, Administrator")]
    public class OrganizationsController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: External/Organizations
        public async Task<ActionResult> Index()
        {

            // Get User Id


            var user = UserManager.FindById(User.Identity.GetUserId());

            if (Session["UserId"] == null)
            {

                Session["UserId"] = user.Id;
            }

            var organizations = db.Organizations.Where(o => o.organizationId.Equals(user.organizationId)).Include(o => o.OrganizationState).Include(o => o.OrganizationType1);
            return View(await organizations.ToListAsync());
        }

        // GET: External/Organizations/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = UserManager.FindById(User.Identity.GetUserId());

            if(id != user.organizationId)
            {
                return HttpNotFound();
            }

            Organization organization = await db.Organizations.FindAsync(id);
            if (organization == null)
            {
                return HttpNotFound();
            }
            return View(organization);
        }

        // GET: External/Organizations/Create
        //public ActionResult Create()
        //{
        //    ViewBag.organizationStateId = new SelectList(db.OrganizationStates, "organizationStateId", "name");
        //    ViewBag.organizationTypeId = new SelectList(db.OrganizationTypes, "organizationTypeId", "name");
        //    return View();
        //}

        // POST: External/Organizations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "organizationId,name,organizationTypeId,organizationType,ftpServer,ftpUser,ftpPassword,ftpLocation,createdDate,dateChanged,organizationStateId,state")] Organization organization)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Organizations.Add(organization);
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.organizationStateId = new SelectList(db.OrganizationStates, "organizationStateId", "name", organization.organizationStateId);
        //    ViewBag.organizationTypeId = new SelectList(db.OrganizationTypes, "organizationTypeId", "name", organization.organizationTypeId);
        //    return View(organization);
        //}

        // GET: External/Organizations/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = UserManager.FindById(User.Identity.GetUserId());

            if (id != user.organizationId)
            {
                return HttpNotFound();
            }

            Organization org = await db.Organizations.FindAsync(id);


            if (org == null)
            {
                return HttpNotFound();
            }

            OrganizationViewModel organization = new OrganizationViewModel();

            organization.name = org.name;
            organization.ParentCompany = org.ParentCompany;
            organization.ParentCompanyOCN = org.ParentCompanyOCN;
            organization.OCN = org.OCN;
            organization.OFN = org.OFN;
            organization.organizationId = org.organizationId;
            //ViewBag.organizationStateId = new SelectList(db.OrganizationStates, "organizationStateId", "name", organization.organizationStateId);
            //ViewBag.organizationTypeId = new SelectList(db.OrganizationTypes, "organizationTypeId", "name", organization.organizationTypeId);
            return View(organization);
        }

        // POST: External/Organizations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "organizationId,name,ParentCompany,ParentCompanyOCN,OCN,OFN")] OrganizationViewModel organization)
        {

            var user = UserManager.FindById(User.Identity.GetUserId());



            if (ModelState.IsValid)
            {

                if (organization.organizationId != user.organizationId)
                {
                    return View(organization);
                }

                var org = db.Organizations.Find(organization.organizationId);

                org.name = organization.name;
                org.ParentCompany = organization.ParentCompany;
                org.ParentCompanyOCN = organization.ParentCompanyOCN;
                org.OCN = organization.OCN;
                org.OFN = organization.OFN;
                org.dateChanged = DateTime.Now;

                db.Entry(org).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            //ViewBag.organizationStateId = new SelectList(db.OrganizationStates, "organizationStateId", "name", organization.organizationStateId);
            //ViewBag.organizationTypeId = new SelectList(db.OrganizationTypes, "organizationTypeId", "name", organization.organizationTypeId);
            return View(organization);
        }

        // GET: External/Organizations/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Organization organization = await db.Organizations.FindAsync(id);
        //    if (organization == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(organization);
        //}

        // POST: External/Organizations/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    Organization organization = await db.Organizations.FindAsync(id);
        //    db.Organizations.Remove(organization);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}
        // TODO: External User can view everyone mail by manipulating Edit/#
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
