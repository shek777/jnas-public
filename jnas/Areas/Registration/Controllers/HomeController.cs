﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace jnas.Areas.Registration.Controllers
{
    public class HomeController : Controller
    {
        // GET: Registration/Home
        public ActionResult Index()
        {
            ViewBag.Header = "Welcome to the Registration Subsite";
            return View();
        }
    }
}