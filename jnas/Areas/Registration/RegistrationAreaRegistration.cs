﻿using System.Web.Mvc;

namespace jnas.Areas.Registration
{
    public class RegistrationAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Registration";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "Registration_default",
                url: "Registration/{controller}/{action}/{id}",
                defaults: new { area = "Registration", controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "jnas.Areas.Registration.Controllers" }
            );
        }
    }
}