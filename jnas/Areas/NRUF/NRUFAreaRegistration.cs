﻿using System.Web.Mvc;

namespace jnas.Areas.NRUF
{
    public class NRUFAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "NRUF";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "NRUF_default",
                url: "NRUF/{controller}/{action}/{id}",
                defaults: new { area = "NRUF", controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "jnas.Areas.NRUF.Controllers" }
            );
        }
    }
}