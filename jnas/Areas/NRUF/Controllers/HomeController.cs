﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace jnas.Areas.NRUF.Controllers
{
    public class HomeController : Controller
    {
        // GET: NRUF/Home
        public ActionResult Index()
        {
            ViewBag.Header = "Welcome to the NRUF Subsite";
            return View();
        }
    }
}