﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace jnas.Areas.Security.Controllers
{
    public class HomeController : Controller
    {
        // GET: Security/Home
        public ActionResult Index()
        {
            ViewBag.Header = "Welcome to the Security Subsite";
            return View();
        }
    }
}