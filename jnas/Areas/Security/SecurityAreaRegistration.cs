﻿using System.Web.Mvc;

namespace jnas.Areas.Security
{
    public class SecurityAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Security";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "Security_default",
                url: "Security/{controller}/{action}/{id}",
                defaults: new { area = "Security", controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "jnas.Areas.Security.Controllers" }
               );
        }
    }
}