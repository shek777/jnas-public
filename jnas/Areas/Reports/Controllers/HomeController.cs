﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace jnas.Areas.Reports.Controllers
{
    public class HomeController : Controller
    {
        // GET: Reports/Home
        public ActionResult Index()
        {

            ViewBag.Header = "Welcome to the Report Subsite";
            return View();
        }
    }
}