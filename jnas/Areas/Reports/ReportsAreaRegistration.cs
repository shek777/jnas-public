﻿using System.Web.Mvc;

namespace jnas.Areas.Reports
{
    public class ReportsAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Reports";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "Reports_default",
                url: "Reports/{controller}/{action}/{id}",
                defaults: new { area = "Reports", controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "jnas.Areas.Reports.Controllers" }
               );
        }
    }
}