﻿
using System.Web.Mvc;

namespace jnas.Areas.Administration
{
    public class AdministrationAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Administration";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {

            //  context.Routes.MapMvcAttributeRoutes();


            context.MapRoute(
               name: "Administration_default",
               url: "Administration/{controller}/{action}/{UserId}/{id}",
               defaults: new { area = "Administration", controller = "Home", action = "Index", id = UrlParameter.Optional },
               namespaces: new string[] { "jnas.Areas.Administration.Controllers" }
           );

            context.MapRoute(
                name: "Administration_UserRoles",
                url: "Administration/{controller}/{action}/{UserId}/{RoleId}",
                defaults: new { area = "Administration", controller = "Home", action = "Index", UserId = UrlParameter.Optional, RoleId = UrlParameter.Optional },
                namespaces: new string[] { "jnas.Areas.Administration.Controllers" }
            );


        }
    }
}