﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class OrganizationStatesController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administration/OrganizationStates
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            return View(await db.OrganizationStates.ToListAsync());
        }

        // GET: Administration/OrganizationStates/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganizationState organizationState = await db.OrganizationStates.FindAsync(id);
            if (organizationState == null)
            {
                return HttpNotFound();
            }
            return View(organizationState);
        }

        // GET: Administration/OrganizationStates/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Administration/OrganizationStates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "organizationStateId,name")] OrganizationState organizationState)
        {
            if (ModelState.IsValid)
            {
                db.OrganizationStates.Add(organizationState);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(organizationState);
        }

        // GET: Administration/OrganizationStates/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganizationState organizationState = await db.OrganizationStates.FindAsync(id);
            if (organizationState == null)
            {
                return HttpNotFound();
            }
            return View(organizationState);
        }

        // POST: Administration/OrganizationStates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "organizationStateId,name")] OrganizationState organizationState)
        {
            if (ModelState.IsValid)
            {
                db.Entry(organizationState).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(organizationState);
        }

        // GET: Administration/OrganizationStates/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganizationState organizationState = await db.OrganizationStates.FindAsync(id);
            if (organizationState == null)
            {
                return HttpNotFound();
            }
            return View(organizationState);
        }

        // POST: Administration/OrganizationStates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            OrganizationState organizationState = await db.OrganizationStates.FindAsync(id);
            db.OrganizationStates.Remove(organizationState);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
