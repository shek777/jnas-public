﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class ApplicationStatesController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administration/ApplicationStates
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            return View(await db.ApplicationStates.ToListAsync());
        }

        // GET: Administration/ApplicationStates/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationState applicationState = await db.ApplicationStates.FindAsync(id);
            if (applicationState == null)
            {
                return HttpNotFound();
            }
            return View(applicationState);
        }

        // GET: Administration/ApplicationStates/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Administration/ApplicationStates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "applicationStateId,name")] ApplicationState applicationState)
        {
            if (ModelState.IsValid)
            {
                db.ApplicationStates.Add(applicationState);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(applicationState);
        }

        // GET: Administration/ApplicationStates/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationState applicationState = await db.ApplicationStates.FindAsync(id);
            if (applicationState == null)
            {
                return HttpNotFound();
            }
            return View(applicationState);
        }

        // POST: Administration/ApplicationStates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "applicationStateId,name")] ApplicationState applicationState)
        {
            if (ModelState.IsValid)
            {
                db.Entry(applicationState).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(applicationState);
        }

        // GET: Administration/ApplicationStates/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationState applicationState = await db.ApplicationStates.FindAsync(id);
            if (applicationState == null)
            {
                return HttpNotFound();
            }
            return View(applicationState);
        }

        // POST: Administration/ApplicationStates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ApplicationState applicationState = await db.ApplicationStates.FindAsync(id);
            db.ApplicationStates.Remove(applicationState);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
