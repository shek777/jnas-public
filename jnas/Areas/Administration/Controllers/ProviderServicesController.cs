﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class ProviderServicesController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administration/ProviderServices
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            return View(await db.ProviderServices.ToListAsync());
        }

        // GET: Administration/ProviderServices/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProviderService providerService = await db.ProviderServices.FindAsync(id);
            if (providerService == null)
            {
                return HttpNotFound();
            }
            return View(providerService);
        }

        // GET: Administration/ProviderServices/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Administration/ProviderServices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ProviderServiceId,name")] ProviderService providerService)
        {
            if (ModelState.IsValid)
            {
                db.ProviderServices.Add(providerService);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(providerService);
        }

        // GET: Administration/ProviderServices/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProviderService providerService = await db.ProviderServices.FindAsync(id);
            if (providerService == null)
            {
                return HttpNotFound();
            }
            return View(providerService);
        }

        // POST: Administration/ProviderServices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ProviderServiceId,name")] ProviderService providerService)
        {
            if (ModelState.IsValid)
            {
                db.Entry(providerService).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(providerService);
        }

        // GET: Administration/ProviderServices/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProviderService providerService = await db.ProviderServices.FindAsync(id);
            if (providerService == null)
            {
                return HttpNotFound();
            }
            return View(providerService);
        }

        // POST: Administration/ProviderServices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ProviderService providerService = await db.ProviderServices.FindAsync(id);
            db.ProviderServices.Remove(providerService);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
