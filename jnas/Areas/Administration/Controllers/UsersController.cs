﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using jnas.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;


namespace jnas.Areas.Administration.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class UsersController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private ApplicationDbContext db1 = new ApplicationDbContext();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administration/Users
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            var users = db.Users.Where(u => u.UserName != "andrew.williams@our.org.jm").Include(u => u.Organization1).Include(u => u.UserState).Include(u => u.UserType1);
            return View(await users.ToListAsync());
        }

        // GET: Administration/Users/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            jnas.Models.Jnasdb.User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            var match = await (from d in db.Users where d.UserId.Equals(id) && d.UserName.Equals("andrew.williams@our.org.jm") select d).FirstOrDefaultAsync();

            if (match != null)
            {
                return RedirectToAction("Index");
            }

            var upi = await (from d in db.UserProfiles where d.userProfileId == user.userProfileId select new { d.name }).FirstOrDefaultAsync();
            ViewBag.userProfileId = upi.name;

            return View(user);
        }

        // GET: Administration/Users/Create
        public ActionResult Create()
        {
            ViewBag.organizationId = new SelectList(db.Organizations, "organizationId", "name");
            ViewBag.userStateId = new SelectList(db.UserStates, "userStateId", "name");
            ViewBag.userTypeId = new SelectList(db.UserTypes, "userTypeId", "name");
            ViewBag.userProfileId = new SelectList(db.UserProfiles, "userProfileId", "name");
            return View();
        }

        // POST: Administration/Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "name,organizationId,userTypeId,userProfileId,primaryContact,userStateId,Email,PhoneNumber,PhoneNumberConfirmed,AccessFailedCount,UserName")] ManageUserViewModel user)
        {
            if (ModelState.IsValid)
            {

                var match = await (from d in db.Users where d.Email.Equals("andrew.williams@our.org.jm") select d).FirstOrDefaultAsync();

                if (match.Email == user.Email)
                {
                    return RedirectToAction("Index");
                }


                if (user.primaryContact == true)
                {
                    var listofusers = db1.Users.Where(d => d.organizationId == user.organizationId).ToList();
                    listofusers.ForEach(item =>
                    {
                        item.primaryContact = false;
                        //  db.Entry(item).State = EntityState.Modified;

                    });

                    db1.SaveChanges();

                }
                else
                    if (user.primaryContact == false)
                    {
                        if (db1.Users.Where(d => d.organizationId == user.organizationId).Count() == 0)
                        {
                            user.primaryContact = true;
                        }

                    }


                DateTime? now = DateTime.Now;

                //usr.organizationId = user.organizationId;
                //usr.userTypeId = user.userTypeId;
                //usr.userProfileId = user.userProfileId;
                //usr.userStateId = user.userStateId;
                //usr.name = user.name;
                //usr.primaryContact = user.primaryContact;
                //usr.PhoneNumber = user.PhoneNumber;
                //usr.PhoneNumberConfirmed = user.PhoneNumberConfirmed;
                //usr.AccessFailedCount = user.AccessFailedCount;
                //usr.Email = user.Email;
                //usr.UserName = user.UserName;



                UserManager manager = new UserManager(new UserStore<jnas.Models.User>(db1));



                var usr = new jnas.Models.User { Email = user.Email, name = user.name, UserName = user.UserName, securityQuestion = "Empty", securityAnswer = "Empty", userProfileId = user.userProfileId, organizationId = user.organizationId, userTypeId = user.userTypeId, userStateId = user.userStateId, createdDate = now, dateChanged = now, lastConnectionDate = null, primaryContact = user.primaryContact, AccessFailedCount = user.AccessFailedCount, PhoneNumber = user.PhoneNumber, PhoneNumberConfirmed = user.PhoneNumberConfirmed };

                IdentityResult result = await manager.CreateAsync(usr, "password");
                IdentityResult result1 = await manager.AddToRoleAsync(usr.Id, "Default");

                //db.Users.Add(usr);
                //await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.organizationId = new SelectList(db.Organizations, "organizationId", "name", user.organizationId);
            ViewBag.userStateId = new SelectList(db.UserStates, "userStateId", "name", user.userStateId);
            ViewBag.userTypeId = new SelectList(db.UserTypes, "userTypeId", "name", user.userTypeId);
            ViewBag.userProfileId = new SelectList(db.UserProfiles, "userProfileId", "name", user.userProfileId);
            return View(user);
        }

        // GET: Administration/Users/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            jnas.Models.Jnasdb.User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            var match = await (from d in db.Users where d.UserId.Equals(id) && d.UserName.Equals("andrew.williams@our.org.jm") select d).FirstOrDefaultAsync();

            if (match != null)
            {
                return RedirectToAction("Index");
            }

            var usr = new ManageUserViewModel();

            usr.organizationId = user.organizationId;
            usr.userTypeId = user.userTypeId;
            usr.userProfileId = user.userProfileId;
            usr.userStateId = user.userStateId;
            usr.name = user.name;
            usr.primaryContact = user.primaryContact;
            usr.PhoneNumber = user.PhoneNumber;
            usr.PhoneNumberConfirmed = user.PhoneNumberConfirmed;
            usr.AccessFailedCount = user.AccessFailedCount;
            usr.Email = user.Email;
            usr.UserName = user.UserName;
            usr.UserId = user.UserId;


            ViewBag.organizationId = new SelectList(db.Organizations, "organizationId", "name", user.organizationId);
            ViewBag.userStateId = new SelectList(db.UserStates, "userStateId", "name", user.userStateId);
            ViewBag.userTypeId = new SelectList(db.UserTypes, "userTypeId", "name", user.userTypeId);
            ViewBag.userProfileId = new SelectList(db.UserProfiles, "userProfileId", "name", user.userProfileId);
            return View(usr);
        }

        // POST: Administration/Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "UserId,name,organizationId,userTypeId,userProfileId,primaryContact,userStateId,Email,PhoneNumber,PhoneNumberConfirmed,AccessFailedCount,UserName")] ManageUserViewModel user)
        {
            if (ModelState.IsValid)
            {
                var match = await (from d in db.Users where d.UserId.Equals(user.UserId) && d.UserName.Equals("andrew.williams@our.org.jm") select d).FirstOrDefaultAsync();

                if (match != null)
                {
                    return RedirectToAction("Index");
                }

                UserManager manager = new UserManager(new UserStore<jnas.Models.User>(db1));

                var usr = manager.FindById(user.UserId);

                if (user.primaryContact == true)
                {
                    var listofusers = db.Users.Where(d => d.UserId != user.UserId && d.organizationId == usr.organizationId).ToList();

                    listofusers.ForEach(item =>
                    {
                        item.primaryContact = false;
                        //  db.Entry(item).State = EntityState.Modified;

                    });

                    db.SaveChanges();
                }
                else
                {
                    if (db.Users.Where(d => d.organizationId == usr.organizationId).Count() == 1)
                    {
                        user.primaryContact = true;
                    }
                    else
                    {

                        var chosenusers = db.Users.Where(d => d.UserId != user.UserId && d.primaryContact == false && d.organizationId == usr.organizationId).FirstOrDefault();

                        if (chosenusers != null)
                        {
                            chosenusers.primaryContact = true;
                            db.Entry(chosenusers).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }

                usr.organizationId = user.organizationId;
                usr.userTypeId = user.userTypeId;
                usr.userProfileId = user.userProfileId;
                usr.userStateId = user.userStateId;
                usr.name = user.name;
                usr.primaryContact = user.primaryContact;
                usr.PhoneNumber = user.PhoneNumber;
                usr.PhoneNumberConfirmed = user.PhoneNumberConfirmed;
                usr.AccessFailedCount = user.AccessFailedCount;
                usr.Email = user.Email;
                usr.UserName = user.UserName;



                //var usr = new jnas.Models.User { Email = user.Email, name = user.name, UserName = user.UserName, securityQuestion = "Empty", securityAnswer = "Empty", userProfileId = user.userProfileId, organizationId = user.organizationId, userTypeId = user.userTypeId, userStateId = user.userStateId, createdDate = now, dateChanged = now, lastConnectionDate = null, primaryContact = user.primaryContact, AccessFailedCount = user.AccessFailedCount, PhoneNumber = user.PhoneNumber, PhoneNumberConfirmed = user.PhoneNumberConfirmed };

                IdentityResult result = await manager.UpdateAsync(usr);
                //  manager.AddToRole(usr.Id, "Default");

                // db.Entry(usr).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.organizationId = new SelectList(db.Organizations, "organizationId", "name", user.organizationId);
            ViewBag.userStateId = new SelectList(db.UserStates, "userStateId", "name", user.userStateId);
            ViewBag.userTypeId = new SelectList(db.UserTypes, "userTypeId", "name", user.userTypeId);
            ViewBag.userProfileId = new SelectList(db.UserProfiles, "userProfileId", "name", user.userProfileId);
            return View(user);
        }

        // GET: Administration/Users/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            jnas.Models.Jnasdb.User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            var match = await (from d in db.Users where d.UserId.Equals(id) && d.UserName.Equals("andrew.williams@our.org.jm") select d).FirstOrDefaultAsync();

            if (match != null)
            {
                return RedirectToAction("Index");
            }

            var upi = await (from d in db.UserProfiles where d.userProfileId == user.userProfileId select new { d.name }).FirstOrDefaultAsync();
            ViewBag.userProfileId = upi.name;


            return View(user);
        }

        // POST: Administration/Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            var match = await (from d in db.Users where d.UserId.Equals(id) && d.UserName.Equals("andrew.williams@our.org.jm") select d).FirstOrDefaultAsync();

            if (match != null)
            {
                return RedirectToAction("Index");
            }

            jnas.Models.Jnasdb.User user = await db.Users.FindAsync(id);
            db.Users.Remove(user);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
