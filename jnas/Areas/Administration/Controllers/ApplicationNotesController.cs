﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    public class ApplicationNotesController : Controller
    {
        private Jnasdb db = new Jnasdb();

        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administration/ApplicationNotes
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }


            var applicationNotes = db.ApplicationNotes.Include(a => a.Application1);
            return View(await applicationNotes.ToListAsync());
        }

        // GET: Administration/ApplicationNotes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationNote applicationNote = await db.ApplicationNotes.FindAsync(id);
            if (applicationNote == null)
            {
                return HttpNotFound();
            }
            return View(applicationNote);
        }

        // GET: Administration/ApplicationNotes/Create
        public ActionResult Create()
        {
            var results1 = (from tabledata in db.Applications select new { tabledata.applicationId, name = tabledata.applicationReference }).ToList();
            ViewBag.applicationId = new SelectList(results1.AsEnumerable(), "applicationId", "name");
            //ViewBag.applicationId = new SelectList(db.Applications, "applicationId", "application1");
            return View();
        }

        // POST: Administration/ApplicationNotes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "applicationNotesId,applicationId,application,note,UserId,user,internalNote,createdDate,dateChanged")] ApplicationNote applicationNote)
        {
            if (ModelState.IsValid)
            {
                db.ApplicationNotes.Add(applicationNote);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            var results1 = (from tabledata in db.Applications select new { tabledata.applicationId, name = tabledata.applicationReference }).ToList();
            ViewBag.applicationId = new SelectList(results1.AsEnumerable(), "applicationId", "name", applicationNote.applicationId);
            //ViewBag.applicationId = new SelectList(db.Applications, "applicationId", "application1", applicationNote.applicationId);
            return View(applicationNote);
        }

        // GET: Administration/ApplicationNotes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationNote applicationNote = await db.ApplicationNotes.FindAsync(id);
            if (applicationNote == null)
            {
                return HttpNotFound();
            }
            var results1 = (from tabledata in db.Applications select new { tabledata.applicationId, name = tabledata.applicationReference }).ToList();
            ViewBag.applicationId = new SelectList(results1.AsEnumerable(), "applicationId", "name", applicationNote.applicationId);
            //ViewBag.applicationId = new SelectList(db.Applications, "applicationId", "application1", applicationNote.applicationId);
            return View(applicationNote);
        }

        // POST: Administration/ApplicationNotes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "applicationNotesId,applicationId,application,note,UserId,user,internalNote,createdDate,dateChanged")] ApplicationNote applicationNote)
        {
            if (ModelState.IsValid)
            {
                db.Entry(applicationNote).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            var results1 = (from tabledata in db.Applications select new { tabledata.applicationId, name = tabledata.applicationReference }).ToList();
            ViewBag.applicationId = new SelectList(results1.AsEnumerable(), "applicationId", "name", applicationNote.applicationId);
            //ViewBag.applicationId = new SelectList(db.Applications, "applicationId", "application1", applicationNote.applicationId);
            return View(applicationNote);
        }

        // GET: Administration/ApplicationNotes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationNote applicationNote = await db.ApplicationNotes.FindAsync(id);
            if (applicationNote == null)
            {
                return HttpNotFound();
            }
            return View(applicationNote);
        }

        // POST: Administration/ApplicationNotes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ApplicationNote applicationNote = await db.ApplicationNotes.FindAsync(id);
            db.ApplicationNotes.Remove(applicationNote);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
