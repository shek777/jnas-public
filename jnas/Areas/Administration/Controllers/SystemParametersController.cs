﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;


namespace jnas.Areas.Administration.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class SystemParametersController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administration/SystemParameters
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            var SysPar = (from d in db.SystemParameters where d.name != "Seed Secondary Level Data" select d).ToListAsync();
            return View(await SysPar); //db.SystemParameters.ToListAsync()
        }

        // GET: Administration/SystemParameters/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SystemParameter systemParameter = await db.SystemParameters.FindAsync(id);
            if (systemParameter == null)
            {
                return HttpNotFound();
            }
            return View(systemParameter);
        }

        // GET: Administration/SystemParameters/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Administration/SystemParameters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "systemParameterId,name,value")] SystemParameter systemParameter)
        {
            if (ModelState.IsValid)
            {
                var time = DateTime.Now;

                systemParameter.createdDate = time;
                systemParameter.dateChanged = time;
                systemParameter.UserId = User.Identity.GetUserId();

                db.SystemParameters.Add(systemParameter);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(systemParameter);
        }

        // GET: Administration/SystemParameters/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SystemParameter systemParameter = await db.SystemParameters.FindAsync(id);
            if (systemParameter == null)
            {
                return HttpNotFound();
            }
            return View(systemParameter);
        }

        // POST: Administration/SystemParameters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "systemParameterId,name,value")] SystemParameter systemParameter)
        {
            if (ModelState.IsValid)
            {
                var time = DateTime.Now;

                systemParameter.dateChanged = time;
                systemParameter.UserId = User.Identity.GetUserId();


                db.Entry(systemParameter).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(systemParameter);
        }

        // GET: Administration/SystemParameters/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SystemParameter systemParameter = await db.SystemParameters.FindAsync(id);
            if (systemParameter == null)
            {
                return HttpNotFound();
            }
            return View(systemParameter);
        }

        // POST: Administration/SystemParameters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            SystemParameter systemParameter = await db.SystemParameters.FindAsync(id);
            db.SystemParameters.Remove(systemParameter);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
