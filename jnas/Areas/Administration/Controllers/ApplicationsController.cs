﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using System.Text.RegularExpressions;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    public class ApplicationsController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private Jnasdb db1 = new Jnasdb();

        private Random _random = new Random((int)DateTime.Now.Ticks);

        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        public string GenerateRandomNo()
        {
            return _random.Next(0, 9999).ToString("D4");
        }

        public string GenerateDateForApplRef()
        {
            DateTime _now = DateTime.Now;
            return _now.ToString("yyyyMMdd");
        }


        // GET: Administration/Applications
        public async Task<ActionResult> Index()
        {

            // Get User Id

            var user = UserManager.FindById(User.Identity.GetUserId());

            if (Session["UserId"] == null)
            {

                Session["UserId"] = user.Id;
            }


            var applications = db.Applications.Include(a => a.ApplicationState).Include(a => a.ApplicationType).Include(a => a.Organization).Include(a => a.ResourceSubType1).Include(a => a.ResourceType1).Include(a => a.User1);

            return View(await applications.ToListAsync());
        }

        // GET: Administration/Applications/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = await db.Applications.FindAsync(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            return View(application);
        }

        // GET: Administration/Applications/Create
        public ActionResult Create()
        {

            ViewBag.appRef = string.Format("AFR-{0}-{1}", GenerateDateForApplRef(), GenerateRandomNo());

            ViewBag.applicationStateId = new SelectList(db.ApplicationStates, "applicationStateId", "name");
            ViewBag.applicationTypeId = new SelectList(db.ApplicationTypes, "applicationTypeId", "name");
            ViewBag.organizationId = new SelectList(db.Organizations, "organizationId", "name");
            ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name");
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name");
            ViewBag.UserId = new SelectList(db.Users, "UserId", "name");
            return View();
        }

        // POST: Administration/Applications/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "applicationId,organizationId,application1,applicationReference,applicant,recipient,UserId,user,serviceId,service,resourceTypeId,resourceType,resourceSubTypeId,resourceSubType,subscriber,customerContactNumber,localRoutingNumber,FileName,ContentType,additionalDocumentation,createdDate,dateChanged,applicationResourceId,applicationTypeId,applicationStateId,state")] Application application, HttpPostedFileBase additionalDocumentation1)
        {

            var TollFree = db1.ResourceTypes.Where(p => p.name.Contains("Toll")).FirstOrDefault();

            if (TollFree != null)
            {
                if (application.resourceTypeId == TollFree.resourceTypeId && string.IsNullOrWhiteSpace(application.localRoutingNumber))
                {
                    ModelState.AddModelError("", "The Local Routing Number cannot be empty if the Resource type selected is a Toll - Free Number.");
                    ViewBag.applicationTypeId = new SelectList(db.ApplicationTypes, "applicationTypeId", "name", application.applicationTypeId);
                    ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", application.resourceSubTypeId);
                    ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", application.resourceTypeId);
                    ViewBag.appRef = application.applicationReference;
                    return View(application);
                }
            }

            if (additionalDocumentation1 != null && additionalDocumentation1.ContentLength > 0 && additionalDocumentation1.ContentType != "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
            {
                ModelState.AddModelError("", "This online application only accepts a Microsoft Word Document (.docx).");
                ViewBag.applicationTypeId = new SelectList(db.ApplicationTypes, "applicationTypeId", "name", application.applicationTypeId);
                ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", application.resourceSubTypeId);
                ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", application.resourceTypeId);
                ViewBag.appRef = application.applicationReference;
                return View(application);
            }

            if (ModelState.IsValid)
            {
                // var user = UserManager.FindById(User.Identity.GetUserId());

                var appType = db1.ApplicationTypes.Where(p => p.applicationTypeId == application.applicationTypeId).FirstOrDefault();
                var serv = db1.Services.Where(p => p.name == appType.name).FirstOrDefault();
                var orgname = db1.Organizations.Where(p => p.organizationId == application.organizationId).FirstOrDefault();
                var resTypeName = db1.ResourceTypes.Where(p => p.resourceTypeId == application.resourceTypeId).FirstOrDefault();
                // var resSubTypeName = db1.ResourceSubTypes.Where(p => p.resourceSubTypeId == application.resourceSubTypeId).FirstOrDefault();



                // Add
                switch (resTypeName.name)
                {
                    case "NPA Codes":
                        application.recipient = "North American Numbering Plan Administration";
                        break;
                    case "Premium Number Codes":
                        application.recipient = "North American Numbering Plan Administration";
                        break;
                    case "Premium Number Code":
                        application.recipient = "North American Numbering Plan Administration";
                        break;
                    case "5YY-NXX Codes":
                        application.recipient = "North American Numbering Plan Administration";
                        break;
                    case "555-XXXX Line Numbers":
                        application.recipient = "North American Numbering Plan Administration";
                        break;
                    case "456-NXX Codes":
                        application.recipient = "North American Numbering Plan Administration";
                        break;
                    case "CIC":
                        application.recipient = "North American Numbering Plan Administration";
                        break;
                    case "ANSI SPC Block":
                        application.recipient = "Telcordia Technologies, Inc.";
                        break;
                    case "ANSI SPC":
                        application.recipient = "Telcordia Technologies, Inc.";
                        break;
                    case "SANC":
                        application.recipient = "International Telecommunication Union Telecommunication";
                        break;
                    default:
                        application.recipient = "Office of Utilities Regulation";
                        break;
                }

                // application.organizationId = user.organizationId;
                application.serviceId = serv.serviceId;
                // application.UserId = user.Id;
                application.subscriber = orgname.name;

                var Registered = db1.ApplicationStates.Where(p => p.name.Contains("Registered")).FirstOrDefault();
                if (Registered != null)
                {
                    application.applicationStateId = Registered.applicationStateId;  //registered
                }
                else
                {
                    application.applicationStateId = 1;  // guess registered
                }

                //if (user.primaryContact == true)
                //{
                //    if (!string.IsNullOrWhiteSpace(user.PhoneNumber))
                //    {
                //        application.customerContactNumber = user.PhoneNumber;
                //    }
                //}
                //else
                //{
                //    var primeContact = db1.Users.Where(p => p.primaryContact == true && p.organizationId == user.organizationId).FirstOrDefault();
                //    if (primeContact != null)
                //    {
                //        application.customerContactNumber = primeContact.PhoneNumber;
                //    }
                //}

                application.subscriber = orgname.name;

                var now = DateTime.Now;
                application.createdDate = now;
                application.dateChanged = now;

                if (additionalDocumentation1 != null && additionalDocumentation1.ContentLength > 0)
                {

                    application.FileName = System.IO.Path.GetFileName(additionalDocumentation1.FileName);

                    application.ContentType = additionalDocumentation1.ContentType;

                    using (var reader = new System.IO.BinaryReader(additionalDocumentation1.InputStream))
                    {
                        application.additionalDocumentation = reader.ReadBytes(additionalDocumentation1.ContentLength);
                    }

                }

                db.Applications.Add(application);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }


            ViewBag.appRef = application.applicationReference;
            ViewBag.applicationStateId = new SelectList(db.ApplicationStates, "applicationStateId", "name", application.applicationStateId);
            ViewBag.applicationTypeId = new SelectList(db.ApplicationTypes, "applicationTypeId", "name", application.applicationTypeId);
            ViewBag.organizationId = new SelectList(db.Organizations, "organizationId", "name", application.organizationId);
            ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", application.resourceSubTypeId);
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", application.resourceTypeId);
            ViewBag.UserId = new SelectList(db.Users, "UserId", "name", application.UserId);
            return View(application);
        }

        [HttpGet]
        public string GetAppTypeAbr(int? id, string appRef)
        {
            var regex = new Regex(@"^[A-Z]{3,5}-[0-9]{8}-[0-9]{4}$");
            var match = regex.Match(appRef);
            if (!match.Success)
            {
                appRef = string.Format("AFR-{0}-{1}", GenerateDateForApplRef(), GenerateRandomNo());
            }

            if (id == null) id = 1;



            var data = db.ApplicationTypes.FirstOrDefault(d => d.applicationTypeId == id);

            if (data == null) return appRef;
            var temp = appRef.Split('-');
            appRef = string.Format("{0}-{1}-{2}", data.abbreviation, temp[1], temp[2]);

            return appRef;

        }

        // GET: Administration/Applications/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = await db.Applications.FindAsync(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            ViewBag.applicationStateId = new SelectList(db.ApplicationStates, "applicationStateId", "name", application.applicationStateId);
            ViewBag.applicationTypeId = new SelectList(db.ApplicationTypes, "applicationTypeId", "name", application.applicationTypeId);
            ViewBag.organizationId = new SelectList(db.Organizations, "organizationId", "name", application.organizationId);
            ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", application.resourceSubTypeId);
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", application.resourceTypeId);
            ViewBag.UserId = new SelectList(db.Users, "UserId", "name", application.UserId);
            return View(application);
        }

        // POST: Administration/Applications/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "applicationId,organizationId,application1,applicationReference,applicant,recipient,UserId,user,serviceId,service,resourceTypeId,resourceType,resourceSubTypeId,resourceSubType,subscriber,customerContactNumber,localRoutingNumber,FileName,ContentType,additionalDocumentation,createdDate,dateChanged,applicationResourceId,applicationTypeId,applicationStateId,state")] Application application, HttpPostedFileBase additionalDocumentation1)
        {

            var TollFree = db1.ResourceTypes.Where(p => p.name.Contains("Toll")).FirstOrDefault();

            if (TollFree != null)
            {
                if (application.resourceTypeId == TollFree.resourceTypeId && string.IsNullOrWhiteSpace(application.localRoutingNumber))
                {
                    ModelState.AddModelError("", "The Local Routing Number cannot be empty if the Resource type selected is a Toll - Free Number.");
                    ViewBag.applicationTypeId = new SelectList(db.ApplicationTypes, "applicationTypeId", "name", application.applicationTypeId);
                    ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", application.resourceSubTypeId);
                    ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", application.resourceTypeId);
                    ViewBag.appRef = application.applicationReference;
                    return View(application);
                }
            }

            if (additionalDocumentation1 != null && additionalDocumentation1.ContentLength > 0 && additionalDocumentation1.ContentType != "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
            {
                ModelState.AddModelError("", "This online application only accepts a Microsoft Word Document (.docx).");
                ViewBag.applicationTypeId = new SelectList(db.ApplicationTypes, "applicationTypeId", "name", application.applicationTypeId);
                ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", application.resourceSubTypeId);
                ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", application.resourceTypeId);
                ViewBag.appRef = application.applicationReference;
                return View(application);
            }

            if (ModelState.IsValid)
            {

                //  var user = UserManager.FindById(User.Identity.GetUserId());

                var appType = db1.ApplicationTypes.Where(p => p.applicationTypeId == application.applicationTypeId).FirstOrDefault();
                var serv = db1.Services.Where(p => p.name == appType.name).FirstOrDefault();
                var orgname = db1.Organizations.Where(p => p.organizationId == application.organizationId).FirstOrDefault();
                var resTypeName = db1.ResourceTypes.Where(p => p.resourceTypeId == application.resourceTypeId).FirstOrDefault();
                // var resSubTypeName = db1.ResourceSubTypes.Where(p => p.resourceSubTypeId == application.resourceSubTypeId).FirstOrDefault();



                // Add
                switch (resTypeName.name)
                {
                    case "NPA Codes":
                        application.recipient = "North American Numbering Plan Administration";
                        break;
                    case "Premium Number Codes":
                        application.recipient = "North American Numbering Plan Administration";
                        break;
                    case "Premium Number Code":
                        application.recipient = "North American Numbering Plan Administration";
                        break;
                    case "5YY-NXX Codes":
                        application.recipient = "North American Numbering Plan Administration";
                        break;
                    case "555-XXXX Line Numbers":
                        application.recipient = "North American Numbering Plan Administration";
                        break;
                    case "456-NXX Codes":
                        application.recipient = "North American Numbering Plan Administration";
                        break;
                    case "CIC":
                        application.recipient = "North American Numbering Plan Administration";
                        break;
                    case "ANSI SPC Block":
                        application.recipient = "Telcordia Technologies, Inc.";
                        break;
                    case "ANSI SPC":
                        application.recipient = "Telcordia Technologies, Inc.";
                        break;
                    case "SANC":
                        application.recipient = "International Telecommunication Union Telecommunication";
                        break;
                    default:
                        application.recipient = "Office of Utilities Regulation";
                        break;
                }

                var app1 = db1.Applications.FirstOrDefault(p => p.applicationId == application.applicationId);

                if (serv != null) application.serviceId = serv.serviceId;


                var now = DateTime.Now;

                application.dateChanged = now;

                if (additionalDocumentation1 != null && additionalDocumentation1.ContentLength > 0)
                {

                    application.FileName = System.IO.Path.GetFileName(additionalDocumentation1.FileName);

                    application.ContentType = additionalDocumentation1.ContentType;

                    using (var reader = new System.IO.BinaryReader(additionalDocumentation1.InputStream))
                    {
                        application.additionalDocumentation = reader.ReadBytes(additionalDocumentation1.ContentLength);
                    }

                }
                else
                {
                    if (app1 != null)
                    {
                        application.FileName = app1.FileName;
                        application.ContentType = app1.ContentType;
                        application.additionalDocumentation = app1.additionalDocumentation;
                    }
                }

                db.Entry(application).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.applicationStateId = new SelectList(db.ApplicationStates, "applicationStateId", "name", application.applicationStateId);
            ViewBag.applicationTypeId = new SelectList(db.ApplicationTypes, "applicationTypeId", "name", application.applicationTypeId);
            ViewBag.organizationId = new SelectList(db.Organizations, "organizationId", "name", application.organizationId);
            ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", application.resourceSubTypeId);
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", application.resourceTypeId);
            ViewBag.UserId = new SelectList(db.Users, "UserId", "name", application.UserId);
            return View(application);
        }

        // GET: Administration/Applications/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = await db.Applications.FindAsync(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            return View(application);
        }

        // POST: Administration/Applications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Application application = await db.Applications.FindAsync(id);
            db.Applications.Remove(application);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
