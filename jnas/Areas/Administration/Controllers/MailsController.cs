﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class MailsController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administration/Mails
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            return View(await db.Mails.OrderByDescending(d => d.createdDate).ToListAsync());
        }

        // GET: Administration/Mails/Details/5
        public async Task<ActionResult> Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mail mail = await db.Mails.FindAsync(id);
            if (mail == null)
            {
                return HttpNotFound();
            }

            ViewBag.To = "";
            try
            {
                var filter = Convert.ToInt32(mail.toAddress);
                var orgTo = db.Organizations.Where(o => o.organizationId == filter).FirstOrDefault();
                if (orgTo != null)
                {
                    ViewBag.To = orgTo.name;
                }
            }
            catch { }
                  

            ViewBag.From = "";
            try
            {
                var filter1 = Convert.ToInt32(mail.fromAddress);
                var orgFrom = db.Organizations.Where(o => o.organizationId == filter1).FirstOrDefault();
                if (orgFrom != null)
                {
                    ViewBag.From = orgFrom.name;
                }
            }
            catch { }


            return View(mail);
        }

        // GET: Administration/Mails/Create
        public ActionResult Create()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            //  var orgId = user.organizationId.ToString();

            
            var result = db.Organizations.Where(o => o.OrganizationType1.name == "Primary Service Provider" || o.OrganizationType1.name == "Secondary Service Provider").ToList();
            
            ViewBag.To = new SelectList(result, "organizationId", "name");
            

            ViewBag.From = "";
            try
            {
               
                var orgFrom = db.Organizations.Where(o => o.organizationId == user.organizationId).FirstOrDefault();
                if (orgFrom != null)
                {
                    ViewBag.From = orgFrom.name;
                }
            }
            catch { }

            return View();
        }

        // POST: Administration/Mails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "mailId,title,toAddress,fromAddress,message")] Mail mail)
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            var orgId = user.organizationId.ToString();


            if (ModelState.IsValid)
            {
                var currentDate = DateTime.Now;

                mail.fromAddress = orgId;
                mail.createdDate = currentDate;
                mail.dateChanged = currentDate;

                db.Mails.Add(mail);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            var result = db.Organizations.Where(o => o.OrganizationType1.name == "Primary Service Provider" || o.OrganizationType1.name == "Secondary Service Provider").ToList();

            try
            {
                ViewBag.To = new SelectList(result, "organizationId", "name", Convert.ToInt32(mail.toAddress));
            }
            catch
            {
                ViewBag.To = new SelectList(result, "organizationId", "name");
            }

            ViewBag.From = "";
            try
            {
               // var filter1 = Convert.ToInt32(mail.fromAddress);
                var orgFrom = db.Organizations.Where(o => o.organizationId == user.organizationId).FirstOrDefault();
                if (orgFrom != null)
                {
                    ViewBag.From = orgFrom.name;
                }
            }
            catch { }

            return View(mail);
        }

        // GET: Administration/Mails/Edit/5
        public async Task<ActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mail mail = await db.Mails.FindAsync(id);
            if (mail == null)
            {
                return HttpNotFound();
            }


            ViewBag.To = "";
            try
            {
                var filter = Convert.ToInt32(mail.toAddress);
                var orgTo = db.Organizations.Where(o => o.organizationId == filter).FirstOrDefault();
                if (orgTo != null)
                {
                    ViewBag.To = orgTo.name;
                }
            }
            catch { }

            ViewBag.From = "";
            try
            {
                var filter1 = Convert.ToInt32(mail.fromAddress);
                var orgFrom = db.Organizations.Where(o => o.organizationId == filter1).FirstOrDefault();
                if (orgFrom != null)
                {
                    ViewBag.From = orgFrom.name;
                }
            }
            catch { }

            TempData["To"] = mail.toAddress;
            return View(mail);
        }

        // POST: Administration/Mails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "mailId,title,toAddress,fromAddress,message")] Mail mail)
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            var orgId = user.organizationId.ToString();
            var To = TempData["To"].ToString();

            if (ModelState.IsValid)
            {
                var currentDate = DateTime.Now;

                mail.toAddress = To;
                mail.fromAddress = orgId;
                mail.dateChanged = currentDate;

                db.Entry(mail).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }


            ViewBag.To = "";
            try
            {
                var filter = Convert.ToInt32(To);
                var orgTo = db.Organizations.Where(o => o.organizationId == filter).FirstOrDefault();
                if (orgTo != null)
                {
                    ViewBag.To = orgTo.name;
                }
            }
            catch { }

            ViewBag.From = "";
            try
            {
                var filter1 = Convert.ToInt32(mail.fromAddress);
                var orgFrom = db.Organizations.Where(o => o.organizationId == filter1).FirstOrDefault();
                if (orgFrom != null)
                {
                    ViewBag.From = orgFrom.name;
                }
            }
            catch { }

            TempData["To"] = To;
            return View(mail);
        }

        // GET: Administration/Mails/Delete/5
        public async Task<ActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mail mail = await db.Mails.FindAsync(id);
            if (mail == null)
            {
                return HttpNotFound();
            }

            ViewBag.To = "";
            try
            {
                var filter = Convert.ToInt32(mail.toAddress);
                var orgTo = db.Organizations.Where(o => o.organizationId == filter).FirstOrDefault();
                if (orgTo != null)
                {
                    ViewBag.To = orgTo.name;
                }
            }
            catch { }


            ViewBag.From = "";
            try
            {
                var filter1 = Convert.ToInt32(mail.fromAddress);
                var orgFrom = db.Organizations.Where(o => o.organizationId == filter1).FirstOrDefault();
                if (orgFrom != null)
                {
                    ViewBag.From = orgFrom.name;
                }
            }
            catch
            { }

                return View(mail);
        }

        // POST: Administration/Mails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(long id)
        {
            Mail mail = await db.Mails.FindAsync(id);
            db.Mails.Remove(mail);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
