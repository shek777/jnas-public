﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using jnas.Models;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class UserRolesController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private ApplicationDbContext db1 = new ApplicationDbContext();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administration/UserRoles
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            var userRoles = db.UserRoles.Include(u => u.Role).Include(u => u.User);
            return View(await userRoles.ToListAsync());
        }

        // GET: Administration/UserRoles/Details/5
        public async Task<ActionResult> Details(string UserId, string RoleId)
        {
            if (UserId == null || RoleId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            UserRole userRole = await (from d in db.UserRoles where d.UserId == UserId && d.RoleId == RoleId select d).FirstOrDefaultAsync();


            if (userRole == null)
            {
                return HttpNotFound();
            }
            return View(userRole);
        }

        // GET: Administration/UserRoles/Create
        public ActionResult Create()
        {
            ViewBag.RoleId = new SelectList(db.Roles, "RoleId", "Name");
            ViewBag.UserId = new SelectList(db.Users, "UserId", "name");
            return View();
        }

        // POST: Administration/UserRoles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "UserId,RoleId")] UserRole userRole)
        {
            if (ModelState.IsValid)
            {
                UserRole userRoleDuplicationCheck = await (from d in db.UserRoles where d.UserId == userRole.UserId && d.RoleId == userRole.RoleId select d).FirstOrDefaultAsync();

                if (userRoleDuplicationCheck != null)
                {
                    ViewBag.RoleId = new SelectList(db.Roles, "RoleId", "Name", userRole.RoleId);
                    ViewBag.UserId = new SelectList(db.Users, "UserId", "name", userRole.UserId);
                    return View(userRole);
                }

                UserManager manager = new UserManager(new UserStore<jnas.Models.User>(db1));



                Role GetRoleName = await (from d in db.Roles where d.RoleId == userRole.RoleId select d).FirstOrDefaultAsync();

                IdentityResult result1 = await manager.AddToRoleAsync(userRole.UserId, GetRoleName.Name);

                //userRole.Discriminator = "IdentityUserRole";
                //db.UserRoles.Add(userRole);
                //await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.RoleId = new SelectList(db.Roles, "RoleId", "Name", userRole.RoleId);
            ViewBag.UserId = new SelectList(db.Users, "UserId", "name", userRole.UserId);
            return View(userRole);
        }

        // GET: Administration/UserRoles/Edit/5
        public async Task<ActionResult> Edit(string UserId, string RoleId)
        {
            if (UserId == null || RoleId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            UserRole userRole = await (from d in db.UserRoles where d.UserId == UserId && d.RoleId == RoleId select d).FirstOrDefaultAsync();

            if (userRole == null)
            {
                return HttpNotFound();
            }
            ViewBag.RoleId = new SelectList(db.Roles, "RoleId", "Name", userRole.RoleId);
            ViewBag.UserId = new SelectList(db.Users, "UserId", "name", userRole.UserId);
            return View(userRole);
        }

        // POST: Administration/UserRoles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "UserId,RoleId")] UserRole userRole)
        {
            if (ModelState.IsValid)
            {
                UserRole userRoleDuplicationCheck = await (from d in db.UserRoles where d.UserId == userRole.UserId && d.RoleId == userRole.RoleId select d).FirstOrDefaultAsync();

                if (userRoleDuplicationCheck != null)
                {
                    ViewBag.RoleId = new SelectList(db.Roles, "RoleId", "Name", userRole.RoleId);
                    ViewBag.UserId = new SelectList(db.Users, "UserId", "name", userRole.UserId);
                    return View(userRole);
                }

                db.Entry(userRole).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.RoleId = new SelectList(db.Roles, "RoleId", "Name", userRole.RoleId);
            ViewBag.UserId = new SelectList(db.Users, "UserId", "name", userRole.UserId);
            return View(userRole);
        }

        // GET: Administration/UserRoles/Delete/5
        public async Task<ActionResult> Delete(string UserId, string RoleId)
        {
            if (UserId == null || RoleId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            UserRole userRole = await (from d in db.UserRoles where d.UserId == UserId && d.RoleId == RoleId select d).FirstOrDefaultAsync();

            if (userRole == null)
            {
                return HttpNotFound();
            }
            return View(userRole);
        }

        // POST: Administration/UserRoles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string UserId, string RoleId)
        {
            UserRole userRole = await (from d in db.UserRoles where d.UserId == UserId && d.RoleId == RoleId select d).FirstOrDefaultAsync();


            UserManager manager = new UserManager(new UserStore<jnas.Models.User>(db1));





            IdentityResult result1 = await manager.RemoveFromRoleAsync(userRole.UserId, userRole.Role.Name);


            //db.UserRoles.Remove(userRole);
            //await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
