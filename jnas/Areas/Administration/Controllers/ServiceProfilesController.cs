﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;


namespace jnas.Areas.Administration.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class ServiceProfilesController : Controller
    {
        private Jnasdb db = new Jnasdb();
                
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        // GET: Administration/ServiceProfiles
        public async Task<ActionResult> Index()
        {

            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            var serviceProfiles = db.ServiceProfiles.Include(s => s.ProviderService1).Include(s => s.ResourceSubType1).Include(s => s.ResourceType1).Include(s => s.Service1);
            return View(await serviceProfiles.ToListAsync());
        }

        // GET: Administration/ServiceProfiles/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceProfile serviceProfile = await db.ServiceProfiles.FindAsync(id);
            if (serviceProfile == null)
            {
                return HttpNotFound();
            }
            return View(serviceProfile);
        }

        // GET: Administration/ServiceProfiles/Create
        public ActionResult Create()
        {
            ViewBag.providerServiceId = new SelectList(db.ProviderServices, "ProviderServiceId", "name");
            ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name");
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name");
            ViewBag.serviceId = new SelectList(db.Services, "serviceId", "name");
            return View();
        }

        // POST: Administration/ServiceProfiles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "serviceProfileId,serviceId,service,resourceTypeId,resourceType,resourceSubTypeId,resourceSubType,providerServiceId,providerService,assigned")] ServiceProfile serviceProfile)
        {
            if (ModelState.IsValid)
            {
                db.ServiceProfiles.Add(serviceProfile);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.providerServiceId = new SelectList(db.ProviderServices, "ProviderServiceId", "name", serviceProfile.providerServiceId);
            ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", serviceProfile.resourceSubTypeId);
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", serviceProfile.resourceTypeId);
            ViewBag.serviceId = new SelectList(db.Services, "serviceId", "name", serviceProfile.serviceId);
            return View(serviceProfile);
        }

        // GET: Administration/ServiceProfiles/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceProfile serviceProfile = await db.ServiceProfiles.FindAsync(id);
            if (serviceProfile == null)
            {
                return HttpNotFound();
            }
            ViewBag.providerServiceId = new SelectList(db.ProviderServices, "ProviderServiceId", "name", serviceProfile.providerServiceId);
            ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", serviceProfile.resourceSubTypeId);
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", serviceProfile.resourceTypeId);
            ViewBag.serviceId = new SelectList(db.Services, "serviceId", "name", serviceProfile.serviceId);
            return View(serviceProfile);
        }

        // POST: Administration/ServiceProfiles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "serviceProfileId,serviceId,service,resourceTypeId,resourceType,resourceSubTypeId,resourceSubType,providerServiceId,providerService,assigned")] ServiceProfile serviceProfile)
        {
            if (ModelState.IsValid)
            {
                db.Entry(serviceProfile).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.providerServiceId = new SelectList(db.ProviderServices, "ProviderServiceId", "name", serviceProfile.providerServiceId);
            ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", serviceProfile.resourceSubTypeId);
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", serviceProfile.resourceTypeId);
            ViewBag.serviceId = new SelectList(db.Services, "serviceId", "name", serviceProfile.serviceId);
            return View(serviceProfile);
        }

        // GET: Administration/ServiceProfiles/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceProfile serviceProfile = await db.ServiceProfiles.FindAsync(id);
            if (serviceProfile == null)
            {
                return HttpNotFound();
            }
            return View(serviceProfile);
        }

        // POST: Administration/ServiceProfiles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ServiceProfile serviceProfile = await db.ServiceProfiles.FindAsync(id);
            db.ServiceProfiles.Remove(serviceProfile);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
