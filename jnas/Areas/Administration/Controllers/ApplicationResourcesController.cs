﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;


namespace jnas.Areas.Administration.Controllers
{
    public class ApplicationResourcesController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        // GET: Administration/ApplicationResources
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            var applicationResources = db.ApplicationResources.Include(a => a.Application1).Include(a => a.Resource1);
            return View(await applicationResources.ToListAsync());
        }

        // GET: Administration/ApplicationResources/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationResource applicationResource = await db.ApplicationResources.FindAsync(id);
            if (applicationResource == null)
            {
                return HttpNotFound();
            }
            return View(applicationResource);
        }

        private int getVacantResourceState()
        {
            var rs = 3;

            var result = db.ResourceStates.Where(res => res.name == "Vacant").Select(d => d.resourceStateId).FirstOrDefault();

            return result != 0 ? result : rs;

        }

        private int getReservedResourceState()
        {
            var rs = 1;

            var result = db.ResourceStates.Where(res => res.name == "Reserved").Select(d => d.resourceStateId).FirstOrDefault();

            return result != 0 ? result : rs;

        }


        // GET: Administration/ApplicationResources/Create
        public ActionResult Create()
        {
            var results = (from tabledata in db.Resources.Where(r => r.resourceStateId == getVacantResourceState()) select new { tabledata.resourceId, name = tabledata.resource1 + " - " + tabledata.type + " " + tabledata.subtype }).ToList();
            ViewBag.resourceId = new SelectList(results.AsEnumerable(), "resourceId", "name");

            var results1 = (from tabledata in db.Applications select new { tabledata.applicationId, name = tabledata.applicationReference }).ToList();
            ViewBag.applicationId = new SelectList(results1.AsEnumerable(), "applicationId", "name");

            //ViewBag.Application_applicationId = new SelectList(db.Applications, "applicationId", "application1");
            //ViewBag.Application_applicationId1 = new SelectList(db.Applications, "applicationId", "application1");
            ViewBag.applicationResourceStateId = new SelectList(db.ApplicationResourceStates, "applicationResourceStateId", "name");
            //ViewBag.ApplicationResourceState_applicationResourceStateId = new SelectList(db.ApplicationResourceStates, "applicationResourceStateId", "name");
            //ViewBag.ApplicationResourceState_applicationResourceStateId1 = new SelectList(db.ApplicationResourceStates, "applicationResourceStateId", "name");
            //ViewBag.Organization_organizationId = new SelectList(db.Organizations, "organizationId", "name");
            //ViewBag.Resource_resourceId = new SelectList(db.Resources, "resourceId", "resource1");
            //ViewBag.Resource_resourceId1 = new SelectList(db.Resources, "resourceId", "resource1");
            return View();
        }

        // POST: Administration/ApplicationResources/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "applicationResourceId,applicationId,application,resourceId,resource,createdDate,dateChanged,applicationResourceStateId,state")] ApplicationResource applicationResource)
        {
            if (ModelState.IsValid)
            {
                db.ApplicationResources.Add(applicationResource);
                await db.SaveChangesAsync();

                //Chamge Resource State to Reserved

                var original = db.Resources.Find(applicationResource.resourceId);

                if (original != null)
                {
                    original.resourceStateId = getReservedResourceState();
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }

            ViewBag.applicationResourceStateId = new SelectList(db.ApplicationResourceStates, "applicationResourceStateId", "name", applicationResource.applicationResourceStateId);
            var results1 = (from tabledata in db.Applications select new { tabledata.applicationId, name = tabledata.applicationReference }).ToList();
            ViewBag.applicationId = new SelectList(results1.AsEnumerable(), "applicationId", "name", applicationResource.applicationId);
            var results = (from tabledata in db.Resources.Where(r => r.resourceStateId == getVacantResourceState()) select new { tabledata.resourceId, name = tabledata.resource1 + " - " + tabledata.type + " " + tabledata.subtype }).ToList();
            ViewBag.resourceId = new SelectList(results.AsEnumerable(), "resourceId", "name", Convert.ToInt32(applicationResource.resourceId));
            //ViewBag.Application_applicationId = new SelectList(db.Applications, "applicationId", "application1", applicationResource.Application_applicationId);
            //ViewBag.Application_applicationId1 = new SelectList(db.Applications, "applicationId", "application1", applicationResource.Application_applicationId1);
            //ViewBag.ApplicationResourceState_applicationResourceStateId = new SelectList(db.ApplicationResourceStates, "applicationResourceStateId", "name", applicationResource.ApplicationResourceState_applicationResourceStateId);
            //ViewBag.ApplicationResourceState_applicationResourceStateId1 = new SelectList(db.ApplicationResourceStates, "applicationResourceStateId", "name", applicationResource.ApplicationResourceState_applicationResourceStateId1);
            //ViewBag.Organization_organizationId = new SelectList(db.Organizations, "organizationId", "name", applicationResource.Organization_organizationId);
            //ViewBag.Resource_resourceId = new SelectList(db.Resources, "resourceId", "resource1", applicationResource.Resource_resourceId);
            //ViewBag.Resource_resourceId1 = new SelectList(db.Resources, "resourceId", "resource1", applicationResource.Resource_resourceId1);
            return View(applicationResource);
        }

        // GET: Administration/ApplicationResources/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationResource applicationResource = await db.ApplicationResources.FindAsync(id);
            if (applicationResource == null)
            {
                return HttpNotFound();
            }
            ViewBag.applicationResourceStateId = new SelectList(db.ApplicationResourceStates, "applicationResourceStateId", "name", applicationResource.applicationResourceStateId);
            var results1 = (from tabledata in db.Applications select new { tabledata.applicationId, name = tabledata.applicationReference }).ToList();
            ViewBag.applicationId = new SelectList(results1.AsEnumerable(), "applicationId", "name", applicationResource.applicationId);
            var results = (from tabledata in db.Resources.Where(r => r.resourceStateId == getVacantResourceState() || r.resourceId == applicationResource.resourceId) select new { tabledata.resourceId, name = tabledata.resource1 + " - " + tabledata.type + " " + tabledata.subtype }).ToList();
            ViewBag.resourceId = new SelectList(results.AsEnumerable(), "resourceId", "name", Convert.ToInt32(applicationResource.resourceId));
            //ViewBag.Application_applicationId = new SelectList(db.Applications, "applicationId", "application1", applicationResource.Application_applicationId);
            //ViewBag.Application_applicationId1 = new SelectList(db.Applications, "applicationId", "application1", applicationResource.Application_applicationId1);
            //ViewBag.ApplicationResourceState_applicationResourceStateId = new SelectList(db.ApplicationResourceStates, "applicationResourceStateId", "name", applicationResource.ApplicationResourceState_applicationResourceStateId);
            //ViewBag.ApplicationResourceState_applicationResourceStateId1 = new SelectList(db.ApplicationResourceStates, "applicationResourceStateId", "name", applicationResource.ApplicationResourceState_applicationResourceStateId1);
            //ViewBag.Organization_organizationId = new SelectList(db.Organizations, "organizationId", "name", applicationResource.Organization_organizationId);
            //ViewBag.Resource_resourceId = new SelectList(db.Resources, "resourceId", "resource1", applicationResource.Resource_resourceId);
            //ViewBag.Resource_resourceId1 = new SelectList(db.Resources, "resourceId", "resource1", applicationResource.Resource_resourceId1);
            return View(applicationResource);
        }

        // POST: Administration/ApplicationResources/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "applicationResourceId,applicationId,application,resourceId,resource,createdDate,dateChanged,applicationResourceStateId,state")] ApplicationResource applicationResource)
        {
            if (ModelState.IsValid)
            {
                db.Entry(applicationResource).State = EntityState.Modified;
                await db.SaveChangesAsync();

                //Chamge Resource State to Reserved

                var original = db.Resources.Find(applicationResource.resourceId);

                if (original != null)
                {
                    if (original.resourceStateId == getVacantResourceState())
                    {
                        original.resourceStateId = getReservedResourceState();
                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Index");
            }
            ViewBag.applicationResourceStateId = new SelectList(db.ApplicationResourceStates, "applicationResourceStateId", "name", applicationResource.applicationResourceStateId);
            var results1 = (from tabledata in db.Applications select new { tabledata.applicationId, name = tabledata.applicationReference }).ToList();
            ViewBag.applicationId = new SelectList(results1.AsEnumerable(), "applicationId", "name", applicationResource.applicationId);
            var results = (from tabledata in db.Resources.Where(r => r.resourceStateId == getVacantResourceState() || r.resourceId == applicationResource.resourceId) select new { tabledata.resourceId, name = tabledata.resource1 + " - " + tabledata.type + " " + tabledata.subtype }).ToList();
            ViewBag.resourceId = new SelectList(results.AsEnumerable(), "resourceId", "name", Convert.ToInt32(applicationResource.resourceId));
            //ViewBag.Application_applicationId = new SelectList(db.Applications, "applicationId", "application1", applicationResource.Application_applicationId);
            //ViewBag.Application_applicationId1 = new SelectList(db.Applications, "applicationId", "application1", applicationResource.Application_applicationId1);
            //ViewBag.ApplicationResourceState_applicationResourceStateId = new SelectList(db.ApplicationResourceStates, "applicationResourceStateId", "name", applicationResource.ApplicationResourceState_applicationResourceStateId);
            //ViewBag.ApplicationResourceState_applicationResourceStateId1 = new SelectList(db.ApplicationResourceStates, "applicationResourceStateId", "name", applicationResource.ApplicationResourceState_applicationResourceStateId1);
            //ViewBag.Organization_organizationId = new SelectList(db.Organizations, "organizationId", "name", applicationResource.Organization_organizationId);
            //ViewBag.Resource_resourceId = new SelectList(db.Resources, "resourceId", "resource1", applicationResource.Resource_resourceId);
            //ViewBag.Resource_resourceId1 = new SelectList(db.Resources, "resourceId", "resource1", applicationResource.Resource_resourceId1);
            return View(applicationResource);
        }

        // GET: Administration/ApplicationResources/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationResource applicationResource = await db.ApplicationResources.FindAsync(id);
            if (applicationResource == null)
            {
                return HttpNotFound();
            }
            return View(applicationResource);
        }

        // POST: Administration/ApplicationResources/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ApplicationResource applicationResource = await db.ApplicationResources.FindAsync(id);
            db.ApplicationResources.Remove(applicationResource);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
