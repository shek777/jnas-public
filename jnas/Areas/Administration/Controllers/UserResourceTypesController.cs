﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class UserResourceTypesController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administration/UserResourceTypes
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            var urt = await (from d in db.UserResourceTypes
                             select new jnas.Models.Jnasdb.UserResourceTypeDTO
                             {
                                 userResourceTypeId = d.userResourceTypeId,
                                 resourceTypeId = d.resourceTypeId,
                                 resourceSubTypeId = d.resourceSubTypeId,
                                 linkedResourceStateId = d.linkedResourceStateId,
                                 userTypeId = d.userTypeId,
                                 resourceType = (from t in db.ResourceTypes where t.resourceTypeId.Equals(d.resourceTypeId) select t.name).FirstOrDefault().ToString(),
                                 resourceSubType = (from t in db.ResourceSubTypes where t.resourceSubTypeId.Equals(d.resourceSubTypeId) select t.name).FirstOrDefault().ToString(),
                                 state = (from t in db.LinkedResourceStates where t.linkedResourceStateId.Equals(d.linkedResourceStateId) select t.name).FirstOrDefault().ToString(),
                                 userType = (from t in db.UserTypes where t.userTypeId.Equals(d.userTypeId) select t.name).FirstOrDefault().ToString(),
                             }).ToListAsync();
            return View(urt);
        }

        // GET: Administration/UserResourceTypes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           // UserResourceType userResourceType = await db.UserResourceTypes.FindAsync(id);
            var urt = await (from d in db.UserResourceTypes where d.userResourceTypeId == id
                             select new jnas.Models.Jnasdb.UserResourceTypeDTO
                             {
                                 userResourceTypeId = d.userResourceTypeId,
                                 resourceTypeId = d.resourceTypeId,
                                 resourceSubTypeId = d.resourceSubTypeId,
                                 linkedResourceStateId = d.linkedResourceStateId,
                                 userTypeId = d.userTypeId,
                                 resourceType = (from t in db.ResourceTypes where t.resourceTypeId.Equals(d.resourceTypeId) select t.name).FirstOrDefault().ToString(),
                                 resourceSubType = (from t in db.ResourceSubTypes where t.resourceSubTypeId.Equals(d.resourceSubTypeId) select t.name).FirstOrDefault().ToString(),
                                 state = (from t in db.LinkedResourceStates where t.linkedResourceStateId.Equals(d.linkedResourceStateId) select t.name).FirstOrDefault().ToString(),
                                 userType = (from t in db.UserTypes where t.userTypeId.Equals(d.userTypeId) select t.name).FirstOrDefault().ToString(),
                             }).FirstOrDefaultAsync();
            if (urt == null)
            {
                return HttpNotFound();
            }
            return View(urt);
        }

        // GET: Administration/UserResourceTypes/Create
        public ActionResult Create()
        {
            ViewBag.userTypeId = new SelectList(db.UserTypes, "userTypeId", "name");
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name");
            ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name");
            ViewBag.linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name");
            return View();
        }

        // POST: Administration/UserResourceTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "userResourceTypeId,userTypeId,resourceTypeId,resourceSubTypeId,linkedResourceStateId")] UserResourceType userResourceType)
        {
            if (ModelState.IsValid)
            {
                db.UserResourceTypes.Add(userResourceType);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.userTypeId = new SelectList(db.UserTypes, "userTypeId", "name", userResourceType.userTypeId);
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", userResourceType.resourceTypeId);
            ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", userResourceType.resourceSubTypeId);
            ViewBag.linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", userResourceType.linkedResourceStateId);
            return View(userResourceType);
        }

        // GET: Administration/UserResourceTypes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserResourceType userResourceType = await db.UserResourceTypes.FindAsync(id);
            if (userResourceType == null)
            {
                return HttpNotFound();
            }
            ViewBag.userTypeId = new SelectList(db.UserTypes, "userTypeId", "name", userResourceType.userTypeId);
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", userResourceType.resourceTypeId);
            ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", userResourceType.resourceSubTypeId);
            ViewBag.linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", userResourceType.linkedResourceStateId);
            return View(userResourceType);
        }

        // POST: Administration/UserResourceTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "userResourceTypeId,userTypeId,resourceTypeId,resourceSubTypeId,linkedResourceStateId")] UserResourceType userResourceType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userResourceType).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.userTypeId = new SelectList(db.UserTypes, "userTypeId", "name", userResourceType.userTypeId);
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", userResourceType.resourceTypeId);
            ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", userResourceType.resourceSubTypeId);
            ViewBag.linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", userResourceType.linkedResourceStateId);
            return View(userResourceType);
        }

        // GET: Administration/UserResourceTypes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           // UserResourceType userResourceType = await db.UserResourceTypes.FindAsync(id);
            var urt = await (from d in db.UserResourceTypes
                             where d.userResourceTypeId == id
                             select new jnas.Models.Jnasdb.UserResourceTypeDTO
                             {
                                 userResourceTypeId = d.userResourceTypeId,
                                 resourceTypeId = d.resourceTypeId,
                                 resourceSubTypeId = d.resourceSubTypeId,
                                 linkedResourceStateId = d.linkedResourceStateId,
                                 userTypeId = d.userTypeId,
                                 resourceType = (from t in db.ResourceTypes where t.resourceTypeId.Equals(d.resourceTypeId) select t.name).FirstOrDefault().ToString(),
                                 resourceSubType = (from t in db.ResourceSubTypes where t.resourceSubTypeId.Equals(d.resourceSubTypeId) select t.name).FirstOrDefault().ToString(),
                                 state = (from t in db.LinkedResourceStates where t.linkedResourceStateId.Equals(d.linkedResourceStateId) select t.name).FirstOrDefault().ToString(),
                                 userType = (from t in db.UserTypes where t.userTypeId.Equals(d.userTypeId) select t.name).FirstOrDefault().ToString(),
                             }).FirstOrDefaultAsync();
            if (urt == null)
            {
                return HttpNotFound();
            }
            return View(urt);
        }

        // POST: Administration/UserResourceTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            UserResourceType userResourceType = await db.UserResourceTypes.FindAsync(id);
            db.UserResourceTypes.Remove(userResourceType);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
