﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class OrganizationServicesController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administration/OrganizationServices
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            var organizationServices = db.OrganizationServices.Include(o => o.Address).Include(o => o.ProviderService1);
            return View(await organizationServices.ToListAsync());
        }

        // GET: Administration/OrganizationServices/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganizationService organizationService = await db.OrganizationServices.FindAsync(id);
            if (organizationService == null)
            {
                return HttpNotFound();
            }
            return View(organizationService);
        }

        // GET: Administration/OrganizationServices/Create
        public ActionResult Create()
        {
            ViewBag.AddressId = new SelectList(db.Addresses, "AddressId", "organization");
            ViewBag.ProviderServiceId = new SelectList(db.ProviderServices, "ProviderServiceId", "name");
            return View();
        }

        // POST: Administration/OrganizationServices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "organizationServicesId,AddressId,ProviderServiceId")] OrganizationService organizationService)
        {
            if (ModelState.IsValid)
            {
                if (organizationService.AddressId == 0)
                {
                    ViewBag.AddressId = new SelectList(db.Addresses, "AddressId", "organization", organizationService.AddressId);
                    ViewBag.ProviderServiceId = new SelectList(db.ProviderServices, "ProviderServiceId", "name", organizationService.ProviderServiceId);
                    return View(organizationService);
                }

                DateTime? now = DateTime.Now;

                organizationService.createdDate = now;
                organizationService.dateChanged = now;

                db.OrganizationServices.Add(organizationService);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.AddressId = new SelectList(db.Addresses, "AddressId", "organization", organizationService.AddressId);
            ViewBag.ProviderServiceId = new SelectList(db.ProviderServices, "ProviderServiceId", "name", organizationService.ProviderServiceId);
            return View(organizationService);
        }

        // GET: Administration/OrganizationServices/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganizationService organizationService = await db.OrganizationServices.FindAsync(id);
            if (organizationService == null)
            {
                return HttpNotFound();
            }
            ViewBag.AddressId = new SelectList(db.Addresses, "AddressId", "organization", organizationService.AddressId);
            ViewBag.ProviderServiceId = new SelectList(db.ProviderServices, "ProviderServiceId", "name", organizationService.ProviderServiceId);
            return View(organizationService);
        }

        // POST: Administration/OrganizationServices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "organizationServicesId,AddressId,ProviderServiceId")] OrganizationService organizationService)
        {
            if (ModelState.IsValid)
            {

                if (organizationService.AddressId == 0)
                {
                    ViewBag.AddressId = new SelectList(db.Addresses, "AddressId", "organization", organizationService.AddressId);
                    ViewBag.ProviderServiceId = new SelectList(db.ProviderServices, "ProviderServiceId", "name", organizationService.ProviderServiceId);
                    return View(organizationService);
                }

                DateTime? now = DateTime.Now;


                organizationService.dateChanged = now;

                db.Entry(organizationService).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.AddressId = new SelectList(db.Addresses, "AddressId", "organization", organizationService.AddressId);
            ViewBag.ProviderServiceId = new SelectList(db.ProviderServices, "ProviderServiceId", "name", organizationService.ProviderServiceId);
            return View(organizationService);
        }

        // GET: Administration/OrganizationServices/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganizationService organizationService = await db.OrganizationServices.FindAsync(id);
            if (organizationService == null)
            {
                return HttpNotFound();
            }
            return View(organizationService);
        }

        // POST: Administration/OrganizationServices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            OrganizationService organizationService = await db.OrganizationServices.FindAsync(id);
            db.OrganizationServices.Remove(organizationService);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
