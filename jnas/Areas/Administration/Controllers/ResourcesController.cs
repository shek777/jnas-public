﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    public class ResourcesController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        // GET: Administration/Resources
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }


            var resources = db.Resources.Include(r => r.ResourceState);
            return View(await resources.ToListAsync());
        }

        // GET: Administration/Resources/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Resource resource = await db.Resources.FindAsync(id);
            if (resource == null)
            {
                return HttpNotFound();
            }
            return View(resource);
        }

        // GET: Administration/Resources/Create
        public ActionResult Create()
        {
            ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name");
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name");
            //  var results = (from tabledata in db.ResourceLinks select new { tabledata.resourceLinkId, name = "Parent: " + (from tabledata1 in db.Resources where tabledata1.resourceId == tabledata.parentResource select tabledata1.resource1).FirstOrDefault() + " - " + tabledata.childResource }).ToList();
            var query = @"SELECT resourceLinkId, (SELECT resource1 FROM Resources WHERE(resourceId = CONVERT(int, ISNULL(ResourceLinks.parentResource, 0)))) AS parentresourcename, parentResource, (SELECT resource1 FROM Resources WHERE(resourceId = CONVERT(int, ISNULL(ResourceLinks.childResource, 0)))) AS childresourcename, childResource FROM ResourceLinks";


            List<ResourceState> resouceLink = new List<ResourceState>();

            // 1. Instantiate the connection
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(jnas.Configuration.Config.ConnectionString);
            System.Data.SqlClient.SqlDataReader rdr = null;
            try
            {
                // 2. Open the connection
                conn.Open();

                // 3. Pass the connection to a command object
                System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(query, conn);

                //
                // 4. Use the connection
                //

                // get query results
                rdr = cmd.ExecuteReader();

                // print the CustomerID of each record
                while (rdr.Read())
                {
                    // Get all Codes
                    
                    resouceLink.Add( new ResourceState { resourceStateId = Convert.ToInt32(rdr[0].ToString()), name = "Parent: " + rdr[1].ToString() + " Child: " + rdr[3].ToString()});
                }
            }
            finally
            {
                // close the reader
                if (rdr != null)
                {
                    rdr.Close();
                }

                // 5. Close the connection
                if (conn != null)
                {
                    conn.Close();
                }
            }


            
            ViewBag.resourceLinkId = new SelectList(resouceLink.AsEnumerable(), "resourceStateId", "name");
            var resourcestate = db.ResourceStates.Where(rs => rs.name == "Blocked").ToList();
            ViewBag.resourceStateId = new SelectList(resourcestate, "resourceStateId", "name");
            //ViewBag.ResourceState_resourceStateId = new SelectList(db.ResourceStates, "resourceStateId", "name");
            //ViewBag.ResourceState_resourceStateId1 = new SelectList(db.ResourceStates, "resourceStateId", "name");
            return View();
        }

        // POST: Administration/Resources/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "resourceId,resource1,resourceTypeId,type,resourceSubTypeId,subtype,binary,createdDate,dateChanged,resourceLinkId,resourceStateId,state")] Resource resource)
        {
            if (ModelState.IsValid)
            {
                var type = db.ResourceTypes.Where(rt => rt.resourceTypeId == resource.resourceTypeId).FirstOrDefault();
                if(type != null)
                 resource.type = type.name;
                var subtype = db.ResourceSubTypes.Where(rt => rt.resourceSubTypeId == resource.resourceSubTypeId).FirstOrDefault();
                if (subtype != null)
                    resource.subtype = subtype.name;

                var resourcestate1 = db.ResourceStates.Where(rs => rs.name == "Blocked").FirstOrDefault();
                if (resourcestate1 != null)
                {
                    resource.resourceStateId = resourcestate1.resourceStateId;
                }
                else
                {
                    resource.resourceStateId = 2;
                }
                resource.createdDate = DateTime.Now;
                resource.dateChanged = DateTime.Now;
                db.Resources.Add(resource);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            //

            ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", resource.resourceSubTypeId);
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", resource.resourceTypeId);
            //  var results = (from tabledata in db.ResourceLinks select new { tabledata.resourceLinkId, name = "Parent: " + (from tabledata1 in db.Resources where tabledata1.resourceId == tabledata.parentResource select tabledata1.resource1).FirstOrDefault() + " - " + tabledata.childResource }).ToList();
            var query = @"SELECT resourceLinkId, (SELECT resource1 FROM Resources WHERE(resourceId = CONVERT(int, ISNULL(ResourceLinks.parentResource, 0)))) AS parentresourcename, parentResource, (SELECT resource1 FROM Resources WHERE(resourceId = CONVERT(int, ISNULL(ResourceLinks.childResource, 0)))) AS childresourcename, childResource FROM ResourceLinks";


            List<ResourceState> resouceLink = new List<ResourceState>();

            // 1. Instantiate the connection
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(jnas.Configuration.Config.ConnectionString);
            System.Data.SqlClient.SqlDataReader rdr = null;
            try
            {
                // 2. Open the connection
                conn.Open();

                // 3. Pass the connection to a command object
                System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(query, conn);

                //
                // 4. Use the connection
                //

                // get query results
                rdr = cmd.ExecuteReader();

                // print the CustomerID of each record
                while (rdr.Read())
                {
                    // Get all Codes

                    resouceLink.Add(new ResourceState { resourceStateId = Convert.ToInt32(rdr[0].ToString()), name = "Parent: " + rdr[1].ToString() + " Child: " + rdr[3].ToString() });
                }
            }
            finally
            {
                // close the reader
                if (rdr != null)
                {
                    rdr.Close();
                }

                // 5. Close the connection
                if (conn != null)
                {
                    conn.Close();
                }
            }



            ViewBag.resourceLinkId = new SelectList(resouceLink.AsEnumerable(), "resourceStateId", "name", resource.resourceLinkId);

            //
            var resourcestate = db.ResourceStates.Where(rs => rs.name == "Blocked").ToList();
            ViewBag.resourceStateId = new SelectList(resourcestate, "resourceStateId", "name", resource.resourceStateId);
           
            return View(resource);
        }

        // GET: Administration/Resources/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Resource resource = await db.Resources.FindAsync(id);
            if (resource == null)
            {
                return HttpNotFound();
            }


            //

            ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", resource.resourceSubTypeId);
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", resource.resourceTypeId);
            //  var results = (from tabledata in db.ResourceLinks select new { tabledata.resourceLinkId, name = "Parent: " + (from tabledata1 in db.Resources where tabledata1.resourceId == tabledata.parentResource select tabledata1.resource1).FirstOrDefault() + " - " + tabledata.childResource }).ToList();
            var query = @"SELECT resourceLinkId, (SELECT resource1 FROM Resources WHERE(resourceId = CONVERT(int, ISNULL(ResourceLinks.parentResource, 0)))) AS parentresourcename, parentResource, (SELECT resource1 FROM Resources WHERE(resourceId = CONVERT(int, ISNULL(ResourceLinks.childResource, 0)))) AS childresourcename, childResource FROM ResourceLinks";


            List<ResourceState> resouceLink = new List<ResourceState>();

            // 1. Instantiate the connection
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(jnas.Configuration.Config.ConnectionString);
            System.Data.SqlClient.SqlDataReader rdr = null;
            try
            {
                // 2. Open the connection
                conn.Open();

                // 3. Pass the connection to a command object
                System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(query, conn);

                //
                // 4. Use the connection
                //

                // get query results
                rdr = cmd.ExecuteReader();

                // print the CustomerID of each record
                while (rdr.Read())
                {
                    // Get all Codes

                    resouceLink.Add(new ResourceState { resourceStateId = Convert.ToInt32(rdr[0].ToString()), name = "Parent: " + rdr[1].ToString() + " Child: " + rdr[3].ToString() });
                }
            }
            finally
            {
                // close the reader
                if (rdr != null)
                {
                    rdr.Close();
                }

                // 5. Close the connection
                if (conn != null)
                {
                    conn.Close();
                }
            }



            ViewBag.resourceLinkId = new SelectList(resouceLink.AsEnumerable(), "resourceStateId", "name", resource.resourceLinkId);

            //Set the resourcestates
            var resourceStates = getResourceStates(resource.resourceStateId);
            ViewBag.resourceStateId = new SelectList(resourceStates, "resourceStateId", "name", resource.resourceStateId);


            //ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", resource.resourceSubTypeId);
            //ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", resource.resourceTypeId);
            //var results = (from tabledata in db.ResourceLinks select new { tabledata.resourceLinkId, name = tabledata.parentResource + " - " + tabledata.childResource }).ToList();
            //ViewBag.resourceLinkId = new SelectList(results.AsEnumerable(), "resourceLinkId", "name", resource.resourceLinkId);
            //ViewBag.resourceStateId = new SelectList(db.ResourceStates, "resourceStateId", "name", resource.resourceStateId);
            //ViewBag.ResourceState_resourceStateId = new SelectList(db.ResourceStates, "resourceStateId", "name", resource.ResourceState_resourceStateId);
            //ViewBag.ResourceState_resourceStateId1 = new SelectList(db.ResourceStates, "resourceStateId", "name", resource.ResourceState_resourceStateId1);
            TempData["resource"] = resource;
            return View(resource);
        }


        private int getAgingResourceState()
        {
            var rs = 7;

            var result = db.ResourceStates.Where(res => res.name == "Aging").Select(d => d.resourceStateId).FirstOrDefault();

            return result != 0 ? result : rs;

        }

        private List<ResourceState> getResourceStates(int resourceStateId)
        {
            List<ResourceState> rs = new List<ResourceState>();

            var name = db.ResourceStates.Where(res => res.resourceStateId == resourceStateId).Select(d => d.name).FirstOrDefault();

            switch (name)
            {
                case "Blocked":
                    rs = db.ResourceStates.Where(r => r.name == "Vacant" || r.name == "Discontinued" || r.resourceStateId == resourceStateId).ToList();
                break;

                case "Vacant":
                    rs = db.ResourceStates.Where(r => r.name == "Blocked" || r.name == "Reserved" || r.resourceStateId == resourceStateId).ToList();
                    break;

                case "Reserved":
                    rs = db.ResourceStates.Where(r => r.name == "Assigned" || r.name == "Suspended" || r.resourceStateId == resourceStateId).ToList();
                    break;

                case "Assigned":
                    rs = db.ResourceStates.Where(r => r.name == "Active" || r.name == "Suspended" || r.resourceStateId == resourceStateId).ToList();
                    break;

                case "Active":
                    rs = db.ResourceStates.Where(r => r.name == "Aging" || r.name == "Suspended" || r.resourceStateId == resourceStateId).ToList();
                    break;

                case "Suspended":
                    rs = db.ResourceStates.Where(r => r.name == "Blocked" || r.name == "Discontinued" || r.name == "Reserved" || r.name == "Assigned" || r.name == "Aging" || r.name == "Assigned" || r.resourceStateId == resourceStateId).ToList();
                    break;

                case "Aging":
                    rs = db.ResourceStates.Where(r => r.name == "Blocked" || r.resourceStateId == resourceStateId).ToList();
                    break;

               
                default:
                    rs = db.ResourceStates.Where(r => r.name == "Vacant" || r.name == "Discontinued" || r.resourceStateId == resourceStateId).ToList();
                    break;
            }

            return rs;
        }

        // POST: Administration/Resources/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "resourceId,resource1,resourceTypeId,type,resourceSubTypeId,subtype,binary,createdDate,dateChanged,resourceLinkId,resourceStateId,state")] Resource resource)
        {
            var resource_original = TempData["resource"] as Resource;

            if (ModelState.IsValid)
            {

                //Don't Edit when resourcestate is set to aging 
                if(resource_original.resourceStateId == getAgingResourceState())
                {
                    return RedirectToAction("Index");
                }

                var type = db.ResourceTypes.Where(rt => rt.resourceTypeId == resource.resourceTypeId).FirstOrDefault();
                if (type != null)
                    resource.type = type.name;
                var subtype = db.ResourceSubTypes.Where(rt => rt.resourceSubTypeId == resource.resourceSubTypeId).FirstOrDefault();
                if (subtype != null)
                    resource.subtype = subtype.name;
              //resource.createdDate = DateTime.Now;
                resource.dateChanged = DateTime.Now;
                db.Entry(resource).State = EntityState.Modified;

               await db.SaveChangesAsync();

                // send Alerts
                var alert = new jnas.Alerts.Alerts();

                if (resource_original.resourceStateId != resource.resourceStateId && resource.resourceStateId == alert.getAssignedId())
                {
                    alert.Changed(resource);
                }


                return RedirectToAction("Index");
            }

            //

            ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", resource.resourceSubTypeId);
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", resource.resourceTypeId);
            //  var results = (from tabledata in db.ResourceLinks select new { tabledata.resourceLinkId, name = "Parent: " + (from tabledata1 in db.Resources where tabledata1.resourceId == tabledata.parentResource select tabledata1.resource1).FirstOrDefault() + " - " + tabledata.childResource }).ToList();
            var query = @"SELECT resourceLinkId, (SELECT resource1 FROM Resources WHERE(resourceId = CONVERT(int, ISNULL(ResourceLinks.parentResource, 0)))) AS parentresourcename, parentResource, (SELECT resource1 FROM Resources WHERE(resourceId = CONVERT(int, ISNULL(ResourceLinks.childResource, 0)))) AS childresourcename, childResource FROM ResourceLinks";


            List<ResourceState> resouceLink = new List<ResourceState>();

            // 1. Instantiate the connection
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(jnas.Configuration.Config.ConnectionString);
            System.Data.SqlClient.SqlDataReader rdr = null;
            try
            {
                // 2. Open the connection
                conn.Open();

                // 3. Pass the connection to a command object
                System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(query, conn);

                //
                // 4. Use the connection
                //

                // get query results
                rdr = cmd.ExecuteReader();

                // print the CustomerID of each record
                while (rdr.Read())
                {
                    // Get all Codes

                    resouceLink.Add(new ResourceState { resourceStateId = Convert.ToInt32(rdr[0].ToString()), name = "Parent: " + rdr[1].ToString() + " Child: " + rdr[3].ToString() });
                }
            }
            finally
            {
                // close the reader
                if (rdr != null)
                {
                    rdr.Close();
                }

                // 5. Close the connection
                if (conn != null)
                {
                    conn.Close();
                }
            }



            ViewBag.resourceLinkId = new SelectList(resouceLink.AsEnumerable(), "resourceStateId", "name", resource.resourceLinkId);

            //
            //Set the resourcestates
            var resourceStates = getResourceStates(resource.resourceStateId);
            ViewBag.resourceStateId = new SelectList(resourceStates, "resourceStateId", "name", resource.resourceStateId);

           // ViewBag.resourceStateId = new SelectList(db.ResourceStates, "resourceStateId", "name", resource.resourceStateId);



            //ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", resource.resourceSubTypeId);
            //ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", resource.resourceTypeId);
            //var results = (from tabledata in db.ResourceLinks select new { tabledata.resourceLinkId, name = tabledata.parentResource + " - " + tabledata.childResource }).ToList();
            //ViewBag.resourceLinkId = new SelectList(results.AsEnumerable(), "resourceLinkId", "name", resource.resourceLinkId);
            //ViewBag.resourceStateId = new SelectList(db.ResourceStates, "resourceStateId", "name", resource.resourceStateId);
            //ViewBag.ResourceState_resourceStateId = new SelectList(db.ResourceStates, "resourceStateId", "name", resource.ResourceState_resourceStateId);
            //ViewBag.ResourceState_resourceStateId1 = new SelectList(db.ResourceStates, "resourceStateId", "name", resource.ResourceState_resourceStateId1);
            TempData["resource"] = resource_original;
            return View(resource);
        }

        // GET: Administration/Resources/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Resource resource = await db.Resources.FindAsync(id);
            if (resource == null)
            {
                return HttpNotFound();
            }
            return View(resource);
        }

        // POST: Administration/Resources/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Resource resource = await db.Resources.FindAsync(id);
            db.Resources.Remove(resource);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
