﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class NrufFormU2Controller : Controller
    {
        private UserManager _userManager;
        private Jnasdb db = new Jnasdb();

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: External/NrufFormU2
        public async Task<ActionResult> Index(int? id)
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            if (id != null)
            {
                // var user = UserManager.FindById(User.Identity.GetUserId());
                var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == id).FirstOrDefaultAsync();

                if (nrufcompinfo != null)
                {
                    Session["NrufCompanyInfo"] = id;
                    Session["AppRef"] = nrufcompinfo.applicationReference;

                    //if (!CheckExpiryDate())
                    //{
                    //    return RedirectToAction("Index", "NrufCompanyInfoes");
                    //}
                    var nrufFormU2 = await (from d in db.NrufFormU2 where d.NrufCompanyInfoId == id select d).ToListAsync();
                    return View(nrufFormU2);
                }
            }



            // return View("Index", "NrufCompanyInfoes"); view viewName and masterLayout
            return RedirectToAction("Index", "NrufCompanyInfoes");
        }

        // GET: External/NrufFormU2/Details/5
        public async Task<ActionResult> Details(int? id)
        {

            if (Session["NrufCompanyInfo"] == null)
                return RedirectToAction("Index", "NrufCompanyInfoes");

            var compInfo = (int)Session["NrufCompanyInfo"];

            //if (!CheckExpiryDate())
            //{
            //    return RedirectToAction("Index", "NrufCompanyInfoes");
            //}

            if (id != null)
            {
                //  var user = UserManager.FindById(User.Identity.GetUserId());

                var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo).FirstOrDefaultAsync();

                if (nrufcompinfo == null)
                    return RedirectToAction("Index", "NrufCompanyInfoes");

                NrufFormU2 nrufFormU2 = await db.NrufFormU2.FindAsync(id);
                if (nrufFormU2 == null)
                {
                    return HttpNotFound();
                }
                return View(nrufFormU2);

            }



            // return View("Index", "NrufCompanyInfoes"); view viewName and masterLayout
            return RedirectToAction("Index", "NrufCompanyInfoes");


        }

        // GET: External/NrufFormU2/Create
        public ActionResult Create()
        {
            if (Session["NrufCompanyInfo"] == null)
                return RedirectToAction("Index", "NrufCompanyInfoes");


            //if (!CheckExpiryDate())
            //{
            //    return RedirectToAction("Index", "NrufCompanyInfoes");
            //}

            // ViewBag.NrufCompanyInfoId = new SelectList(db.NrufCompanyInfo, "NrufCompanyInfoId", "UserId");
            return View();
        }

        // POST: External/NrufFormU2/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "FormU2Id,NrufCompanyInfoId,Npa,Nxx,Rate,Assigned,Intermediate,Reserved,Aging,Admin,PortedOut,Notes")] NrufFormU2 nrufFormU2)
        {

            try
            {

                if (Session["NrufCompanyInfo"] == null)
                    return RedirectToAction("Index", "NrufCompanyInfoes");

                var compInfo = (int)Session["NrufCompanyInfo"];

                //if (!CheckExpiryDate())
                //{
                //    return RedirectToAction("Index", "NrufCompanyInfoes");
                //}

                if (ModelState.IsValid)
                {
                    // var user = UserManager.FindById(User.Identity.GetUserId());

                    var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo).FirstOrDefaultAsync();

                    if (nrufcompinfo == null)
                        return RedirectToAction("Index", "NrufCompanyInfoes");

                    string message;
                    var error = nrufFormU2.Calculate(out message);
                    if (error == 0)
                    {
                        throw new Exception(message);
                    }
                    nrufFormU2.NrufCompanyInfoId = compInfo;

                    var time = DateTime.Now;
                    nrufFormU2.createdDate = time;
                    nrufFormU2.dateChanged = time;

                    db.NrufFormU2.Add(nrufFormU2);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index", new { id = compInfo });
                }

            }
            catch (Exception dex)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator. " + dex.Message);
            }
            // ViewBag.NrufCompanyInfoId = new SelectList(db.NrufCompanyInfo, "NrufCompanyInfoId", "UserId", nrufFormU2.NrufCompanyInfoId);
            return View(nrufFormU2);
        }

        // GET: External/NrufFormU2/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {

            if (Session["NrufCompanyInfo"] == null)
                return RedirectToAction("Index", "NrufCompanyInfoes");

            var compInfo = (int)Session["NrufCompanyInfo"];

            //if (!CheckExpiryDate())
            //{
            //    return RedirectToAction("Index", "NrufCompanyInfoes");
            //}

            if (id != null)
            {
                // var user = UserManager.FindById(User.Identity.GetUserId());

                var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo).FirstOrDefaultAsync();

                if (nrufcompinfo == null)
                    return RedirectToAction("Index", "NrufCompanyInfoes");

                NrufFormU2 nrufFormU2 = await db.NrufFormU2.FindAsync(id);
                if (nrufFormU2 == null)
                {
                    return HttpNotFound();
                }
                return View(nrufFormU2);

            }



            // return View("Index", "NrufCompanyInfoes"); view viewName and masterLayout
            return RedirectToAction("Index", "NrufCompanyInfoes");
        }

        // POST: External/NrufFormU2/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = " FormU2Id,NrufCompanyInfoId,Npa,Nxx,Rate,Assigned,Intermediate,Reserved,Aging,Admin,PortedOut,Notes")] NrufFormU2 nrufFormU2)
        {

            try
            {

                if (Session["NrufCompanyInfo"] == null)
                    return RedirectToAction("Index", "NrufCompanyInfoes");

                var compInfo = (int)Session["NrufCompanyInfo"];

                //if (!CheckExpiryDate())
                //{
                //    return RedirectToAction("Index", "NrufCompanyInfoes");
                //}

                if (ModelState.IsValid)
                {
                    var user = UserManager.FindById(User.Identity.GetUserId());

                    var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo).FirstOrDefaultAsync();

                    if (nrufcompinfo == null)
                        return RedirectToAction("Index", "NrufCompanyInfoes");

                    NrufFormU2 nrufFormU2Val = await db.NrufFormU2.FindAsync(nrufFormU2.FormU2Id);

                    string message;
                    var error = nrufFormU2.Calculate(out message);
                    if (error == 0)
                    {
                        throw new Exception(message);
                    }

                    if (compInfo != nrufFormU2.NrufCompanyInfoId)
                    {
                        if (compInfo != nrufFormU2Val.NrufCompanyInfoId)
                        {
                            nrufFormU2.NrufCompanyInfoId = nrufFormU2Val.NrufCompanyInfoId;
                            Session["NrufCompanyInfo"] = nrufFormU2Val.NrufCompanyInfoId;
                        }
                        else
                        {
                            nrufFormU2.NrufCompanyInfoId = compInfo;
                        }
                    }


                    var time = DateTime.Now;
                    nrufFormU2.createdDate = nrufFormU2Val.createdDate;
                    nrufFormU2.dateChanged = time;

                    Detach(nrufFormU2Val);
                    db.Entry(nrufFormU2).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index", new { id = compInfo });
                }

            }
            catch (Exception dex)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator. " + dex.Message);
            }

            return View(nrufFormU2);


        }

        // GET: External/NrufFormU2/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {

            if (Session["NrufCompanyInfo"] == null)
                return RedirectToAction("Index", "NrufCompanyInfoes");

            var compInfo = (int)Session["NrufCompanyInfo"];

            //if (!CheckExpiryDate())
            //{
            //    return RedirectToAction("Index", "NrufCompanyInfoes");
            //}

            if (id != null)
            {
                //  var user = UserManager.FindById(User.Identity.GetUserId());

                var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo).FirstOrDefaultAsync();

                if (nrufcompinfo == null)
                    return RedirectToAction("Index", "NrufCompanyInfoes");

                NrufFormU2 nrufFormU2 = await db.NrufFormU2.FindAsync(id);
                if (nrufFormU2 == null)
                {
                    return HttpNotFound();
                }
                return View(nrufFormU2);

            }



            // return View("Index", "NrufCompanyInfoes"); view viewName and masterLayout
            return RedirectToAction("Index", "NrufCompanyInfoes");
        }

        // POST: External/NrufFormU2/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            // Only view my NRUF forms
            NrufFormU2 nrufFormU2 = await db.NrufFormU2.FindAsync(id);

            if (nrufFormU2 == null)
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            if ((int)Session["NrufCompanyInfo"] != nrufFormU2.NrufCompanyInfoId)
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            //if (!CheckExpiryDate())
            //{
            //    return RedirectToAction("Index", "NrufCompanyInfoes");
            //}

            db.NrufFormU2.Remove(nrufFormU2);
            await db.SaveChangesAsync();
            var compInfo = (int)Session["NrufCompanyInfo"];
            return RedirectToAction("Index", new { id = compInfo });
        }

        bool CheckExpiryDate()
        {
            // CHeck the Expiry Date
            NrufCompanyInfo nrufCompanyInfo = db.NrufCompanyInfo.Find((int)Session["NrufCompanyInfo"]);

            if (nrufCompanyInfo != null)
            {
                if (nrufCompanyInfo.expiryDate < DateTime.Now)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

            return true;
        }

        void Update(NrufFormU2 obj)
        {
            db.Entry(obj).State = EntityState.Modified;
            db.SaveChanges();
        }

        void Detach(NrufFormU2 obj)
        {
            db.Entry(obj).State = EntityState.Detached;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
