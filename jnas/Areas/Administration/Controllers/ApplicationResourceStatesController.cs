﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    public class ApplicationResourceStatesController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        // GET: Administration/ApplicationResourceStates
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            return View(await db.ApplicationResourceStates.ToListAsync());
        }

        // GET: Administration/ApplicationResourceStates/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationResourceState applicationResourceState = await db.ApplicationResourceStates.FindAsync(id);
            if (applicationResourceState == null)
            {
                return HttpNotFound();
            }
            return View(applicationResourceState);
        }

        // GET: Administration/ApplicationResourceStates/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Administration/ApplicationResourceStates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "applicationResourceStateId,name")] ApplicationResourceState applicationResourceState)
        {
            if (ModelState.IsValid)
            {
                db.ApplicationResourceStates.Add(applicationResourceState);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(applicationResourceState);
        }

        // GET: Administration/ApplicationResourceStates/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationResourceState applicationResourceState = await db.ApplicationResourceStates.FindAsync(id);
            if (applicationResourceState == null)
            {
                return HttpNotFound();
            }
            return View(applicationResourceState);
        }

        // POST: Administration/ApplicationResourceStates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "applicationResourceStateId,name")] ApplicationResourceState applicationResourceState)
        {
            if (ModelState.IsValid)
            {
                db.Entry(applicationResourceState).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(applicationResourceState);
        }

        // GET: Administration/ApplicationResourceStates/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationResourceState applicationResourceState = await db.ApplicationResourceStates.FindAsync(id);
            if (applicationResourceState == null)
            {
                return HttpNotFound();
            }
            return View(applicationResourceState);
        }

        // POST: Administration/ApplicationResourceStates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ApplicationResourceState applicationResourceState = await db.ApplicationResourceStates.FindAsync(id);
            db.ApplicationResourceStates.Remove(applicationResourceState);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
