﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class LinkedResourceStatesController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administration/LinkedResourceStates
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            return View(await db.LinkedResourceStates.ToListAsync());
        }

        // GET: Administration/LinkedResourceStates/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LinkedResourceState linkedResourceState = await db.LinkedResourceStates.FindAsync(id);
            if (linkedResourceState == null)
            {
                return HttpNotFound();
            }
            return View(linkedResourceState);
        }

        // GET: Administration/LinkedResourceStates/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Administration/LinkedResourceStates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "linkedResourceStateId,name")] LinkedResourceState linkedResourceState)
        {
            if (ModelState.IsValid)
            {
                db.LinkedResourceStates.Add(linkedResourceState);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(linkedResourceState);
        }

        // GET: Administration/LinkedResourceStates/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LinkedResourceState linkedResourceState = await db.LinkedResourceStates.FindAsync(id);
            if (linkedResourceState == null)
            {
                return HttpNotFound();
            }
            return View(linkedResourceState);
        }

        // POST: Administration/LinkedResourceStates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "linkedResourceStateId,name")] LinkedResourceState linkedResourceState)
        {
            if (ModelState.IsValid)
            {
                db.Entry(linkedResourceState).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(linkedResourceState);
        }

        // GET: Administration/LinkedResourceStates/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LinkedResourceState linkedResourceState = await db.LinkedResourceStates.FindAsync(id);
            if (linkedResourceState == null)
            {
                return HttpNotFound();
            }
            return View(linkedResourceState);
        }

        // POST: Administration/LinkedResourceStates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            LinkedResourceState linkedResourceState = await db.LinkedResourceStates.FindAsync(id);
            db.LinkedResourceStates.Remove(linkedResourceState);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
