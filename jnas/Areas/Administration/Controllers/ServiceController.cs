﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class ServiceController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administration/Service
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            return View(await db.Services.ToListAsync());
        }

        // GET: Administration/Service/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = await db.Services.FindAsync(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            return View(service);
        }

        // GET: Administration/Service/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Administration/Service/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "serviceId,name,applicationYN")] Service service)
        {
            if (ModelState.IsValid)
            {
                //Update applicationType table with the ew service
                if(service.applicationYN == true)
                {
                    var chk_appType = db.ApplicationTypes.Where(at => at.name.Equals(service.name)).FirstOrDefault();
                    if (chk_appType == null)
                    {
                        ApplicationType appType = new ApplicationType()
                        {
                            name = service.name
                        };
                        db.ApplicationTypes.Add(appType);
                    }
                }
                db.Services.Add(service);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(service);
        }

        // GET: Administration/Service/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = await db.Services.FindAsync(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            return View(service);
        }

        // POST: Administration/Service/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "serviceId,name,applicationYN")] Service service)
        {
            if (ModelState.IsValid)
            {

                //Update applicationType table with the updated service

                var chk_appType = db.ApplicationTypes.Where(at => at.name.Equals(service.name)).FirstOrDefault();
                if (chk_appType == null)
                {
                    var old_service = db.Services.Where(sv => sv.serviceId.Equals(service.serviceId)).FirstOrDefault();
                    var appType = db.ApplicationTypes.Where(at => at.name.Equals(old_service.name)).FirstOrDefault();
                    if (appType != null)
                    {
                        appType.name = service.name;
                        db.Entry(appType).State = EntityState.Modified;
                    }
                }

                db.Entry(service).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(service);
        }

        // GET: Administration/Service/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = await db.Services.FindAsync(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            return View(service);
        }

        // POST: Administration/Service/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {

            Service service = await db.Services.FindAsync(id);

            //Update applicationType table with the deletion of a service
           
               
                var appType = db.ApplicationTypes.Where(at => at.name.Equals(service.name)).FirstOrDefault();
                if (appType != null)
                {
                    db.ApplicationTypes.Remove(appType);
                }
            

            db.Services.Remove(service);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
