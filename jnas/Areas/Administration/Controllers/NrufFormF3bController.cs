﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class NrufFormF3bController : Controller
    {
        private UserManager _userManager;
        private Jnasdb db = new Jnasdb();

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: External/NrufFormF3b
        public async Task<ActionResult> Index(int? id)
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            if (id != null)
            {
                // var user = UserManager.FindById(User.Identity.GetUserId());
                var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == id).FirstOrDefaultAsync();

                if (nrufcompinfo != null)
                {
                    Session["NrufCompanyInfo"] = id;
                    Session["AppRef"] = nrufcompinfo.applicationReference;

                    //if (!CheckExpiryDate())
                    //{
                    //    return RedirectToAction("Index", "NrufCompanyInfoes");
                    //}
                    var nrufFormF3b = await (from d in db.NrufFormF3b where d.NrufCompanyInfoId == id select d).ToListAsync();
                    return View(nrufFormF3b);
                }
            }



            // return View("Index", "NrufCompanyInfoes"); view viewName and masterLayout
            return RedirectToAction("Index", "NrufCompanyInfoes");
        }

        // GET: External/NrufFormF3b/Details/5
        public async Task<ActionResult> Details(int? id)
        {

            if (Session["NrufCompanyInfo"] == null)
                return RedirectToAction("Index", "NrufCompanyInfoes");

            var compInfo = (int)Session["NrufCompanyInfo"];

            //if (!CheckExpiryDate())
            //{
            //    return RedirectToAction("Index", "NrufCompanyInfoes");
            //}

            if (id != null)
            {
                //  var user = UserManager.FindById(User.Identity.GetUserId());

                var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo).FirstOrDefaultAsync();

                if (nrufcompinfo == null)
                    return RedirectToAction("Index", "NrufCompanyInfoes");

                NrufFormF3b nrufFormF3b = await db.NrufFormF3b.FindAsync(id);
                if (nrufFormF3b == null)
                {
                    return HttpNotFound();
                }
                return View(nrufFormF3b);

            }



            // return View("Index", "NrufCompanyInfoes"); view viewName and masterLayout
            return RedirectToAction("Index", "NrufCompanyInfoes");


        }

        // GET: External/NrufFormF3b/Create
        public ActionResult Create()
        {
            if (Session["NrufCompanyInfo"] == null)
                return RedirectToAction("Index", "NrufCompanyInfoes");


            //if (!CheckExpiryDate())
            //{
            //    return RedirectToAction("Index", "NrufCompanyInfoes");
            //}

            // ViewBag.NrufCompanyInfoId = new SelectList(db.NrufCompanyInfo, "NrufCompanyInfoId", "UserId");
            return View();
        }

        // POST: External/NrufFormF3b/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Npa,Year1,Year2,Year3,Year4,Year5")] NrufFormF3b nrufFormF3b)
        {

            try
            {

                if (Session["NrufCompanyInfo"] == null)
                    return RedirectToAction("Index", "NrufCompanyInfoes");

                var compInfo = (int)Session["NrufCompanyInfo"];

                //if (!CheckExpiryDate())
                //{
                //    return RedirectToAction("Index", "NrufCompanyInfoes");
                //}

                if (ModelState.IsValid)
                {
                    // var user = UserManager.FindById(User.Identity.GetUserId());

                    var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo).FirstOrDefaultAsync();

                    if (nrufcompinfo == null)
                        return RedirectToAction("Index", "NrufCompanyInfoes");

                    string message;
                    var error = nrufFormF3b.Calculate(out message);
                    if (error == 0)
                    {
                        throw new Exception(message);
                    }
                    nrufFormF3b.NrufCompanyInfoId = compInfo;

                    var time = DateTime.Now;
                    nrufFormF3b.createdDate = time;
                    nrufFormF3b.dateChanged = time;

                    db.NrufFormF3b.Add(nrufFormF3b);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index", new { id = compInfo });
                }

            }
            catch (Exception dex)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator. " + dex.Message);
            }
            // ViewBag.NrufCompanyInfoId = new SelectList(db.NrufCompanyInfo, "NrufCompanyInfoId", "UserId", nrufFormF3b.NrufCompanyInfoId);
            return View(nrufFormF3b);
        }

        // GET: External/NrufFormF3b/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {

            if (Session["NrufCompanyInfo"] == null)
                return RedirectToAction("Index", "NrufCompanyInfoes");

            var compInfo = (int)Session["NrufCompanyInfo"];

            //if (!CheckExpiryDate())
            //{
            //    return RedirectToAction("Index", "NrufCompanyInfoes");
            //}

            if (id != null)
            {
                // var user = UserManager.FindById(User.Identity.GetUserId());

                var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo).FirstOrDefaultAsync();

                if (nrufcompinfo == null)
                    return RedirectToAction("Index", "NrufCompanyInfoes");

                NrufFormF3b nrufFormF3b = await db.NrufFormF3b.FindAsync(id);
                if (nrufFormF3b == null)
                {
                    return HttpNotFound();
                }
                return View(nrufFormF3b);

            }



            // return View("Index", "NrufCompanyInfoes"); view viewName and masterLayout
            return RedirectToAction("Index", "NrufCompanyInfoes");
        }

        // POST: External/NrufFormF3b/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "FormF3bId,NrufCompanyInfoId,Npa,Year1,Year2,Year3,Year4,Year5")] NrufFormF3b nrufFormF3b)
        {

            try
            {

                if (Session["NrufCompanyInfo"] == null)
                    return RedirectToAction("Index", "NrufCompanyInfoes");

                var compInfo = (int)Session["NrufCompanyInfo"];

                //if (!CheckExpiryDate())
                //{
                //    return RedirectToAction("Index", "NrufCompanyInfoes");
                //}

                if (ModelState.IsValid)
                {
                    //  var user = UserManager.FindById(User.Identity.GetUserId());

                    var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo).FirstOrDefaultAsync();

                    if (nrufcompinfo == null)
                        return RedirectToAction("Index", "NrufCompanyInfoes");

                    NrufFormF3b nrufFormF3bVal = await db.NrufFormF3b.FindAsync(nrufFormF3b.FormF3bId);

                    string message;
                    var error = nrufFormF3b.Calculate(out message);
                    if (error == 0)
                    {
                        throw new Exception(message);
                    }

                    if (compInfo != nrufFormF3b.NrufCompanyInfoId)
                    {
                        if (compInfo != nrufFormF3bVal.NrufCompanyInfoId)
                        {
                            nrufFormF3b.NrufCompanyInfoId = nrufFormF3bVal.NrufCompanyInfoId;
                            Session["NrufCompanyInfo"] = nrufFormF3bVal.NrufCompanyInfoId;
                        }
                        else
                        {
                            nrufFormF3b.NrufCompanyInfoId = compInfo;
                        }
                    }


                    var time = DateTime.Now;
                    nrufFormF3b.createdDate = nrufFormF3bVal.createdDate;
                    nrufFormF3b.dateChanged = time;

                    Detach(nrufFormF3bVal);
                    db.Entry(nrufFormF3b).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index", new { id = compInfo });
                }

            }
            catch (Exception dex)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator. " + dex.Message);
            }

            return View(nrufFormF3b);


        }

        // GET: External/NrufFormF3b/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {

            if (Session["NrufCompanyInfo"] == null)
                return RedirectToAction("Index", "NrufCompanyInfoes");

            var compInfo = (int)Session["NrufCompanyInfo"];

            //if (!CheckExpiryDate())
            //{
            //    return RedirectToAction("Index", "NrufCompanyInfoes");
            //}

            if (id != null)
            {
                //  var user = UserManager.FindById(User.Identity.GetUserId());

                var nrufcompinfo = await db.NrufCompanyInfo.Where(d => d.NrufCompanyInfoId == compInfo).FirstOrDefaultAsync();

                if (nrufcompinfo == null)
                    return RedirectToAction("Index", "NrufCompanyInfoes");

                NrufFormF3b nrufFormF3b = await db.NrufFormF3b.FindAsync(id);
                if (nrufFormF3b == null)
                {
                    return HttpNotFound();
                }
                return View(nrufFormF3b);

            }



            // return View("Index", "NrufCompanyInfoes"); view viewName and masterLayout
            return RedirectToAction("Index", "NrufCompanyInfoes");
        }

        // POST: External/NrufFormF3b/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            // Only view my NRUF forms
            NrufFormF3b nrufFormF3b = await db.NrufFormF3b.FindAsync(id);

            if (nrufFormF3b == null)
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            if ((int)Session["NrufCompanyInfo"] != nrufFormF3b.NrufCompanyInfoId)
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            //if (!CheckExpiryDate())
            //{
            //    return RedirectToAction("Index", "NrufCompanyInfoes");
            //}

            db.NrufFormF3b.Remove(nrufFormF3b);
            await db.SaveChangesAsync();
            var compInfo = (int)Session["NrufCompanyInfo"];
            return RedirectToAction("Index", new { id = compInfo });
        }

        bool CheckExpiryDate()
        {
            // CHeck the Expiry Date
            NrufCompanyInfo nrufCompanyInfo = db.NrufCompanyInfo.Find((int)Session["NrufCompanyInfo"]);

            if (nrufCompanyInfo != null)
            {
                if (nrufCompanyInfo.expiryDate < DateTime.Now)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

            return true;
        }

        void Update(NrufFormF3b obj)
        {
            db.Entry(obj).State = EntityState.Modified;
            db.SaveChanges();
        }

        void Detach(NrufFormF3b obj)
        {
            db.Entry(obj).State = EntityState.Detached;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
