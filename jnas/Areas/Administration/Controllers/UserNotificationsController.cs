﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;


namespace jnas.Areas.Administration.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class UserNotificationsController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administration/UserNotifications
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            var userNotifications = db.UserNotifications.Include(u => u.Notification1).Include(u => u.User1);
            return View(await userNotifications.ToListAsync());
        }

        // GET: Administration/UserNotifications/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserNotification userNotification = await db.UserNotifications.FindAsync(id);
            if (userNotification == null)
            {
                return HttpNotFound();
            }
            return View(userNotification);
        }

        // GET: Administration/UserNotifications/Create
        public ActionResult Create()
        {
            ViewBag.notificationId = new SelectList(db.Notifications, "notificationId", "name");
            var usid = db.UserStates.Where(d => d.name.Equals("Validated")).FirstOrDefault();
            var results1 = db.Users.ToList();
            if (usid != null)
            {
                var results2a = results1.Where(d => d.userStateId == usid.userStateId).Select(s => new { s.UserId, s.name });
                ViewBag.UserId = new SelectList(results2a, "UserId", "name");
            }
            else
            {
                var results2 = results1.Select(s => new { s.UserId, s.name });
                ViewBag.UserId = new SelectList(results2, "UserId", "name");
            }
           // ViewBag.UserId = new SelectList(db.Users, "UserId", "name");
            return View();
        }

        // POST: Administration/UserNotifications/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "userNotificationId,UserId,user,notificationId,notification,assigned")] UserNotification userNotification)
        {
            if (ModelState.IsValid)
            {
                //Add default value to the subscription field
                userNotification.notification = "No";
                db.UserNotifications.Add(userNotification);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.notificationId = new SelectList(db.Notifications, "notificationId", "name", userNotification.notificationId);

            var usid = db.UserStates.Where(d => d.name.Equals("Validated")).FirstOrDefault();
            var results1 = db.Users.ToList();
            if (usid != null)
            {
                var results2a = results1.Where(d => d.userStateId == usid.userStateId).Select(s => new { s.UserId, s.name });
                ViewBag.UserId = new SelectList(results2a, "UserId", "name", userNotification.UserId);
            }
            else
            {
                var results2 = results1.Select(s => new { s.UserId, s.name });
                ViewBag.UserId = new SelectList(results2, "UserId", "name", userNotification.UserId);
            }
           // var results2 = results1.Where(d => d.UserState.name.Equals("Validated")).Select(s => new { s.UserId, s.name });
           // ViewBag.UserId = new SelectList(results2, "UserId", "name", userNotification.UserId);
            return View(userNotification);
        }

        // GET: Administration/UserNotifications/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserNotification userNotification = await db.UserNotifications.FindAsync(id);
            if (userNotification == null)
            {
                return HttpNotFound();
            }
            ViewBag.notificationId = new SelectList(db.Notifications, "notificationId", "name", userNotification.notificationId);
            var results1 = db.Users.ToList();

            var usid = await db.UserStates.Where(d => d.name.Equals("Validated")).FirstOrDefaultAsync();
            if(usid != null)
            {
                var results2a = results1.Where(d => d.userStateId == usid.userStateId).Select(s => new { s.UserId, s.name });
                ViewBag.UserId = new SelectList(results2a, "UserId", "name", userNotification.UserId);
            }
            else
            {
                var results2 = results1.Select(s => new { s.UserId, s.name });
                ViewBag.UserId = new SelectList(results2, "UserId", "name", userNotification.UserId);
            }


            return View(userNotification);
        }

        // POST: Administration/UserNotifications/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "userNotificationId,UserId,user,notificationId,notification,assigned")] UserNotification userNotification)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userNotification).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.notificationId = new SelectList(db.Notifications, "notificationId", "name", userNotification.notificationId);
            var results1 = db.Users.ToList();
            var usid = await db.UserStates.Where(d => d.name.Equals("Validated")).FirstOrDefaultAsync();
            if (usid != null)
            {
                var results2a = results1.Where(d => d.userStateId == usid.userStateId).Select(s => new { s.UserId, s.name });
                ViewBag.UserId = new SelectList(results2a, "UserId", "name", userNotification.UserId);
            }
            else
            {
                var results2 = results1.Select(s => new { s.UserId, s.name });
                ViewBag.UserId = new SelectList(results2, "UserId", "name", userNotification.UserId);
            }
            return View(userNotification);
        }

        // GET: Administration/UserNotifications/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserNotification userNotification = await db.UserNotifications.FindAsync(id);
            if (userNotification == null)
            {
                return HttpNotFound();
            }
            return View(userNotification);
        }

        // POST: Administration/UserNotifications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            UserNotification userNotification = await db.UserNotifications.FindAsync(id);
            db.UserNotifications.Remove(userNotification);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
