﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    public class OrganizationResourceTypesController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        // GET: Administration/OrganizationResourceTypes
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }


            var organizationResourceTypes = db.OrganizationResourceTypes.Include(o => o.LinkedResourceState).Include(o => o.Organization1).Include(o => o.ResourceSubType1).Include(o => o.ResourceType1);
            return View(await organizationResourceTypes.ToListAsync());
        }

        // GET: Administration/OrganizationResourceTypes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganizationResourceType organizationResourceType = await db.OrganizationResourceTypes.FindAsync(id);
            if (organizationResourceType == null)
            {
                return HttpNotFound();
            }
            return View(organizationResourceType);
        }

        // GET: Administration/OrganizationResourceTypes/Create
        public ActionResult Create()
        {
            ViewBag.linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name");
            ViewBag.organizationId = new SelectList(db.Organizations, "organizationId", "name");
            ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name");
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name");
            return View();
        }

        // POST: Administration/OrganizationResourceTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "organizationResourceTypeId,organizationId,organization,resourceTypeId,resourceType,resourceSubTypeId,resourceSubType,linkedResourceStateId,state")] OrganizationResourceType organizationResourceType)
        {
            if (ModelState.IsValid)
            {
                db.OrganizationResourceTypes.Add(organizationResourceType);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", organizationResourceType.linkedResourceStateId);
            ViewBag.organizationId = new SelectList(db.Organizations, "organizationId", "name", organizationResourceType.organizationId);
            ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", organizationResourceType.resourceSubTypeId);
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", organizationResourceType.resourceTypeId);
            return View(organizationResourceType);
        }

        // GET: Administration/OrganizationResourceTypes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganizationResourceType organizationResourceType = await db.OrganizationResourceTypes.FindAsync(id);
            if (organizationResourceType == null)
            {
                return HttpNotFound();
            }
            ViewBag.linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", organizationResourceType.linkedResourceStateId);
            ViewBag.organizationId = new SelectList(db.Organizations, "organizationId", "name", organizationResourceType.organizationId);
            ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", organizationResourceType.resourceSubTypeId);
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", organizationResourceType.resourceTypeId);
            return View(organizationResourceType);
        }

        // POST: Administration/OrganizationResourceTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "organizationResourceTypeId,organizationId,organization,resourceTypeId,resourceType,resourceSubTypeId,resourceSubType,linkedResourceStateId,state")] OrganizationResourceType organizationResourceType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(organizationResourceType).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", organizationResourceType.linkedResourceStateId);
            ViewBag.organizationId = new SelectList(db.Organizations, "organizationId", "name", organizationResourceType.organizationId);
            ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", organizationResourceType.resourceSubTypeId);
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", organizationResourceType.resourceTypeId);
            return View(organizationResourceType);
        }

        // GET: Administration/OrganizationResourceTypes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganizationResourceType organizationResourceType = await db.OrganizationResourceTypes.FindAsync(id);
            if (organizationResourceType == null)
            {
                return HttpNotFound();
            }
            return View(organizationResourceType);
        }

        // POST: Administration/OrganizationResourceTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            OrganizationResourceType organizationResourceType = await db.OrganizationResourceTypes.FindAsync(id);
            db.OrganizationResourceTypes.Remove(organizationResourceType);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
