﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class AddressesController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private Jnasdb db1 = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administration/Addresses
        public async Task<ActionResult> Index(int? id)
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            if (id == null)
            {
                await db.Addresses.LoadAsync();  // Load fresh data
                                                 //http://www.entityframeworktutorial.net/EntityFramework4.3/raw-sql-query-in-entity-framework.aspx
                                                 // var addressList = db.Addresses.SqlQuery("Select * from Address").ToList<Address>();

                var addresses = db.Addresses.Include(a => a.Organization1);
                return View(await addresses.ToListAsync());
            }
            else
            {
                var addresses = db.Addresses.Where(d  => d.organizationId == id).Include(a => a.Organization1);
                return View(await addresses.ToListAsync());
            }

        }

        // GET: Administration/Addresses/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Address address = await db.Addresses.FindAsync(id);
            if (address == null)
            {
                return HttpNotFound();
            }
            return View(address);
        }

        // GET: Administration/Addresses/Create
        public ActionResult Create()
        {
            ViewBag.organizationId = new SelectList(db.Organizations, "organizationId", "name");
            return View();
        }

        // POST: Administration/Addresses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "AddressId,organizationId,line1,line2,city,zipCode,defaultAddress")] Address address)
        {
            if (ModelState.IsValid)
            {
                // set default address
                if (address.defaultAddress == true)
                {
                    var listofOrgAddresses = db1.Addresses.Where(d => d.organizationId == address.organizationId).ToList();
                    listofOrgAddresses.ForEach(item =>
                    {
                        item.defaultAddress = false;
                        //  db.Entry(item).State = EntityState.Modified;

                    });

                    db1.SaveChanges();

                }
                else
               if (address.defaultAddress == false)
                {
                    if (db1.Addresses.Where(d => d.organizationId == address.organizationId).Count() == 0)
                    {
                        address.defaultAddress = true;
                    }

                }

                // Get Org Name
                var orgname = (from d in db.Organizations where d.organizationId == address.organizationId select d.name ).FirstOrDefault().ToString();

                address.organization = orgname + " - " + address.line1;

                db.Addresses.Add(address);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.organizationId = new SelectList(db.Organizations, "organizationId", "name", address.organizationId);
            return View(address);
        }

        // GET: Administration/Addresses/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Address address = await db.Addresses.FindAsync(id);
            if (address == null)
            {
                return HttpNotFound();
            }
            ViewBag.organizationId = new SelectList(db.Organizations, "organizationId", "name", address.organizationId);
            return View(address);
        }

        // POST: Administration/Addresses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AddressId,organizationId,organization,line1,line2,city,zipCode,defaultAddress")] Address address)
        {
            if (ModelState.IsValid)
            {

                // set default address
                if (address.defaultAddress == true)
                {
                    var listofOrgAddresses = db1.Addresses.Where(d => d.organizationId == address.organizationId).ToList();
                    listofOrgAddresses.ForEach(item =>
                    {
                        item.defaultAddress = false;
                        //  db.Entry(item).State = EntityState.Modified;

                    });

                    db1.SaveChanges();

                }
                else
               if (address.defaultAddress == false)
                {
                    if (db1.Addresses.Where(d => d.organizationId == address.organizationId).Count() == 0)
                    {
                        address.defaultAddress = true;
                    }

                }

                var orgname = (from d in db.Organizations where d.organizationId == address.organizationId select d.name).FirstOrDefault().ToString();

                address.organization = orgname + " - " + address.line1;


                db.Entry(address).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.organizationId = new SelectList(db.Organizations, "organizationId", "name", address.organizationId);
            return View(address);
        }

        // GET: Administration/Addresses/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Address address = await db.Addresses.FindAsync(id);
            if (address == null)
            {
                return HttpNotFound();
            }
            return View(address);
        }

        // POST: Administration/Addresses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Address address = await db.Addresses.FindAsync(id);
            db.Addresses.Remove(address);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
