﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class OrganizationsController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administration/Organizations
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            var organizations = db.Organizations.Include(o => o.OrganizationState).Include(o => o.OrganizationType1);
            return View(await organizations.ToListAsync());
        }

        // GET: Administration/Organizations/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Organization organization = await db.Organizations.FindAsync(id);
            if (organization == null)
            {
                return HttpNotFound();
            }
            return View(organization);
        }

        // GET: Administration/Organizations/Create
        public ActionResult Create()
        {
            ViewBag.organizationStateId = new SelectList(db.OrganizationStates, "organizationStateId", "name");
            ViewBag.organizationTypeId = new SelectList(db.OrganizationTypes, "organizationTypeId", "name");
            return View();
        }

        // POST: Administration/Organizations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "organizationId,name,organizationTypeId,ParentCompany,ParentCompanyOCN,OCN,OFN,organizationStateId")] Organization organization)
        {
            if (ModelState.IsValid)
            {

                // Avoid Duplicates
                var match = await (from d in db.Organizations where d.name.Equals(organization.name) select d).FirstOrDefaultAsync();

                if (match != null)
                {
                    return View(organization);
                }

                // no Internals
                if (organization.organizationTypeId == 2)
                {
                    return View(organization);
                }


                var time = DateTime.Now;

                organization.createdDate = time;
                organization.dateChanged = time;

                db.Organizations.Add(organization);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.organizationStateId = new SelectList(db.OrganizationStates, "organizationStateId", "name", organization.organizationStateId);
            ViewBag.organizationTypeId = new SelectList(db.OrganizationTypes, "organizationTypeId", "name", organization.organizationTypeId);
            return View(organization);
        }

        // GET: Administration/Organizations/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Organization organization = await db.Organizations.FindAsync(id);
            if (organization == null)
            {
                return HttpNotFound();
            }
            ViewBag.organizationStateId = new SelectList(db.OrganizationStates, "organizationStateId", "name", organization.organizationStateId);
            ViewBag.organizationTypeId = new SelectList(db.OrganizationTypes, "organizationTypeId", "name", organization.organizationTypeId);
            return View(organization);
        }

        // POST: Administration/Organizations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "organizationId,name,organizationTypeId,ParentCompany,ParentCompanyOCN,OCN,OFN,organizationStateId")] Organization organization)
        {
            if (ModelState.IsValid)
            {

                var org = db.Organizations.Find(organization.organizationId);

                org.name = organization.name;
                org.organizationTypeId = organization.organizationTypeId;
                org.organizationStateId = organization.organizationStateId;
                org.ParentCompany = organization.ParentCompany;
                org.ParentCompanyOCN = organization.ParentCompanyOCN;
                org.OCN = organization.OCN;
                org.OFN = organization.OFN;
                org.dateChanged = DateTime.Now;

                var match = await (from d in db.Organizations where d.organizationId.Equals(organization.organizationId) && d.name.Equals("Office of Utilities Regulation") select d).FirstOrDefaultAsync();

                if (match != null)
                {
                    org.name = match.name;
                    org.organizationTypeId = match.organizationTypeId;
                    org.organizationStateId = match.organizationStateId;
                }


                db.Entry(org).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.organizationStateId = new SelectList(db.OrganizationStates, "organizationStateId", "name", organization.organizationStateId);
            ViewBag.organizationTypeId = new SelectList(db.OrganizationTypes, "organizationTypeId", "name", organization.organizationTypeId);
            return View(organization);
        }

        // GET: Administration/Organizations/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Organization organization = await db.Organizations.FindAsync(id);
            if (organization == null)
            {
                return HttpNotFound();
            }
            return View(organization);
        }

        // POST: Administration/Organizations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            var match = await (from d in db.Organizations where d.organizationId.Equals(id) && d.name.Equals("Office of Utilities Regulation") select d).FirstOrDefaultAsync();

            if (match != null)
            {
                return RedirectToAction("Index");
            }

            Organization organization = await db.Organizations.FindAsync(id);
            db.Organizations.Remove(organization);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
