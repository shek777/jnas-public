﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class ApplicationTypesController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administration/ApplicationTypes
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            return View(await db.ApplicationTypes.ToListAsync());
        }

        // GET: Administration/ApplicationTypes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationType applicationType = await db.ApplicationTypes.FindAsync(id);
            if (applicationType == null)
            {
                return HttpNotFound();
            }
            return View(applicationType);
        }

        // GET: Administration/ApplicationTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Administration/ApplicationTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "applicationTypeId,name,abbreviation")] ApplicationType applicationType)
        {
            if (ModelState.IsValid)
            {

                //Update service table with the new applicationType
                var service = db.Services.Where(sve => sve.name.Equals(applicationType.name)).FirstOrDefault();
                if (service == null)
                {
                    Service sv = new Service()
                    {
                        name = applicationType.name,
                        applicationYN = true
                    };
                    db.Services.Add(sv);
                }
                else
                {
                    if(service.applicationYN == false)
                    {
                        service.applicationYN = true;
                        db.Entry(service).State = EntityState.Modified;
                    }
                }

                db.ApplicationTypes.Add(applicationType);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(applicationType);
        }

        // GET: Administration/ApplicationTypes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationType applicationType = await db.ApplicationTypes.FindAsync(id);
            if (applicationType == null)
            {
                return HttpNotFound();
            }
            return View(applicationType);
        }

        // POST: Administration/ApplicationTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "applicationTypeId,name,abbreviation")] ApplicationType applicationType)
        {
            if (ModelState.IsValid)
            {

                //Update service table with the updated applicationType
                var service = db.Services.Where(sve => sve.name.Equals(applicationType.name)).FirstOrDefault();
                if (service == null)
                {
                    var old_appType = db.ApplicationTypes.Where(at => at.applicationTypeId.Equals(applicationType.applicationTypeId)).FirstOrDefault();
                    var service1 = db.Services.Where(sv => sv.name.Equals(old_appType.name)).FirstOrDefault();
                    if (service1 != null)
                    {
                        service1.name = applicationType.name;
                        service1.applicationYN = true;
                        db.Entry(service1).State = EntityState.Modified;
                    }
                }
                else
                {
                    if (service.applicationYN == false)
                    {
                        service.applicationYN = true;
                        db.Entry(service).State = EntityState.Modified;
                    }
                }
               


                db.Entry(applicationType).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(applicationType);
        }

        // GET: Administration/ApplicationTypes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationType applicationType = await db.ApplicationTypes.FindAsync(id);
            if (applicationType == null)
            {
                return HttpNotFound();
            }
            return View(applicationType);
        }

        // POST: Administration/ApplicationTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ApplicationType applicationType = await db.ApplicationTypes.FindAsync(id);

            //Update service table with the deletion of applicationType
                     
            var service = db.Services.Where(sv => sv.name.Equals(applicationType.name)).FirstOrDefault();
            if (service != null)
            {
                db.Services.Remove(service);
            }

            db.ApplicationTypes.Remove(applicationType);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
