﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class OrganizationTypesController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administration/OrganizationTypes
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            return View(await db.OrganizationTypes.ToListAsync());
        }

        // GET: Administration/OrganizationTypes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganizationType organizationType = await db.OrganizationTypes.FindAsync(id);
            if (organizationType == null)
            {
                return HttpNotFound();
            }
            return View(organizationType);
        }

        // GET: Administration/OrganizationTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Administration/OrganizationTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "organizationTypeId,name")] OrganizationType organizationType)
        {
            if (ModelState.IsValid)
            {
                db.OrganizationTypes.Add(organizationType);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(organizationType);
        }

        // GET: Administration/OrganizationTypes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganizationType organizationType = await db.OrganizationTypes.FindAsync(id);
            if (organizationType == null)
            {
                return HttpNotFound();
            }
            return View(organizationType);
        }

        // POST: Administration/OrganizationTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "organizationTypeId,name")] OrganizationType organizationType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(organizationType).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(organizationType);
        }

        // GET: Administration/OrganizationTypes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganizationType organizationType = await db.OrganizationTypes.FindAsync(id);
            if (organizationType == null)
            {
                return HttpNotFound();
            }
            return View(organizationType);
        }

        // POST: Administration/OrganizationTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            OrganizationType organizationType = await db.OrganizationTypes.FindAsync(id);
            db.OrganizationTypes.Remove(organizationType);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
