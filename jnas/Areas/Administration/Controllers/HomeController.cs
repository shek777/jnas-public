﻿
using jnas.CustomFilter;
using jnas.Models.Jnasdb;
using System;
using System.Collections.Generic;
// using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace jnas.Areas.Administration.Controllers
{


    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class HomeController : System.Web.Mvc.Controller
    {
        jnas.Models.Jnasdb.Jnasdb context1;
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public HomeController()
        {
            context1 = new jnas.Models.Jnasdb.Jnasdb();
        }

        public ActionResult Index(int? id)
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }


            var seed = (from d in context1.SystemParameters where d.name.Equals("Seed Secondary Level Data") && d.value.Equals("0") select d).FirstOrDefault();

            if (seed != null)
                ViewBag.SeedDatabase = Convert.ToInt32(seed.value);
            else
                ViewBag.SeedDatabase = 1;

            ViewBag.Header = "Welcome to Administration Subsite";

            //  var report = "SELECT        Organizations.name AS Organization, ApplicationResourceStates.name AS AppResState, ApplicationResources.dateChanged AS AppResourceDate, Applications_1.applicationTypeId, (SELECT        ResourceTypes.name                              FROM            Applications INNER JOIN                                                          ResourceTypes ON Applications.resourceTypeId = ResourceTypes.resourceTypeId                               WHERE(Applications.resourceTypeId = Applications.resourceTypeId)) AS ResourceTypeName, (SELECT        ResourceSubTypes.name FROM            Applications AS Applications_2 INNER JOIN ResourceSubTypes ON Applications_2.resourceSubTypeId = ResourceSubTypes.resourceSubTypeId                       WHERE(Applications_2.resourceSubTypeId = Applications_2.resourceSubTypeId)) AS ResourceSubTypeName FROM Applications AS Applications_1 INNER JOIN Organizations ON Applications_1.organizationId = Organizations.organizationId INNER JOIN                          ApplicationResources ON Applications_1.applicationId = ApplicationResources.Application_applicationId AND Applications_1.applicationId = ApplicationResources.Application_applicationId1 AND                          Organizations.organizationId = ApplicationResources.Organization_organizationId INNER JOIN                          ApplicationResourceStates ON ApplicationResources.ApplicationResourceState_applicationResourceStateId = ApplicationResourceStates.applicationResourceStateId AND                          ApplicationResources.ApplicationResourceState_applicationResourceStateId1 = ApplicationResourceStates.applicationResourceStateId INNER JOIN                          ResourceTypes AS ResourceTypes_1 ON Applications_1.resourceTypeId = ResourceTypes_1.resourceTypeId INNER JOIN                          ResourceSubTypes AS ResourceSubTypes_1 ON Applications_1.resourceSubTypeId = ResourceSubTypes_1.resourceSubTypeId AND ResourceTypes_1.resourceTypeId = ResourceSubTypes_1.resourceTypeId";

            //  var reportDemo = context1.Database.SqlQuery<ReportDemo>(report).ToList<ReportDemo>();

            if(id is null)
            { id = 876;}
            else if(id == 0)
            { id = 876; }
            var reportDemo = (from d in context1.ReportOnNXXAndNPACodeses where d.NPA == id select d).ToList();

            var reportDemoKey = (from d in context1.ReportOnNXXAndNPACodesKeys.OrderBy(o => o.ReportOnNXXAndNPACodesKeyId) select d).ToList();

            ViewBag.NPA = id;
            ViewBag.ReportKeyList = (List<ReportOnNXXAndNPACodesKey>)reportDemoKey;
            return View(reportDemo);
        }

        public ActionResult Mail()
        {
            // ViewBag.Header = "Welcome to Administration Subsite";
            return View();
        }


        public ActionResult SeedDatabase()
        {

            string message;

            var status = RunSeedDatabase(out message);

            ViewBag.status = status;
            ViewBag.message = message;

            return View();
        }

        private int RunSeedDatabase(out string message)
        {
            message = "";
            var seed = (from d in context1.SystemParameters where d.name.Equals("Seed Secondary Level Data") && d.value.Equals("0") select d).FirstOrDefault();

            var SeedDatabase = Convert.ToInt32(seed.value);
            var status = 2;

            if (SeedDatabase == 0)
            {
                // Save to Database
                using (var context = new jnas.Models.Jnasdb.Jnasdb())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {

                        try
                        {

                            //var userTypes = from d in context.UserTypes select d;

                            //var resTypes = from d in context.ResourceTypes select d;

                            //var resSubTypes = from d in context.ResourceSubTypes select d;

                            //var linkedResState = from d in context.LinkedResourceStates select d;

                            //var userProf = from d in context.UserProfiles select d;

                            //var serv = from d in context.Services select d;

                            //var providerServ = from d in context.ProviderServices select d;

                            var usrResTypes = new List<UserResourceType>
                {
                     new UserResourceType {userTypeId = 6, resourceTypeId = 3, resourceSubTypeId = 2, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 6, resourceTypeId = 3, resourceSubTypeId = 3, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 6, resourceTypeId = 4, resourceSubTypeId = 3, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 6, resourceTypeId = 4, resourceSubTypeId = 3, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 6, resourceTypeId = 4, resourceSubTypeId = 3, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 6, resourceTypeId = 5, resourceSubTypeId = 17, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 6, resourceTypeId = 5, resourceSubTypeId = 18, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 6, resourceTypeId = 5, resourceSubTypeId = 19, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 6, resourceTypeId = 5, resourceSubTypeId = 20, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 6, resourceTypeId = 6, resourceSubTypeId = 21, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 6, resourceTypeId = 6, resourceSubTypeId = 22, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 6, resourceTypeId = 6, resourceSubTypeId = 23, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 6, resourceTypeId = 6, resourceSubTypeId = 24, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 6, resourceTypeId = 17, resourceSubTypeId = 25, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 6, resourceTypeId = 17, resourceSubTypeId = 25, linkedResourceStateId = 1 },
                      new UserResourceType {userTypeId = 6, resourceTypeId = 18, resourceSubTypeId = 27, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 6, resourceTypeId = 18, resourceSubTypeId = 28, linkedResourceStateId = 1 },
                      new UserResourceType {userTypeId = 6, resourceTypeId = 19, resourceSubTypeId = 29, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 6, resourceTypeId = 19, resourceSubTypeId = 30, linkedResourceStateId = 1 },



                      new UserResourceType {userTypeId = 6, resourceTypeId = 10, resourceSubTypeId = 7, linkedResourceStateId = 1 },

                      new UserResourceType {userTypeId = 6, resourceTypeId = 10, resourceSubTypeId = 8, linkedResourceStateId = 1 },

                     new UserResourceType {userTypeId = 6, resourceTypeId = 10, resourceSubTypeId = 9, linkedResourceStateId = 1 },

                      new UserResourceType {userTypeId = 6, resourceTypeId = 11, resourceSubTypeId = 10, linkedResourceStateId = 1 },

                      new UserResourceType {userTypeId = 6, resourceTypeId = 11, resourceSubTypeId = 11, linkedResourceStateId = 1 },

                     new UserResourceType {userTypeId = 6, resourceTypeId = 11, resourceSubTypeId = 12, linkedResourceStateId = 1 },

                      new UserResourceType {userTypeId = 6, resourceTypeId = 12, resourceSubTypeId = 13, linkedResourceStateId = 1 },

                      new UserResourceType {userTypeId = 6, resourceTypeId = 12, resourceSubTypeId = 14, linkedResourceStateId = 1 },

                     new UserResourceType {userTypeId = 6, resourceTypeId = 12, resourceSubTypeId = 15, linkedResourceStateId = 1 },



                     new UserResourceType {userTypeId = 6, resourceTypeId = 14, resourceSubTypeId = 1, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 6, resourceTypeId = 14, resourceSubTypeId = 1, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 6, resourceTypeId = 20, resourceSubTypeId = 1, linkedResourceStateId = 1 },


                     new UserResourceType {userTypeId = 3, resourceTypeId = 3, resourceSubTypeId = 2, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 3, resourceTypeId = 3, resourceSubTypeId = 3, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 3, resourceTypeId = 4, resourceSubTypeId = 3, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 3, resourceTypeId = 4, resourceSubTypeId = 3, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 3, resourceTypeId = 4, resourceSubTypeId = 3, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 3, resourceTypeId = 5, resourceSubTypeId = 17, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 3, resourceTypeId = 5, resourceSubTypeId = 18, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 3, resourceTypeId = 5, resourceSubTypeId = 19, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 3, resourceTypeId = 5, resourceSubTypeId = 20, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 3, resourceTypeId = 6, resourceSubTypeId = 21, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 3, resourceTypeId = 6, resourceSubTypeId = 26, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 3, resourceTypeId = 6, resourceSubTypeId = 23, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 3, resourceTypeId = 6, resourceSubTypeId = 24, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 3, resourceTypeId = 17, resourceSubTypeId = 25, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 3, resourceTypeId = 17, resourceSubTypeId = 25, linkedResourceStateId = 1 },
                      new UserResourceType {userTypeId = 3, resourceTypeId = 18, resourceSubTypeId = 27, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 3, resourceTypeId = 18, resourceSubTypeId = 28, linkedResourceStateId = 1 },
                      new UserResourceType {userTypeId = 3, resourceTypeId = 19, resourceSubTypeId = 29, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 3, resourceTypeId = 19, resourceSubTypeId = 30, linkedResourceStateId = 1 },



                      new UserResourceType {userTypeId = 3, resourceTypeId = 10, resourceSubTypeId = 7, linkedResourceStateId = 1 },

                      new UserResourceType {userTypeId = 3, resourceTypeId = 10, resourceSubTypeId = 8, linkedResourceStateId = 1 },

                     new UserResourceType {userTypeId = 3, resourceTypeId = 10, resourceSubTypeId = 9, linkedResourceStateId = 1 },

                      new UserResourceType {userTypeId = 3, resourceTypeId = 11, resourceSubTypeId = 10, linkedResourceStateId = 1 },

                      new UserResourceType {userTypeId = 3, resourceTypeId = 11, resourceSubTypeId = 11, linkedResourceStateId = 1 },

                     new UserResourceType {userTypeId = 3, resourceTypeId = 11, resourceSubTypeId = 12, linkedResourceStateId = 1 },

                      new UserResourceType {userTypeId = 3, resourceTypeId = 12, resourceSubTypeId = 13, linkedResourceStateId = 1 },

                      new UserResourceType {userTypeId = 3, resourceTypeId = 12, resourceSubTypeId = 14, linkedResourceStateId = 1 },

                     new UserResourceType {userTypeId = 3, resourceTypeId = 12, resourceSubTypeId = 15, linkedResourceStateId = 1 },



                     new UserResourceType {userTypeId = 3, resourceTypeId = 14, resourceSubTypeId = 1, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 3, resourceTypeId = 14, resourceSubTypeId = 1, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 3, resourceTypeId = 20, resourceSubTypeId = 1, linkedResourceStateId = 1 },
                      new UserResourceType {userTypeId = 3, resourceTypeId = 16, resourceSubTypeId = 1, linkedResourceStateId = 1 },
                       new UserResourceType {userTypeId = 3, resourceTypeId = 22, resourceSubTypeId = 1, linkedResourceStateId = 1 },
                        new UserResourceType {userTypeId = 3, resourceTypeId = 24, resourceSubTypeId = 1, linkedResourceStateId = 1 },
                          new UserResourceType {userTypeId = 3, resourceTypeId = 25, resourceSubTypeId = 1, linkedResourceStateId = 1 },


                     new UserResourceType {userTypeId = 4, resourceTypeId = 3, resourceSubTypeId = 2, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 4, resourceTypeId = 3, resourceSubTypeId = 3, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 4, resourceTypeId = 4, resourceSubTypeId = 3, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 4, resourceTypeId = 4, resourceSubTypeId = 3, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 4, resourceTypeId = 4, resourceSubTypeId = 3, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 4, resourceTypeId = 5, resourceSubTypeId = 17, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 4, resourceTypeId = 5, resourceSubTypeId = 18, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 4, resourceTypeId = 5, resourceSubTypeId = 19, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 4, resourceTypeId = 5, resourceSubTypeId = 20, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 4, resourceTypeId = 6, resourceSubTypeId = 21, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 4, resourceTypeId = 6, resourceSubTypeId = 22, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 4, resourceTypeId = 6, resourceSubTypeId = 23, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 4, resourceTypeId = 6, resourceSubTypeId = 24, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 4, resourceTypeId = 17, resourceSubTypeId = 25, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 4, resourceTypeId = 17, resourceSubTypeId = 25, linkedResourceStateId = 1 },
                      new UserResourceType {userTypeId = 4, resourceTypeId = 18, resourceSubTypeId = 27, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 4, resourceTypeId = 18, resourceSubTypeId = 28, linkedResourceStateId = 1 },
                      new UserResourceType {userTypeId = 4, resourceTypeId = 19, resourceSubTypeId = 29, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 4, resourceTypeId = 19, resourceSubTypeId = 30, linkedResourceStateId = 1 },


                      new UserResourceType {userTypeId = 4, resourceTypeId = 10, resourceSubTypeId = 7, linkedResourceStateId = 1 },

                      new UserResourceType {userTypeId = 4, resourceTypeId = 10, resourceSubTypeId = 8, linkedResourceStateId = 1 },

                     new UserResourceType {userTypeId = 4, resourceTypeId = 10, resourceSubTypeId = 9, linkedResourceStateId = 1 },

                      new UserResourceType {userTypeId = 4, resourceTypeId = 11, resourceSubTypeId = 10, linkedResourceStateId = 1 },

                      new UserResourceType {userTypeId = 4, resourceTypeId = 11, resourceSubTypeId = 11, linkedResourceStateId = 1 },

                     new UserResourceType {userTypeId = 4, resourceTypeId = 11, resourceSubTypeId = 12, linkedResourceStateId = 1 },

                      new UserResourceType {userTypeId = 4, resourceTypeId = 12, resourceSubTypeId = 13, linkedResourceStateId = 1 },

                      new UserResourceType {userTypeId = 4, resourceTypeId = 12, resourceSubTypeId = 14, linkedResourceStateId = 1 },

                     new UserResourceType {userTypeId = 4, resourceTypeId = 12, resourceSubTypeId = 15, linkedResourceStateId = 1 },



                     new UserResourceType {userTypeId = 4, resourceTypeId = 14, resourceSubTypeId = 1, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 4, resourceTypeId = 14, resourceSubTypeId = 1, linkedResourceStateId = 1 },
                     new UserResourceType {userTypeId = 4, resourceTypeId = 20, resourceSubTypeId = 1, linkedResourceStateId = 1 },
                      new UserResourceType {userTypeId = 4, resourceTypeId = 16, resourceSubTypeId = 1, linkedResourceStateId = 1 },
                       new UserResourceType {userTypeId = 4, resourceTypeId = 22, resourceSubTypeId = 1, linkedResourceStateId = 1 },
                        new UserResourceType {userTypeId = 4, resourceTypeId = 24, resourceSubTypeId = 1, linkedResourceStateId = 1 },
                          new UserResourceType {userTypeId = 4, resourceTypeId = 25, resourceSubTypeId = 1, linkedResourceStateId = 1 }

                };
                            //usrResTypes.ForEach(j => context.UserResourceTypes.AddOrUpdate(p => new { p.userTypeId, p.resourceTypeId, p.resourceSubTypeId }, j));
                            usrResTypes.ForEach(j => context.UserResourceTypes.Add(j));
                            context.SaveChanges();

                            DateTime? now = DateTime.Now;

                            var organs = context.Organizations.ToList();

                            organs.ForEach(item =>
                            {


                                item.createdDate = now;
                                item.dateChanged = now;
                            });

                            context.SaveChanges();


                            var SysParas = context.SystemParameters.ToList();

                            SysParas.ForEach(item =>
                            {
                                if (item.name == "Seed Secondary Level Data")
                                    item.value = "1";

                                item.createdDate = now;
                                item.dateChanged = now;
                            });

                            context.SaveChanges();


                            //seed.value = "1";

                            //context.SystemParameters.Attach(seed);
                            //context.Entry(seed).State = System.Data.Entity.EntityState.Modified;
                            //context.SaveChanges();


                            dbContextTransaction.Commit();
                            status = 1;

                        }
                        catch (Exception m)
                        {
                            message = m.Message;
                            dbContextTransaction.Rollback();
                            status = 0;
                        }

                    } // dbcontextTransaction
                } // context

            } // if end
            return status;
        } // method end
    }

    public class ReportDemo
    {

        public string Organization { get; set; }
        public string AppResState { get; set; }
        public DateTime? AppResourceDate { get; set; }
        public int? applicationTypeId { get; set; }
        public string ResourceTypeName { get; set; }
        public string ResourceSubTypeName { get; set; }

    }
}
