﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class ResourceTypesController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administration/ResourceTypes
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            return View(await db.ResourceTypes.ToListAsync());
        }

        // GET: Administration/ResourceTypes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ResourceType resourceType = await db.ResourceTypes.FindAsync(id);
            if (resourceType == null)
            {
                return HttpNotFound();
            }
            return View(resourceType);
        }

        // GET: Administration/ResourceTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Administration/ResourceTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "resourceTypeId,name,format,formatRegex")] ResourceType resourceType)
        {
            if (ModelState.IsValid)
            {
                db.ResourceTypes.Add(resourceType);

                var notification = new Notification();

                var chk = db.Notifications.Where(n => n.name == resourceType.name).FirstOrDefault();
                if(chk == null)
                {
                    notification.name = resourceType.name;
                    notification.mandatory = false;
                    db.Notifications.Add(notification);
                }

                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(resourceType);
        }

        // GET: Administration/ResourceTypes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ResourceType resourceType = await db.ResourceTypes.FindAsync(id);
            if (resourceType == null)
            {
                return HttpNotFound();
            }
            var notification = db.Notifications.Where(n => n.name == resourceType.name).FirstOrDefault();
                       
            TempData["notification"] = notification;
            return View(resourceType);
        }

        // POST: Administration/ResourceTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "resourceTypeId,name,format,formatRegex")] ResourceType resourceType)
        {
           var notification = TempData["notification"] as Notification;

            if (ModelState.IsValid)
            {
                db.Entry(resourceType).State = EntityState.Modified;

                if (notification == null)
                {
                    var chk = db.Notifications.Where(n => n.name == resourceType.name).FirstOrDefault();
                    if (chk == null)
                    {
                        notification.name = resourceType.name;
                        notification.mandatory = false;
                        db.Notifications.Add(notification);
                    }
                }
                else
                {
                    var chk = db.Notifications.Where(n => n.name == resourceType.name).FirstOrDefault();
                    if (chk == null)
                    {
                        var original = db.Notifications.Find(notification.notificationId);
                        if(original != null)
                        {
                            original.name = resourceType.name;
                        }
                        else
                        {
                            notification.name = resourceType.name;
                            notification.mandatory = false;
                            db.Notifications.Add(notification);
                        }
                    }
                }

                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            TempData["notification"] = notification;
            return View(resourceType);
        }

        // GET: Administration/ResourceTypes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ResourceType resourceType = await db.ResourceTypes.FindAsync(id);
            if (resourceType == null)
            {
                return HttpNotFound();
            }
            return View(resourceType);
        }

        // POST: Administration/ResourceTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ResourceType resourceType = await db.ResourceTypes.FindAsync(id);
            db.ResourceTypes.Remove(resourceType);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
