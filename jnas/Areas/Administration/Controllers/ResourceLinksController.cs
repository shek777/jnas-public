﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    public class ResourceLinksController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        // GET: Administration/ResourceLinks
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }


            var resourceLinks = db.ResourceLinks.Include(r => r.LinkedResourceState).Include(r => r.Resource);
            return View(await resourceLinks.ToListAsync());
        }

        // GET: Administration/ResourceLinks/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ResourceLink resourceLink = await db.ResourceLinks.FindAsync(id);
            if (resourceLink == null)
            {
                return HttpNotFound();
            }
            int pr = Convert.ToInt32(resourceLink.parentResource);
            ViewBag.parentResource = (from tabledata in db.Resources where tabledata.resourceId == pr select tabledata.resource1).FirstOrDefault();
            int cr = Convert.ToInt32(resourceLink.childResource);
            ViewBag.childResource = (from tabledata in db.Resources where tabledata.resourceId == cr select tabledata.resource1).FirstOrDefault();
            return View(resourceLink);
        }

        // GET: Administration/ResourceLinks/Create
        public ActionResult Create()
        {
            var results = (from tabledata in db.Resources select new { tabledata.resourceId, name = tabledata.resource1 }).ToList();
            ViewBag.parentResource = new SelectList(results.AsEnumerable(), "resourceId", "name");
            ViewBag.childResource = new SelectList(results.AsEnumerable(), "resourceId", "name");
            ViewBag.linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name");
            //ViewBag.LinkedResourceState_linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name");
            //ViewBag.LinkedResourceState_linkedResourceStateId1 = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name");
            //ViewBag.Resource_resourceId = new SelectList(db.Resources, "resourceId", "resource1");
            //ViewBag.Resource_resourceId1 = new SelectList(db.Resources, "resourceId", "resource1");
            return View();
        }

        // POST: Administration/ResourceLinks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "resourceLinkId,parentResource,childResource,linkedResourceStateId,state")] ResourceLink resourceLink)
        {
            if (ModelState.IsValid)
            {
                //resourceLink.LinkedResourceState_linkedResourceStateId1 = resourceLink.linkedResourceStateId;

                db.ResourceLinks.Add(resourceLink);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            var results = (from tabledata in db.Resources select new { tabledata.resourceId, name = tabledata.resource1 }).ToList();
            ViewBag.parentResource = new SelectList(results.AsEnumerable(), "resourceId", "name", Convert.ToInt32(resourceLink.parentResource));
            ViewBag.childResource = new SelectList(results.AsEnumerable(), "resourceId", "name", Convert.ToInt32(resourceLink.childResource));
            ViewBag.linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", resourceLink.linkedResourceStateId);
            //ViewBag.LinkedResourceState_linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", resourceLink.LinkedResourceState_linkedResourceStateId);
            //ViewBag.LinkedResourceState_linkedResourceStateId1 = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", resourceLink.LinkedResourceState_linkedResourceStateId1);
            //ViewBag.Resource_resourceId = new SelectList(db.Resources, "resourceId", "resource1", resourceLink.Resource_resourceId);
            //ViewBag.Resource_resourceId1 = new SelectList(db.Resources, "resourceId", "resource1", resourceLink.Resource_resourceId1);
            return View(resourceLink);
        }

        // GET: Administration/ResourceLinks/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ResourceLink resourceLink = await db.ResourceLinks.FindAsync(id);
            if (resourceLink == null)
            {
                return HttpNotFound();
            }
            var results = (from tabledata in db.Resources select new { tabledata.resourceId, name = tabledata.resource1 }).ToList();
            ViewBag.parentResource = new SelectList(results.AsEnumerable(), "resourceId", "name", Convert.ToInt32(resourceLink.parentResource));
            ViewBag.childResource = new SelectList(results.AsEnumerable(), "resourceId", "name", Convert.ToInt32(resourceLink.childResource));
            ViewBag.linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", resourceLink.linkedResourceStateId);
            //ViewBag.LinkedResourceState_linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", resourceLink.LinkedResourceState_linkedResourceStateId);
            //ViewBag.LinkedResourceState_linkedResourceStateId1 = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", resourceLink.LinkedResourceState_linkedResourceStateId1);
            //ViewBag.Resource_resourceId = new SelectList(db.Resources, "resourceId", "resource1", resourceLink.Resource_resourceId);
            //ViewBag.Resource_resourceId1 = new SelectList(db.Resources, "resourceId", "resource1", resourceLink.Resource_resourceId1);
            return View(resourceLink);
        }

        // POST: Administration/ResourceLinks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "resourceLinkId,parentResource,childResource,linkedResourceStateId,state")] ResourceLink resourceLink)
        {
            if (ModelState.IsValid)
            {
                //resourceLink.LinkedResourceState_linkedResourceStateId1 = resourceLink.linkedResourceStateId;

                db.Entry(resourceLink).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            var results = (from tabledata in db.Resources select new { tabledata.resourceId, name = tabledata.resource1 }).ToList();
            ViewBag.parentResource = new SelectList(results.AsEnumerable(), "resourceId", "name", Convert.ToInt32(resourceLink.parentResource));
            ViewBag.childResource = new SelectList(results.AsEnumerable(), "resourceId", "name", Convert.ToInt32(resourceLink.childResource));
            ViewBag.linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", resourceLink.linkedResourceStateId);
            //ViewBag.LinkedResourceState_linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", resourceLink.LinkedResourceState_linkedResourceStateId);
            //ViewBag.LinkedResourceState_linkedResourceStateId1 = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", resourceLink.LinkedResourceState_linkedResourceStateId1);
            //ViewBag.Resource_resourceId = new SelectList(db.Resources, "resourceId", "resource1", resourceLink.Resource_resourceId);
            //ViewBag.Resource_resourceId1 = new SelectList(db.Resources, "resourceId", "resource1", resourceLink.Resource_resourceId1);
            return View(resourceLink);
        }

        // GET: Administration/ResourceLinks/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ResourceLink resourceLink = await db.ResourceLinks.FindAsync(id);
            if (resourceLink == null)
            {
                return HttpNotFound();
            }
            int pr = Convert.ToInt32(resourceLink.parentResource);
            ViewBag.parentResource = (from tabledata in db.Resources where tabledata.resourceId == pr select tabledata.resource1).FirstOrDefault();
            int cr = Convert.ToInt32(resourceLink.childResource);
            ViewBag.childResource = (from tabledata in db.Resources where tabledata.resourceId == cr select tabledata.resource1).FirstOrDefault();
            return View(resourceLink);
        }

        // POST: Administration/ResourceLinks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ResourceLink resourceLink = await db.ResourceLinks.FindAsync(id);
            db.ResourceLinks.Remove(resourceLink);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
