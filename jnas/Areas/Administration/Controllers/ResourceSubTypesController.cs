﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class ResourceSubTypesController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administration/ResourceSubTypes
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            var resourceSubTypes = db.ResourceSubTypes.Include(r => r.ResourceType);
            return View(await resourceSubTypes.ToListAsync());
        }

        // GET: Administration/ResourceSubTypes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ResourceSubType resourceSubType = await db.ResourceSubTypes.FindAsync(id);
            if (resourceSubType == null)
            {
                return HttpNotFound();
            }
            return View(resourceSubType);
        }

        // GET: Administration/ResourceSubTypes/Create
        public ActionResult Create()
        {
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name");
            return View();
        }

        // POST: Administration/ResourceSubTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "resourceSubTypeId,resourceTypeId,name,format,formatRegex")] ResourceSubType resourceSubType)
        {
            if (ModelState.IsValid)
            {
                db.ResourceSubTypes.Add(resourceSubType);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", resourceSubType.resourceTypeId);
            return View(resourceSubType);
        }

        // GET: Administration/ResourceSubTypes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ResourceSubType resourceSubType = await db.ResourceSubTypes.FindAsync(id);
            if (resourceSubType == null)
            {
                return HttpNotFound();
            }
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", resourceSubType.resourceTypeId);
            return View(resourceSubType);
        }

        // POST: Administration/ResourceSubTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "resourceSubTypeId,resourceTypeId,name,format,formatRegex")] ResourceSubType resourceSubType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(resourceSubType).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", resourceSubType.resourceTypeId);
            return View(resourceSubType);
        }

        // GET: Administration/ResourceSubTypes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ResourceSubType resourceSubType = await db.ResourceSubTypes.FindAsync(id);
            if (resourceSubType == null)
            {
                return HttpNotFound();
            }
            return View(resourceSubType);
        }

        // POST: Administration/ResourceSubTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ResourceSubType resourceSubType = await db.ResourceSubTypes.FindAsync(id);
            db.ResourceSubTypes.Remove(resourceSubType);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
