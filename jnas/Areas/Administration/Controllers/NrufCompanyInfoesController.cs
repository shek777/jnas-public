﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class NrufCompanyInfoesController : Controller
    {
        private Jnasdb db = new Jnasdb();

        private UserManager _userManager;

        private Random _random = new Random((int)DateTime.Now.Ticks);

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: External/NrufCompanyInfoes/GeneralInfo
        public ActionResult GeneralInfo()
        {

            return View();
        }

        // GET: External/NrufCompanyInfoes/Instructions
        public ActionResult Instructions()
        {

            return View();
        }

        // GET: External/NrufCompanyInfoes/ExportToExcel
        public async Task<ActionResult> ExportToExcel(int id)
        {


            // Only view my NRUF forms
            List<NrufCompanyInfo> nrufCompanyIf = db.NrufCompanyInfo.Include(n => n.ProviderService).ToList();

            var nrufCompanyInfo = nrufCompanyIf.Find(d => d.NrufCompanyInfoId == id);

            var user = UserManager.FindById(User.Identity.GetUserId());


            if (nrufCompanyInfo == null)
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }
            if (nrufCompanyInfo.organizationId != user.organizationId)
            {
                return RedirectToAction("Index", "NrufCompanyInfoes");
            }

            var message = "";
            try
            {

                var output = string.Format("{0}-{1}", nrufCompanyInfo.applicationReference, DateTime.Now.ToString("yyyyMMddhhss"));
                var excelFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Documents\NRUFFormOUR_Mar2016.xlsx");
                var outputexcelFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Heap\" + output + ".xlsx");
                System.IO.File.Copy(excelFile, outputexcelFile, true);
                // Stream stream = System.IO.File.Open(outputexcelFile, FileMode.Open);
                // ExcelOpenSpreadsheets(stream, 10, 13);
                //string result = ExcelAddRows(outputexcelFile);
                //stream.Close();
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.ParentCompany, 8, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.name, 9, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.line1, 10, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.line2, 11, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.line3, 12, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.ContactName, 13, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.ContactTelNo, 14, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.ContactFaxNo, 15, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.ContactEmail, 16, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.ParentCompanyOCN, 8, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.OCN, 9, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.OFN, 10, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Company Info", nrufCompanyInfo.ProviderService.name, 11, "K", CellValues.String);

                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.ParentCompany, 8, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.name, 9, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.line1, 10, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.line2, 11, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.line3, 12, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.ContactName, 13, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.ContactTelNo, 14, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.ContactFaxNo, 15, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.ContactEmail, 16, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.ParentCompanyOCN, 8, "J", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.OCN, 9, "J", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.OFN, 10, "J", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3a", nrufCompanyInfo.ProviderService.name, 11, "J", CellValues.String);

                var formf3a = await db.NrufFormF3a.Where(p => p.NrufCompanyInfoId == id).ToListAsync();

                uint count = 22;
                foreach (var item in formf3a)
                {
                    UpdateCell(outputexcelFile, "Form F-3a", item.Npa, count, "B", CellValues.String);
                    UpdateCell(outputexcelFile, "Form F-3a", item.Year1.ToString(), count, "D", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form F-3a", item.Year2.ToString(), count, "E", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form F-3a", item.Year3.ToString(), count, "F", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form F-3a", item.Year4.ToString(), count, "G", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form F-3a", item.Year5.ToString(), count, "H", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form F-3a", item.Total.ToString(), count, "I", CellValues.Number);

                    count++;
                }

                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.ParentCompany, 8, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.name, 9, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.line1, 10, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.line2, 11, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.line3, 12, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.ContactName, 13, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.ContactTelNo, 14, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.ContactFaxNo, 15, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.ContactEmail, 16, "C", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.ParentCompanyOCN, 8, "J", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.OCN, 9, "J", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.OFN, 10, "J", CellValues.String);
                UpdateCell(outputexcelFile, "Form F-3b", nrufCompanyInfo.ProviderService.name, 11, "J", CellValues.String);

                var formf3b = await db.NrufFormF3b.Where(p => p.NrufCompanyInfoId == id).ToListAsync();

                count = 22;
                foreach (var item in formf3b)
                {
                    UpdateCell(outputexcelFile, "Form F-3b", item.Npa, count, "B", CellValues.String);
                    UpdateCell(outputexcelFile, "Form F-3b", item.Year1.ToString(), count, "D", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form F-3b", item.Year2.ToString(), count, "E", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form F-3b", item.Year3.ToString(), count, "F", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form F-3b", item.Year4.ToString(), count, "G", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form F-3b", item.Year5.ToString(), count, "H", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form F-3b", item.Total.ToString(), count, "I", CellValues.Number);

                    count++;
                }

                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.ParentCompany, 7, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.name, 8, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.line1, 9, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.line2, 10, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.line3, 11, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.ContactName, 12, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.ContactTelNo, 13, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.ContactFaxNo, 14, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.ContactEmail, 15, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.ParentCompanyOCN, 7, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.OCN, 8, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.OFN, 9, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Form U1", nrufCompanyInfo.ProviderService.name, 10, "K", CellValues.String);

                var formU1 = await db.NrufFormU1.Where(p => p.NrufCompanyInfoId == id).ToListAsync();

                count = 21;
                foreach (var item in formU1)
                {
                    UpdateCell(outputexcelFile, "Form U1", item.Npa + "-" + item.Nxx, count, "A", CellValues.String);
                    UpdateCell(outputexcelFile, "Form U1", item.X.ToString(), count, "B", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U1", item.Rate.ToString(), count, "C", CellValues.String);
                    UpdateCell(outputexcelFile, "Form U1", item.Assigned.ToString(), count, "D", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U1", item.Intermediate.ToString(), count, "E", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U1", item.Reserved.ToString(), count, "F", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U1", item.Aging.ToString(), count, "G", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U1", item.Admin.ToString(), count, "H", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U1", item.PortedOut.ToString(), count, "I", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U1", item.Notes.ToString(), count, "J", CellValues.String);
                    UpdateCell(outputexcelFile, "Form U1", item.Available.ToString(), count, "K", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U1", item.Utilization.ToString(), count, "L", CellValues.Number);

                    count++;
                }

                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.ParentCompany, 7, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.name, 8, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.line1, 9, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.line2, 10, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.line3, 11, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.ContactName, 12, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.ContactTelNo, 13, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.ContactFaxNo, 14, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.ContactEmail, 15, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.ParentCompanyOCN, 7, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.OCN, 8, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.OFN, 9, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Form U2", nrufCompanyInfo.ProviderService.name, 10, "K", CellValues.String);

                var formU2 = await db.NrufFormU2.Where(p => p.NrufCompanyInfoId == id).ToListAsync();

                count = 21;
                foreach (var item in formU2)
                {
                    UpdateCell(outputexcelFile, "Form U2", item.Npa + "-" + item.Nxx, count, "A", CellValues.String);
                    UpdateCell(outputexcelFile, "Form U2", item.Rate.ToString(), count, "B", CellValues.String);
                    UpdateCell(outputexcelFile, "Form U2", item.Assigned.ToString(), count, "D", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U2", item.Intermediate.ToString(), count, "E", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U2", item.Reserved.ToString(), count, "F", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U2", item.Aging.ToString(), count, "G", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U2", item.Admin.ToString(), count, "H", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U2", item.PortedOut.ToString(), count, "I", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U2", item.Notes.ToString(), count, "J", CellValues.String);
                    UpdateCell(outputexcelFile, "Form U2", item.Available.ToString(), count, "K", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U2", item.Utilization.ToString(), count, "L", CellValues.Number);


                    count++;
                }


                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.ParentCompany, 7, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.name, 8, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.line1, 9, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.line2, 10, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.line3, 11, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.ContactName, 12, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.ContactTelNo, 13, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.ContactFaxNo, 14, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.ContactEmail, 15, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.ParentCompanyOCN, 7, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.OCN, 8, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.OFN, 9, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Form U3", nrufCompanyInfo.ProviderService.name, 10, "K", CellValues.String);

                var formU3 = await db.NrufFormU3.Where(p => p.NrufCompanyInfoId == id).ToListAsync();

                count = 21;
                foreach (var item in formU3)
                {
                    UpdateCell(outputexcelFile, "Form U3", item.Npa + "-" + item.Nxx, count, "A", CellValues.String);
                    UpdateCell(outputexcelFile, "Form U3", item.X.ToString(), count, "B", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U3", item.Rate.ToString(), count, "C", CellValues.String);
                    UpdateCell(outputexcelFile, "Form U3", item.Assigned.ToString(), count, "D", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U3", item.PortedOut.ToString(), count, "E", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U3", item.Reserved.ToString(), count, "F", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U3", item.Aging.ToString(), count, "G", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U3", item.Admin.ToString(), count, "H", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U3", item.NumbersReceived.ToString(), count, "I", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U3", item.Notes.ToString(), count, "J", CellValues.String);
                    UpdateCell(outputexcelFile, "Form U3", item.Available.ToString(), count, "K", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U3", item.Utilization.ToString(), count, "L", CellValues.Number);

                    count++;
                }

                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.ParentCompany, 7, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.name, 8, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.line1, 9, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.line2, 10, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.line3, 11, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.ContactName, 12, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.ContactTelNo, 13, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.ContactFaxNo, 14, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.ContactEmail, 15, "D", CellValues.String);
                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.ParentCompanyOCN, 7, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.OCN, 8, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.OFN, 9, "K", CellValues.String);
                UpdateCell(outputexcelFile, "Form U4", nrufCompanyInfo.ProviderService.name, 10, "K", CellValues.String);

                var formU4 = await db.NrufFormU4.Where(p => p.NrufCompanyInfoId == id).ToListAsync();

                count = 21;
                foreach (var item in formU4)
                {
                    UpdateCell(outputexcelFile, "Form U4", item.Npa + "-" + item.Nxx, count, "A", CellValues.String);
                    UpdateCell(outputexcelFile, "Form U4", item.Rate.ToString(), count, "B", CellValues.String);
                    UpdateCell(outputexcelFile, "Form U4", item.Assigned.ToString(), count, "D", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U4", item.Aging.ToString(), count, "G", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U4", item.PortedIn.ToString(), count, "I", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U4", item.Notes.ToString(), count, "J", CellValues.String);
                    UpdateCell(outputexcelFile, "Form U4", item.Available.ToString(), count, "K", CellValues.Number);
                    UpdateCell(outputexcelFile, "Form U4", item.Utilization.ToString(), count, "L", CellValues.Number);

                    count++;
                }



                // Send File
                string filename = output + ".xlsx";
                byte[] filedata = System.IO.File.ReadAllBytes(outputexcelFile);
                string contentType = MimeMapping.GetMimeMapping(outputexcelFile);

                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = filename,
                    Inline = true,
                };

                Response.AppendHeader("Content-Disposition", cd.ToString());

                return File(filedata, contentType);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            TempData["ErrorMessageExportToExcel"] = message;
            return RedirectToAction("Index", "NrufCompanyInfoes");
        }

        public static void UpdateCell(string docName, string sheetname, string text,
          uint rowIndex, string columnName, CellValues number)
        {
            // Open the document for editing.
            using (SpreadsheetDocument spreadSheet =
                     SpreadsheetDocument.Open(docName, true))
            {
                WorksheetPart worksheetPart =
                      GetWorksheetPartByName(spreadSheet, sheetname);

                if (worksheetPart != null)
                {
                    Cell cell = GetCell(worksheetPart.Worksheet,
                                             columnName, rowIndex);

                    cell.CellValue = new CellValue(text);
                    cell.DataType =
                        new EnumValue<CellValues>(number);

                    // Save the worksheet.
                    worksheetPart.Worksheet.Save();
                }
            }

        }

        private static WorksheetPart
             GetWorksheetPartByName(SpreadsheetDocument document,
             string sheetName)
        {
            IEnumerable<Sheet> sheets =
               document.WorkbookPart.Workbook.GetFirstChild<Sheets>().
               Elements<Sheet>().Where(s => s.Name == sheetName);

            if (sheets.Count() == 0)
            {
                // The specified worksheet does not exist.

                return null;
            }

            string relationshipId = sheets.First().Id.Value;
            WorksheetPart worksheetPart = (WorksheetPart)
                 document.WorkbookPart.GetPartById(relationshipId);
            return worksheetPart;

        }



        // Given a worksheet, a column name, and a row index,
        // gets the cell at the specified column and
        private static Cell GetCell(Worksheet worksheet,
                  string columnName, uint rowIndex)
        {
            Row row = GetRow(worksheet, rowIndex);

            if (row == null)
                return null;

            return row.Elements<Cell>().Where(c => string.Compare
                   (c.CellReference.Value, columnName +
                   rowIndex, true) == 0).First();
        }


        // Given a worksheet and a row index, return the row.
        private static Row GetRow(Worksheet worksheet, uint rowIndex)
        {
            return worksheet.GetFirstChild<SheetData>().
              Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
        }




        // GET: Administration/NrufCompanyInfoes
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            var nrufCompanyInfo = db.NrufCompanyInfo.Include(n => n.ProviderService);
            return View(await nrufCompanyInfo.ToListAsync());
        }

        // GET: Administration/NrufCompanyInfoes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NrufCompanyInfo nrufCompanyInfo = await db.NrufCompanyInfo.FindAsync(id);
            if (nrufCompanyInfo == null)
            {
                return HttpNotFound();
            }
            return View(nrufCompanyInfo);
        }

        // GET: Administration/NrufCompanyInfoes/Create
        public ActionResult Create()
        {
            ViewBag.ProviderServiceId = new SelectList(db.ProviderServices, "ProviderServiceId", "name");
            return View();
        }

        // POST: Administration/NrufCompanyInfoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "NrufCompanyInfoId,organizationId,UserId,applicationReference,ProviderServiceId,name,ParentCompany,ParentCompanyOCN,OCN,OFN,line1,line2,line3,ContactName,ContactTelNo,ContactFaxNo,ContactEmail,Status,createdDate,dateChanged,expiryDate")] NrufCompanyInfo nrufCompanyInfo)
        {
            if (ModelState.IsValid)
            {
                db.NrufCompanyInfo.Add(nrufCompanyInfo);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.ProviderServiceId = new SelectList(db.ProviderServices, "ProviderServiceId", "name", nrufCompanyInfo.ProviderServiceId);
            return View(nrufCompanyInfo);
        }

        // GET: Administration/NrufCompanyInfoes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NrufCompanyInfo nrufCompanyInfo = await db.NrufCompanyInfo.FindAsync(id);
            if (nrufCompanyInfo == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProviderServiceId = new SelectList(db.ProviderServices, "ProviderServiceId", "name", nrufCompanyInfo.ProviderServiceId);
            return View(nrufCompanyInfo);
        }

        // POST: Administration/NrufCompanyInfoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "NrufCompanyInfoId,organizationId,UserId,applicationReference,ProviderServiceId,name,ParentCompany,ParentCompanyOCN,OCN,OFN,line1,line2,line3,ContactName,ContactTelNo,ContactFaxNo,ContactEmail,Status,createdDate,dateChanged,expiryDate")] NrufCompanyInfo nrufCompanyInfo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(nrufCompanyInfo).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.ProviderServiceId = new SelectList(db.ProviderServices, "ProviderServiceId", "name", nrufCompanyInfo.ProviderServiceId);
            return View(nrufCompanyInfo);
        }

        // GET: Administration/NrufCompanyInfoes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NrufCompanyInfo nrufCompanyInfo = await db.NrufCompanyInfo.FindAsync(id);
            if (nrufCompanyInfo == null)
            {
                return HttpNotFound();
            }
            return View(nrufCompanyInfo);
        }

        // POST: Administration/NrufCompanyInfoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            NrufCompanyInfo nrufCompanyInfo = await db.NrufCompanyInfo.FindAsync(id);
            db.NrufCompanyInfo.Remove(nrufCompanyInfo);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
