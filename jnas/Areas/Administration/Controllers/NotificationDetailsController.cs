﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    public class NotificationDetailsController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administration/NotificationDetails
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            var notificationDetails = db.NotificationDetails.Include(n => n.LinkedResourceState).Include(n => n.Notification1).Include(n => n.ResourceSubType1).Include(n => n.ResourceType1).Include(n => n.UserType1);
            return View(await notificationDetails.ToListAsync());
        }

        // GET: Administration/NotificationDetails/Details/5
        public async Task<ActionResult> Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NotificationDetail notificationDetail = await db.NotificationDetails.FindAsync(id);
            if (notificationDetail == null)
            {
                return HttpNotFound();
            }
            return View(notificationDetail);
        }

        // GET: Administration/NotificationDetails/Create
        public ActionResult Create()
        {
            ViewBag.linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name");
            ViewBag.notificationId = new SelectList(db.Notifications, "notificationId", "name");
            ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name");
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name");
            ViewBag.userTypeId = new SelectList(db.UserTypes, "userTypeId", "name");
            return View();
        }

        // POST: Administration/NotificationDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "notificationDetailId,notificationId,notification,userTypeId,userType,resourceTypeId,resourceType,resourceSubTypeId,resourceSubType,linkedResourceStateId,state")] NotificationDetail notificationDetail)
        {
            if (ModelState.IsValid)
            {
                db.NotificationDetails.Add(notificationDetail);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", notificationDetail.linkedResourceStateId);
            ViewBag.notificationId = new SelectList(db.Notifications, "notificationId", "name", notificationDetail.notificationId);
            ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", notificationDetail.resourceSubTypeId);
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", notificationDetail.resourceTypeId);
            ViewBag.userTypeId = new SelectList(db.UserTypes, "userTypeId", "name", notificationDetail.userTypeId);
            return View(notificationDetail);
        }

        // GET: Administration/NotificationDetails/Edit/5
        public async Task<ActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NotificationDetail notificationDetail = await db.NotificationDetails.FindAsync(id);
            if (notificationDetail == null)
            {
                return HttpNotFound();
            }
            ViewBag.linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", notificationDetail.linkedResourceStateId);
            ViewBag.notificationId = new SelectList(db.Notifications, "notificationId", "name", notificationDetail.notificationId);
            ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", notificationDetail.resourceSubTypeId);
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", notificationDetail.resourceTypeId);
            ViewBag.userTypeId = new SelectList(db.UserTypes, "userTypeId", "name", notificationDetail.userTypeId);
            return View(notificationDetail);
        }

        // POST: Administration/NotificationDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "notificationDetailId,notificationId,notification,userTypeId,userType,resourceTypeId,resourceType,resourceSubTypeId,resourceSubType,linkedResourceStateId,state")] NotificationDetail notificationDetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(notificationDetail).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.linkedResourceStateId = new SelectList(db.LinkedResourceStates, "linkedResourceStateId", "name", notificationDetail.linkedResourceStateId);
            ViewBag.notificationId = new SelectList(db.Notifications, "notificationId", "name", notificationDetail.notificationId);
            ViewBag.resourceSubTypeId = new SelectList(db.ResourceSubTypes, "resourceSubTypeId", "name", notificationDetail.resourceSubTypeId);
            ViewBag.resourceTypeId = new SelectList(db.ResourceTypes, "resourceTypeId", "name", notificationDetail.resourceTypeId);
            ViewBag.userTypeId = new SelectList(db.UserTypes, "userTypeId", "name", notificationDetail.userTypeId);
            return View(notificationDetail);
        }

        // GET: Administration/NotificationDetails/Delete/5
        public async Task<ActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NotificationDetail notificationDetail = await db.NotificationDetails.FindAsync(id);
            if (notificationDetail == null)
            {
                return HttpNotFound();
            }
            return View(notificationDetail);
        }

        // POST: Administration/NotificationDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(long id)
        {
            NotificationDetail notificationDetail = await db.NotificationDetails.FindAsync(id);
            db.NotificationDetails.Remove(notificationDetail);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
