﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace jnas.Areas.Administration.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class OrganizationUserTypesController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administration/OrganizationUserTypes
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            var organizationUserTypes = db.OrganizationUserTypes.Include(o => o.OrganizationType1).Include(o => o.UserType1);
            return View(await organizationUserTypes.ToListAsync());
        }

        // GET: Administration/OrganizationUserTypes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganizationUserType organizationUserType = await db.OrganizationUserTypes.FindAsync(id);
            if (organizationUserType == null)
            {
                return HttpNotFound();
            }
            return View(organizationUserType);
        }

        // GET: Administration/OrganizationUserTypes/Create
        public ActionResult Create()
        {
            ViewBag.organizationTypeId = new SelectList(db.OrganizationTypes, "organizationTypeId", "name");
            ViewBag.userTypeId = new SelectList(db.UserTypes, "userTypeId", "name");
            return View();
        }

        // POST: Administration/OrganizationUserTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "organizationUserTypeId,organizationTypeId,userTypeId")] OrganizationUserType organizationUserType)
        {
            if (ModelState.IsValid)
            {
                db.OrganizationUserTypes.Add(organizationUserType);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.organizationTypeId = new SelectList(db.OrganizationTypes, "organizationTypeId", "name", organizationUserType.organizationTypeId);
            ViewBag.userTypeId = new SelectList(db.UserTypes, "userTypeId", "name", organizationUserType.userTypeId);
            return View(organizationUserType);
        }

        // GET: Administration/OrganizationUserTypes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganizationUserType organizationUserType = await db.OrganizationUserTypes.FindAsync(id);
            if (organizationUserType == null)
            {
                return HttpNotFound();
            }
            ViewBag.organizationTypeId = new SelectList(db.OrganizationTypes, "organizationTypeId", "name", organizationUserType.organizationTypeId);
            ViewBag.userTypeId = new SelectList(db.UserTypes, "userTypeId", "name", organizationUserType.userTypeId);
            return View(organizationUserType);
        }

        // POST: Administration/OrganizationUserTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "organizationUserTypeId,organizationTypeId,organizationType,userTypeId,userType")] OrganizationUserType organizationUserType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(organizationUserType).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.organizationTypeId = new SelectList(db.OrganizationTypes, "organizationTypeId", "name", organizationUserType.organizationTypeId);
            ViewBag.userTypeId = new SelectList(db.UserTypes, "userTypeId", "name", organizationUserType.userTypeId);
            return View(organizationUserType);
        }

        // GET: Administration/OrganizationUserTypes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganizationUserType organizationUserType = await db.OrganizationUserTypes.FindAsync(id);
            if (organizationUserType == null)
            {
                return HttpNotFound();
            }
            return View(organizationUserType);
        }

        // POST: Administration/OrganizationUserTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            OrganizationUserType organizationUserType = await db.OrganizationUserTypes.FindAsync(id);
            db.OrganizationUserTypes.Remove(organizationUserType);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
