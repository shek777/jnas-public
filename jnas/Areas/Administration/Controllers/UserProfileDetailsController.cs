﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jnas.Models.Jnasdb;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;


namespace jnas.Areas.Administration.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Internal, Administrator")]
    public class UserProfileDetailsController : Controller
    {
        private Jnasdb db = new Jnasdb();
        private UserManager _userManager;

        public UserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administration/UserProfileDetails
        public async Task<ActionResult> Index()
        {
            // Get User Id
            if (Session["UserId"] == null)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                Session["UserId"] = user.Id;
            }

            var userProfileDetails = db.UserProfileDetails.Include(u => u.Service1).Include(u => u.UserProfile1).Include(u => u.UserType1);
            return View(await userProfileDetails.ToListAsync());
        }

        // GET: Administration/UserProfileDetails/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProfileDetail userProfileDetail = await db.UserProfileDetails.FindAsync(id);
            if (userProfileDetail == null)
            {
                return HttpNotFound();
            }
            return View(userProfileDetail);
        }

        // GET: Administration/UserProfileDetails/Create
        public ActionResult Create()
        {
            ViewBag.serviceId = new SelectList(db.Services, "serviceId", "name");
            ViewBag.userProfileId = new SelectList(db.UserProfiles, "userProfileId", "name");
            ViewBag.userTypeId = new SelectList(db.UserTypes, "userTypeId", "name");
            return View();
        }

        // POST: Administration/UserProfileDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "userProfileDetailId,userProfileId,userProfile,serviceId,service,userTypeId,userType,assigned")] UserProfileDetail userProfileDetail)
        {
            if (ModelState.IsValid)
            {
                db.UserProfileDetails.Add(userProfileDetail);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.serviceId = new SelectList(db.Services, "serviceId", "name", userProfileDetail.serviceId);
            ViewBag.userProfileId = new SelectList(db.UserProfiles, "userProfileId", "name", userProfileDetail.userProfileId);
            ViewBag.userTypeId = new SelectList(db.UserTypes, "userTypeId", "name", userProfileDetail.userTypeId);
            return View(userProfileDetail);
        }

        // GET: Administration/UserProfileDetails/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProfileDetail userProfileDetail = await db.UserProfileDetails.FindAsync(id);
            if (userProfileDetail == null)
            {
                return HttpNotFound();
            }
            ViewBag.serviceId = new SelectList(db.Services, "serviceId", "name", userProfileDetail.serviceId);
            ViewBag.userProfileId = new SelectList(db.UserProfiles, "userProfileId", "name", userProfileDetail.userProfileId);
            ViewBag.userTypeId = new SelectList(db.UserTypes, "userTypeId", "name", userProfileDetail.userTypeId);
            return View(userProfileDetail);
        }

        // POST: Administration/UserProfileDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "userProfileDetailId,userProfileId,userProfile,serviceId,service,userTypeId,userType,assigned")] UserProfileDetail userProfileDetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userProfileDetail).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.serviceId = new SelectList(db.Services, "serviceId", "name", userProfileDetail.serviceId);
            ViewBag.userProfileId = new SelectList(db.UserProfiles, "userProfileId", "name", userProfileDetail.userProfileId);
            ViewBag.userTypeId = new SelectList(db.UserTypes, "userTypeId", "name", userProfileDetail.userTypeId);
            return View(userProfileDetail);
        }

        // GET: Administration/UserProfileDetails/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProfileDetail userProfileDetail = await db.UserProfileDetails.FindAsync(id);
            if (userProfileDetail == null)
            {
                return HttpNotFound();
            }
            return View(userProfileDetail);
        }

        // POST: Administration/UserProfileDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            UserProfileDetail userProfileDetail = await db.UserProfileDetails.FindAsync(id);
            db.UserProfileDetails.Remove(userProfileDetail);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
