﻿/*
  
   ELMAH - Error Logging Modules and Handlers for ASP.NET
   Copyright (c) 2004-9 Atif Aziz. All rights reserved.
  
    Author(s):
  
        Atif Aziz, http://www.raboof.com
        Phil Haacked, http://haacked.com
  
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
  
      http://www.apache.org/licenses/LICENSE-2.0
  
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  
*/

-- ELMAH DDL script for Microsoft SQL Server 2000 or later.

-- $Id: SQLServer.sql 677 2009-09-29 18:02:39Z azizatif $

DECLARE @DBCompatibilityLevel INT
DECLARE @DBCompatibilityLevelMajor INT
DECLARE @DBCompatibilityLevelMinor INT

SELECT 
    @DBCompatibilityLevel = cmptlevel 
FROM 
    master.dbo.sysdatabases 
WHERE 
    name = DB_NAME()

IF @DBCompatibilityLevel <> 80
BEGIN

    SELECT @DBCompatibilityLevelMajor = @DBCompatibilityLevel / 10, 
           @DBCompatibilityLevelMinor = @DBCompatibilityLevel % 10
           
    PRINT N'
    ===========================================================================
    WARNING! 
    ---------------------------------------------------------------------------
    
    This script is designed for Microsoft SQL Server 2000 (8.0) but your 
    database is set up for compatibility with version ' 
    + CAST(@DBCompatibilityLevelMajor AS NVARCHAR(80)) 
    + N'.' 
    + CAST(@DBCompatibilityLevelMinor AS NVARCHAR(80)) 
    + N'. Although 
    the script should work with later versions of Microsoft SQL Server, 
    you can ensure compatibility by executing the following statement:
    
    ALTER DATABASE [' 
    + DB_NAME() 
    + N'] 
    SET COMPATIBILITY_LEVEL = 80

    If you are hosting ELMAH in the same database as your application 
    database and do not wish to change the compatibility option then you 
    should create a separate database to host ELMAH where you can set the 
    compatibility level more freely.
    
    If you continue with the current setup, please report any compatibility 
    issues you encounter over at:
    
    http://code.google.com/p/elmah/issues/list

    ===========================================================================
'
END

GO

/* ------------------------------------------------------------------------ 
        TABLES
   ------------------------------------------------------------------------ */

CREATE TABLE [dbo].[ELMAH_Error]
(
    [ErrorId]     UNIQUEIDENTIFIER NOT NULL,
    [Application] NVARCHAR(60)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Host]        NVARCHAR(50)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Type]        NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Source]      NVARCHAR(60)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Message]     NVARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [User]        NVARCHAR(50)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [StatusCode]  INT NOT NULL,
    [TimeUtc]     DATETIME NOT NULL,
    [Sequence]    INT IDENTITY (1, 1) NOT NULL,
    [AllXml]      NTEXT COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) 
ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[ELMAH_Error] WITH NOCHECK ADD 
    CONSTRAINT [PK_ELMAH_Error] PRIMARY KEY NONCLUSTERED ([ErrorId]) ON [PRIMARY] 

GO

ALTER TABLE [dbo].[ELMAH_Error] ADD 
    CONSTRAINT [DF_ELMAH_Error_ErrorId] DEFAULT (NEWID()) FOR [ErrorId]

GO

CREATE NONCLUSTERED INDEX [IX_ELMAH_Error_App_Time_Seq] ON [dbo].[ELMAH_Error] 
(
    [Application]   ASC,
    [TimeUtc]       DESC,
    [Sequence]      DESC
) 
ON [PRIMARY]

GO

/* ------------------------------------------------------------------------ 
        STORED PROCEDURES                                                      
   ------------------------------------------------------------------------ */

SET QUOTED_IDENTIFIER ON 

GO

SET ANSI_NULLS ON 

GO

CREATE PROCEDURE [dbo].[ELMAH_GetErrorXml]
(
    @Application NVARCHAR(60),
    @ErrorId UNIQUEIDENTIFIER
)
AS

    SET NOCOUNT ON

    SELECT 
        [AllXml]
    FROM 
        [ELMAH_Error]
    WHERE
        [ErrorId] = @ErrorId
    AND
        [Application] = @Application

GO

SET QUOTED_IDENTIFIER OFF 

GO

SET ANSI_NULLS ON 

GO

SET QUOTED_IDENTIFIER ON 

GO

SET ANSI_NULLS ON 

GO

CREATE PROCEDURE [dbo].[ELMAH_GetErrorsXml]
(
    @Application NVARCHAR(60),
    @PageIndex INT = 0,
    @PageSize INT = 15,
    @TotalCount INT OUTPUT
)
AS 

    SET NOCOUNT ON

    DECLARE @FirstTimeUTC DATETIME
    DECLARE @FirstSequence INT
    DECLARE @StartRow INT
    DECLARE @StartRowIndex INT

    SELECT 
        @TotalCount = COUNT(1) 
    FROM 
        [ELMAH_Error]
    WHERE 
        [Application] = @Application

    -- Get the ID of the first error for the requested page

    SET @StartRowIndex = @PageIndex * @PageSize + 1

    IF @StartRowIndex <= @TotalCount
    BEGIN

        SET ROWCOUNT @StartRowIndex

        SELECT  
            @FirstTimeUTC = [TimeUtc],
            @FirstSequence = [Sequence]
        FROM 
            [ELMAH_Error]
        WHERE   
            [Application] = @Application
        ORDER BY 
            [TimeUtc] DESC, 
            [Sequence] DESC

    END
    ELSE
    BEGIN

        SET @PageSize = 0

    END

    -- Now set the row count to the requested page size and get
    -- all records below it for the pertaining application.

    SET ROWCOUNT @PageSize

    SELECT 
        errorId     = [ErrorId], 
        application = [Application],
        host        = [Host], 
        type        = [Type],
        source      = [Source],
        message     = [Message],
        [user]      = [User],
        statusCode  = [StatusCode], 
        time        = CONVERT(VARCHAR(50), [TimeUtc], 126) + 'Z'
    FROM 
        [ELMAH_Error] error
    WHERE
        [Application] = @Application
    AND
        [TimeUtc] <= @FirstTimeUTC
    AND 
        [Sequence] <= @FirstSequence
    ORDER BY
        [TimeUtc] DESC, 
        [Sequence] DESC
    FOR
        XML AUTO

GO

SET QUOTED_IDENTIFIER OFF 

GO

SET ANSI_NULLS ON 

GO

SET QUOTED_IDENTIFIER ON 

GO

SET ANSI_NULLS ON 

GO

CREATE PROCEDURE [dbo].[ELMAH_LogError]
(
    @ErrorId UNIQUEIDENTIFIER,
    @Application NVARCHAR(60),
    @Host NVARCHAR(30),
    @Type NVARCHAR(100),
    @Source NVARCHAR(60),
    @Message NVARCHAR(500),
    @User NVARCHAR(50),
    @AllXml NTEXT,
    @StatusCode INT,
    @TimeUtc DATETIME
)
AS

    SET NOCOUNT ON

    INSERT
    INTO
        [ELMAH_Error]
        (
            [ErrorId],
            [Application],
            [Host],
            [Type],
            [Source],
            [Message],
            [User],
            [AllXml],
            [StatusCode],
            [TimeUtc]
        )
    VALUES
        (
            @ErrorId,
            @Application,
            @Host,
            @Type,
            @Source,
            @Message,
            @User,
            @AllXml,
            @StatusCode,
            @TimeUtc
        )

GO

SET QUOTED_IDENTIFIER OFF 

GO

SET ANSI_NULLS ON 

CREATE TABLE [dbo].[Addresses] (
    [AddressId] [int] NOT NULL IDENTITY,
    [organizationId] [int] NOT NULL,
    [organization] [nvarchar](max),
    [line1] [nvarchar](max) NOT NULL,
    [line2] [nvarchar](max),
    [city] [nvarchar](max),
    [zipCode] [nvarchar](max),
    [defaultAddress] [bit] NOT NULL,
    CONSTRAINT [PK_dbo.Addresses] PRIMARY KEY ([AddressId])
)
CREATE INDEX [IX_organizationId] ON [dbo].[Addresses]([organizationId])
CREATE TABLE [dbo].[Organizations] (
    [organizationId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max) NOT NULL,
    [organizationTypeId] [int] NOT NULL,
    [organizationType] [nvarchar](max),
    [ParentCompany] [nvarchar](max),
    [ParentCompanyOCN] [nvarchar](max),
    [OCN] [nvarchar](max),
    [OFN] [nvarchar](max),
    [createdDate] [datetime],
    [dateChanged] [datetime],
    [organizationStateId] [int] NOT NULL,
    [state] [nvarchar](max),
    CONSTRAINT [PK_dbo.Organizations] PRIMARY KEY ([organizationId])
)
CREATE INDEX [IX_organizationTypeId] ON [dbo].[Organizations]([organizationTypeId])
CREATE INDEX [IX_organizationStateId] ON [dbo].[Organizations]([organizationStateId])
CREATE TABLE [dbo].[Applications] (
    [applicationId] [int] NOT NULL IDENTITY,
    [organizationId] [int] NOT NULL,
    [application1] [nvarchar](max),
    [applicationReference] [nvarchar](max) NOT NULL,
    [applicant] [nvarchar](max),
    [recipient] [nvarchar](max),
    [UserId] [nvarchar](128) NOT NULL,
    [user] [nvarchar](max),
    [serviceId] [int] NOT NULL,
    [service] [nvarchar](max),
    [resourceTypeId] [int] NOT NULL,
    [resourceType] [nvarchar](max),
    [resourceSubTypeId] [int] NOT NULL,
    [resourceSubType] [nvarchar](max),
    [subscriber] [nvarchar](max),
    [customerContactNumber] [nvarchar](max),
    [localRoutingNumber] [nvarchar](max),
    [FileName] [nvarchar](255),
    [ContentType] [nvarchar](100),
    [additionalDocumentation] [varbinary](max),
    [createdDate] [datetime],
    [dateChanged] [datetime],
    [applicationResourceId] [int] NOT NULL,
    [applicationTypeId] [int] NOT NULL,
    [applicationStateId] [int] NOT NULL,
    [state] [nvarchar](max),
    CONSTRAINT [PK_dbo.Applications] PRIMARY KEY ([applicationId])
)
CREATE INDEX [IX_organizationId] ON [dbo].[Applications]([organizationId])
CREATE INDEX [IX_UserId] ON [dbo].[Applications]([UserId])
CREATE INDEX [IX_resourceTypeId] ON [dbo].[Applications]([resourceTypeId])
CREATE INDEX [IX_resourceSubTypeId] ON [dbo].[Applications]([resourceSubTypeId])
CREATE INDEX [IX_applicationTypeId] ON [dbo].[Applications]([applicationTypeId])
CREATE INDEX [IX_applicationStateId] ON [dbo].[Applications]([applicationStateId])
CREATE TABLE [dbo].[ApplicationNotes] (
    [applicationNotesId] [int] NOT NULL IDENTITY,
    [applicationId] [int] NOT NULL,
    [application] [nvarchar](max),
    [note] [nvarchar](max) NOT NULL,
    [UserId] [nvarchar](max) NOT NULL,
    [user] [nvarchar](max),
    [internalNote] [nvarchar](max),
    [createdDate] [datetime],
    [dateChanged] [datetime],
    CONSTRAINT [PK_dbo.ApplicationNotes] PRIMARY KEY ([applicationNotesId])
)
CREATE INDEX [IX_applicationId] ON [dbo].[ApplicationNotes]([applicationId])
CREATE TABLE [dbo].[ApplicationResources] (
    [applicationResourceId] [int] NOT NULL IDENTITY,
    [applicationId] [int] NOT NULL,
    [application] [nvarchar](max),
    [resourceId] [int] NOT NULL,
    [resource] [nvarchar](max),
    [createdDate] [datetime],
    [dateChanged] [datetime],
    [applicationResourceStateId] [int] NOT NULL,
    [state] [nvarchar](max),
    [Application_applicationId] [int] NOT NULL,
    [ApplicationResourceState_applicationResourceStateId] [int],
    [ApplicationResourceState_applicationResourceStateId1] [int] NOT NULL,
    [Resource_resourceId] [int],
    [Resource_resourceId1] [int] NOT NULL,
    [Application_applicationId1] [int],
    [Organization_organizationId] [int],
    CONSTRAINT [PK_dbo.ApplicationResources] PRIMARY KEY ([applicationResourceId])
)
CREATE INDEX [IX_Application_applicationId] ON [dbo].[ApplicationResources]([Application_applicationId])
CREATE INDEX [IX_ApplicationResourceState_applicationResourceStateId] ON [dbo].[ApplicationResources]([ApplicationResourceState_applicationResourceStateId])
CREATE INDEX [IX_ApplicationResourceState_applicationResourceStateId1] ON [dbo].[ApplicationResources]([ApplicationResourceState_applicationResourceStateId1])
CREATE INDEX [IX_Resource_resourceId] ON [dbo].[ApplicationResources]([Resource_resourceId])
CREATE INDEX [IX_Resource_resourceId1] ON [dbo].[ApplicationResources]([Resource_resourceId1])
CREATE INDEX [IX_Application_applicationId1] ON [dbo].[ApplicationResources]([Application_applicationId1])
CREATE INDEX [IX_Organization_organizationId] ON [dbo].[ApplicationResources]([Organization_organizationId])
CREATE TABLE [dbo].[ApplicationResourceStates] (
    [applicationResourceStateId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max),
    CONSTRAINT [PK_dbo.ApplicationResourceStates] PRIMARY KEY ([applicationResourceStateId])
)
CREATE TABLE [dbo].[Resources] (
    [resourceId] [int] NOT NULL IDENTITY,
    [resource1] [nvarchar](max) NOT NULL,
    [resourceTypeId] [int] NOT NULL,
    [type] [nvarchar](max),
    [resourceSubTypeId] [int] NOT NULL,
    [subtype] [nvarchar](max),
    [binary] [nvarchar](max),
    [createdDate] [datetime],
    [dateChanged] [datetime],
    [resourceLinkId] [int] NOT NULL,
    [resourceStateId] [int] NOT NULL,
    [state] [nvarchar](max),
    [ResourceState_resourceStateId] [int],
    [ResourceState_resourceStateId1] [int] NOT NULL,
    CONSTRAINT [PK_dbo.Resources] PRIMARY KEY ([resourceId])
)
CREATE INDEX [IX_ResourceState_resourceStateId] ON [dbo].[Resources]([ResourceState_resourceStateId])
CREATE INDEX [IX_ResourceState_resourceStateId1] ON [dbo].[Resources]([ResourceState_resourceStateId1])
CREATE TABLE [dbo].[ResourceLinks] (
    [resourceLinkId] [int] NOT NULL IDENTITY,
    [parentResource] [nvarchar](max),
    [childResource] [nvarchar](max),
    [linkedResourceStateId] [int] NOT NULL,
    [state] [nvarchar](max),
    [LinkedResourceState_linkedResourceStateId] [int],
    [LinkedResourceState_linkedResourceStateId1] [int] NOT NULL,
    [Resource_resourceId] [int] NOT NULL,
    [Resource_resourceId1] [int],
    CONSTRAINT [PK_dbo.ResourceLinks] PRIMARY KEY ([resourceLinkId])
)
CREATE INDEX [IX_LinkedResourceState_linkedResourceStateId] ON [dbo].[ResourceLinks]([LinkedResourceState_linkedResourceStateId])
CREATE INDEX [IX_LinkedResourceState_linkedResourceStateId1] ON [dbo].[ResourceLinks]([LinkedResourceState_linkedResourceStateId1])
CREATE INDEX [IX_Resource_resourceId] ON [dbo].[ResourceLinks]([Resource_resourceId])
CREATE INDEX [IX_Resource_resourceId1] ON [dbo].[ResourceLinks]([Resource_resourceId1])
CREATE TABLE [dbo].[LinkedResourceStates] (
    [linkedResourceStateId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max),
    CONSTRAINT [PK_dbo.LinkedResourceStates] PRIMARY KEY ([linkedResourceStateId])
)
CREATE TABLE [dbo].[NotificationDetails] (
    [notificationDetailId] [bigint] NOT NULL IDENTITY,
    [notificationId] [int] NOT NULL,
    [notification] [nvarchar](max),
    [userTypeId] [int] NOT NULL,
    [userType] [nvarchar](max),
    [resourceTypeId] [int] NOT NULL,
    [resourceType] [nvarchar](max),
    [resourceSubTypeId] [int] NOT NULL,
    [resourceSubType] [nvarchar](max),
    [linkedResourceStateId] [int] NOT NULL,
    [state] [nvarchar](max),
    CONSTRAINT [PK_dbo.NotificationDetails] PRIMARY KEY ([notificationDetailId])
)
CREATE INDEX [IX_notificationId] ON [dbo].[NotificationDetails]([notificationId])
CREATE INDEX [IX_userTypeId] ON [dbo].[NotificationDetails]([userTypeId])
CREATE INDEX [IX_resourceTypeId] ON [dbo].[NotificationDetails]([resourceTypeId])
CREATE INDEX [IX_resourceSubTypeId] ON [dbo].[NotificationDetails]([resourceSubTypeId])
CREATE INDEX [IX_linkedResourceStateId] ON [dbo].[NotificationDetails]([linkedResourceStateId])
CREATE TABLE [dbo].[Notifications] (
    [notificationId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max) NOT NULL,
    [mandatory] [bit] NOT NULL,
    CONSTRAINT [PK_dbo.Notifications] PRIMARY KEY ([notificationId])
)
CREATE TABLE [dbo].[UserNotifications] (
    [userNotificationId] [int] NOT NULL IDENTITY,
    [UserId] [nvarchar](128) NOT NULL,
    [user] [nvarchar](max),
    [notificationId] [int] NOT NULL,
    [notification] [nvarchar](max),
    [assigned] [bit] NOT NULL,
    CONSTRAINT [PK_dbo.UserNotifications] PRIMARY KEY ([userNotificationId])
)
CREATE INDEX [IX_UserId] ON [dbo].[UserNotifications]([UserId])
CREATE INDEX [IX_notificationId] ON [dbo].[UserNotifications]([notificationId])
CREATE TABLE [dbo].[Users] (
    [UserId] [nvarchar](128) NOT NULL,
    [name] [nvarchar](max) NOT NULL,
    [userNo] [int] NOT NULL IDENTITY,
    [organizationId] [int] NOT NULL,
    [organization] [nvarchar](max),
    [userTypeId] [int] NOT NULL,
    [userType] [nvarchar](max),
    [userProfileId] [int] NOT NULL,
    [userProfile] [nvarchar](max),
    [primaryContact] [bit] NOT NULL,
    [securityQuestion] [nvarchar](max) NOT NULL,
    [securityAnswer] [nvarchar](max) NOT NULL,
    [createdDate] [datetime],
    [dateChanged] [datetime],
    [lastConnectionDate] [datetime],
    [userStateId] [int] NOT NULL,
    [state] [nvarchar](max),
    [Email] [nvarchar](256),
    [EmailConfirmed] [bit] NOT NULL,
    [PasswordHash] [nvarchar](max),
    [SecurityStamp] [nvarchar](max),
    [PhoneNumber] [nvarchar](max),
    [PhoneNumberConfirmed] [bit] NOT NULL,
    [TwoFactorEnabled] [bit] NOT NULL,
    [LockoutEndDateUtc] [datetime],
    [LockoutEnabled] [bit] NOT NULL,
    [AccessFailedCount] [int] NOT NULL,
    [UserName] [nvarchar](256) NOT NULL,
    CONSTRAINT [PK_dbo.Users] PRIMARY KEY ([UserId])
)
CREATE INDEX [IX_organizationId] ON [dbo].[Users]([organizationId])
CREATE INDEX [IX_userTypeId] ON [dbo].[Users]([userTypeId])
CREATE INDEX [IX_userStateId] ON [dbo].[Users]([userStateId])
CREATE UNIQUE INDEX [UserNameIndex] ON [dbo].[Users]([UserName])
CREATE TABLE [dbo].[UserClaims] (
    [UserClaimId] [int] NOT NULL IDENTITY,
    [UserId] [nvarchar](128) NOT NULL,
    [ClaimType] [nvarchar](max),
    [ClaimValue] [nvarchar](max),
    CONSTRAINT [PK_dbo.UserClaims] PRIMARY KEY ([UserClaimId])
)
CREATE INDEX [IX_UserId] ON [dbo].[UserClaims]([UserId])
CREATE TABLE [dbo].[UserLogins] (
    [LoginProvider] [nvarchar](128) NOT NULL,
    [ProviderKey] [nvarchar](128) NOT NULL,
    [UserId] [nvarchar](128) NOT NULL,
    CONSTRAINT [PK_dbo.UserLogins] PRIMARY KEY ([LoginProvider], [ProviderKey], [UserId])
)
CREATE INDEX [IX_UserId] ON [dbo].[UserLogins]([UserId])
CREATE TABLE [dbo].[UserRoles] (
    [UserId] [nvarchar](128) NOT NULL,
    [RoleId] [nvarchar](128) NOT NULL,
    [Discriminator] [nvarchar](128) NOT NULL,
    CONSTRAINT [PK_dbo.UserRoles] PRIMARY KEY ([UserId], [RoleId])
)
CREATE INDEX [IX_UserId] ON [dbo].[UserRoles]([UserId])
CREATE INDEX [IX_RoleId] ON [dbo].[UserRoles]([RoleId])
CREATE TABLE [dbo].[UserStates] (
    [userStateId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max),
    CONSTRAINT [PK_dbo.UserStates] PRIMARY KEY ([userStateId])
)
CREATE TABLE [dbo].[UserTypes] (
    [userTypeId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max),
    CONSTRAINT [PK_dbo.UserTypes] PRIMARY KEY ([userTypeId])
)
CREATE TABLE [dbo].[OrganizationUserTypes] (
    [organizationUserTypeId] [int] NOT NULL IDENTITY,
    [organizationTypeId] [int] NOT NULL,
    [organizationType] [nvarchar](max),
    [userTypeId] [int] NOT NULL,
    [userType] [nvarchar](max),
    CONSTRAINT [PK_dbo.OrganizationUserTypes] PRIMARY KEY ([organizationUserTypeId])
)
CREATE INDEX [IX_organizationTypeId] ON [dbo].[OrganizationUserTypes]([organizationTypeId])
CREATE INDEX [IX_userTypeId] ON [dbo].[OrganizationUserTypes]([userTypeId])
CREATE TABLE [dbo].[OrganizationTypes] (
    [organizationTypeId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max),
    CONSTRAINT [PK_dbo.OrganizationTypes] PRIMARY KEY ([organizationTypeId])
)
CREATE TABLE [dbo].[UserProfileDetails] (
    [userProfileDetailId] [int] NOT NULL IDENTITY,
    [userProfileId] [int] NOT NULL,
    [userProfile] [nvarchar](max),
    [serviceId] [int] NOT NULL,
    [service] [nvarchar](max),
    [userTypeId] [int] NOT NULL,
    [userType] [nvarchar](max),
    [assigned] [bit] NOT NULL,
    CONSTRAINT [PK_dbo.UserProfileDetails] PRIMARY KEY ([userProfileDetailId])
)
CREATE INDEX [IX_userProfileId] ON [dbo].[UserProfileDetails]([userProfileId])
CREATE INDEX [IX_serviceId] ON [dbo].[UserProfileDetails]([serviceId])
CREATE INDEX [IX_userTypeId] ON [dbo].[UserProfileDetails]([userTypeId])
CREATE TABLE [dbo].[Services] (
    [serviceId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max) NOT NULL,
    [applicationYN] [bit] NOT NULL,
    CONSTRAINT [PK_dbo.Services] PRIMARY KEY ([serviceId])
)
CREATE TABLE [dbo].[ServiceProfiles] (
    [serviceProfileId] [int] NOT NULL IDENTITY,
    [serviceId] [int] NOT NULL,
    [service] [nvarchar](max),
    [resourceTypeId] [int] NOT NULL,
    [resourceType] [nvarchar](max),
    [resourceSubTypeId] [int] NOT NULL,
    [resourceSubType] [nvarchar](max),
    [providerServiceId] [int] NOT NULL,
    [providerService] [nvarchar](max),
    [assigned] [bit] NOT NULL,
    CONSTRAINT [PK_dbo.ServiceProfiles] PRIMARY KEY ([serviceProfileId])
)
CREATE INDEX [IX_serviceId] ON [dbo].[ServiceProfiles]([serviceId])
CREATE INDEX [IX_resourceTypeId] ON [dbo].[ServiceProfiles]([resourceTypeId])
CREATE INDEX [IX_resourceSubTypeId] ON [dbo].[ServiceProfiles]([resourceSubTypeId])
CREATE INDEX [IX_providerServiceId] ON [dbo].[ServiceProfiles]([providerServiceId])
CREATE TABLE [dbo].[ProviderServices] (
    [ProviderServiceId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max) NOT NULL,
    CONSTRAINT [PK_dbo.ProviderServices] PRIMARY KEY ([ProviderServiceId])
)
CREATE TABLE [dbo].[NrufCompanyInfoes] (
    [NrufCompanyInfoId] [int] NOT NULL IDENTITY,
    [organizationId] [int] NOT NULL,
    [UserId] [nvarchar](max),
    [ProviderServiceId] [int] NOT NULL,
    [name] [nvarchar](max),
    [ParentCompany] [nvarchar](max),
    [ParentCompanyOCN] [nvarchar](max),
    [OCN] [nvarchar](max),
    [OFN] [nvarchar](max),
    [line1] [nvarchar](max),
    [line2] [nvarchar](max),
    [line3] [nvarchar](max),
    [ContactName] [nvarchar](max),
    [ContactTelNo] [nvarchar](max),
    [ContactFaxNo] [nvarchar](max),
    [ContactEmail] [nvarchar](max),
    [Status] [nvarchar](max),
    [expiryDate] [datetime],
    [createdDate] [datetime],
    [dateChanged] [datetime],
    CONSTRAINT [PK_dbo.NrufCompanyInfoes] PRIMARY KEY ([NrufCompanyInfoId])
)
CREATE INDEX [IX_ProviderServiceId] ON [dbo].[NrufCompanyInfoes]([ProviderServiceId])
CREATE TABLE [dbo].[NrufFormF3a] (
    [FormF3aId] [int] NOT NULL IDENTITY,
    [NrufCompanyInfoId] [int] NOT NULL,
    [Npa] [nvarchar](max),
    [Year1] [int] NOT NULL,
    [Year2] [int] NOT NULL,
    [Year3] [int] NOT NULL,
    [Year4] [int] NOT NULL,
    [Year5] [int] NOT NULL,
    [Total] [int] NOT NULL,
    [createdDate] [datetime],
    [dateChanged] [datetime],
    CONSTRAINT [PK_dbo.NrufFormF3a] PRIMARY KEY ([FormF3aId])
)
CREATE INDEX [IX_NrufCompanyInfoId] ON [dbo].[NrufFormF3a]([NrufCompanyInfoId])
CREATE TABLE [dbo].[NrufFormF3b] (
    [FormF3bId] [int] NOT NULL IDENTITY,
    [NrufCompanyInfoId] [int] NOT NULL,
    [Npa] [nvarchar](max),
    [Year1] [int] NOT NULL,
    [Year2] [int] NOT NULL,
    [Year3] [int] NOT NULL,
    [Year4] [int] NOT NULL,
    [Year5] [int] NOT NULL,
    [Total] [int] NOT NULL,
    [createdDate] [datetime],
    [dateChanged] [datetime],
    CONSTRAINT [PK_dbo.NrufFormF3b] PRIMARY KEY ([FormF3bId])
)
CREATE INDEX [IX_NrufCompanyInfoId] ON [dbo].[NrufFormF3b]([NrufCompanyInfoId])
CREATE TABLE [dbo].[NrufFormU1] (
    [FormU1Id] [int] NOT NULL IDENTITY,
    [NrufCompanyInfoId] [int] NOT NULL,
    [Npa] [nvarchar](max),
    [Nxx] [nvarchar](max),
    [X] [int] NOT NULL,
    [Rate] [nvarchar](max),
    [Assigned] [int] NOT NULL,
    [Intermediate] [int] NOT NULL,
    [Reserved] [int] NOT NULL,
    [Aging] [int] NOT NULL,
    [Admin] [int] NOT NULL,
    [PortedOut] [int] NOT NULL,
    [Notes] [nvarchar](max),
    [Available] [int] NOT NULL,
    [Utilization] [nvarchar](max),
    [createdDate] [datetime],
    [dateChanged] [datetime],
    CONSTRAINT [PK_dbo.NrufFormU1] PRIMARY KEY ([FormU1Id])
)
CREATE INDEX [IX_NrufCompanyInfoId] ON [dbo].[NrufFormU1]([NrufCompanyInfoId])
CREATE TABLE [dbo].[NrufFormU2] (
    [FormU2Id] [int] NOT NULL IDENTITY,
    [NrufCompanyInfoId] [int] NOT NULL,
    [Npa] [nvarchar](max),
    [Nxx] [nvarchar](max),
    [Rate] [nvarchar](max),
    [Assigned] [int] NOT NULL,
    [Intermediate] [int] NOT NULL,
    [Reserved] [int] NOT NULL,
    [Aging] [int] NOT NULL,
    [Admin] [int] NOT NULL,
    [PortedOut] [int] NOT NULL,
    [Notes] [nvarchar](max),
    [Available] [int] NOT NULL,
    [Utilization] [nvarchar](max),
    [createdDate] [datetime],
    [dateChanged] [datetime],
    CONSTRAINT [PK_dbo.NrufFormU2] PRIMARY KEY ([FormU2Id])
)
CREATE INDEX [IX_NrufCompanyInfoId] ON [dbo].[NrufFormU2]([NrufCompanyInfoId])
CREATE TABLE [dbo].[NrufFormU3] (
    [FormU3Id] [int] NOT NULL IDENTITY,
    [NrufCompanyInfoId] [int] NOT NULL,
    [Npa] [nvarchar](max),
    [Nxx] [nvarchar](max),
    [X] [int] NOT NULL,
    [Rate] [nvarchar](max),
    [Assigned] [int] NOT NULL,
    [NumbersReceived] [int] NOT NULL,
    [Reserved] [int] NOT NULL,
    [Aging] [int] NOT NULL,
    [Admin] [int] NOT NULL,
    [PortedOut] [int] NOT NULL,
    [Notes] [nvarchar](max),
    [Available] [int] NOT NULL,
    [Utilization] [nvarchar](max),
    [createdDate] [datetime],
    [dateChanged] [datetime],
    CONSTRAINT [PK_dbo.NrufFormU3] PRIMARY KEY ([FormU3Id])
)
CREATE INDEX [IX_NrufCompanyInfoId] ON [dbo].[NrufFormU3]([NrufCompanyInfoId])
CREATE TABLE [dbo].[NrufFormU4] (
    [FormU4Id] [int] NOT NULL IDENTITY,
    [NrufCompanyInfoId] [int] NOT NULL,
    [Npa] [nvarchar](max),
    [Nxx] [nvarchar](max),
    [Rate] [nvarchar](max),
    [Assigned] [int] NOT NULL,
    [Aging] [int] NOT NULL,
    [PortedIn] [int] NOT NULL,
    [Notes] [nvarchar](max),
    [Available] [int] NOT NULL,
    [Utilization] [nvarchar](max),
    [createdDate] [datetime],
    [dateChanged] [datetime],
    CONSTRAINT [PK_dbo.NrufFormU4] PRIMARY KEY ([FormU4Id])
)
CREATE INDEX [IX_NrufCompanyInfoId] ON [dbo].[NrufFormU4]([NrufCompanyInfoId])
CREATE TABLE [dbo].[OrganizationServices] (
    [organizationServicesId] [int] NOT NULL IDENTITY,
    [AddressId] [int] NOT NULL,
    [ProviderServiceId] [int] NOT NULL,
    [organization] [nvarchar](max),
    [providerService] [nvarchar](max),
    [createdDate] [datetime],
    [dateChanged] [datetime],
    CONSTRAINT [PK_dbo.OrganizationServices] PRIMARY KEY ([organizationServicesId])
)
CREATE INDEX [IX_AddressId] ON [dbo].[OrganizationServices]([AddressId])
CREATE INDEX [IX_ProviderServiceId] ON [dbo].[OrganizationServices]([ProviderServiceId])
CREATE TABLE [dbo].[ResourceSubTypes] (
    [resourceSubTypeId] [int] NOT NULL IDENTITY,
    [resourceTypeId] [int] NOT NULL,
    [name] [nvarchar](max) NOT NULL,
    [subType] [nvarchar](max),
    [format] [nvarchar](max) NOT NULL,
    [formatRegex] [nvarchar](max) NOT NULL,
    CONSTRAINT [PK_dbo.ResourceSubTypes] PRIMARY KEY ([resourceSubTypeId])
)
CREATE INDEX [IX_resourceTypeId] ON [dbo].[ResourceSubTypes]([resourceTypeId])
CREATE TABLE [dbo].[OrganizationResourceTypes] (
    [organizationResourceTypeId] [int] NOT NULL IDENTITY,
    [organizationId] [int] NOT NULL,
    [organization] [nvarchar](max),
    [resourceTypeId] [int] NOT NULL,
    [resourceType] [nvarchar](max),
    [resourceSubTypeId] [int] NOT NULL,
    [resourceSubType] [nvarchar](max),
    [linkedResourceStateId] [int] NOT NULL,
    [state] [nvarchar](max),
    CONSTRAINT [PK_dbo.OrganizationResourceTypes] PRIMARY KEY ([organizationResourceTypeId])
)
CREATE INDEX [IX_organizationId] ON [dbo].[OrganizationResourceTypes]([organizationId])
CREATE INDEX [IX_resourceTypeId] ON [dbo].[OrganizationResourceTypes]([resourceTypeId])
CREATE INDEX [IX_resourceSubTypeId] ON [dbo].[OrganizationResourceTypes]([resourceSubTypeId])
CREATE INDEX [IX_linkedResourceStateId] ON [dbo].[OrganizationResourceTypes]([linkedResourceStateId])
CREATE TABLE [dbo].[ResourceTypes] (
    [resourceTypeId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max) NOT NULL,
    [format] [nvarchar](max) NOT NULL,
    [formatRegex] [nvarchar](max) NOT NULL,
    CONSTRAINT [PK_dbo.ResourceTypes] PRIMARY KEY ([resourceTypeId])
)
CREATE TABLE [dbo].[UserProfiles] (
    [userProfileId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max) NOT NULL,
    [defaultProfile] [bit] NOT NULL,
    CONSTRAINT [PK_dbo.UserProfiles] PRIMARY KEY ([userProfileId])
)
CREATE TABLE [dbo].[ResourceStates] (
    [resourceStateId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max) NOT NULL,
    CONSTRAINT [PK_dbo.ResourceStates] PRIMARY KEY ([resourceStateId])
)
CREATE TABLE [dbo].[ApplicationStates] (
    [applicationStateId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max) NOT NULL,
    CONSTRAINT [PK_dbo.ApplicationStates] PRIMARY KEY ([applicationStateId])
)
CREATE TABLE [dbo].[ApplicationTypes] (
    [applicationTypeId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max) NOT NULL,
    [abbreviation] [nvarchar](max) NOT NULL,
    CONSTRAINT [PK_dbo.ApplicationTypes] PRIMARY KEY ([applicationTypeId])
)
CREATE TABLE [dbo].[OrganizationStates] (
    [organizationStateId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max),
    CONSTRAINT [PK_dbo.OrganizationStates] PRIMARY KEY ([organizationStateId])
)
CREATE TABLE [dbo].[Mails] (
    [mailId] [bigint] NOT NULL IDENTITY,
    [title] [nvarchar](max) NOT NULL,
    [toAddress] [nvarchar](max) NOT NULL,
    [fromAddress] [nvarchar](max) NOT NULL,
    [message] [nvarchar](max) NOT NULL,
    [attachment] [varbinary](max),
    [createdDate] [datetime],
    [dateChanged] [datetime],
    CONSTRAINT [PK_dbo.Mails] PRIMARY KEY ([mailId])
)
CREATE TABLE [dbo].[Reports] (
    [ReportId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max) NOT NULL,
    CONSTRAINT [PK_dbo.Reports] PRIMARY KEY ([ReportId])
)
CREATE TABLE [dbo].[Roles] (
    [RoleId] [nvarchar](128) NOT NULL,
    [Name] [nvarchar](256) NOT NULL,
    CONSTRAINT [PK_dbo.Roles] PRIMARY KEY ([RoleId])
)
CREATE UNIQUE INDEX [RoleNameIndex] ON [dbo].[Roles]([Name])
CREATE TABLE [dbo].[SystemMessages] (
    [systemMessageId] [bigint] NOT NULL IDENTITY,
    [title] [nvarchar](max) NOT NULL,
    [message] [nvarchar](max) NOT NULL,
    [expiryDate] [datetime],
    [createdDate] [datetime],
    [dateChanged] [datetime],
    CONSTRAINT [PK_dbo.SystemMessages] PRIMARY KEY ([systemMessageId])
)
CREATE TABLE [dbo].[SystemParameters] (
    [systemParameterId] [int] NOT NULL IDENTITY,
    [name] [nvarchar](max) NOT NULL,
    [value] [nvarchar](max) NOT NULL,
    [UserId] [nvarchar](max),
    [createdDate] [datetime],
    [dateChanged] [datetime],
    CONSTRAINT [PK_dbo.SystemParameters] PRIMARY KEY ([systemParameterId])
)
CREATE TABLE [dbo].[UserResourceTypes] (
    [userResourceTypeId] [int] NOT NULL IDENTITY,
    [userTypeId] [int] NOT NULL,
    [userType] [nvarchar](max),
    [resourceTypeId] [int] NOT NULL,
    [resourceType] [nvarchar](max),
    [resourceSubTypeId] [int] NOT NULL,
    [resourceSubType] [nvarchar](max),
    [linkedResourceStateId] [int] NOT NULL,
    [state] [nvarchar](max),
    CONSTRAINT [PK_dbo.UserResourceTypes] PRIMARY KEY ([userResourceTypeId])
)
ALTER TABLE [dbo].[Addresses] ADD CONSTRAINT [FK_dbo.Addresses_dbo.Organizations_organizationId] FOREIGN KEY ([organizationId]) REFERENCES [dbo].[Organizations] ([organizationId])
ALTER TABLE [dbo].[Organizations] ADD CONSTRAINT [FK_dbo.Organizations_dbo.OrganizationTypes_organizationTypeId] FOREIGN KEY ([organizationTypeId]) REFERENCES [dbo].[OrganizationTypes] ([organizationTypeId])
ALTER TABLE [dbo].[Organizations] ADD CONSTRAINT [FK_dbo.Organizations_dbo.OrganizationStates_organizationStateId] FOREIGN KEY ([organizationStateId]) REFERENCES [dbo].[OrganizationStates] ([organizationStateId])
ALTER TABLE [dbo].[Applications] ADD CONSTRAINT [FK_dbo.Applications_dbo.Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserId])
ALTER TABLE [dbo].[Applications] ADD CONSTRAINT [FK_dbo.Applications_dbo.ResourceSubTypes_resourceSubTypeId] FOREIGN KEY ([resourceSubTypeId]) REFERENCES [dbo].[ResourceSubTypes] ([resourceSubTypeId])
ALTER TABLE [dbo].[Applications] ADD CONSTRAINT [FK_dbo.Applications_dbo.ResourceTypes_resourceTypeId] FOREIGN KEY ([resourceTypeId]) REFERENCES [dbo].[ResourceTypes] ([resourceTypeId])
ALTER TABLE [dbo].[Applications] ADD CONSTRAINT [FK_dbo.Applications_dbo.ApplicationStates_applicationStateId] FOREIGN KEY ([applicationStateId]) REFERENCES [dbo].[ApplicationStates] ([applicationStateId])
ALTER TABLE [dbo].[Applications] ADD CONSTRAINT [FK_dbo.Applications_dbo.ApplicationTypes_applicationTypeId] FOREIGN KEY ([applicationTypeId]) REFERENCES [dbo].[ApplicationTypes] ([applicationTypeId])
ALTER TABLE [dbo].[Applications] ADD CONSTRAINT [FK_dbo.Applications_dbo.Organizations_organizationId] FOREIGN KEY ([organizationId]) REFERENCES [dbo].[Organizations] ([organizationId])
ALTER TABLE [dbo].[ApplicationNotes] ADD CONSTRAINT [FK_dbo.ApplicationNotes_dbo.Applications_applicationId] FOREIGN KEY ([applicationId]) REFERENCES [dbo].[Applications] ([applicationId])
ALTER TABLE [dbo].[ApplicationResources] ADD CONSTRAINT [FK_dbo.ApplicationResources_dbo.Applications_Application_applicationId] FOREIGN KEY ([Application_applicationId]) REFERENCES [dbo].[Applications] ([applicationId])
ALTER TABLE [dbo].[ApplicationResources] ADD CONSTRAINT [FK_dbo.ApplicationResources_dbo.ApplicationResourceStates_ApplicationResourceState_applicationResourceStateId] FOREIGN KEY ([ApplicationResourceState_applicationResourceStateId]) REFERENCES [dbo].[ApplicationResourceStates] ([applicationResourceStateId])
ALTER TABLE [dbo].[ApplicationResources] ADD CONSTRAINT [FK_dbo.ApplicationResources_dbo.ApplicationResourceStates_ApplicationResourceState_applicationResourceStateId1] FOREIGN KEY ([ApplicationResourceState_applicationResourceStateId1]) REFERENCES [dbo].[ApplicationResourceStates] ([applicationResourceStateId])
ALTER TABLE [dbo].[ApplicationResources] ADD CONSTRAINT [FK_dbo.ApplicationResources_dbo.Resources_Resource_resourceId] FOREIGN KEY ([Resource_resourceId]) REFERENCES [dbo].[Resources] ([resourceId])
ALTER TABLE [dbo].[ApplicationResources] ADD CONSTRAINT [FK_dbo.ApplicationResources_dbo.Resources_Resource_resourceId1] FOREIGN KEY ([Resource_resourceId1]) REFERENCES [dbo].[Resources] ([resourceId])
ALTER TABLE [dbo].[ApplicationResources] ADD CONSTRAINT [FK_dbo.ApplicationResources_dbo.Applications_Application_applicationId1] FOREIGN KEY ([Application_applicationId1]) REFERENCES [dbo].[Applications] ([applicationId])
ALTER TABLE [dbo].[ApplicationResources] ADD CONSTRAINT [FK_dbo.ApplicationResources_dbo.Organizations_Organization_organizationId] FOREIGN KEY ([Organization_organizationId]) REFERENCES [dbo].[Organizations] ([organizationId])
ALTER TABLE [dbo].[Resources] ADD CONSTRAINT [FK_dbo.Resources_dbo.ResourceStates_ResourceState_resourceStateId] FOREIGN KEY ([ResourceState_resourceStateId]) REFERENCES [dbo].[ResourceStates] ([resourceStateId])
ALTER TABLE [dbo].[Resources] ADD CONSTRAINT [FK_dbo.Resources_dbo.ResourceStates_ResourceState_resourceStateId1] FOREIGN KEY ([ResourceState_resourceStateId1]) REFERENCES [dbo].[ResourceStates] ([resourceStateId])
ALTER TABLE [dbo].[ResourceLinks] ADD CONSTRAINT [FK_dbo.ResourceLinks_dbo.LinkedResourceStates_LinkedResourceState_linkedResourceStateId] FOREIGN KEY ([LinkedResourceState_linkedResourceStateId]) REFERENCES [dbo].[LinkedResourceStates] ([linkedResourceStateId])
ALTER TABLE [dbo].[ResourceLinks] ADD CONSTRAINT [FK_dbo.ResourceLinks_dbo.LinkedResourceStates_LinkedResourceState_linkedResourceStateId1] FOREIGN KEY ([LinkedResourceState_linkedResourceStateId1]) REFERENCES [dbo].[LinkedResourceStates] ([linkedResourceStateId])
ALTER TABLE [dbo].[ResourceLinks] ADD CONSTRAINT [FK_dbo.ResourceLinks_dbo.Resources_Resource_resourceId] FOREIGN KEY ([Resource_resourceId]) REFERENCES [dbo].[Resources] ([resourceId])
ALTER TABLE [dbo].[ResourceLinks] ADD CONSTRAINT [FK_dbo.ResourceLinks_dbo.Resources_Resource_resourceId1] FOREIGN KEY ([Resource_resourceId1]) REFERENCES [dbo].[Resources] ([resourceId])
ALTER TABLE [dbo].[NotificationDetails] ADD CONSTRAINT [FK_dbo.NotificationDetails_dbo.LinkedResourceStates_linkedResourceStateId] FOREIGN KEY ([linkedResourceStateId]) REFERENCES [dbo].[LinkedResourceStates] ([linkedResourceStateId])
ALTER TABLE [dbo].[NotificationDetails] ADD CONSTRAINT [FK_dbo.NotificationDetails_dbo.Notifications_notificationId] FOREIGN KEY ([notificationId]) REFERENCES [dbo].[Notifications] ([notificationId])
ALTER TABLE [dbo].[NotificationDetails] ADD CONSTRAINT [FK_dbo.NotificationDetails_dbo.UserTypes_userTypeId] FOREIGN KEY ([userTypeId]) REFERENCES [dbo].[UserTypes] ([userTypeId])
ALTER TABLE [dbo].[NotificationDetails] ADD CONSTRAINT [FK_dbo.NotificationDetails_dbo.ResourceSubTypes_resourceSubTypeId] FOREIGN KEY ([resourceSubTypeId]) REFERENCES [dbo].[ResourceSubTypes] ([resourceSubTypeId])
ALTER TABLE [dbo].[NotificationDetails] ADD CONSTRAINT [FK_dbo.NotificationDetails_dbo.ResourceTypes_resourceTypeId] FOREIGN KEY ([resourceTypeId]) REFERENCES [dbo].[ResourceTypes] ([resourceTypeId])
ALTER TABLE [dbo].[UserNotifications] ADD CONSTRAINT [FK_dbo.UserNotifications_dbo.Notifications_notificationId] FOREIGN KEY ([notificationId]) REFERENCES [dbo].[Notifications] ([notificationId])
ALTER TABLE [dbo].[UserNotifications] ADD CONSTRAINT [FK_dbo.UserNotifications_dbo.Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserId])
ALTER TABLE [dbo].[Users] ADD CONSTRAINT [FK_dbo.Users_dbo.Organizations_organizationId] FOREIGN KEY ([organizationId]) REFERENCES [dbo].[Organizations] ([organizationId])
ALTER TABLE [dbo].[Users] ADD CONSTRAINT [FK_dbo.Users_dbo.UserStates_userStateId] FOREIGN KEY ([userStateId]) REFERENCES [dbo].[UserStates] ([userStateId])
ALTER TABLE [dbo].[Users] ADD CONSTRAINT [FK_dbo.Users_dbo.UserTypes_userTypeId] FOREIGN KEY ([userTypeId]) REFERENCES [dbo].[UserTypes] ([userTypeId])
ALTER TABLE [dbo].[UserClaims] ADD CONSTRAINT [FK_dbo.UserClaims_dbo.Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserId])
ALTER TABLE [dbo].[UserLogins] ADD CONSTRAINT [FK_dbo.UserLogins_dbo.Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserId])
ALTER TABLE [dbo].[UserRoles] ADD CONSTRAINT [FK_dbo.UserRoles_dbo.Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserId])
ALTER TABLE [dbo].[UserRoles] ADD CONSTRAINT [FK_dbo.UserRoles_dbo.Roles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Roles] ([RoleId])
ALTER TABLE [dbo].[OrganizationUserTypes] ADD CONSTRAINT [FK_dbo.OrganizationUserTypes_dbo.OrganizationTypes_organizationTypeId] FOREIGN KEY ([organizationTypeId]) REFERENCES [dbo].[OrganizationTypes] ([organizationTypeId])
ALTER TABLE [dbo].[OrganizationUserTypes] ADD CONSTRAINT [FK_dbo.OrganizationUserTypes_dbo.UserTypes_userTypeId] FOREIGN KEY ([userTypeId]) REFERENCES [dbo].[UserTypes] ([userTypeId])
ALTER TABLE [dbo].[UserProfileDetails] ADD CONSTRAINT [FK_dbo.UserProfileDetails_dbo.Services_serviceId] FOREIGN KEY ([serviceId]) REFERENCES [dbo].[Services] ([serviceId])
ALTER TABLE [dbo].[UserProfileDetails] ADD CONSTRAINT [FK_dbo.UserProfileDetails_dbo.UserProfiles_userProfileId] FOREIGN KEY ([userProfileId]) REFERENCES [dbo].[UserProfiles] ([userProfileId])
ALTER TABLE [dbo].[UserProfileDetails] ADD CONSTRAINT [FK_dbo.UserProfileDetails_dbo.UserTypes_userTypeId] FOREIGN KEY ([userTypeId]) REFERENCES [dbo].[UserTypes] ([userTypeId])
ALTER TABLE [dbo].[ServiceProfiles] ADD CONSTRAINT [FK_dbo.ServiceProfiles_dbo.ProviderServices_providerServiceId] FOREIGN KEY ([providerServiceId]) REFERENCES [dbo].[ProviderServices] ([ProviderServiceId])
ALTER TABLE [dbo].[ServiceProfiles] ADD CONSTRAINT [FK_dbo.ServiceProfiles_dbo.ResourceTypes_resourceTypeId] FOREIGN KEY ([resourceTypeId]) REFERENCES [dbo].[ResourceTypes] ([resourceTypeId])
ALTER TABLE [dbo].[ServiceProfiles] ADD CONSTRAINT [FK_dbo.ServiceProfiles_dbo.ResourceSubTypes_resourceSubTypeId] FOREIGN KEY ([resourceSubTypeId]) REFERENCES [dbo].[ResourceSubTypes] ([resourceSubTypeId])
ALTER TABLE [dbo].[ServiceProfiles] ADD CONSTRAINT [FK_dbo.ServiceProfiles_dbo.Services_serviceId] FOREIGN KEY ([serviceId]) REFERENCES [dbo].[Services] ([serviceId])
ALTER TABLE [dbo].[NrufCompanyInfoes] ADD CONSTRAINT [FK_dbo.NrufCompanyInfoes_dbo.ProviderServices_ProviderServiceId] FOREIGN KEY ([ProviderServiceId]) REFERENCES [dbo].[ProviderServices] ([ProviderServiceId])
ALTER TABLE [dbo].[NrufFormF3a] ADD CONSTRAINT [FK_dbo.NrufFormF3a_dbo.NrufCompanyInfoes_NrufCompanyInfoId] FOREIGN KEY ([NrufCompanyInfoId]) REFERENCES [dbo].[NrufCompanyInfoes] ([NrufCompanyInfoId])
ALTER TABLE [dbo].[NrufFormF3b] ADD CONSTRAINT [FK_dbo.NrufFormF3b_dbo.NrufCompanyInfoes_NrufCompanyInfoId] FOREIGN KEY ([NrufCompanyInfoId]) REFERENCES [dbo].[NrufCompanyInfoes] ([NrufCompanyInfoId])
ALTER TABLE [dbo].[NrufFormU1] ADD CONSTRAINT [FK_dbo.NrufFormU1_dbo.NrufCompanyInfoes_NrufCompanyInfoId] FOREIGN KEY ([NrufCompanyInfoId]) REFERENCES [dbo].[NrufCompanyInfoes] ([NrufCompanyInfoId])
ALTER TABLE [dbo].[NrufFormU2] ADD CONSTRAINT [FK_dbo.NrufFormU2_dbo.NrufCompanyInfoes_NrufCompanyInfoId] FOREIGN KEY ([NrufCompanyInfoId]) REFERENCES [dbo].[NrufCompanyInfoes] ([NrufCompanyInfoId])
ALTER TABLE [dbo].[NrufFormU3] ADD CONSTRAINT [FK_dbo.NrufFormU3_dbo.NrufCompanyInfoes_NrufCompanyInfoId] FOREIGN KEY ([NrufCompanyInfoId]) REFERENCES [dbo].[NrufCompanyInfoes] ([NrufCompanyInfoId])
ALTER TABLE [dbo].[NrufFormU4] ADD CONSTRAINT [FK_dbo.NrufFormU4_dbo.NrufCompanyInfoes_NrufCompanyInfoId] FOREIGN KEY ([NrufCompanyInfoId]) REFERENCES [dbo].[NrufCompanyInfoes] ([NrufCompanyInfoId])
ALTER TABLE [dbo].[OrganizationServices] ADD CONSTRAINT [FK_dbo.OrganizationServices_dbo.Addresses_AddressId] FOREIGN KEY ([AddressId]) REFERENCES [dbo].[Addresses] ([AddressId])
ALTER TABLE [dbo].[OrganizationServices] ADD CONSTRAINT [FK_dbo.OrganizationServices_dbo.ProviderServices_ProviderServiceId] FOREIGN KEY ([ProviderServiceId]) REFERENCES [dbo].[ProviderServices] ([ProviderServiceId])
ALTER TABLE [dbo].[ResourceSubTypes] ADD CONSTRAINT [FK_dbo.ResourceSubTypes_dbo.ResourceTypes_resourceTypeId] FOREIGN KEY ([resourceTypeId]) REFERENCES [dbo].[ResourceTypes] ([resourceTypeId])
ALTER TABLE [dbo].[OrganizationResourceTypes] ADD CONSTRAINT [FK_dbo.OrganizationResourceTypes_dbo.LinkedResourceStates_linkedResourceStateId] FOREIGN KEY ([linkedResourceStateId]) REFERENCES [dbo].[LinkedResourceStates] ([linkedResourceStateId])
ALTER TABLE [dbo].[OrganizationResourceTypes] ADD CONSTRAINT [FK_dbo.OrganizationResourceTypes_dbo.Organizations_organizationId] FOREIGN KEY ([organizationId]) REFERENCES [dbo].[Organizations] ([organizationId])
ALTER TABLE [dbo].[OrganizationResourceTypes] ADD CONSTRAINT [FK_dbo.OrganizationResourceTypes_dbo.ResourceSubTypes_resourceSubTypeId] FOREIGN KEY ([resourceSubTypeId]) REFERENCES [dbo].[ResourceSubTypes] ([resourceSubTypeId])
ALTER TABLE [dbo].[OrganizationResourceTypes] ADD CONSTRAINT [FK_dbo.OrganizationResourceTypes_dbo.ResourceTypes_resourceTypeId] FOREIGN KEY ([resourceTypeId]) REFERENCES [dbo].[ResourceTypes] ([resourceTypeId])
CREATE TABLE [dbo].[__MigrationHistory] (
    [MigrationId] [nvarchar](150) NOT NULL,
    [ContextKey] [nvarchar](300) NOT NULL,
    [Model] [varbinary](max) NOT NULL,
    [ProductVersion] [nvarchar](32) NOT NULL,
    CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY ([MigrationId], [ContextKey])
)
INSERT [dbo].[__MigrationHistory]([MigrationId], [ContextKey], [Model], [ProductVersion])
VALUES (N'201605212340329_InitialCreation', N'jnas.Migrations.Configuration',  0x1F8B0800000000000400ED7DDB6E1C4992E5FB02FB0F049F660635A47851A15A9066A0A6C46EA1458A4D8A85AE2722323348C65466444E44A48AEAC57ED93EEC27ED2F6CDC33DCDDCCDDFC12B754A2816A31C3DCDCCCFCB8B9F9D5FEDFFFF9BF6FFFF365B53CF8E6C7491085EF0E4F8E5E1D1EF8E13C5A04E1D3BBC34DFAF8EFBF1CFEE77FFCCFFFF1F6E362F572F06B4D7796D36525C3E4DDE1739AAEDF1C1F27F3677FE52547AB601E4749F4981ECDA3D5B1B7888E4F5FBDFAD3F1C9C9B19FB138CC781D1CBCBDDD8469B0F28B3FB23F2FA270EEAFD38DB7BC8A16FE32A97ECFBEDC155C0FAEBD959FACBDB9FFEEF0BFC2AC9292ECF0E0FD32F03211EEFCE5E3E181178651EAA599806FEE13FF2E8DA3F0E96E9DFDE02DBF7E5FFB19DDA3B74CFC4AF0375B72AA0EAF4E731D8EB7056B56F34D92462B4D86276795518EF9E246A63D6C8C9699ED6366DEF47BAE7561BA7787EF178BD84F328BF175BDB958C6391D63D8A38AFCA783FCC79F9A76CFE091FFEFA7838BCD32DDC4FEBBD0DFA4B1B7FCE9E066335B06F3BFF9DFBF46BFFBE1BB70B35CB605CA44CABE313F643FDDC4D1DA8FD3EFB7FE232BE6A7C5E1C1315BFC982FDF94168B96FA7C0AD3B3D3C383EB4C146FB6F49BD66FE97E9746B1FF173FF4632FF517375E9AFA71D6789F167E613F4108AECA287EF2C2E09F052F75BD745E35A70CC259373C3CB8F25E3EFBE153FAFCEE30FBE7E1C165F0E22FEA5F2AEEF76190F5DAAC501A6F94952D83D03F71508BA68E79B5A79D2B372F5AAEE34AFE19AC2F32E2CEEB59F88F5ED6D79AEE5B56F7E7285AFA5EA86E806BEF5BF054608AE3FB8581DBADBF2CFE953C07EBD29B1EB5091E9AEA2FE368751B2D3906F5F787AF5EFCE4A799949184E82EDAC4734351EFFCF85B30F713A5C835A14A749E4EAA82400CA9F2F678EB7CA52E996D029A5F6E97E9DD39F3EE4ED743EBB94B776E3ACCFEDBBFB36B6B9BD7ED7280C8F974EE7A6EBC38B3EF45B45A7B61F70E95A9EDCBC575E715F652C765F775CC633FEF151FB2FFD475E5FFFE1AACD4834B4677F1EC854FFE42B7681B8D775937B5867792B61470672C745469C60557635F3D1648C7BE7A74214BB95E679E1B1FA55BDF1FD8F1642B2D46230C7528A1EE88DD6274EB275561A59DA14298CD455A85FD8102BA6DD1665833291DB142B53631DA4AA402D2E8042F65157195FD52156EE55424DD444A79C82592DB68436A2F723BE9B58F7DBBDC277E0C4A9F7F4025163E0A528A1456812DE3B488EB0DDB22BD87B5DEB66E93A8962B3EC5B587960A2E5605C895656DE06751DF7C80E8BC92224C3BD737F6E7C13AF07BA829EFC65B2800D59C9CFED2852D37855BEA58B9A49C7B5B8799259B1E5A7D3B18DBCA1C33C1464F82DF6D662E65AFD8750F93CD2C99C7C1AC074496239B1F5F4461EACDD3EBCDAA8F5A97D1DC5BDE469B3463DF539597C1D2BF96AFA19CBE7EEDA0A2DC9299A35400E5E4D52B17C3D06211E46390B7FC10CD37ABAC5A66E9FDCF41E8C5DF69C61CE3FCDC13273C0EA304179EA1C56E9A0B085BF9AFA3145992E6891E98D8189CA30B74B279BA48DCD55CBD5D8762AAAE201566EA2A7A8B45137CF6CA13115A46A093B58C486CD132E8B495A32128C193C97410686DA6DEEED7AF643802E7BB54B16FF9A005909CA3C12C2F21132C2FA3D5B5BC72818A592D52084F96DC5A6CF90A072226FF0D5EDF508965B2BC51397EED358EA2DC900B1D850096AB1D0D8FBE963CB4565AC8AC3A0F9F33F50758E0502E0674546F2F8B01419823C75B5EBB31ED280278177B40E631A6642CC503525BF7B90D1EB53D685D744827DA9E5859F851FAFCEC8777A5B1A3A96CDC006F377C871A5C539F62AB3C5F3359543B3F8854E6FF407A8B3929D324267A710CC84A32E57435660B9BCEA8C8EA2AD615043A923AD215059B01AC6A09E351AC283F86A1AC71130EC633A2CB19DD39BF0E57CED84E445F465396A3C01F2F6CD51D7483B8C122B776F0A00B6F7AE0E10ECE759D031CD477B97B994E70D732D9CC7A117B56ED32ED64145AB7C9E720FCDDD936F2C4C358E940210BBBA06559D2402025360DE3F22695ABC05202B2B70970A1192AE3657C34D016EA11426A98422D301E266B0FACA509F506D7BCCC60036CDDE14D07599AC37037D0AE8BAB07DB9ED6B5377E0E968BDE6A5B66C6F417B753F69F9F4515A4BDB970166021B15F63B4680F470B389D1233B5C9C60086402EB49B701FB42BCD3901457BF7514877D0755546BD6AB233DDEB280D1EABD8E1839F7AC11204AD48A6EA85B412C27E2EB198CD6905F30B1E0A9575CAD1AF7BB8505F19DC419560719E8A567055CA02565E0B4230CD6989257BF759A1208289CB82B9341EEBE7F36E3D56AB76DBF8A3CDABF3F829DFCF76B1A450F3D99F3C575536D193E73F50A0DDD940CF0F0A9AF181493CA354F0011A3B60CD005269EC02D19B8EDA3A6715E51AA9A99527171DEA453BC048D348571D57BADC37BE1F39D948D04142069E77D491DD288C3209A0060D9D6C83A6094EF034C79BAC2D175E2678B32361FFCE90C1D4D1D2E1CAC61059E7D1E9CCCA0184277A604BB0DD1A25043B364E6DD5B545B568DD9B2FD77B17DF700298747388475F5D7DA76FED4E76DAE72549F0146E373DDD7AC22E7D06EFFF681EC6CD050DD18970B7340461C0AB1A0295B567D3F166BD7B30138F3584C7182628297DF3341FD6E8F551CFC9AD54E515653F3D064B273257AC3A177B1D072B2FFE5E5DFBD71E20840729E69B3883E5DF377EE208268612BC0F933F9C84049AF50F733A69E92569D684A13F2FE6020695E7981BF3321F57CBC7553191426B397DFDB3AB5A32BB3E06F1CA207AE2B8DD6461D81F51BCF8AB973C776EA0BBAA1F646DBA5A775EDBCD7314FA3D3DE0D1AACB59D37CFD23BACCDC5F147F0CF352D6FC3E47F3DFA34DFA312C1CC17D3AD7ED8E0D0327E2BC9FCFFD24B9CCC0EC2F2EA2CDF6F526B32E5E44B4F2F089DAFF8C2720AADB345A57ACA15986D505998BA517ACE047340ADEF5774EA6F267589CEA9BAE249FA3A7209448527FE724297F8625A9BED9BC38E9E2DD3F5036BB9712323E1253559F39998A5F6161CA4F9D2DC9194C504131D169AC8EC4F8FE5AF3F5A19CBBB2326EBF80B3E7D6E76EF62920999A0FF85E042A1179225FCFE0724E45DF96CDEAAF9A5C19EF93F5B59F1ED5A58F4ABE9771C6338B2D7E3F12D8FE74402EBC5D2538A5AE129C9DCC1ECF7E79FDB3B738FBF9DC3F7BDDFF8AC1EEAE6916CDD7CBDCB5A8E9576FB9715D95516F28C617F7BDA1603BFEDE508899FDFC2D58E49EE9585DA226CED893E86B3CEBF6394EB2BEBB03A366DF95F7E3038CBA4B3E32B9EF2D39D7F1771618CA2069AE9009EA87F2FEB5BC23415C8D09F203514D01BB1D88833F7B895FD9200FDC44E41F2BE5D63AD7DF141864EFD7F80CBFC6FADDE80E7638D9AE234D31A0F90F300331EE2165B446075AFEEB2038ABB7564C6046DB96992CCA88877C1C9C7483D0A83A1567B2DA229D0943840FDB12F05D0F81507AB943A43699CBDBCCE3514BA3DD5E254DB53FA7000843055A15A702171F1052AB9508182AFA29DC0673681120846D4AB7FB011CDDE493AC8D7FD3BEC3BC3A2A9F8916D0CBB363E543BB1E05783F4B1B33ACBD96A9C71ADC5BB9F0543B1E8E69E576157A0B3DD9950CBA22752FE1984317A3A59D9B0E0A4449F4891153709019122381E9544960D257279DE869B4645A7991C61FEF747E10FCAE3635E0CBAA6F0F8027D8FA2F9448088B704A9350A881B47CEA26175E4A289BC0395242BABF6D35F98466CC8A79AAD128D1E087363654E4BD8F082DC7A43B0E907DDAE84234F3B7B37FBB76EE6164FD95257968302578198E0273313C99857F912C1059BA47BE87AA1DA94D076DCCAFD54FAB524375D7560464D86BC93194BBCE3BB11868FF4207C8AE878B1BE571883B3770E1D84D3012BDE135005C2E47F3C03BB7ADDF95530A038782BC8F7730705DE494CAF72F1CE9A25484A8858E0A2EE427CC70F4030F64F0C6E213A3915BE810B4A19B2BD6FBD87D233A36DDC11B60B163A137BE7B1D6F1E2FA2D5DA0BBF7F0A1F23F87D0A96E641004AEB7D0A39A9F8209082DEE615C38A077C421F22942946A1976ECCA0850CFD8B6C7EE36CD8E2DD0E719433723F0212892FECB0C57A773F5CFD26EE076031C416B26D48A83CF9E8E83E9FDB40B69BED295EE6E291EDAA8DFBADEDCBC575E715F652C765F7752C83D049C211752DA7BDD472D6FD4D90F21D00C5ED4EA7757DF597DB1731BAAEECD27BE9AF32D525754737BDB3916393745E8DFFB20EE2EF264F0B0C9D72341F8E2FA3787579E6A1E170F5FD41085AD850182103C3608C56373EDCF29A29C49FD1C417C824E28BB4A6E2DF9F48A5BF3FA1082F52A1B203A4C6A29FCA453F25892E50E1A28BA4C6A29FC9453F23892E50E1A28BA4C6A29FCB453F27892E50E1A28BA4BAA25396201D4EBE85C721899375E3E95CE345E953B9AA48EFD3B8AA5E93E95BAB685FD336ED19A382DDDAEB3C1EF8CDF7E2133B317316A7F62CCEEC599CDBB3786DC7E26B86A4A51D8B31C45994A547CB580BF27AAAB8CCD2E3CDF43DDE6C208F3733F778B3BDC7DB7BBCBDC7EBCCE3194FCF708F874FE5AC3C5E3E57D37378F72783F8BBFB1353775796DC7B3BBC8E9797CEEBF8879D196EFB7869F23D7750C54CD4AC949F3F5418B4443654DACF4F76D9CAF3FEA9B096158BC52A08ED58DC4471D69BBE6C2CDF23BC8E52BFFB05D0F7DFBCA090CA72572D0D967D3DA23CA5F1D17801101B1D256B857683E3A9F6E0783ACCE0786A3C389EEE07C7C107C7FDE8B61FDDF6A3DBAE8C6EA67B44E8E8866F27D98D6E67DAA3DBD930A3DB99F1E876B61FDD061FDD7E9CA95FF9547D72EBCFFDC07A64DB8F8FFBF17127C747D38308E8F8889F59B01B1FCFB5C7C7F361C6C773E3F1F17C3F3E0E3E3E4E688073309A9443C127CB01653F1220754D6A24303DD7858E04F81130A39100BE92A4FFF8585D76D007C86A216C1F216BF3E96BE478BF58C47E42A8B2D7FB27BD66495CF77C6B7B6847523539FD0660534071F3AFA293BE932610EB3E0247391EDAC93D4692564E4F8B0A17D869FE912BD6BB6B04DE84D0F58ADACF4AB873882E9FE208BD219E104A7A7A3CE3318B0BBCB47FFDCA7A6FFD27DFC50CA2B3E479FC0B10481E3D0999E07264B4BA7E94F8423B5FA5FCA17635B55227C7CFB62B1FCEC0881F04EF0B8F17B232D2BBEFD282DDBE0F22514D4B1DA72A10AEF03B7BAD4505429757F87134EA4F77DAE5079DF2DC72A3A4CDB487E7D5D7483FD9B4EEFBD7C240769D8BBF0CC2DFFD45E32B469CE81BCF642BAAA0372A820C082323504E3AA35216769E3917AD9DF2143A5A80AEA45DD65DC25B679D463A6435B198A29B48071F7C919007955B1AFBA0A5AC72CA98440B834608FCC0643AEDEF3B1218669EBE9F3F93E6CF94C93379E6DCF7B45963CEAC3B611E64B6DC917395EAA874C92E074927F364A93AB603A0C63C993849D699213B9F1E336904B4739B0C99D5C4329FC9EE0F700BFFD1CB9AA169DDB27EFB478AB55307DAE49FE03B022D5B85DD7E8F4EAA5DA6D0707B3DA6297705063BD6259483917CA1B398EA6E4981054E86025FD864C9AC60DA0A9FB490CA97EB1DAC1E2780095E211E3F0A6455113BDFC058D42EA313102C257685639D1936576C48149BCEB301163B86615EE1D92CF6BF05AE96E7FBE841B269AF844CD67F48935FB3237C3AE3805070D8C37BA62301C86462DDA893C5F3D24D5356CD454AF9292B91DC0AC1571AF934AF8648A1B932CC9A59976BD0F8F379A768CC689CE4B5D4F4EA69D49C4CEC7D0D35C3F36095AFB25ABDA72186D134F5E6CFAB0C14CD643E08BDF83BADEEE14FC56ACCC4D7519CD2A7E03975EFDEA1ACD6C43F6C4B4E6CBC92E295DCBAB5D0F908236BE3AB601E4749F4981EBD4FD6D77E7A54173C2A595EC619BB3FA2F8F7A336C79F0EC8E5B66039A582E5EC64F678F6CBEB9FBDC5D9CFE7FED96B13E09840469AC3E2E4F4972EDCCDB51C34A7AF7FEE365ACF97F7E063F2EDF67EA8C8B6218EF855086B0012BB0493DF93D45F5DD5030331BF64BB50FFE925DBB51B6597E419EC78B833D8B03FEEAC059A7DE4C6CBBD6FD6E07ABDA42936503F69EA37EF290C8B5D1AF8C55ABF79CBCD00D5F694EC6954BD2AD7D9E4A0125F6E909D5CDB63CC108FBE7AD6A64A1D6F7B04B5E6B33FB6ACAA6C7F6CB9CB63CB98C7799F24D1BC5CBA0756239BCBA9ACBC1FC3C581B88028DE502D15E05730335D32A713E40BE59940D9FC46B087827F735874CBBF592D6259FF9BC03AF3507E9EAA2EF09617D9A423F37941988AEE2C08E7C1DA5B12B4E4CA2A57C4454798374E5323FFE583BFF6C3DC67110CE24694A646CE5BAB2CF7F6B8852539C45ABB24C58318CC4E0C0605592108723CBD1A1BC4BA20F8B5779254E836822045224AFBB736272D9148690F2712F50BC8E614872626A1720A586E4F9E182113ACD10A9C7616130E1034FA6928833221DAB2DA2B65D57C7574241B64B4C4901B98D8A4EEA0C9EA6D881AE4E2505FA085AF1E19B4B3399C6562A156951582AC88994E014F69357DC31138120F5D5AC374A115878C071DF3A72390582F604DF04A5E2743BC9E8C94A115999B580CFA7AEDE754C61EC280B676D0BD120ABAA4974C6038EB4C885455010836E939D600969881828AB055DA216425167323550F20CD97F6509D50884A4B4100E50BE8785B79650A8876E45D4932F50C4D52A34C0A98A459134F88C14FC72F0A3CF526428E3C20A619A50D1D60C97451A0DE59190E36174B2F58E1CB8C2D1A6760A9D80138A9F70872B282AA4BB4B05AF5051456F96960E473F414840A8C9434CE3052B15360A4A0EA1223AC567D6184557E1A1861F619A44D0B9F8B96E34515F3C0A7A78DB741CC0103C9D1F346056AE4C9EC52141AE4CA287C4E41E2CCE594DC141EA73C5BD99DC36154EACBDF309A4FC3DD08932469D30AE1BE2BCC088C01F8E8CFE7CCE18329DA1792307B4C0254E50A5A810359A36FC9301811D72A51B608883A048EA813A5C536754907D011D57720414FC0292E206AAC5A4ACA60902A4FD4E8210AAB62C8954A82EAD476870FB769034F61257B697A00613BDA2BF42145E3D252AAB344BA809457A619B17770BA0835806ED4EC0096245BB9936B688036FECD083B4D1FEE13B04DA50AE062AEBB0F00F386191B90791B4E16D08D225A40164AA9004CC712B54224CEA4F5146BC0A2F2F41800901A6412210023BD3AF2944D5F8C63CD81262F8242434490263397410153A740BB3CF384A46A584B4BCA80472DB6E45A478624B54073173E235C376784D442519A1DC8686973C242DD1ECE84EA15923303480A65E4909C994152AC65049044851A1292687B4C1192F727DA88148BC800797F628247A08EC1E188CB341C1AF1B69824184FF5C128149182F1D4088C621DC3831195694030A26D3149308AD9D9954091A46A07C0786604463CC7FB706044651A108C685B4C128C628268255024D9A201309E1B81114F333D1C185199060423DA165303635B851B2E43AF0C2D9272182C2590516153561B00505E91EE004A108C82881B317DB7254A09EDE34CB09E97B1EFF8BCD8944565BE906A11BBA6375DC416EAA33F49D0C11236260D0502AD0CF58E16B0B1B6B096662820521DA75E9EF38E81391A3FAA235DFFCE54A7CDA6E4510515D834451894E4C520F06A83885C1980584E8B6EF6726836E81FAA347351E45A8F04A47CAE2ECADD314919D995FBE635283A3C65350D72B98CA03AA5F581F7B62C5049B09233A10680A4C6714A75D18E013AB23396747B0C8F5ABB83976301AF565A694A34A962A28A66D9972BCD225AA50C83BED96122290552EE5FEE306957A7920ED901B40F22A3A57B87FC08AE146AC946C14C47EF212A1BCD8D6C4302990F17B4E18426D3ED09CE587E5E9B90C82DA81109FB0F5274DB704AA10AA38FCEBC4F67D2671A508F6BBAE762AEE730647631CB1B0FF40CE67706933B2B208E745AE7704EE71A9D6E6673E301293A2093F0830E235D0116AD5011BDEAC51AF6E05519664808AB6C384D2053A357DD88D5096CED23D32E40EA20D6738D4D0701F2782049DC189394E90C9023D90F23A83E241ACDB7C14605C6A67FEBE1112ED6F146C3C88029B7C1F09B0BF6081D7AB6CE5B56711006268740A98B13651D38163B5A4692CB4169E4C4C9E900B9CDADE5E80F64C515D64A0DC58C1C2D21819A8EDFC3F923B78B59B1BBF17A4AA57BC71C6E9F29C0AE25BC06F4A4A5B01BED889B535D6AC7EB191086240350DA7FB32DECE012BCD2564E44EA17969502CA673DF0220A40EAA7D39054D5FF6B1E6A61A88DEE20F8533782BD303DC00F3A1450FF957F4341A82A084191705C4391224759AB6431A750C7597E1CA6569D7337AA82B2391D4105BDBA9C9CA931B55AFD07CD54F51F9DD9A7A94082A10E1251917A1B48DD45EAA9613B13DBA1D58B220CB97431C4C0D720B568A0C3DE200F7A3EE581EC494C7AC983D27774E435A07C684A90C80AF594FAAE5F8FD2DEAB374CCE68908FD13C05E328D2DCB5980BA9202916130A29CCA5FD56B3B4AE41CEAB50B4A7C4BE1EC7C75D9A5AD450EEC4EA37572DF9F494A48C0298BA7B05B29A8686A5CD492A8F65E30E9436E7A94842F50B49DA516BAC80E6A0421C78C770865A250EA5B91D1E9B5635801B717A3E29AD15DEA80AAA4E466B0638CAEAFA8E70989BCFC5C0A87D49422CA6BCDEAE1BE628AA1BFEC176DC06BA5DC8F5251FDC5C0E25EBA187D7798F720D8BC554FC4910911402649B4A078B00F7E1D234E1AA525AD71266B8252895E7A57A42D3C742CCAC4C9A95F063315CF930CB3FFA2F591BCE37491AADBC308CD2E2D39B4CB18B65B10C9FBC3B4CE38D3896E4CCEFFC947DBCE4F0A0FCD076E4F517015B2C07D677096CD8CF0A5E4CD8244AD4FE4AE7741DA53EA8A04042E7B91DDA646CB754FA9CAB5187C2BE2255D42111992C27BB628AF2293F2B78816BF3024B904AC119BA562030868834F82A382A798999CD047E2209812704F4F20D794561217F2CC4094832ABC1B6CA852A675BE525D5609BBB6615D7725023D80FC361EB1B814BB9000132293F69F854093F984C8337812F8927B02F0F2ACFD128B836A79F045ECD171A87E60C0BC6A82150F0131E0E12180A142A9FC2BF4228BA159E82C0B1C91C00726BBE9239CDA49C66644EF94BDD28A37B288E45F89CCAF89CD2F99CC9F89CD1F99CCBF89C6BF5C9ED6B6AD27EB925238ED9CD196A74D86E2834A4652F0D4825664989522B44D6F54E72BF44B725324AE8C529E2568A2CDAD3E689984EA0D0C126222640A3E07A050F12579471E1D65F47710A9ABFFCA02AAF081848C1C2DDF724F55757D90CC97B024716F63B89DB8D17677FA579B486F06B511040AFE8442209C7B3354185F1D0BC777AD022053001BF8B2A596BE45E446D94E3A79BC2745CC1B05EFB6831C4E6B9C7ACF604CBF0134A66970BB090941E574C560CB2986A9E4B670E594F3227B7B260B3C64C37225884A42A5452614A6CCEAC55C51006153694B79A90AC8B97D7B203CA866877709831AF54DE12E80289738873EA69E35D72C64A0799F0D9ABEE7A027C1ACB0001062D2293076A01293D6E015931C8C224B34A99F68669E0ED0A70DD4EB425B1246E001A03C8BEEA0540938A009B539630ED6C0EBD08A330B6F211195479D933328879C98695BD1BA3D5740606E5975B1F580D4473CA0BE00A4BCB41A6542D046B705718D3911955319B40235747159555EBDE0AA3F41B781535D6EBE88805AACF0AB14B2A0BBD2B0680CAD09ABF1BCDEBA57E44F3EAB342F092CA42F38A8142F35A56379AB3F353447FFCD48AA804785445DF16E01114E2BCDAD416D5420B6284F2AB42EC82C842EDB2BC0201959C6E9416BD35A2BF40A85045F0ECE65611580106526E3F9AD9A70C93CA2D49D8302D0AB91A5B42CC14EA4922C707B1832B7014E76669C19A8C5CAE0E520AB311B4F247E6D95770C6EF482ADDACBC006DD55028A75A8F549A52CEBD7BAF2CAD7F0B064D73C297BCE98A0B37BE3B31AF70D91BA905DD48B73377533DD5CC62019AE242399559494E405E03E2361D9991A94FEA2B0983464367EF0FBB1E312429EAC1E9BD845C32FDC64B81D37AFCFC00992D3468280E39D8184F48A62E379E484ED112CDD80E1B4F38324166DBABF180D4ED12DBA912BD833A4A52BD039613CF885099F66B3731CBB8CC6E8A9CE4B08A785672C86EC299182AD37EED2626C496D94D913E1B56114FA00DD94D38034465DAAFDDC4DCCD32BB29323DC32AE2B99E21BB09679EA84C7BB39B2CCD30623C72666241594A6E62BAC65AEC0183AA0E29DA45BAF5C934EAE110819E1685F2C554612E76608E5E41A7874748595CA996A483532BFFAB73CB0E8255459A51C0C63A894919A589A949E9EAEA70078CA938EE6CBE770E65C3946C9D2B9367829BDCB2F499C0C639729C95CCBAF33D22BE72DAC220A1145D5FDA32A18D4547B268D83EE7483D9C609E9A10F57C2A362A572B3BAE695E696F871668B9EE749AC26059172DDFBDF1075CEE65E4103AB486C5F9B20666E07D44E776E72B044CAFBA0461EEDE7506468351516748247BEFE106438391D07C18341803F54C38E0E8873A1E5613855969699C7003281339599B5899B9C9C0ABD89A9BE05F0D7DAAAE1F35B3E9F0FE523D29939113D5544FC7CCCC37828918DD7E94242ED2089F6E4593F9C300B6E415C2175F104A5C3BB8006433B95614A6B8A9DCD908C83E809B4995AA00524A92A840AD179121B2852BBFAC6EB68D8D276D40B6B589591E840D6A759E07585BE5CEB73AB1437F66E4930CC86D284D4980E9892525D05192CABBDB5314CAD7F101EBE9BDA8CFE8497E535F6FD141AF0E49DC02BE51633EEEA24FE74B465EDA73FBE0E8A87C709FA8B11EF3DED68918492877B1246FF0E32A526E5FE9194D76F5CAE17DABA61A55FF8509D5CA287BAADEE5B481FA24F702BF2C0696BCD50F47A7F06BFD50CCABF46208CF9E21A4F456B2E7FBE5CDAEF44C7A28427D916B2F247DAA9F7835978029D2E3FE8EAFDEF68432D533FE72239A5F3237B8574E35E1686E8F8B4FFCCBADA9480980E98B27058095D5BA978F6701E8E34504CAC23CF9E17A4C45CAF2BCE2491E32EB3E8DA7DCB2A3BDAF8E7633D5C69C49D71D70FB8DEAFEF49E064777BFA80ED0F0359D215DA0E20D70D51129C5ABE1F89925FCDD70EC5894D213121F0AEF0EA1C033D580FD548F59333A499EB36EC718D08D4B151FC7B738DF1E978C9A97969B6F6F8FEFE6CFFECAAB7E787B9C91CCFD75BAF19657D1C25F26F5872B6FBD0EC2A7645BB2FAE5E06EEDCD332D2EFEFDEEF0E065B50C937787CF69BA7E737C9C14AC93A355308FA3247A4C8FE6D1EAD85B44C7A7AF5EFDE9F8E4E47855F2389E33767ECB49DBD49446B1F7E4735FB3AA33492F8338493F78A937F392AC012E162B810C7C579A356163EFBA4AF6E968B10D73F27C70AAE9F37F9765FE2BCC342F6D788494DF1AF132D36B95B56FA1A20F9CBA144A6665EFE6DED28BEB07BC5951F307BC2FA2E566150A3FF348C479F11924DA0C55D925685C719E3A1C9759FB9EB0ACAA9FF4789C8A3C80CB04388FFC05799645F90B9DC33F83750E669649F3239DCFC27FF436CBB439B3DB66C77F13B9BE3DE660C9E3FE58003EE789F87E44EA658AA18AD6D5644C08FD4D5EBCDF8E1266FF657995BF9875B5DC5C32D9EAEFE6DCE5BC7538DF78793A81EAA83FCB96FB64C8F3CBC5B5846DF195CE5960A65BFE922F7FA9557E1EFB5940B5F850C4608CF7697FD0701E19FDC5B3173EF91C5C980F66386972956050419399E0FC9354D03C8103D2C15C9B7C2E4D0C22701E944042561AB3ACB72DC4B719F769E8A0A225CE092AA85640E0B5A774457E93B98F726E5168D710A620DB5078DD57C62BF6E7C13AF0795EAD9FE9BCF21906DF32F56F742E9BE2067B9B47F98B46BF2E8F31F0A2B47ED6E60572D2B3F3F6F0132F18FFCD8C2BCED3846375B60813B5F5D998B794B3561B6D66C93C0E663C6EDABF6B8C8A45BE203F2E2680F3F47AB3121823241A938628FBF536DAA499E7852A80BED3B95F06CB621860796E7FA5732A26BB612AB616F341C3732D1641EEF6BCE58768BEC9C71B60368712ED4E64E389CB7E9271B24D625407D49381CF46BCC1C00CFABEAB7159F960B883E00C64A417A1212C08CD5894943463F3DD0824CE8240AFBDA58170D49A2A473CD2CA5FA617F40499438E339F792D68C47E999E131DBE8B4B7696B47B39C64BAFA3E35C4632E48CB6C7C788F2B191C671B36907F19A626FD3C08F6AE817E8763504501DFB3276122043334F81B01A4773D357AC076A6CEB11C0C2EDEBFBFA6E7CDC09CC4C6B71AC9B659054989FA6235AF648363351C0E6473A9F59107A31B7AB51FFB63BE34C6DEAFCA033D60CF53783F6857C98F071A7C629C5B9763DF70531D1706170F17EA1B02E76EC6EC1C08DFFA6D1AD9E83E50266CA7DD23A5AC05F1CE1CD8090EC148069D77F683826F022C099C465C8561D7D3C457A8A82D6A46A568416A53091AC217165F9F68429B456A99AF232DEE65C719EBAAB5850F8D4FE5D9F1BCC6BF8A873BFF9B61FA44087E6C495593A3133F7E5D0B99047208CC3CA0BB32944C4CF785A3F8FA6F109696A68005031228040CD42E66FAF256080BE4F6F9F642A63A99724C153C84FAAB7BF8E0AFCE6FB9F503A0A22CAE9C8B6C79DBD3B2B7B0FD4A3863F04E7FE64FD5823C1CDF6691148B4D627239E2847AD058A385879F1F7EA7011B740C17DD339C836DFC459F7F9FBC64FC47616BFEA737E1F267F0847B0B86FBBB34EB9F492346B86D09F17B33A414CE8BB1EA6C0589AF9D04D048DF1F8B82A1EEE69F3A87ED2E49199E53188577C43F0DF748EE827C91F51BCF8AB973CF3C7F3DB5FE81CEF2ADC66A65EAD5996DC270D299FA3D0874EFB311F8CF821168529E8357CFD23BACCDC4C147F0CBDD992E72E7EA573FE1CCD7F8F36E9C7B0E8C7F7E99C650D7C36E00DC8CC7FA3737D3F9FFB49729941D45F5C441BFEC034F0592F36BE16228CEDAFA389F5806CB8A6819FC0CA200A24F0D009090B1643CC6A8A8A8143B6DB9F3579FDEA2D3710B3EAF751E2094D78AC8FA78295259E101EB8D3C9C86FAA6C13BCCF613E6938F8AACCDF7CFE2E5BFB4357601D0124B0F4C738223E25F9BFBF3CFE0B068D9CE5BF5A4223E74147861B1791D7C973A97F73DF769A1378B09D76CEA819972C8A2AEF261C149EB4ACB37A54A1CDE443905F3C590561BE52397003A98DFC03B493A041BFCD65B1B466B7DF8D7220AEB169EE6DBB9EB6D297DD066C21E4A55C7A03410C88ED0317ED67556EF48D434CC0ADFF3E8445A3D1D850166AEF91C6C468CC9683A7F4FAC3D08BCE2380B933883B80B739B4DD836EF4BE8AF2783D7D449172220E2D0A1E84DD10F8301448B05B3B38637C806068E738F97DEE3B347309AD5F22E509BD112DD907FEE89E136DE1ED15A1DFB8078BB84F636BEBBAEB5B3639C286DEF228030500103F297E9DB66BDB1FEFECEE78E79A4DA0CCCB0C7C36E62DE5BC9343CA8D320739CDCF28F8101C8D92836A8F02C107F0798782F6EB78F3583DFDF7297C8CCCCFFDCAF910DA4FC901B33157906F3FE0F3D087D5DC2C298F03B7A874FB772CF5AE208CE219DFBCC099C8E34C6BFBBE7A524C8010F3419BDF577FC91F3E65BF6873BCF45E108ED5176D8EC0F932F68BC6F1ADD44B37DC9E5CFD1B9D8BFFB20EE2EFE2D1BEF6EFD33BD038E0587919C5ABCB33CF6A9C447810C748B43466E5AA003F3EB47EA6B7589763EDF5DAE3B8E53FD0CBFFE67B31E743AB9FF4789C8A3CB47C685EE04CE4A1E543F302E7228F735D1EAF451EAFB58E4F46A9C739B4EAA7BDD7D0F61A33075E43E0A1E53580D272AF3183BDC66CEF35F65E03E7B1F71AE27743AF717F62ED3444161A3E032A2C7319F72790C7287FDD0D8771FDF2C295CF7FA097FF075BFA1F5AA7C884DE70ABD90DDE830B8BEF250B8B18A74FF99B9C2B7F110832B15F34B4F3F395735EB6EDAF1A5A3E65D8E6542C7FD2E0B15805DC15B3EA278D058B28CE5CD5970D77D3A2F5B306EE8A877159E4953F69E8F42D9B85E6F74638BDB63F6B2C62A5C112BC70C97CD83B7C5D877F6AEFF005163A0E1F282C75F8A7A0C33FDD3BFCBDD3DE3BEDBDD3FE319CF699BDD31658E8386DA0B0D4699F814EFB6CEFB4772D4A2FAF4E27B7FEDC0F042F2D7CDCBBFDBDDBA7F0DBBBFDD2ED9EDBBB7D81858EDB070A4BDDFE39E8F6CFF76E7F8CAEDB85632CBDD7A710F2699FB4BAFCDEA5EDB44B6392D4972786CC1F32A03023B8391A1BAC0122A0B4EC08579B4627F2709773BCCB035DEEDF47EBEAE8EB0FDE116FB983CEA67D50C187D0FD941C308BF77148DCED41F9D0B33DEE984087D613FDC3EA8F5940E6715390FA375D2EB7FE93FF02B1AA3E8C06F36D477FDBBE99E062009231D41C84E4AC28AEF756025F19DDD02796DD0F1FFB1B2FFB07CDB7DF3B1D4A9D8CA39683A8D9083AB651EE871C9D5A37C95DDC68B7BBCB6E748BDDD92D737B042DFC476FB34C6FA0ABEAFCB7D120C04DE21EDB943D86C97ABACA1B46C7C2F0D92D9D65B57490CDD23C8B650779CAA7D48856C3B8828F5E13EA0DE61E5B50D280430CE9DE6C16FBDF02283B31F3653488605605ADFAB59293EE1AA55ECF8EF8A2D2D5C91DECDB57366F04418509ED0517C34CB8021EFF5969BFF79355CE873AD54F1A3CA26A4999E3B3FD5923FE8EA315C88DF940E7B7CAE8BD274EC3E6470D5794A6DEFC396F36CE11B57EDFAF239363D67514A716C12A549C14A5C205D11DD7825E781AB5F97587FC9DFE5BD0D023B8960FE0EA3D7EABF5642DBC6F2CB4CAF5B85AE5EE7B92FAABABCA5D99B68E940BA17914E5D155B87631E14523FE63BF4396AB61617F87DD39DA6FBC38FB2B15139069E21DE54346BC84831CF34D4118F5CCE73EE753DFC4BC15DFB09415180F37EFC3FCE0482FDE4977B103A062445CC035DB09D87025A1A55CF3CDC2B1BEA8B9DF1CDC6F0E6EBFDBB987F74912CD83EA8817BEBA586BFBD0FAED50BD98081793AC1DD6050077B4E0CF96C9AB79F8EAC54F3E3447532D430AA7D55A4C3932BEBD726B3772DAAB7007DA82A042CD4DB29ADA26A1ABF1F618048C15A68A1EF40081800E30098FCED086D669DF6EA0DF41459031708F505C6DE31ED7195C0FDA99623E25D79BE5F2DDE1A3B74CF833D6F6A6EDA26740562E97D1CD5C2FC7A3173FCCDAD31A22AE7A06F0828D5BE7CD0AB2839E5CA634864F7919377894D5A1D90E3162FC865D6CEA9534C59EAE672598CA1A899FC580FCA1FE2BFF86A1515D0E3B64527C55B7A9AA024D3812E626508DC4F98A265E95BAE961366E15C5268AF537F72835369B332F5A980C1043E549F17296D85555A0D9BE2EB16B3F762B95331B27DC82D72DB6A8433347EC124586C3D910A3302BEF4EA181343422C48ED06033086AA0C19D9F98EEC846318B3368B151800A5B3C35062E0D60B12C0D91050E4E6C0D02A9239C71F2BBF193C6EE510B6024C3B87762A42089A776803496A5614BE923AD03976613F0B8815A274B672A2F242FD3DDD258F74150374B5B864E75124B59ED3D17B37D889EB61EDCEE369037BA9C226AC716B8E846B38669FB34BD0E4ED5E5DC0055558F26526577B599AA5417B735B1AA5463BA60D5309B1AADF5D18222874F10FA314FD29C5DA87E69FE4EEA1F72DC794F7E89D36DB9BBF9B3BFF20AFB256B6FEEE7798216FE651027E9072FF5665EE29724870737D5532AF589B9A39CE0E8EEBF9717CBA038125F135C65BA3EFA49FA35FADD0FDF1D9EBE7AF5CBE1C1FB65E0257916CEE5E3E1C1CB6A19266FE69B248D565E184669A1FABBC3E7345DBF393E4E8A1A93A355308FA3247A4C8FE6D1EAD85B44C7A7AF4ECE8E4F4E8EFDC5EA982F5EB1257179F5A79A4B922C987B18ADD358B543AD2E22B06DF6375FC0458D975BFF912D0A35395FFE2DEFC6B7457359DE1D06B9890BEFF1173F43407E70EEC64B533F0EB767AA0F0F723CE62F5835983C96D6C203B355156FDE379FC285FFF2EEF07F1545DF1C7CFA0707EB9F0EBEC459EBBF397875F0BFAD04A9C508BF79F1FCD98BFF65E5BDFC6B9B631A8BC718798655D6320527926C55F2320742CD8B3672C0E89FC13AEFA74E7855B79D9BFB3625CB59902A0CD53E0F25ED406D5768D28B54DE53D595242077DD9F426FA56E14ED0E519FD933EF9D2507673DB43CEAE7007B5C0646D71C8BE4890E983AE373E9860F7370BAE4979F7D4E831C7D9ABDBF7D66DA8E15784DD41CB3150B2BD02669CB441A2627FBB6F6694B03D7263DE4A8F66C5CF11F215068A9AC1EDE29A86566048F7EE63DE68E5C78C539B7910339637F1EACCB40DB01B7FA1607CB8AD4986551B611AFBC97CF7EF8943EBF3B3C39FD45DB52F9D17C276A25DB27295B08D5751AF55B914E5AAD9C649A0DE16C69AB7ED366E554B3D69D0233E51A064EF46B2E22B840D36696CCE360E6089CA54DFCB84E3F5C3C7AEF66B213CDBDE56DB449B319BF43B697C1B258E312DD44ABBF9FBE7EADCB373740E6C8A076625DC9AB57DADE7C512ECD78CB0FD17C93DFE260A694592DB320F4E2EF3B1476214B5AE6FEAFC5D0AC670B0C5C8DF88651A5C861324165F9B4B95D6459F0B00C2F1B1E1DC6987834ABD7C6EEF0E6C48B66423B0A28E1A0CD8C97B3902BC8F37565FEF69AA2E654BCAC495785B76FB47B2BBE2DA1D56191A1A0BF3E3B70AF8B9D0C8531B3DFB703C02661061A667B1A23818D10FC4EA8DE088132B21A2D8CEEB3190B4EAD025349D3E0D4EA4E7A50E9C4AA99C013CC7A32032C6CCC0C1F27B216C9CE4CB223168E7A1B2A2069015DB6ABAE27A084155D429B380178A5D13858400F811A440C90971A649FCCC2DC36B1187EB2526D4EC4BBB8365F5D8DA30D6EC922A526A7B49FD545DD606433732658B97CB4C3F1207F51C23E781E5538A938446F36FE82CC5C0407D8C9748762EA040CDAFE57BCADA4E783E1DB3A743F0C80D8B52F5E17070ED883A1B68EE139582E9C7244EE848EA1436A5C08D6433D99B14D47D5B949DB91F8539A18998AD4E9CCC822CE84AE831BB83BE2057895D753779BA9C5F1D7511A3C56F3930F7E2ABC994E336F287031B12ECCA53E2DF9D48D7D5B75EA774CB6B4559F6CB372E2F6DB2F3BEA69B52D69A5D1F639C8FD318F1FFB98879368432BB218C73673DBB9DABA555B873A86718AC429434136B38EB64B000E0FCAE7FBBBB68DB2E17898340CC4A3C3C6D9D1B388BB3B767B49123C85FEA2A32E60027BF8557235D431EC5982C89DBF29FBE28F71B6DBF925B0DD0A32B96C99E68B372D464E045BC7C1CA8BBF57E75AE95E018879FCF926CE30FCF78D9F9050A0C5F57D98FC4170EE249EE35CBC5F7A499A3543E8CF8B59AA03E172AC1886C4ADA28304C222A78F79CE34C569E69F8D9866467F0CE295CE982872BAC906D63FA278F1572F7976A2EF5D05FCAC15566B37D7EB9EA3D07778C4BCC5CF8909BFFE115D663E288A3F8679292B5E9FA3F9EFD126FD1816BDFC3E9DDBF6A586A1B568EFE7733F492E33E0F98B8B68B3BD1464321C14D30E20605177F2BA64F1F34F079F92FB30F8EF4DF6E16B660D2E16E77A964D9058473A79F5174B2F5899468C4561D3B0B129FCC34C8D0A8D9D853305B75FCB9444DD2D72B4B1F2397A0A8C26D545C19BEA790B5E2FB0444D9CB127D19BCE5F38C91C4F63182D1CF31E12DA46E8111316DA4C4D415238ABA1F92C761837516B61204D59D4A9341F82FC0EDF2A08F3553B4B146B2D6318EF063221B7C9DADD2EECFCDDD733674303D6D37E13FB014B0653335FFB74AB8D2923808FED1B2CF7FD98F8C7784165CA4B5C46687681641728DE012771BF5D07343F5CB1E199987A5D81498796C59752891DA7296CDD775C2EC4C2CF5F10546A0ADA2DD4397C3863CA7EADD71DB3BBCAE8065DB70517DD0E0B236DBCDBF6AD7B2EBF5D77D70A7567366F8C9657326C13D8AFB96E9A1D7235FBC35B133EBCB5AE96A8EE4CF12830B05291E336F6C1E38613D7C06FDD880DA0EBB80016231C54E8E7EAE2CD63F500E5A7F03132312AC7C2C4A8008B61CE9038591736DB5CB4F50D0203BB735866D339689F78FF6A6A674F425339B979113AE774E6663BAD7A55CE15C82A7E5FFDE5F6F8990B8697DE8B5B86E0A10EB3E3129923D8244E58F92FEB20FEEEE200CE180E1A690D7D9751BCBA3CF34C86BDAAA8C970D72ADAE130271F5409238AC0C06A44B95E7B4ED0FA9BEFC527368376CEE0D496C1992D83735B06AF6D187CCD5A7D69C360A23D7D66DED367E63D7DB6EFE9FB9EBEEFE93DF6F4FB13D38E7E7F62DACFCB92FB6EAE8B8AEB9717277CFE6183F25B674F00720B6026C27CCA9F295DF98BA0259491527EBEF46C27CBFBA7207CB262B05805A10D839B28CEBACF978DD5B9E1F26D62270DFC2D9BC2E5E4560B4769B0747979689A4EFAD4D8499F1A3BE9D3BD931ED249EFFD2CA6CFDECFEEFD6C477EF6CCD8CF9E19FBD9B3BD9FDD07C32E9C7479E12EB9F5E77E60E963F77E7AEFA7C7ECA7CF8DFDF4B9B19F3EDFFBE97D3CECCAB795AEE993957BDB7BA6F178A6F65D86EA5C895182A708E0637BA7A1CDA743FF05E736A72415A80B5AF9AB911D0B72FEEACDBA83338893EA62B7DC395383DE059C9CD5ED58F2C3B75DA50400AAEAF71073483980447DB6BF683E17F87DCCE2322F752357C9EBD67FF2D5F18ACD39D2F64871DB3E1A6E395ADC7250B11931785E1D227C779F291B4DDFDD5F40D8BF1EAB3BC8DA8EB0B6C36B0F9EC7DD9036C561A8754BD8F27EB0E5CDE0C9B4F2C27FF436CBF486BDD9EBF0DA8E75C20121EF8D71703B96A7466CCCD94ACEE622299B8B646CBB6657D35102C8456E61D5498D15DE6C16FBDF025A98E96A9E618CFF886762BD1635961E6011235D19BEA9B1327C4663D54B5292AC10E1BD0A1AABA85AD2731412C5D1CA29C355C6C97B72D5A1D3D49B3FAFFCED6B9D19BB329FE08FB026B78EE2D4A43394254DBAC3B6E4D03EC4C51B84A6EF0F9A3E2A883DE367F91EDF35602FF51C391726FF57CF2FCDDE7D4F527F7555790103E3276D06468F7AF00C26E3DB9DFACE1FF43E6589BF1B2FCEFE4ACD3263242C0B730C322C46E850454EDF488F0BF7FD2AC1A400583C7C6BB9D2B7E178982E03F5B8D780BC3E66C8A5EB4D8271AFF24F628D7EE0D5F5F74912CDCBA93E3029AFCF3D709DEF63B828E22296B896EACE5F3E1EB11FAE36CB34C817433209B2E04D5072CB909B4415BC9ADF5836FF26B0C97ABE9F3F271278795A8C248DBDCCC4A29B08C279B0F696900A1CB172C1017C5EFBB8A982FFF2C15FFB61EE1A386DDD54DB70E75C9CCA2C6F8F5B289083A3B5B0561CED7A68FD8082A44DC3B46BFB770D887032603CAB8F9D8006551B69BCD652A02D6478059D54DF2F749A1CC9C3A2874F74CEB3DD7EDF5D0C353A522468957B1829A4CA74E54A05E580B86B0FB2182A2A22161AAF8E8E26073EC99E9E1A898AD4E17DC39285819E9CC3FB40D9EE6A4780DDA3750268151AA927B8CA308AC213840A868FC9F84BAD26AE67920321AC6936B91C3D00A89DCBB97C5EFFE1B3382346A104D1B69B1FFC4EF76FA2740C77E87327D8529A0469676471C1027180CA4E05E919720F2A7D1030A0309822BAF0BCEE486BF2E9C3FBC7935A821E8074CFE5A27FC00DD9258C783118A6E2C79D8390A0E2A400445AE12852CFF3CD6A18A85397481C21A39094D222682E42A370C8B2CABE1ABF48348A2F9E5BB7BB988EB6CD0DF83A790C2812F08E160945B6D49E9050269BC590507DDD292400E975478B047CBBA9CB6D3512C07660438D8CC051ECA61580C805EFC9331407F930C7507EDC29BF201E5CD4AEB72F14C8C35C978018C384A24F40184D20060744B9242CDA896B48714DACF5EB388707244335D2109294D4DD0C0C8A0A7B6AFEBC297456AF9A4CCB7C23963F4E6FD50A4E1D2D6933F87C5F1FAB55F2DA7B000C9FA4583FC614A0237EA443C82878ED20F624C3479DA0590B4656F1EF7881842473EF1950A0A383097E088069F9C95102AD19EE94007336C88D0950430C74C6001A7CA863C0D23D50FA8E8BFB068256543C68C3B7F2BBF18F88E27B711C1D13C8F2DF3442E456AA399E65F37B37FB7032C59196532674D50B91B1347B36B5F70A9FD978E03343E033FB01E0339B247CEE4FC6829EFB13103CF9CF3B8E1D3E1BD854A0733A1AE89CC2D039DD7DE89C4E133A67A381CE190C9DB3DD87CED934A1733E1AE89CC3D039DF7DE89C4F0E3A2D39F837C751FCF074EDD616BEE9E1C710968E90243500D29837E24BEF9648D2C53249829E97FAEA57FE95D7CA356E8153D7F99A8C07D83ADF96A09B2B4C1A57BD5B690A1CADF2C1F91E4CAA1F0A3203BBA13141697887640CADB178254E8E87EAFF6FC0D7863B0717577B9B2BFF69470125B33F22C07A2450E2727E904EFCF37942A0AB92CDB729DC0390263E411A5099E9440B41BAB70348B50F001F8D433F1DA1682C478086C794E181A0B1400B4BE332A63BB868AA192CB6628976FC3E2E2D118FAD3843427198EB106344DD90D726AC70368AAB1428C0E429D93A1F4CC788B4E107562BBC8D65786560A613F5A3F89A5EBCAFDD680346FAE3018B418CEF0C33638BEE874090655C3F1E201113357608A7318F6F4340CBC9C8361E80E9464FCE7065118EED008A6C22B4F18087B8A8EE1C3B432FA70F891C8385F45101A75E06D5C48EE359DB581034DC0CCD024743CFCB58D11F541BC6D05EDE04F7F074F6EE92A1F6ECA415F7078D87566659C5FCCB113AC40AF95B45DCD71DC188C2D02385494B6A0DA8B4D31523AD3B3DC8A0399891A693265DEE013ACAFAFB854FF5E86D7F575BC7831A72E0E2F02EA3195E068D5680CDC666D69D7FEB726356F1FA3623061459971F767C0F166F0C4404A845C7B417DB56684C4701F670EB166E43671128F0E6367DC058E13444B6006DE08C274D40230969E0D342C88887B8A98264683FC2C648EAA541619C72111391C0E77A35903C2CC5ACE77702192DB8946D44106308EF228F720C71B3074D37A019CADB40A9BAFA0B5EF6898F1C253E1A017C4CB311528F744D2687D60E25BC1C01AC844C97144C491305DA2408ECF9FCA120B5269E1C270074587DBF20229F5DE5CA60CD3D9D13ACBCD09A2D38C0395652ED3D0F6DC35CDF180E3583268BD7C4CBE8EE6568054246A0994C2834121C6905434C538E0E5C6538A4ED8FC48008F83ABDB7CFE94151C4971AF2F5F361E3A23A754CAE48B1658F3F50D3260533EA941FA695A687919D34DF8EAC4F7918A5EBC1EB758E928F85745999342BE1C7950017D1C2BF0CE224FDE0A5DECC4BC4612C2F75E7A7EC8B3A8707E587F690537FB99B3FFB2BEFDDE16296BF71E5CD5A6F06E50FC908C0612B60DD8B500BFB19AAAA4DA1AE8E09B4449DDA5F41BDB6045A755D47A90F1A512051D45A51D16BDE8603B2CAB7548AFA6B422319AA41892248454A94A6A0568B24B185DC0074ADD9DD37B49AF2B3ACAA9C425D1D787041A815A4822A0708D5324017EF041120224802914E4F0045D5EA4AD5D58999C6842A4512A85A9E8A5635521D5E859A2D906F55A803A0C12AAC92C7EAD45AE5F694D65AD160B556894A756A2D231269A525095667FE95D66A58D76C7DC32A2176C3ED994BB092F2135647FE556FAC96540793A9C66E33310822D0AAA7550D1C4305ADCDD1606667C8D4B537C7F7853A9B2F504DDB47FD68FC9B23DF58350D81A4B68A465DA9F0FA9C50AB400155CB1111460DFE095671E0E029C0B183252256DC64DB002B6DBE62153604E4AA66D2AA66AAAA66E4AAF277FCD19AF28FB28AEEA1892652CFA9AC9E53453DA7F47ACE64F59C29EA39A3D7732EABE75C51CFB996D3DCF611A9E3DC92A99C27B9D309371BD108B9A19005C91591DE90C15ECE955A80255559A14D4D3785C20E6A23688F5CF2314B395A69B43212F41066229A7310712F5836C9A44D2EF5AB469A52A05054AC0F694C678046D995695A5FC1B1CF151AEE5C91229C5B7F1DC5290899F2038C95FC1B3DF45784FD78C84F0BF7EFBE27A9BFBAF293C47B028328F63B1843B549A835DE7871F6570ACE10050ABCD68688E653146E4C2441E7525277D65AE484BB40F332FB418B14E806F00BEE92AD904669FE83B086DBDAAE63574D8BC2CD6F9C66C7AC6A04B5F9E540E61804A0BE945EA284B8545A2AD2FE5D660464F99367022F695A99A5394E47B70C58A417E3F0AB923C9FEDF72E4C241CC6DA5647B2175E5E4B796604C32C50114DC1AC90452AF1B561283D10BDC36695D90232A3941E570ED408536304661197C8C1BB9080818825718525DB0D85C2E0778901F1AD83821DF4D9A9F9A0F70C1576533E81882A88AA363A13F11B130FACB4A281E405BA340FB6195370113F3A318D2AAA1068E4E20B22537D0E35F03055B3DEC74134AC3EBB540EDD882A8A035FDD285A6F1D218A569FBB5294D9FB1214ADBEBA51949DC020EACACE41399D2591CC66AA6A355747742CBF76D5A2ED0506A141CB8F6E94141D1CA2AF40E852F5217C7039ED284583756E51C84517C398D6AF03C1B7B8DD400B4E64E472E1DB4B368D02E58F230B48F88D5AA5239317A03935C13CE24789998CDCA36BD36C1B54D344D833489D9B0AC4254CE0D4744D2FA29A4C2CD045671BCA348C7652AF4370B066AA77E55E5BFBE742866168EE2721973849F87041E921F96F32672B9E1568786087006C4C32D3338948DEA749668849C4C30AE626B93FD1B10840DD9F41B6272F187B00672A2CCC71AA650E91BA47739CC2E6108F7E589843CC6F2F338748DDA339CE60738827542CCC21E66C97994391E1BD5B739CC3E6100FD298994396871CB109397539A310727CAED04AF8A6308CA1812D43AFFAF010750757A077B20F8B865DFC612921EC420F3F39320B013EFA79A73BC1D0506653E444060CA69345B91353C1476D0B36FC2767FB68509E5FC9369A322D30B88BC69DEC6336D39A6F83AF6FF33AD2965A08A5BA37D0D0EB2FED733AD48D47F37CAE9D6E3FAA4E850A3E8B25EACEAC3ACB5B78C12ED7EE476B3AA12B69588F2FDB7D671EDA8C8CFAC471C16050406D34B2E1C0602C301F08CC8D32C41080BA19566A85A1A849E69C9A6B549D8CE09F2C7D92B9A12CBC9AAD59D421BB8CBC4BA30C19A9D3AD42C93ED6C908D6A77958860F92093142A95403927F6CEA03897D700BA8B2003931027A2BB6D9B7E1BE3AD99FC2F31C21FB55C4C448986A9852E3350D9FC3476E1769C61F879B7AFD9A4399A506308A5E669B4EA7A8D07B228C8F2E3F381B79D0D42A92B187968E65F7CC44398B2E491D022AA07F08BD6FD595BD0726DC05D5B9FC0DB2704C92E9010EC3842EA00F7E92015DB4BCD223C812164C4E7DE97BFAC4DB3C3B780D45F54EBCDC32E637C6A679DD4E7CF35C6E1FC51BE99872D2FB5E5AF7BC3A5C6D93BDE22DB78AC6D223722D9F5767240B90E8CBD48A5ED4DB7A7F3F6650BEB9ACBCDCADE5559C9AA547BFA2783D58754040F1DE30BEA52F3817E0EB088E4403AFE1022651BD99CB880E3D1D71D7BED901DDEA0039B8BE1FF2F6B8E4D33CF4DA7C7B7B5C3EB550FD90FD9946B1F7E45F450B7F9914BFBE3DBEDD64A5577EF9D7073F099EB62CDE663C437F9ED7B9655AD3E40781EAF76D39896A92FA73FD644836A55F78A9F73E4E83476F9E669FE77E9204E1D3E1C1AFDE7293917C5CCDFCC5A7F0CB265D6FD24C657F355B7E6F1B237F275756FFDB6341E6B75FD6F95F890B153231834C05FF4BF8E74DB05C34725F7ACB846B348C45FE00EF5FFCECF7AAD364FFEF3F7D6F385D4721915165BEE6DDE0AFFE6ABDCCDF6EF912DE79DF7C13D932F87DF69FBCF9F7FA240BCE44DD10ACD9DF7E08BCA7D85B25158F6DF9ECCF0CC38BD5CB7FFC7F15CAC7A4C8280400 , N'6.1.3-40302')

