﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jnas.Models.Jnasdb
{
    public partial class OrganizationViewModel
    {
        [ScaffoldColumn(false)]
        public int organizationId { get; set; }

        [Required]
        [Display(Name = "Name of Organization")]
        public string name { get; set; }

        [Display(Name = "Parent Company")]
        public string ParentCompany { get; set; }

        [Display(Name = "Parent Company OCN")]
        public string ParentCompanyOCN { get; set; }

        [Display(Name = "OCN")]
        public string OCN { get; set; }

        [Display(Name = "OFN")]
        public string OFN { get; set; }



    }

    public partial class UserViewModel
    {

        [ScaffoldColumn(false)]
        public string UserId { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string name { get; set; }


        [Required]
        [Display(Name = "Primary Contact (Yes/ No)")]
        [DefaultValue(false)]
        public bool primaryContact { get; set; }

        [Required]
        [Display(Name = "Security Question")]
        public string securityQuestion { get; set; }

        [Required]
        [Display(Name = "Security Answer")]
        public string securityAnswer { get; set; }


        [StringLength(256)]
        public string Email { get; set; }

        [Required(ErrorMessage = "The Phone Number is required")]
        [RegularExpression(@"^[2-9][0-9]{2}-[2-9][0-9]{2}-[0-9]{4}$", ErrorMessage = "Invalid Phone Number, eq. 876-245-8888")]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }


        [Required]
        [StringLength(256)]
        [Display(Name = "User Name")]
        public string UserName { get; set; }


    }

}
