namespace jnas.Models.Jnasdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Report
    {
        [ScaffoldColumn(false)]
        public int ReportId { get; set; }

        [Required]
        [Display(Name = "Name of Report")]
        public string name { get; set; }
    }
}
