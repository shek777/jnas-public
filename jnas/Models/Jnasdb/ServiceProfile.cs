namespace jnas.Models.Jnasdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ServiceProfile
    {
        [ScaffoldColumn(false)]
        public int serviceProfileId { get; set; }

        [Display(Name = "Service")]
        public int serviceId { get; set; }

        [ScaffoldColumn(false)]
        public string service { get; set; }

        [Display(Name = "Resource Type")]
        public int resourceTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string resourceType { get; set; }

        [Display(Name = "Resource Sub Type")]
        public int resourceSubTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string resourceSubType { get; set; }

        [Required]
        [Display(Name = "Provider Service")]
        public int providerServiceId { get; set; }

        [ScaffoldColumn(false)]
        public string providerService { get; set; }

        [Required]
        [Display(Name = "Assigned to Exteral User (Yes/ No)")]
        //  [Range(typeof(bool), "true", "true", ErrorMessage = "Please indicate the primary contact")]
        [DefaultValue(false)]
        public bool assigned { get; set; }

        public virtual ProviderService ProviderService1 { get; set; }

        public virtual ResourceSubType ResourceSubType1 { get; set; }

        public virtual ResourceType ResourceType1 { get; set; }

        public virtual Service Service1 { get; set; }
    }
}
