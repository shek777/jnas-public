namespace jnas.Models.Jnasdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ProviderService
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProviderService()
        {
            OrganizationServices = new HashSet<OrganizationService>();
            ServiceProfiles = new HashSet<ServiceProfile>();
            this.NrufCompanyInfo = new HashSet<NrufCompanyInfo>();
        }

        [ScaffoldColumn(false)]
        public int ProviderServiceId { get; set; }

        [Required]
        [Display(Name = "Provider Service")]
        public string name { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrganizationService> OrganizationServices { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ServiceProfile> ServiceProfiles { get; set; }

        public virtual ICollection<NrufCompanyInfo> NrufCompanyInfo { get; set; }
    }
}
