namespace jnas.Models.Jnasdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class OrganizationResourceType
    {
        [ScaffoldColumn(false)]
        public int organizationResourceTypeId { get; set; }

        [Required]
        [Display(Name = "Organization")]
        public int organizationId { get; set; }

        [ScaffoldColumn(false)]
        public string organization { get; set; }

        [Display(Name = "Resource Type")]
        public int resourceTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string resourceType { get; set; }

        [Display(Name = "Resource Sub Type")]
        public int resourceSubTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string resourceSubType { get; set; }

        [Display(Name = "State")]
        public int linkedResourceStateId { get; set; }

        [ScaffoldColumn(false)]
        public string state { get; set; }

        public virtual LinkedResourceState LinkedResourceState { get; set; }

        public virtual Organization Organization1 { get; set; }

        public virtual ResourceSubType ResourceSubType1 { get; set; }

        public virtual ResourceType ResourceType1 { get; set; }
    }
}
