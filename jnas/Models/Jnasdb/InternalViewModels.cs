﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace jnas.Models.Jnasdb
{
    public partial class ManageUserViewModel
    {

        [ScaffoldColumn(false)]
        public string UserId { get; set; }

        [Required]
        [Display(Name = "Organization")]
        public int organizationId { get; set; }


        [Required]
        [Display(Name = "User Type")]
        public int userTypeId { get; set; }

        [Required]
        [Display(Name = "User Profile")]
        public int userProfileId { get; set; }

        [Required]
        [Display(Name = "User State")]
        public int userStateId { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string name { get; set; }

        [Display(Name = "Phone Number Confirmed")]
        public bool PhoneNumberConfirmed { get; set; }

        [Required]
        [Display(Name = "Primary Contact (Yes/ No)")]
        [DefaultValue(false)]
        public bool primaryContact { get; set; }

        [Display(Name = "Access Failed Count")]
        public int AccessFailedCount { get; set; }

        [StringLength(256)]
        public string Email { get; set; }

        [Required(ErrorMessage = "The Phone Number is required")]
        [RegularExpression(@"^[2-9][0-9]{2}-[2-9][0-9]{2}-[0-9]{4}$", ErrorMessage = "Invalid Phone Number, eq. 876-245-8888")]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }


        [Required]
        [StringLength(256)]
        [Display(Name = "User Name")]
        public string UserName { get; set; }


    }

    // Data Transfer Object
    public partial class UserResourceTypeDTO
    {
        [ScaffoldColumn(false)]
        public int userResourceTypeId { get; set; }

        // [Required]
        [Display(Name = "User Type")]
        public int userTypeId { get; set; }

        [Display(Name = "User Type")]
        // [ScaffoldColumn(false)]
        public string userType { get; set; }

        [Display(Name = "Resource Type")]
        public int resourceTypeId { get; set; }

        [Display(Name = "Resource Type")]
        // [ScaffoldColumn(false)]
        public string resourceType { get; set; }

        [Display(Name = "Resource Sub Type")]
        public int resourceSubTypeId { get; set; }

        [Display(Name = "Resource Sub Type")]
        //[ScaffoldColumn(false)]
        public string resourceSubType { get; set; }

        [Display(Name = "State")]
        public int linkedResourceStateId { get; set; }

        [Display(Name = "State")]
        // [ScaffoldColumn(false)]
        public string state { get; set; }


    }
}