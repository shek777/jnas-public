namespace jnas.Models.Jnasdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Application
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Application()
        {
            ApplicationNotes = new HashSet<ApplicationNote>();
            ApplicationResources = new HashSet<ApplicationResource>();
           // ApplicationResources1 = new HashSet<ApplicationResource>();
        }

        [ScaffoldColumn(false)]
        public int applicationId { get; set; }

        [Display(Name = "Organization")]
        public int organizationId { get; set; }

        [ScaffoldColumn(false)]
        public string application1 { get; set; }

        [Required]
        [Display(Name = "Application Reference")]
        public string applicationReference { get; set; }

        [ScaffoldColumn(false)]
        public string applicant { get; set; }

        [Display(Name = "Recipient")]
        public string recipient { get; set; }

        // [Required]
        [StringLength(128)]
        [Display(Name = "User")]
        public string UserId { get; set; }

        [ScaffoldColumn(false)]
        public string user { get; set; }

        // [Required]
        [ScaffoldColumn(false)]
        [Display(Name = "Service")]
        public int serviceId { get; set; }

        [ScaffoldColumn(false)]
        public string service { get; set; }

        [Required]
        [Display(Name = "Resource Type")]
        public int resourceTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string resourceType { get; set; }

        [Required]
        [Display(Name = "Resource Sub Type")]
        public int resourceSubTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string resourceSubType { get; set; }

        [Display(Name = "Subscriber")]
        public string subscriber { get; set; }

        [Display(Name = "Contact Number")]
        public string customerContactNumber { get; set; }

        [Display(Name = "Local Routing Number")]
        [RegularExpression(@"^[2-9][0-9]{2}-[2-9][0-9]{2}-[0-9]{4}$", ErrorMessage = "Invalid Local Routing Number, eq. 876-245-8888")]
        public string localRoutingNumber { get; set; }

        [ScaffoldColumn(false)]
        [StringLength(255)]
        public string FileName { get; set; }

        [ScaffoldColumn(false)]
        [StringLength(100)]
        public string ContentType { get; set; }

        [Display(Name = "Additional Documentation")]
        public byte[] additionalDocumentation { get; set; }
        
        [Display(Name = "Date Created")]
        public DateTime? createdDate { get; set; }

        [Display(Name = "Date Changed")]
        public DateTime? dateChanged { get; set; }

        [Display(Name = "Resources")]
        public int applicationResourceId { get; set; }

        [Required]
        [Display(Name = "Type of Application")]
        public int applicationTypeId { get; set; }

        [Display(Name = "State of Application")]
        public int applicationStateId { get; set; }

        [ScaffoldColumn(false)]
        public string state { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ApplicationNote> ApplicationNotes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ApplicationResource> ApplicationResources { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<ApplicationResource> ApplicationResources1 { get; set; }

        public virtual ApplicationState ApplicationState { get; set; }

        public virtual ApplicationType ApplicationType { get; set; }

        public virtual Organization Organization { get; set; }

        public virtual ResourceSubType ResourceSubType1 { get; set; }

        public virtual ResourceType ResourceType1 { get; set; }

        public virtual User User1 { get; set; }
    }
}
