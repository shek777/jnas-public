namespace jnas.Models.Jnasdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class OrganizationUserType
    {
        [ScaffoldColumn(false)]
        public int organizationUserTypeId { get; set; }

        [Required]
        [Display(Name = "Organization Type")]
        public int organizationTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string organizationType { get; set; }

        [Required]
        [Display(Name = "User Type")]
        public int userTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string userType { get; set; }

        public virtual OrganizationType OrganizationType1 { get; set; }

        public virtual UserType UserType1 { get; set; }
    }
}
