namespace jnas.Models.Jnasdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ApplicationNote
    {
        [Key]
        [ScaffoldColumn(false)]
        public int applicationNotesId { get; set; }

        [Required]
        [Display(Name = "Application")]
        public int applicationId { get; set; }

        [ScaffoldColumn(false)]
        public string application { get; set; }

        [Required]
        [Display(Name = "Note")]
        public string note { get; set; }

        [Required]
        [Display(Name = "User")]
        public string UserId { get; set; }

        [ScaffoldColumn(false)]
        public string user { get; set; }

        [Display(Name = "Internal Note")]
        public string internalNote { get; set; }

        [Display(Name = "Date Created")]
        public DateTime? createdDate { get; set; }

        [Display(Name = "Date Changed")]
        public DateTime? dateChanged { get; set; }

        public virtual Application Application1 { get; set; }
    }
}
