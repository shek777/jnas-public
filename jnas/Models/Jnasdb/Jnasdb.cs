namespace jnas.Models.Jnasdb
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Configuration;

    public partial class Jnasdb : DbContext
    {
        public Jnasdb()
            : base(Config.ConnectionStringName2)
        {
        }

        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<ApplicationNote> ApplicationNotes { get; set; }
        public virtual DbSet<ApplicationResource> ApplicationResources { get; set; }
        public virtual DbSet<ApplicationResourceState> ApplicationResourceStates { get; set; }
        public virtual DbSet<Application> Applications { get; set; }
        public virtual DbSet<ApplicationState> ApplicationStates { get; set; }
        public virtual DbSet<ApplicationType> ApplicationTypes { get; set; }
        public virtual DbSet<LinkedResourceState> LinkedResourceStates { get; set; }
        public virtual DbSet<Mail> Mails { get; set; }
        public virtual DbSet<NotificationDetail> NotificationDetails { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<OrganizationResourceType> OrganizationResourceTypes { get; set; }
        public virtual DbSet<Organization> Organizations { get; set; }
        public virtual DbSet<OrganizationService> OrganizationServices { get; set; }
        public virtual DbSet<OrganizationState> OrganizationStates { get; set; }
        public virtual DbSet<OrganizationType> OrganizationTypes { get; set; }
        public virtual DbSet<OrganizationUserType> OrganizationUserTypes { get; set; }
        public virtual DbSet<ProviderService> ProviderServices { get; set; }
        public virtual DbSet<Report> Reports { get; set; }
        public virtual DbSet<ReportOnNXXAndNPACodes> ReportOnNXXAndNPACodeses { get; set; }
        public virtual DbSet<ReportOnNXXAndNPACodesKey> ReportOnNXXAndNPACodesKeys { get; set; }
        public virtual DbSet<CentralOfficeCodeAssignments> CentralOfficeCodeAssignmentses { get; set; }
        public virtual DbSet<ResourceLink> ResourceLinks { get; set; }
        public virtual DbSet<Resource> Resources { get; set; }
        public virtual DbSet<ResourceState> ResourceStates { get; set; }
        public virtual DbSet<ResourceSubType> ResourceSubTypes { get; set; }
        public virtual DbSet<ResourceType> ResourceTypes { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<ServiceProfile> ServiceProfiles { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<SystemMessage> SystemMessages { get; set; }
        public virtual DbSet<SystemParameter> SystemParameters { get; set; }
        public virtual DbSet<UserClaim> UserClaims { get; set; }
        public virtual DbSet<UserLogin> UserLogins { get; set; }
        public virtual DbSet<UserNotification> UserNotifications { get; set; }
        public virtual DbSet<UserProfileDetail> UserProfileDetails { get; set; }
        public virtual DbSet<UserProfile> UserProfiles { get; set; }
        public virtual DbSet<UserResourceType> UserResourceTypes { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserState> UserStates { get; set; }
        public virtual DbSet<UserType> UserTypes { get; set; }
        public virtual DbSet<NrufCompanyInfo> NrufCompanyInfo { get; set; }
        public virtual DbSet<NrufFormU1> NrufFormU1 { get; set; }
        public virtual DbSet<NrufFormU2> NrufFormU2 { get; set; }
        public virtual DbSet<NrufFormU3> NrufFormU3 { get; set; }
        public virtual DbSet<NrufFormU4> NrufFormU4 { get; set; }
        public virtual DbSet<NrufFormF3a> NrufFormF3a { get; set; }
        public virtual DbSet<NrufFormF3b> NrufFormF3b { get; set; }

        //  public virtual DbSet<ExceptionLogger> ExceptionLoggers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Address>()
                .HasMany(e => e.OrganizationServices)
                .WithRequired(e => e.Address)
                .WillCascadeOnDelete(false);

             modelBuilder.Entity<ResourceLink>()
                .HasMany(e => e.Resource)
                .WithOptional(e => e.ResourceLinks)
                .HasForeignKey(e => e.resourceLinkId);

            //modelBuilder.Entity<ApplicationResourceState>()
            //    .HasMany(e => e.ApplicationResources)
            //    .WithOptional(e => e.ApplicationResourceState)
            //    .HasForeignKey(e => e.applicationResourceId);

            //modelBuilder.Entity<Organization>()
            //    .HasMany(e => e.ApplicationResources)
            //    .WithOptional(e => e.Organization)
            //    .HasForeignKey(e => e.Organization_organizationId);

            //modelBuilder.Entity<ApplicationResourceState>()
            //    .HasMany(e => e.ApplicationResources1)
            //    .WithRequired(e => e.ApplicationResourceState1)
            //    .HasForeignKey(e => e.ApplicationResourceState_applicationResourceStateId1)
            //    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Application>()
                .HasMany(e => e.ApplicationNotes)
                .WithRequired(e => e.Application1)
                .WillCascadeOnDelete(false);

            //modelBuilder.Entity<ApplicationResource>()
            //    .Ignore(p => p.state);


            //modelBuilder.Entity<Application>()
            //    .HasMany(e => e.ApplicationResources)
            //    .WithOptional(e => e.Application1)
            //    .HasForeignKey(e => e.applicationId);

            //modelBuilder.Entity<Application>()
            //    .HasMany(e => e.ApplicationResources1)
            //    .WithOptional(e => e.Application2)
            //    .HasForeignKey(e => e.Application_applicationId1);

            modelBuilder.Entity<ApplicationState>()
                .HasMany(e => e.Applications)
                .WithRequired(e => e.ApplicationState)
                .HasForeignKey(e => e.applicationStateId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationType>()
                .HasMany(e => e.Applications)
                .WithRequired(e => e.ApplicationType)
                .HasForeignKey(e => e.applicationTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LinkedResourceState>()
                .HasMany(e => e.NotificationDetails)
                .WithRequired(e => e.LinkedResourceState)
                .HasForeignKey(e => e.linkedResourceStateId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LinkedResourceState>()
                .HasMany(e => e.OrganizationResourceTypes)
                .WithRequired(e => e.LinkedResourceState)
                .HasForeignKey(e => e.linkedResourceStateId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LinkedResourceState>()
                .HasMany(e => e.ResourceLinks)
                .WithRequired(e => e.LinkedResourceState)
                .HasForeignKey(e => e.linkedResourceStateId)
                .WillCascadeOnDelete(false);

            //modelBuilder.Entity<LinkedResourceState>()
            //    .HasMany(e => e.ResourceLinks1)
            //    .WithRequired(e => e.LinkedResourceState1)
            //    .HasForeignKey(e => e.LinkedResourceState_linkedResourceStateId1)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<LinkedResourceState>()
            //    .HasMany(e => e.UserResourceTypes)
            //    .WithRequired(e => e.LinkedResourceState)
            //    .HasForeignKey(e => e.LinkedResourceState_linkedResourceStateId)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<LinkedResourceState>()
            //    .HasMany(e => e.UserResourceTypes1)
            //    .WithOptional(e => e.LinkedResourceState1)
            //    .HasForeignKey(e => e.LinkedResourceState_linkedResourceStateId1);

            modelBuilder.Entity<Notification>()
                .HasMany(e => e.NotificationDetails)
                .WithRequired(e => e.Notification1)
                .HasForeignKey(e => e.notificationId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Notification>()
                .HasMany(e => e.UserNotifications)
                .WithRequired(e => e.Notification1)
                .HasForeignKey(e => e.notificationId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.Addresses)
                .WithRequired(e => e.Organization1)
                .HasForeignKey(e => e.organizationId)
                .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Organization>()
            //    .HasMany(e => e.ApplicationResources)
            //    .WithOptional(e => e.Organization)
            //    .HasForeignKey(e => e.Organization_organizationId);

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.Applications)
                .WithRequired(e => e.Organization)
                .HasForeignKey(e => e.organizationId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.OrganizationResourceTypes)
                .WithRequired(e => e.Organization1)
                .HasForeignKey(e => e.organizationId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.Users)
                .WithRequired(e => e.Organization1)
                .HasForeignKey(e => e.organizationId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OrganizationState>()
                .HasMany(e => e.Organizations)
                .WithRequired(e => e.OrganizationState)
                .HasForeignKey(e => e.organizationStateId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OrganizationType>()
                .HasMany(e => e.Organizations)
                .WithRequired(e => e.OrganizationType1)
                .HasForeignKey(e => e.organizationTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OrganizationType>()
                .HasMany(e => e.OrganizationUserTypes)
                .WithRequired(e => e.OrganizationType1)
                .HasForeignKey(e => e.organizationTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProviderService>()
                .HasMany(e => e.OrganizationServices)
                .WithRequired(e => e.ProviderService1)
                .HasForeignKey(e => e.ProviderServiceId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProviderService>()
                .HasMany(e => e.ServiceProfiles)
                .WithRequired(e => e.ProviderService1)
                .HasForeignKey(e => e.providerServiceId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Resource>()
                .HasMany(e => e.ApplicationResources)
                .WithRequired(e => e.Resource1)
                .HasForeignKey(e => e.resourceId)
                .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Resource>()
            //    .HasMany(e => e.ApplicationResources1)
            //    .WithRequired(e => e.Resource2)
            //    .HasForeignKey(e => e.Resource_resourceId1)
            //    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Resource>()
                .HasOptional(e => e.ResourceLinks);
                

            //modelBuilder.Entity<Resource>()
            //    .HasMany(e => e.ResourceLinks)
            //    .WithOptional(e => e)
            //    .HasForeignKey(e => e.);

            modelBuilder.Entity<ResourceState>()
                .HasMany(e => e.Resources)
                .WithRequired(e => e.ResourceState)
                .HasForeignKey(e => e.resourceStateId)
                .WillCascadeOnDelete(false);

            //modelBuilder.Entity<ResourceState>()
            //    .HasMany(e => e.Resources1)
            //    .WithRequired(e => e.ResourceState1)
            //    .HasForeignKey(e => e.ResourceState_resourceStateId1)
            //    .WillCascadeOnDelete(false);

            modelBuilder.Entity<ResourceSubType>()
                .HasMany(e => e.Applications)
                .WithRequired(e => e.ResourceSubType1)
                .HasForeignKey(e => e.resourceSubTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ResourceSubType>()
                .HasMany(e => e.NotificationDetails)
                .WithRequired(e => e.ResourceSubType1)
                .HasForeignKey(e => e.resourceSubTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ResourceSubType>()
                .HasMany(e => e.OrganizationResourceTypes)
                .WithRequired(e => e.ResourceSubType1)
                .HasForeignKey(e => e.resourceSubTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ResourceSubType>()
                .HasMany(e => e.ServiceProfiles)
                .WithRequired(e => e.ResourceSubType1)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ResourceType>()
                .HasMany(e => e.Applications)
                .WithRequired(e => e.ResourceType1)
                .HasForeignKey(e => e.resourceTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ResourceType>()
                .HasMany(e => e.NotificationDetails)
                .WithRequired(e => e.ResourceType1)
                .HasForeignKey(e => e.resourceTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ResourceType>()
                .HasMany(e => e.OrganizationResourceTypes)
                .WithRequired(e => e.ResourceType1)
                .HasForeignKey(e => e.resourceTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ResourceType>()
                .HasMany(e => e.ResourceSubTypes)
                .WithRequired(e => e.ResourceType)
                .HasForeignKey(e => e.resourceTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ResourceType>()
                .HasMany(e => e.ServiceProfiles)
                .WithRequired(e => e.ResourceType1)
                .HasForeignKey(e => e.resourceTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Role>()
                .HasMany(e => e.UserRoles)
                .WithRequired(e => e.Role)
                .HasForeignKey(e => e.RoleId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Service>()
                .HasMany(e => e.ServiceProfiles)
                .WithRequired(e => e.Service1)
                .HasForeignKey(e => e.serviceId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Service>()
                .HasMany(e => e.UserProfileDetails)
                .WithRequired(e => e.Service1)
                .HasForeignKey(e => e.serviceId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.UserProfileDetails)
                .WithRequired(e => e.UserProfile1)
                .HasForeignKey(e => e.userProfileId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Applications)
                .WithRequired(e => e.User1)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.UserClaims)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.UserLogins)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.UserNotifications)
                .WithRequired(e => e.User1)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.UserRoles)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserState>()
                .HasMany(e => e.Users)
                .WithRequired(e => e.UserState)
                .HasForeignKey(e => e.userStateId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserType>()
                .HasMany(e => e.NotificationDetails)
                .WithRequired(e => e.UserType1)
                .HasForeignKey(e => e.userTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserType>()
                .HasMany(e => e.OrganizationUserTypes)
                .WithRequired(e => e.UserType1)
                .HasForeignKey(e => e.userTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserType>()
                .HasMany(e => e.UserProfileDetails)
                .WithRequired(e => e.UserType1)
                .HasForeignKey(e => e.userTypeId)
                .WillCascadeOnDelete(false);

            //modelBuilder.Entity<UserType>()
            //    .HasMany(e => e.UserResourceTypes)
            //    .WithRequired(e => e.UserType1)
            //    .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserType>()
                .HasMany(e => e.Users)
                .WithRequired(e => e.UserType1)
                .HasForeignKey(e => e.userTypeId)
                .WillCascadeOnDelete(false);

            //modelBuilder.Entity<ResourceLink>()
            //    .Property(e => e.LinkedResourceState_linkedResourceStateId1)
            //    .IsOptional();

            //modelBuilder.Entity<ResourceLink>()
            //   .Property(e => e.Resource_resourceId)
            //   .IsOptional();

            //modelBuilder.Entity<Resource>()
            //   .Property(e => e.ResourceState_resourceStateId1)
            //   .IsOptional();


        }
    }
}
