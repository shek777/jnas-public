﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jnas.Models.Jnasdb
{
    public partial class NrufFormU2
    {
        public NrufFormU2()
        {
            Assigned = 0;
            Intermediate = 0;
            Reserved = 0;
            Aging = 0;
            Admin = 0;
            PortedOut = 0;
            Rate = "KGN";
        }

        public virtual NrufCompanyInfo NrufCompanyInfo { get; set; }

        [ScaffoldColumn(false)]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FormU2Id { get; set; }

        [ScaffoldColumn(false)]
        public int NrufCompanyInfoId { get; set; }

        [Required(ErrorMessage = "NPA is required")]
        [Display(Name = "NPA")]
        [RegularExpression(@"^[2-9][0-9]{2}$", ErrorMessage = "Invalid NPA")]
        public string Npa { get; set; }

        [Required(ErrorMessage = "NXX is required")]
        [Display(Name = "NXX")]
        [RegularExpression(@"^(?!(211|311|411|511|611|711|811|911|200|300|400|500|600|700|800|900|222|333|444|555|666|777|888|999|876|555|950|976|959))[2-9][0-9][0-9]$", ErrorMessage = "Invalid NXX")]
        public string Nxx { get; set; }

        [Required(ErrorMessage = "Rate Centre Abreviation is required")]
        [Display(Name = "Rate Centre Abreviation")]
        [RegularExpression(@"^KGN$", ErrorMessage = "Enter KGN")]
        public string Rate { get; set; }

        [Required(ErrorMessage = "Assigned is required")]
        [Display(Name = "Assigned")]
        [Range(0, 10000)]
        public int Assigned { get; set; }

        [Required(ErrorMessage = "Intermediate is required")]
        [Display(Name = "Intermediate")]
        [Range(0, 10000)]
        public int Intermediate { get; set; }

        [Required(ErrorMessage = "Reserved is required")]
        [Display(Name = "Reserved")]
        [Range(0, 10000)]
        public int Reserved { get; set; }

        [Required(ErrorMessage = "Aging is required")]
        [Display(Name = "Aging")]
        [Range(0, 10000)]
        public int Aging { get; set; }

        [Required(ErrorMessage = "Admin is required")]
        [Display(Name = "Admin")]
        [Range(0, 10000)]
        public int Admin { get; set; }

        [Required(ErrorMessage = "Ported-Out is required")]
        [Display(Name = "Ported-Out")]
        [Range(0, 10000)]
        public int PortedOut { get; set; }

        [Display(Name = "Notes/ Assignee (Intermediate Carrier)")]
        public string Notes { get; set; }

        [Display(Name = "Available")]
        public int Available { get; set; }

        [Display(Name = "Utilization")]
        public string Utilization { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? createdDate { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? dateChanged { get; set; }

        public int Calculate(out string message)
        {
            float result;

            if ((Assigned + Intermediate + Reserved + Aging + Admin + PortedOut) > 10000)
            {
                message = "Error!, Sum is over 10000";
                return 0;
            }
            else
            {
                Available = 10000 - (Assigned + Intermediate + Reserved + Aging + Admin + PortedOut);

            }

            if (Intermediate == 10000)
            {
                Utilization = "Intermed";
            }
            else if (PortedOut == 10000)
            {
                Utilization = "Ported";
            }
            else
            {
                result = (float)(Assigned / (10000 - Intermediate - PortedOut));
                Utilization = result.ToString("#0.##%", CultureInfo.InvariantCulture);
            }

            message = "";
            return 1;

        }
    }
}
