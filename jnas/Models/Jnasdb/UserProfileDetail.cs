namespace jnas.Models.Jnasdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UserProfileDetail
    {
        [ScaffoldColumn(false)]
        public int userProfileDetailId { get; set; }

        [Required]
        [Display(Name = "User Profile Name")]
        public int userProfileId { get; set; }

        [ScaffoldColumn(false)]
        public string userProfile { get; set; }

        [Required]
        [Display(Name = "Type of Service")]
        public int serviceId { get; set; }

        [ScaffoldColumn(false)]
        public string service { get; set; }

        [Required]
        [Display(Name = "Type of User")]
        public int userTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string userType { get; set; }

        [Required]
        [Display(Name = "For External Users (Yes/ No)")]
        [DefaultValue(false)]
        public bool assigned { get; set; }

        public virtual Service Service1 { get; set; }

        public virtual UserProfile UserProfile1 { get; set; }

        public virtual UserType UserType1 { get; set; }
    }
}
