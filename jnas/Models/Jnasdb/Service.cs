namespace jnas.Models.Jnasdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Service
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Service()
        {
            ServiceProfiles = new HashSet<ServiceProfile>();
            UserProfileDetails = new HashSet<UserProfileDetail>();
        }

        [ScaffoldColumn(false)]
        public int serviceId { get; set; }

        [Required]
        [Display(Name = "Service Name")]
        public string name { get; set; }

        [Required]
        [Display(Name = "Application (Yes/ No)")]
        [DefaultValue(false)]
        public bool applicationYN { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ServiceProfile> ServiceProfiles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserProfileDetail> UserProfileDetails { get; set; }
    }
}
