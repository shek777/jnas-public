namespace jnas.Models.Jnasdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UserResourceType
    {
        [ScaffoldColumn(false)]
        public int userResourceTypeId { get; set; }

       // [Required]
        [Display(Name = "User Type")]
        public int userTypeId { get; set; }

        [Display(Name = "User Type")]
        // [ScaffoldColumn(false)]
        public string userType { get; set; }

        [Display(Name = "Resource Type")]
        public int resourceTypeId { get; set; }

        [Display(Name = "Resource Type")]
        // [ScaffoldColumn(false)]
        public string resourceType { get; set; }

        [Display(Name = "Resource Sub Type")]
        public int resourceSubTypeId { get; set; }

        [Display(Name = "Resource Sub Type")]
        //[ScaffoldColumn(false)]
        public string resourceSubType { get; set; }

        [Display(Name = "State")]
        public int linkedResourceStateId { get; set; }

        [Display(Name = "State")]
       // [ScaffoldColumn(false)]
        public string state { get; set; }

        //[ScaffoldColumn(false)]
        //public int LinkedResourceState_linkedResourceStateId { get; set; }

        //[ScaffoldColumn(false)]
        //public int? LinkedResourceState_linkedResourceStateId1 { get; set; }

        //public virtual LinkedResourceState LinkedResourceState { get; set; }

        //public virtual LinkedResourceState LinkedResourceState1 { get; set; }

        //public virtual UserType UserType1 { get; set; }
    }
}
