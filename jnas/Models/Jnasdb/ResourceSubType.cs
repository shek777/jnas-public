namespace jnas.Models.Jnasdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ResourceSubType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ResourceSubType()
        {
            Applications = new HashSet<Application>();
            NotificationDetails = new HashSet<NotificationDetail>();
            OrganizationResourceTypes = new HashSet<OrganizationResourceType>();
            ServiceProfiles = new HashSet<ServiceProfile>();
        }

        [ScaffoldColumn(false)]
        public int resourceSubTypeId { get; set; }

        [Required]
        [Display(Name = "Resource Type")]
        public int resourceTypeId { get; set; }

        [Required]
        [Display(Name = "Resource Sub Type")]
        public string name { get; set; }

        [ScaffoldColumn(false)]
        public string subType { get; set; }

        [Required]
        [Display(Name = "Format")]
        public string format { get; set; }

        [Required]
        [Display(Name = "Regular Expression")]
        public string formatRegex { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Application> Applications { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<NotificationDetail> NotificationDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrganizationResourceType> OrganizationResourceTypes { get; set; }

        public virtual ResourceType ResourceType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ServiceProfile> ServiceProfiles { get; set; }
    }
}
