﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jnas.Models.Jnasdb
{
    public partial class NrufCompanyInfo
    {
        public NrufCompanyInfo()
        {
            this.NrufFormU1 = new HashSet<NrufFormU1>();
            this.NrufFormU2 = new HashSet<NrufFormU2>();
            this.NrufFormU3 = new HashSet<NrufFormU3>();
            this.NrufFormU4 = new HashSet<NrufFormU4>();
            this.NrufFormF3a = new HashSet<NrufFormF3a>();
            this.NrufFormF3b = new HashSet<NrufFormF3b>();
        }

        public virtual ICollection<NrufFormU1> NrufFormU1 { get; set; }
        public virtual ICollection<NrufFormU2> NrufFormU2 { get; set; }
        public virtual ICollection<NrufFormU3> NrufFormU3 { get; set; }
        public virtual ICollection<NrufFormU4> NrufFormU4 { get; set; }
        public virtual ICollection<NrufFormF3a> NrufFormF3a { get; set; }
        public virtual ICollection<NrufFormF3b> NrufFormF3b { get; set; }

        public virtual ProviderService ProviderService { get; set; }


        [ScaffoldColumn(false)]
        public int NrufCompanyInfoId { get; set; }

        [ScaffoldColumn(false)]
        public int organizationId { get; set; }

        [ScaffoldColumn(false)]
        public string UserId { get; set; }

        [ScaffoldColumn(false)]
        [Display(Name = "Application Reference")]
        public string applicationReference { get; set; }

        [ScaffoldColumn(false)]
        public int ProviderServiceId { get; set; }

        [Required]
        [Display(Name = "Service Provider Name")]
        public string name { get; set; }

        [Display(Name = "Parent Company Name")]
        public string ParentCompany { get; set; }

        [Display(Name = "Parent Company OCN(s)")]
        // [RegularExpression(@"^[0-9][0-9|A-Z]{3}$", ErrorMessage = "Invalid OCN")]
        public string ParentCompanyOCN { get; set; }

        [Required]
        [Display(Name = "Service Provider OCN")]
        // [RegularExpression(@"^[0-9][0-9|A-Z]{3}$", ErrorMessage = "Invalid OCN")]
        public string OCN { get; set; }

        [Display(Name = "Service Provider OFN")]
        // [RegularExpression(@"^[0-9][0-9|A-Z]{3}$", ErrorMessage = "Invalid OFN")]
        public string OFN { get; set; }

        [Required]
        [Display(Name = "Company Address")]
        public string line1 { get; set; }

        [Display(Name = "Address 2")]
        public string line2 { get; set; }

        [Display(Name = "Address 3")]
        public string line3 { get; set; }

        [Required]
        [Display(Name = "Contact Name")]
        public string ContactName { get; set; }

        [Required]
        [Display(Name = "Contact Tel #")]
        [RegularExpression(@"^[2-9][0-9]{2}-[2-9][0-9]{2}-[0-9]{4}$", ErrorMessage = "Invalid Contact Telephone Number, eq. 876-245-8888")]
        public string ContactTelNo { get; set; }

        [Display(Name = "Fax #")]
        [RegularExpression(@"^[2-9][0-9]{2}-[2-9][0-9]{2}-[0-9]{4}$", ErrorMessage = "Invalid Fax Telephone Number, eq. 876-245-8888")]
        public string ContactFaxNo { get; set; }


        [Display(Name = "E-mail")]
        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "The email address is not valid")]
        public string ContactEmail { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }

        // [ScaffoldColumn(false)]
        [Display(Name = "Created Date")]
        public System.DateTime? createdDate { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? dateChanged { get; set; }

        // [ScaffoldColumn(false)]
        [Display(Name = "Expiration Date")]
        public DateTime? expiryDate { get; set; }


    }
}
