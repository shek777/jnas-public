namespace jnas.Models.Jnasdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class User
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User()
        {
            Applications = new HashSet<Application>();
            UserClaims = new HashSet<UserClaim>();
            UserLogins = new HashSet<UserLogin>();
            UserNotifications = new HashSet<UserNotification>();
            UserRoles = new HashSet<UserRole>();
        }

        [ScaffoldColumn(false)]
        public string UserId { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string name { get; set; }

        [ScaffoldColumn(false)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int userNo { get; set; }

        [Required]
        [Display(Name = "Organization")]
        public int organizationId { get; set; }

        [ScaffoldColumn(false)]
        public string organization { get; set; }

        [Required]
        [Display(Name = "User Type")]
        public int userTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string userType { get; set; }

        [Required]
        [Display(Name = "User Profile")]
        public int userProfileId { get; set; }

        [ScaffoldColumn(false)]
        public string userProfile { get; set; }

        [Required]
        [Display(Name = "Primary Contact (Yes/ No)")]
        //  [Range(typeof(bool), "true", "true", ErrorMessage = "Please indicate the primary contact")]
        [DefaultValue(false)]
        public bool primaryContact { get; set; }

        [Required]
        [Display(Name = "Security Question")]
        public string securityQuestion { get; set; }

        [Required]
        [Display(Name = "Security Answer")]
        public string securityAnswer { get; set; }

        [Display(Name = "Date Created")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd H:mm:ss}", ApplyFormatInEditMode = true)]
        // [ScaffoldColumn(false)]
        public DateTime? createdDate { get; set; }

        [Display(Name = "Date Changed")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd H:mm:ss}", ApplyFormatInEditMode = true)]
        // [ScaffoldColumn(false)]
        public DateTime? dateChanged { get; set; }

        [Display(Name = "Last Connection Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd H:mm:ss}", ApplyFormatInEditMode = true)]
        // [ScaffoldColumn(false)]
        public DateTime? lastConnectionDate { get; set; }

        [Required]
        [Display(Name = "User State")]
        public int userStateId { get; set; }

        [ScaffoldColumn(false)]
        public string state { get; set; }

        [StringLength(256)]
        public string Email { get; set; }

        [Display(Name = "Email Confirmed")]
        public bool EmailConfirmed { get; set; }

        [ScaffoldColumn(false)]
        public string PasswordHash { get; set; }

        [ScaffoldColumn(false)]
        public string SecurityStamp { get; set; }

        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Phone Number Confirmed")]
        public bool PhoneNumberConfirmed { get; set; }

        [Display(Name = "Enable Two Factor Authentication")]
        public bool TwoFactorEnabled { get; set; }

        [Display(Name = "Lockout End Date utc")]
        public DateTime? LockoutEndDateUtc { get; set; }

        [Display(Name = "Enable Lockout")]
        public bool LockoutEnabled { get; set; }

        [Display(Name = "Access Failed Count")]
        public int AccessFailedCount { get; set; }

        [Required]
        [StringLength(256)]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Application> Applications { get; set; }

        public virtual Organization Organization1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserClaim> UserClaims { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserLogin> UserLogins { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserNotification> UserNotifications { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserRole> UserRoles { get; set; }

        public virtual UserState UserState { get; set; }

        public virtual UserType UserType1 { get; set; }
    }
}
