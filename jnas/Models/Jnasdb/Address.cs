namespace jnas.Models.Jnasdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Address
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Address()
        {
            OrganizationServices = new HashSet<OrganizationService>();
        }

        [Display(Name = "Address")]
       // [ScaffoldColumn(false)]
        public int AddressId { get; set; }

        [Required]
        [Display(Name = "Organization")]
        public int organizationId { get; set; }

        [Display(Name = "Organization")]
        [ScaffoldColumn(false)]
        public string organization { get; set; }

        [Required]
        [Display(Name = "Address 1")]
        public string line1 { get; set; }

        [Display(Name = "Address 2")]
        public string line2 { get; set; }

        [Display(Name = "City")]
        public string city { get; set; }

        [Display(Name = "Zip Code")]
        public string zipCode { get; set; }

        [Required]
        [Display(Name = "Default Address (Yes/ No)")]
        [DefaultValue(false)]
        public bool defaultAddress { get; set; }

        public virtual Organization Organization1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrganizationService> OrganizationServices { get; set; }
    }
}
