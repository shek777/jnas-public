namespace jnas.Models.Jnasdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UserProfile
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public UserProfile()
        {
            UserProfileDetails = new HashSet<UserProfileDetail>();
        }

        [ScaffoldColumn(false)]
        public int userProfileId { get; set; }

        [Required]
        [Display(Name = "User Profile Name")]
        public string name { get; set; }

        [Required]
        [Display(Name = "Default Profile (Yes/ No)")]
        [DefaultValue(false)]
        public bool defaultProfile { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserProfileDetail> UserProfileDetails { get; set; }
    }
}
