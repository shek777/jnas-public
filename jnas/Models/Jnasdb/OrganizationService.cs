namespace jnas.Models.Jnasdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class OrganizationService
    {
        [Key]
        [ScaffoldColumn(false)]
        public int organizationServicesId { get; set; }

        [Required]
        [Display(Name = "Address")]
        public int AddressId { get; set; }

        [Required]
        [Display(Name = "Provider Service")]
        public int ProviderServiceId { get; set; }

        [Display(Name = "Organization")]
        public string organization { get; set; }

        [ScaffoldColumn(false)]
        public string providerService { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? createdDate { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? dateChanged { get; set; }

        public virtual Address Address { get; set; }

        public virtual ProviderService ProviderService1 { get; set; }
    }
}
