﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jnas.Models.Jnasdb
{
    public partial class NrufFormU4
    {

        public NrufFormU4()
        {
            Assigned = 0;
            Aging = 0;
            PortedIn = 0;
            Rate = "KGN";
        }
        // (\d)\1\1[A-Z]{1}[,]*$
        public virtual NrufCompanyInfo NrufCompanyInfo { get; set; }

        [ScaffoldColumn(false)]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FormU4Id { get; set; }

        [ScaffoldColumn(false)]
        public int NrufCompanyInfoId { get; set; }

        [Required(ErrorMessage = "NPA is required")]
        [Display(Name = "NPA")]
        [RegularExpression(@"^[2-9][0-9]{2}$", ErrorMessage = "Invalid NPA")]
        public string Npa { get; set; }

        [Required(ErrorMessage = "NXX is required")]
        [Display(Name = "NXX")]
        [RegularExpression(@"^(?!(211|311|411|511|611|711|811|911|200|300|400|500|600|700|800|900|222|333|444|555|666|777|888|999|876|555|950|976|959))[2-9][0-9][0-9]$", ErrorMessage = "Invalid NXX")]
        public string Nxx { get; set; }

        [Required(ErrorMessage = "Rate Centre Abreviation is required")]
        [Display(Name = "Rate Centre Abreviation")]
        [RegularExpression(@"^KGN$", ErrorMessage = "Enter KGN")]
        public string Rate { get; set; }

        [Required(ErrorMessage = "Assigned is required")]
        [Display(Name = "Assigned")]
        [Range(0, 10000)]
        public int Assigned { get; set; }

        [Required(ErrorMessage = "Aging is required")]
        [Display(Name = "Aging")]
        [Range(0, 10000)]
        public int Aging { get; set; }

        [Required(ErrorMessage = "Ported-In is required")]
        [Display(Name = "Ported-In")]
        [Range(0, 10000)]
        public int PortedIn { get; set; }

        [Display(Name = "Notes")]
        public string Notes { get; set; }

        [Display(Name = "Available")]
        public int Available { get; set; }

        [Display(Name = "Utilization")]
        public string Utilization { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? createdDate { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? dateChanged { get; set; }

        public int Calculate(out string message)
        {
            float result;

            if (Assigned + Aging != PortedIn)
            {
                message = "Error!, Sum of Assigned and Aging numbers not equal to the Ported In value";
                return 0;
            }
            else
            {
                Available = 0;

            }

            if (PortedIn == 0)
            {
                Utilization = "0";
            }
            else
            {
                result = Assigned / PortedIn;
                Utilization = result.ToString("#0.##%", CultureInfo.InvariantCulture);
            }

            message = "";
            return 1;

        }
    }
}
