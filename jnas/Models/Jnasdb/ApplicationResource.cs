namespace jnas.Models.Jnasdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ApplicationResource
    {
        [ScaffoldColumn(false)]
        public int applicationResourceId { get; set; }

        [Display(Name = "Application")]
        public int? applicationId { get; set; }

        [ScaffoldColumn(false)]
        public string application { get; set; }

        [Display(Name = "Resource")]
        public int? resourceId { get; set; }

        [ScaffoldColumn(false)]
        public string resource { get; set; }

        [Display(Name = "Date Created")]
        public DateTime? createdDate { get; set; }

        [Display(Name = "Date Chaged")]
        public DateTime? dateChanged { get; set; }

        [Display(Name = "State")]
        public int? applicationResourceStateId { get; set; }

        [ScaffoldColumn(false)]
        public string state { get; set; }

        //[ScaffoldColumn(false)]
        //public int Application_applicationId { get; set; }

        //[ScaffoldColumn(false)]
        //public int? ApplicationResourceState_applicationResourceStateId { get; set; }

        //[ScaffoldColumn(false)]
        //public int ApplicationResourceState_applicationResourceStateId1 { get; set; }

        //[ScaffoldColumn(false)]
        //public int? Resource_resourceId { get; set; }

        //[ScaffoldColumn(false)]
        //public int Resource_resourceId1 { get; set; }

        //[ScaffoldColumn(false)]
        //public int? Application_applicationId1 { get; set; }

       // [ScaffoldColumn(false)]
       // public int? Organization_organizationId { get; set; }
        //[ForeignKey("applicationResourceStateId")]
        //public virtual ApplicationResourceState ApplicationResourceState { get; set; }

        //  public virtual ApplicationResourceState ApplicationResourceState1 { get; set; }
        [ForeignKey("applicationId")]
        public virtual Application Application1 { get; set; }

        //  public virtual Application Application2 { get; set; }

        // public virtual Organization Organization { get; set; }
        [ForeignKey("resourceId")]
        public virtual Resource Resource1 { get; set; }

     //   public virtual Resource Resource2 { get; set; }
    }
}
