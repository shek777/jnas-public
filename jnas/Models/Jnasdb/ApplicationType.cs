namespace jnas.Models.Jnasdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ApplicationType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ApplicationType()
        {
            Applications = new HashSet<Application>();
        }

        [ScaffoldColumn(false)]
        public int applicationTypeId { get; set; }

        [Required(ErrorMessage = "Application Type is required")]
        [Display(Name = "Application Type")]
        public string name { get; set; }

        [Required(ErrorMessage = "Abbreviation is required")]
        [Display(Name = "Abbreviation")]
        [RegularExpression(@"^[A-Z]{3,5}$", ErrorMessage = "Invalid Abbreviation")]
        public string abbreviation { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Application> Applications { get; set; }
    }
}
