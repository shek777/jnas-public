namespace jnas.Models.Jnasdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UserNotification
    {
        [ScaffoldColumn(false)]
        public int userNotificationId { get; set; }

        [Required]
        [StringLength(128)]
        public string UserId { get; set; }

        [ScaffoldColumn(false)]
        public string user { get; set; }

        [Required]
        [Display(Name = "Type of Notification")]
        public int notificationId { get; set; }

        [ScaffoldColumn(false)]
        public string notification { get; set; }

        [Required]
        [Display(Name = "Assigned to User (Yes/ No)")]
        [DefaultValue(true)]
        public bool assigned { get; set; }

        public virtual Notification Notification1 { get; set; }

        public virtual User User1 { get; set; }
    }
}
