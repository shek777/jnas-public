namespace jnas.Models.Jnasdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UserRole
    {
        [Key]
        [Column(Order = 0)]
        [Display(Name = "User")]
        public string UserId { get; set; }

        [Key]
        [Column(Order = 1)]
        [Display(Name = "Role")]
        public string RoleId { get; set; }

       // [Required]
        [StringLength(128)]
        public string Discriminator { get; set; }

        public virtual Role Role { get; set; }

        public virtual User User { get; set; }
    }
}
