namespace jnas.Models.Jnasdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Globalization;
    public partial class Organization
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Organization()
        {
            Addresses = new HashSet<Address>();
            ApplicationResources = new HashSet<ApplicationResource>();
            Applications = new HashSet<Application>();
            OrganizationResourceTypes = new HashSet<OrganizationResourceType>();
            Users = new HashSet<User>();
        }

        [ScaffoldColumn(false)]
        public int organizationId { get; set; }

        [Required]
        [Display(Name = "Name of Organization")]
        public string name { get; set; }

        [Required]
        [Display(Name = "Type of Organization")]
        public int organizationTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string organizationType { get; set; }

        [Display(Name = "Parent Company Name")]
        public string ParentCompany { get; set; }

        [Display(Name = "Parent Company OCN(s)")]
        [RegularExpression(@"^[0-9][0-9|A-Z]{3}$", ErrorMessage = "Invalid OCN")]
        public string ParentCompanyOCN { get; set; }

        [Display(Name = "Service Provider OCN")]
        [RegularExpression(@"^[0-9][0-9|A-Z]{3}$", ErrorMessage = "Invalid OCN")]
        public string OCN { get; set; }

        [Display(Name = "Service Provider OFN")]
        [RegularExpression(@"^[0-9][0-9|A-Z]{3}$", ErrorMessage = "Invalid OFN")]
        public string OFN { get; set; }

        [Display(Name = "Created Date")]
        //[ScaffoldColumn(false)]
        public System.DateTime? createdDate { get; set; }

        [Display(Name = "Date Changed")]
        //[ScaffoldColumn(false)]
        public System.DateTime? dateChanged { get; set; }

        [Required]
        [Display(Name = "State")]
        public int organizationStateId { get; set; }

        [ScaffoldColumn(false)]
        public string state { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Address> Addresses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ApplicationResource> ApplicationResources { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Application> Applications { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrganizationResourceType> OrganizationResourceTypes { get; set; }

        public virtual OrganizationState OrganizationState { get; set; }

        public virtual OrganizationType OrganizationType1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User> Users { get; set; }
    }
     

}
