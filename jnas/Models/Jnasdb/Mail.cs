namespace jnas.Models.Jnasdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Mail
    {
        [ScaffoldColumn(false)]
        public long mailId { get; set; }

        [Required]
        [Display(Name = "Title")]
        public string title { get; set; }

        [Required]
        [Display(Name = "To")]
        public string toAddress { get; set; }

        [Required]
        [Display(Name = "From")]
        public string fromAddress { get; set; }

        [Required]
        [Display(Name = "Message")]
        public string message { get; set; }

        [Display(Name = "Attachment")]
        public byte[] attachment { get; set; }

        [Display(Name = "Date Created")]
        public System.DateTime? createdDate { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? dateChanged { get; set; }
    }
}
