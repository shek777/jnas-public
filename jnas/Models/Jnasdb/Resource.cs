namespace jnas.Models.Jnasdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Resource
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Resource()
        {
            ApplicationResources = new HashSet<ApplicationResource>();
          //  ApplicationResources1 = new HashSet<ApplicationResource>();
          //  ResourceLinks = new HashSet<ResourceLink>();
         //   ResourceLinks1 = new HashSet<ResourceLink>();
        }

        [ScaffoldColumn(false)]
        public int resourceId { get; set; }

        [Required]
        [Display(Name = "Resource")]
        public string resource1 { get; set; }

        // [Required]
        [Display(Name = "Resource Type")]
        public int? resourceTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string type { get; set; }

        // [Required]
        [Display(Name = "Resource Sub Type")]
        public int? resourceSubTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string subtype { get; set; }

        [Display(Name = "Binary")]
        public string binary { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? createdDate { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? dateChanged { get; set; }

        [Display(Name = "Resource Link")]
        public int? resourceLinkId { get; set; }

        [Display(Name = "Resource State")]
        public int resourceStateId { get; set; }

        [ScaffoldColumn(false)]
        public string state { get; set; }

        //[ScaffoldColumn(false)]
        //public int? ResourceState_resourceStateId { get; set; }

        //[ScaffoldColumn(false)]
        //public int ResourceState_resourceStateId1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ApplicationResource> ApplicationResources { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<ApplicationResource> ApplicationResources1 { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<ResourceLink> ResourceLinks { get; set; }

        public virtual ResourceLink ResourceLinks { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<ResourceLink> ResourceLinks1 { get; set; }

        public virtual ResourceState ResourceState { get; set; }

       // public virtual ResourceState ResourceState1 { get; set; }
    }
}
