﻿namespace jnas.Models.Jnasdb
{

    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;


    public partial class ReportOnNXXAndNPACodes
    {

        public ReportOnNXXAndNPACodes()
        {

        }

        [ScaffoldColumn(false)]
        public int ReportOnNXXAndNPACodesId { get; set; }

        public int NPA { get; set; }

        [Display(Name = "NX\\X")]
        public int NX { get; set; }

        [Display(Name = "Service Allocation")]
        public string ServiceAllocation { get; set; }

        [Display(Name = "0")]
        public string X_0 { get; set; }

        [ScaffoldColumn(false)]
        public string X_0_CellStyle { get; set; }

        [Display(Name = "1")]
        public string X_1 { get; set; }

        [ScaffoldColumn(false)]
        public string X_1_CellStyle { get; set; }

        [Display(Name = "2")]
        public string X_2 { get; set; }

        [ScaffoldColumn(false)]
        public string X_2_CellStyle { get; set; }

        [Display(Name = "3")]
        public string X_3 { get; set; }

        [ScaffoldColumn(false)]
        public string X_3_CellStyle { get; set; }

        [Display(Name = "4")]
        public string X_4 { get; set; }

        [ScaffoldColumn(false)]
        public string X_4_CellStyle { get; set; }

        [Display(Name = "5")]
        public string X_5 { get; set; }

        [ScaffoldColumn(false)]
        public string X_5_CellStyle { get; set; }

        [Display(Name = "6")]
        public string X_6 { get; set; }

        [ScaffoldColumn(false)]
        public string X_6_CellStyle { get; set; }

        [Display(Name = "7")]
        public string X_7 { get; set; }

        [ScaffoldColumn(false)]
        public string X_7_CellStyle { get; set; }

        [Display(Name = "8")]
        public string X_8 { get; set; }

        [ScaffoldColumn(false)]
        public string X_8_CellStyle { get; set; }

        [Display(Name = "9")]
        public string X_9 { get; set; }

        [ScaffoldColumn(false)]
        public string X_9_CellStyle { get; set; }

        [Display(Name = "Date Created")]
        public System.DateTime? DateCreated { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? DateChanged { get; set; }

    }


    public partial class ReportOnNXXAndNPACodesKey
    {
        public ReportOnNXXAndNPACodesKey()
        {

        }

        [ScaffoldColumn(false)]
        public int ReportOnNXXAndNPACodesKeyId { get; set; }

        public string Key { get; set; }

        [Display(Name = "Text")]
        public string CellText { get; set; }

        [Display(Name = "Style")]
        public string CellStyle { get; set; }

        [Display(Name = "Date Created")]
        public System.DateTime? DateCreated { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? DateChanged { get; set; }
    }

    public partial class CentralOfficeCodeAssignments
    {
        public CentralOfficeCodeAssignments()
        {

        }

        [ScaffoldColumn(false)]
        public int CentralOfficeCodeAssignmentsId { get; set; }

        [Display(Name = "Name of Organization")]
        public int organizationId { get; set; }


        public string NPA { get; set; }

        public string NXX { get; set; }

        [Display(Name = "Date Created")]
        public System.DateTime? DateCreated { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? DateChanged { get; set; }

        public virtual Organization Organization1 { get; set; }

    }

}