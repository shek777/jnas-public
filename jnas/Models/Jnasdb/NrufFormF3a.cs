﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jnas.Models.Jnasdb
{
    public partial class NrufFormF3a
    {

        public NrufFormF3a()
        {
            Year1 = 0;
            Year2 = 0;
            Year3 = 0;
            Year4 = 0;
            Year5 = 0;
        }

        public virtual NrufCompanyInfo NrufCompanyInfo { get; set; }

        [ScaffoldColumn(false)]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FormF3aId { get; set; }

        [ScaffoldColumn(false)]
        public int NrufCompanyInfoId { get; set; }

        [Required(ErrorMessage = "NPA is required")]
        [Display(Name = "NPA")]
        [RegularExpression(@"^[2-9][0-9]{2}$", ErrorMessage = "Invalid NPA")]
        public string Npa { get; set; }

        [Required(ErrorMessage = "Add an integer value, for example; 0 or 500")]
        [Display(Name = "Year 1")]
        [Range(0, 200)]
        public int Year1 { get; set; }

        [Required(ErrorMessage = "Add an integer value, for example; 0 or 500")]
        [Display(Name = "Year 2")]
        [Range(0, 200)]
        public int Year2 { get; set; }

        [Required(ErrorMessage = "Add an integer value, for example; 0 or 500")]
        [Display(Name = "Year 3")]
        [Range(0, 200)]
        public int Year3 { get; set; }

        [Required(ErrorMessage = "Add an integer value, for example; 0 or 500")]
        [Display(Name = "Year 4")]
        [Range(0, 600)]
        public int Year4 { get; set; }

        [Required(ErrorMessage = "Add an integer value, for example; 0 or 500")]
        [Display(Name = "Year 5")]
        [Range(0, 200)]
        public int Year5 { get; set; }

        [Display(Name = "Total NXX(s)")]
        [Range(0, 200)]
        public int Total { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? createdDate { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? dateChanged { get; set; }

        public int Calculate(out string message)
        {


            if ((Year1 + Year2 + Year3 + Year4 + Year5) > 200)
            {
                message = "Error!, Sum of numbers is greater than 200 ";
                return 0;
            }
            else
            {
                Total = Year1 + Year2 + Year3 + Year4 + Year5;

            }


            message = "";
            return 1;

        }
    }
}
