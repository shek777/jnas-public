namespace jnas.Models.Jnasdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UserClaim
    {
        public int UserClaimId { get; set; }

        [Required]
        [StringLength(128)]
        public string UserId { get; set; }

        [Display(Name = "Type")]
        public string ClaimType { get; set; }

        [Display(Name = "Value")]
        public string ClaimValue { get; set; }

        public virtual User User { get; set; }
    }
}
