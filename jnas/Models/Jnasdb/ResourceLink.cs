namespace jnas.Models.Jnasdb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ResourceLink
    {

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ResourceLink()
        {
            Resource = new HashSet<Resource>();
            //  ApplicationResources1 = new HashSet<ApplicationResource>();
            //  ResourceLinks = new HashSet<ResourceLink>();
            //   ResourceLinks1 = new HashSet<ResourceLink>();
        }

        [ScaffoldColumn(false)]
        public int resourceLinkId { get; set; }

        [Display(Name = "Parent Resource")]
        public string parentResource { get; set; }

        [Display(Name = "Child Resource")]
        public string childResource { get; set; }

        [Display(Name = "State")]
        public int linkedResourceStateId { get; set; }

        [ScaffoldColumn(false)]
        public string state { get; set; }

        //[ScaffoldColumn(false)]
        //public int? LinkedResourceState_linkedResourceStateId { get; set; }

        //[ScaffoldColumn(false)]
        //public int LinkedResourceState_linkedResourceStateId1 { get; set; }

        //[ScaffoldColumn(false)]
        //public int Resource_resourceId { get; set; }

        //[ScaffoldColumn(false)]
        //public int? Resource_resourceId1 { get; set; }

        public virtual LinkedResourceState LinkedResourceState { get; set; }

       // public virtual LinkedResourceState LinkedResourceState1 { get; set; }

        public virtual ICollection<Resource> Resource { get; set; }

      //  public virtual Resource Resource1 { get; set; }
    }
}
