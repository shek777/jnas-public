﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Infrastructure;
using jnas.Configuration;

namespace jnas.Models
{
    // You can add profile data for the user by adding more properties to your User class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class User : IdentityUser
    {

        public User()
        {
            this.UserNotification = new HashSet<UserNotification>();
            this.Application = new HashSet<Application>();
        }

        public virtual ICollection<UserNotification> UserNotification { get; set; }
        public virtual ICollection<Application> Application { get; set; }

        public virtual UserState UserState { get; set; }
        public virtual UserType UserType { get; set; }
        public virtual Organization Organization { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string name { get; set; }

        [ScaffoldColumn(false)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int userNo { get; set; }

        [Required]
        [Display(Name = "Organization")]
        public int organizationId { get; set; }

        [ScaffoldColumn(false)]
        public string organization { get; set; }

        [Required]
        [Display(Name = "User Type")]
        public int userTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string userType { get; set; }

        [Required]
        [Display(Name = "User Profile")]
        public int userProfileId { get; set; }

        [ScaffoldColumn(false)]
        public string userProfile { get; set; }

        [Required]
        [Display(Name = "Primary Contact (Yes/ No)")]
        //  [Range(typeof(bool), "true", "true", ErrorMessage = "Please indicate the primary contact")]
        [DefaultValue(false)]
        public bool primaryContact { get; set; }

        [Required]
        [Display(Name = "Security Question")]
        public string securityQuestion { get; set; }

        [Required]
        [Display(Name = "Security Answer")]
        public string securityAnswer { get; set; }

        [ScaffoldColumn(false)]
        public DateTime? createdDate { get; set; }

        [ScaffoldColumn(false)]
        public DateTime? dateChanged { get; set; }

        [ScaffoldColumn(false)]
        public DateTime? lastConnectionDate { get; set; }

        [Required]
        [Display(Name = "User State")]
        public int userStateId { get; set; }

        [ScaffoldColumn(false)]
        public string state { get; set; }


        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class UserRoles : IdentityUserRole
    {

    }

        public partial class Address
    {
        public Address()
        {
            this.OrganizationServices = new HashSet<OrganizationServices>();
        }

        public virtual ICollection<OrganizationServices> OrganizationServices { get; set; }

        [ScaffoldColumn(false)]
        public int AddressId { get; set; }

        [Required]
        [Display(Name = "Organization")]
        public int organizationId { get; set; }

        [ScaffoldColumn(false)]
        public string organization { get; set; }

        [Required]
        [Display(Name = "Address 1")]
        public string line1 { get; set; }

        [Display(Name = "Address 2")]
        public string line2 { get; set; }

        [Display(Name = "City")]
        public string city { get; set; }

        [Display(Name = "Zip Code")]
        public string zipCode { get; set; }

        [Required]
        [Display(Name = "Default Address (Yes/ No)")]
        [DefaultValue(false)]
        public bool defaultAddress { get; set; }

        public virtual Organization Organization { get; set; }
    }

    public partial class Application
    {
        public Application()
        {
            this.ApplicationNotes = new HashSet<ApplicationNotes>();
            this.ApplicationResource = new HashSet<ApplicationResource>();
        }

        public virtual ICollection<ApplicationNotes> ApplicationNotes { get; set; }
        public virtual ICollection<ApplicationResource> ApplicationResource { get; set; }

        public virtual ApplicationState ApplicationState { get; set; }
        public virtual ApplicationType ApplicationType { get; set; }
        public virtual Organization Organization { get; set; }
        public virtual User User { get; set; }
        public virtual ResourceType ResourceType { get; set; }
        public virtual ResourceSubType ResourceSubType { get; set; }

        [ScaffoldColumn(false)]
        public int applicationId { get; set; }

        [Display(Name = "Organization")]
        public int organizationId { get; set; }

        [ScaffoldColumn(false)]
        public string application1 { get; set; }

        [Required]
        [Display(Name = "Application Reference")]
        public string applicationReference { get; set; }

        [Display(Name = "Applicant")]
        public string applicant { get; set; }

        [Display(Name = "Recipient")]
        public string recipient { get; set; }

        [Required]
        [Display(Name = "User")]
        public string UserId { get; set; }

        [ScaffoldColumn(false)]
        public string user { get; set; }

        [Required]
        [Display(Name = "Service")]
        public int serviceId { get; set; }

        [ScaffoldColumn(false)]
        public string service { get; set; }

        [Display(Name = "Resource Type")]
        public int resourceTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string resourceType { get; set; }

        [Display(Name = "Resource Sub Type")]
        public int resourceSubTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string resourceSubType { get; set; }

        [Display(Name = "Subscriber")]
        public string subscriber { get; set; }

        [Display(Name = "Contact Number")]
        public string customerContactNumber { get; set; }

        [Display(Name = "Local Routing Number")]
        public string localRoutingNumber { get; set; }

        [ScaffoldColumn(false)]
        [StringLength(255)]
        public string FileName { get; set; }

        [ScaffoldColumn(false)]
        [StringLength(100)]
        public string ContentType { get; set; }

        [Display(Name = "Additional Documentation")]
        public byte[] additionalDocumentation { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? createdDate { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? dateChanged { get; set; }

        [Display(Name = "Resources")]
        public int applicationResourceId { get; set; }

        [Display(Name = "Type of Application")]
        public int applicationTypeId { get; set; }

        [Display(Name = "State of Application")]
        public int applicationStateId { get; set; }

        [ScaffoldColumn(false)]
        public string state { get; set; }


    }

    public partial class ApplicationNotes
    {
        [ScaffoldColumn(false)]
        public int applicationNotesId { get; set; }

        [Required]
        [Display(Name = "Application")]
        public int applicationId { get; set; }

        [ScaffoldColumn(false)]
        public string application { get; set; }

        [Required]
        [Display(Name = "Note")]
        public string note { get; set; }

        [Required]
        [Display(Name = "User")]
        public string UserId { get; set; }

        [ScaffoldColumn(false)]
        public string user { get; set; }

        [Display(Name = "Internal Note")]
        public string internalNote { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? createdDate { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? dateChanged { get; set; }

        public virtual Application Application { get; set; }
    }

    public partial class ApplicationResource
    {
        [ScaffoldColumn(false)]
        public int applicationResourceId { get; set; }

       // [Required]
        [Display(Name = "Application")]
        public int applicationId { get; set; }

        [ScaffoldColumn(false)]
        public string application { get; set; }

        //[Required]
        [Display(Name = "Resource")]
        public int resourceId { get; set; }

        [ScaffoldColumn(false)]
        public string resource { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? createdDate { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? dateChanged { get; set; }

        [Display(Name = "State")]
        public int applicationResourceStateId { get; set; }

        [ScaffoldColumn(false)]
        public string state { get; set; }

        public virtual Application Application { get; set; }
        public virtual Resource Resource { get; set; }
        public virtual ApplicationResourceState ApplicationResourceState { get; set; }
    }

    public partial class ApplicationResourceState
    {
        public ApplicationResourceState()
        {
            this.ApplicationResource = new HashSet<ApplicationResource>();
        }

        public virtual ICollection<ApplicationResource> ApplicationResource { get; set; }

        [ScaffoldColumn(false)]
        public int applicationResourceStateId { get; set; }

        [Display(Name = "Name")]
        public string name { get; set; }
    }

    public partial class ApplicationState
    {
        public ApplicationState()
        {
            this.Application = new HashSet<Application>();
        }

        public virtual ICollection<Application> Application { get; set; }

        [ScaffoldColumn(false)]
        public int applicationStateId { get; set; }

        [Required]
        [Display(Name = "Application State")]
        public string name { get; set; }
    }

    public partial class ApplicationType
    {
        public ApplicationType()
        {
            this.Application = new HashSet<Application>();
        }

        public virtual ICollection<Application> Application { get; set; }

        [ScaffoldColumn(false)]
        public int applicationTypeId { get; set; }

        [Required]
        [Display(Name = "Application Type")]
        public string name { get; set; }

        [Required]
        [Display(Name = "Abbreviation")]
        public string abbreviation { get; set; }
    }

    public partial class LinkedResourceState
    {
        public LinkedResourceState()
        {
            this.ResourceLink = new HashSet<ResourceLink>();
            this.OrganizationResourceType = new HashSet<OrganizationResourceType>();
          //  this.UserResourceType = new HashSet<UserResourceType>();
            this.NotificationDetail = new HashSet<NotificationDetail>();
        }

        public virtual ICollection<ResourceLink> ResourceLink { get; set; }
        public virtual ICollection<OrganizationResourceType> OrganizationResourceType { get; set; }
       // public virtual ICollection<UserResourceType> UserResourceType { get; set; }
        public virtual ICollection<NotificationDetail> NotificationDetail { get; set; }

        [ScaffoldColumn(false)]
        public int linkedResourceStateId { get; set; }

        [Display(Name = "Name")]
        public string name { get; set; }
    }

    public partial class Mail
    {
        [ScaffoldColumn(false)]
        public long mailId { get; set; }

        [Required]
        [Display(Name = "Title")]
        public string title { get; set; }

        [Required]
        [Display(Name = "To")]
        public string toAddress { get; set; }

        [Required]
        [Display(Name = "From")]
        public string fromAddress { get; set; }

        [Required]
        [Display(Name = "Message")]
        public string message { get; set; }

        [Display(Name = "Attachment")]
        public byte[] attachment { get; set; }

        [Display(Name = "Date Created")]
        public System.DateTime? createdDate { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? dateChanged { get; set; }
    }

    public partial class Notification
    {
        public Notification()
        {
            this.NotificationDetail = new HashSet<NotificationDetail>();
            this.UserNotification = new HashSet<UserNotification>();
        }

        public virtual ICollection<NotificationDetail> NotificationDetail { get; set; }
        public virtual ICollection<UserNotification> UserNotification { get; set; }

        [ScaffoldColumn(false)]
        public int notificationId { get; set; }

        [Required]
        [Display(Name = "Notification")]
        public string name { get; set; }

        [Required]
        [Display(Name = "Mandatory (Yes/ No)")]
        [DefaultValue(false)]
        public bool mandatory { get; set; }
    }

    public partial class NotificationDetail
    {
        [ScaffoldColumn(false)]
        public long notificationDetailId { get; set; }

        [Required]
        [Display(Name = "Type of Notification")]
        public int notificationId { get; set; }

        [ScaffoldColumn(false)]
        public string notification { get; set; }

        [Required]
        [Display(Name = "Type of User")]
        public int userTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string userType { get; set; }

        [Display(Name = "Resource Type")]
        public int resourceTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string resourceType { get; set; }

        [Display(Name = "Resource Sub Type")]
        public int resourceSubTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string resourceSubType { get; set; }

        [Display(Name = "State")]
        public int linkedResourceStateId { get; set; }

        [ScaffoldColumn(false)]
        public string state { get; set; }

        public virtual UserType UserType { get; set; }
        public virtual Notification Notification { get; set; }
        public virtual ResourceType ResourceType { get; set; }
        public virtual ResourceSubType ResourceSubType { get; set; }
        public virtual LinkedResourceState LinkedResourceState { get; set; }
    }

    public partial class Organization
    {
        public Organization()
        {
            this.User = new HashSet<User>();
            this.Address = new HashSet<Address>();
            this.Application = new HashSet<Application>();
            this.ApplicationResource = new HashSet<ApplicationResource>();
            this.OrganizationResourceType = new HashSet<OrganizationResourceType>();
        }

        public virtual ICollection<User> User { get; set; }
        public virtual ICollection<Address> Address { get; set; }
        public virtual ICollection<Application> Application { get; set; }
        public virtual ICollection<ApplicationResource> ApplicationResource { get; set; }
        public virtual ICollection<OrganizationResourceType> OrganizationResourceType { get; set; }

        public virtual OrganizationState OrganizationState { get; set; }
        public virtual OrganizationType OrganizationType { get; set; }

        [ScaffoldColumn(false)]
        public int organizationId { get; set; }

        [Required]
        [Display(Name = "Name of Organization")]
        public string name { get; set; }

        [Required]
        [Display(Name = "Type of Organization")]
        public int organizationTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string organizationType { get; set; }

        [Display(Name = "Parent Company Name")]
        public string ParentCompany { get; set; }

        [Display(Name = "Parent Company OCN(s)")]
        public string ParentCompanyOCN { get; set; }

        [Display(Name = "Service Provider OCN")]
        public string OCN { get; set; }

        [Display(Name = "Service Provider OFN")]
        public string OFN { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? createdDate { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? dateChanged { get; set; }

        [Required]
        [Display(Name = "State")]
        public int organizationStateId { get; set; }

        [ScaffoldColumn(false)]
        public string state { get; set; }
    }

    public partial class NrufCompanyInfo
    {
        public NrufCompanyInfo()
        {
            this.NrufFormU1 = new HashSet<NrufFormU1>();
            this.NrufFormU2 = new HashSet<NrufFormU2>();
            this.NrufFormU3 = new HashSet<NrufFormU3>();
            this.NrufFormU4 = new HashSet<NrufFormU4>();
            this.NrufFormF3a = new HashSet<NrufFormF3a>();
            this.NrufFormF3b = new HashSet<NrufFormF3b>();
        }

        public virtual ICollection<NrufFormU1> NrufFormU1 { get; set; }
        public virtual ICollection<NrufFormU2> NrufFormU2 { get; set; }
        public virtual ICollection<NrufFormU3> NrufFormU3 { get; set; }
        public virtual ICollection<NrufFormU4> NrufFormU4 { get; set; }
        public virtual ICollection<NrufFormF3a> NrufFormF3a { get; set; }
        public virtual ICollection<NrufFormF3b> NrufFormF3b { get; set; }

        public virtual ProviderService ProviderService { get; set; }


        [ScaffoldColumn(false)]
        public int NrufCompanyInfoId { get; set; }

        [ScaffoldColumn(false)]
        public int organizationId { get; set; }

        [ScaffoldColumn(false)]
        public string UserId { get; set; }

        [ScaffoldColumn(false)]
        public string applicationReference { get; set; }

        [ScaffoldColumn(false)]
        public int ProviderServiceId { get; set; }

       // [Required]
        [Display(Name = "Service Provider Name")]
        public string name { get; set; }

        [Display(Name = "Parent Company Name")]
        public string ParentCompany { get; set; }

        [Display(Name = "Parent Company OCN(s)")]
        public string ParentCompanyOCN { get; set; }

       // [Required]
        [Display(Name = "Service Provider OCN")]
        public string OCN { get; set; }

        [Display(Name = "Service Provider OFN")]
        public string OFN { get; set; }

       // [Required]
        [Display(Name = "Company Address")]
        public string line1 { get; set; }

        [Display(Name = "Address 2")]
        public string line2 { get; set; }

        [Display(Name = "Address 3")]
        public string line3 { get; set; }

        [Display(Name = "Contact Name")]
        public string ContactName { get; set; }

        [Display(Name = "Contact Tel #")]
        public string ContactTelNo { get; set; }

        [Display(Name = "Fax #")]
        public string ContactFaxNo { get; set; }

        [Display(Name = "E-mail")]
        public string ContactEmail { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }

        [Display(Name = "Expiration Date")]
        public DateTime? expiryDate { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? createdDate { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? dateChanged { get; set; }


    }

    public partial class NrufFormU1
    {
        public virtual NrufCompanyInfo NrufCompanyInfo { get; set; }

        [ScaffoldColumn(false)]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FormU1Id { get; set; }

        [ScaffoldColumn(false)]
        public int NrufCompanyInfoId { get; set; }

        [Display(Name = "NPA")]
        public string Npa { get; set; }

        [Display(Name = "NXX")]
        public string Nxx { get; set; }

        [Display(Name = "X")]
        public int X { get; set; }

        [Display(Name = "Rate Centre Abreviation")]
        public string Rate { get; set; }

        [Display(Name = "Assigned")]
        public int Assigned { get; set; }

        [Display(Name = "Intermediate")]
        public int Intermediate { get; set; }

        [Display(Name = "Reserved")]
        public int Reserved { get; set; }

        [Display(Name = "Aging")]
        public int Aging { get; set; }

        [Display(Name = "Admin")]
        public int Admin { get; set; }

        [Display(Name = "Ported-Out")]
        public int PortedOut { get; set; }

        [Display(Name = "Notes/ Assignee (Intermediate Carrier)")]
        public string Notes { get; set; }

        [Display(Name = "Available")]
        public int Available { get; set; }

        [Display(Name = "Utilization")]
        public string Utilization { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? createdDate { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? dateChanged { get; set; }
    }

    public partial class NrufFormF3b
    {
        public virtual NrufCompanyInfo NrufCompanyInfo { get; set; }

        [ScaffoldColumn(false)]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FormF3bId { get; set; }

        [ScaffoldColumn(false)]
        public int NrufCompanyInfoId { get; set; }

        [Display(Name = "NPA")]
        public string Npa { get; set; }

        [Display(Name = "Year 1")]
        public int Year1 { get; set; }

        [Display(Name = "Year 2")]
        public int Year2 { get; set; }

        [Display(Name = "Year 3")]
        public int Year3 { get; set; }

        [Display(Name = "Year 4")]
        public int Year4 { get; set; }

        [Display(Name = "Year 5")]
        public int Year5 { get; set; }

        [Display(Name = "Total NXX(s)")]
        public int Total { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? createdDate { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? dateChanged { get; set; }


    }

    public partial class NrufFormU2
    {
        public virtual NrufCompanyInfo NrufCompanyInfo { get; set; }

        [ScaffoldColumn(false)]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FormU2Id { get; set; }

        [ScaffoldColumn(false)]
        public int NrufCompanyInfoId { get; set; }

        [Display(Name = "NPA")]
        public string Npa { get; set; }

        [Display(Name = "NXX")]
        public string Nxx { get; set; }

        [Display(Name = "Rate Centre Abreviation")]
        public string Rate { get; set; }

        [Display(Name = "Assigned")]
        public int Assigned { get; set; }

        [Display(Name = "Intermediate")]
        public int Intermediate { get; set; }

        [Display(Name = "Reserved")]
        public int Reserved { get; set; }

        [Display(Name = "Aging")]
        public int Aging { get; set; }

        [Display(Name = "Admin")]
        public int Admin { get; set; }

        [Display(Name = "Ported-Out")]
        public int PortedOut { get; set; }

        [Display(Name = "Notes/ Assignee (Intermediate Carrier)")]
        public string Notes { get; set; }

        [Display(Name = "Available")]
        public int Available { get; set; }

        [Display(Name = "Utilization")]
        public string Utilization { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? createdDate { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? dateChanged { get; set; }
    }

    public partial class NrufFormU3
    {
        public virtual NrufCompanyInfo NrufCompanyInfo { get; set; }

        [ScaffoldColumn(false)]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FormU3Id { get; set; }

        [ScaffoldColumn(false)]
        public int NrufCompanyInfoId { get; set; }

        [Display(Name = "NPA")]
        public string Npa { get; set; }

        [Display(Name = "NXX")]
        public string Nxx { get; set; }

        [Display(Name = "X")]
        public int X { get; set; }

        [Display(Name = "Rate Centre Abreviation")]
        public string Rate { get; set; }

        [Display(Name = "Assigned")]
        public int Assigned { get; set; }

        [Display(Name = "Numbers Received")]
        public int NumbersReceived { get; set; }

        [Display(Name = "Reserved")]
        public int Reserved { get; set; }

        [Display(Name = "Aging")]
        public int Aging { get; set; }

        [Display(Name = "Admin")]
        public int Admin { get; set; }

        [Display(Name = "Ported-Out")]
        public int PortedOut { get; set; }

        [Display(Name = "Notes/ Assignee (Intermediate Carrier)")]
        public string Notes { get; set; }

        [Display(Name = "Available")]
        public int Available { get; set; }

        [Display(Name = "Utilization")]
        public string Utilization { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? createdDate { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? dateChanged { get; set; }
    }

    public partial class NrufFormU4
    {
        public virtual NrufCompanyInfo NrufCompanyInfo { get; set; }

        [ScaffoldColumn(false)]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FormU4Id { get; set; }

        [ScaffoldColumn(false)]
        public int NrufCompanyInfoId { get; set; }

        [Display(Name = "NPA")]
        public string Npa { get; set; }

        [Display(Name = "NXX")]
        public string Nxx { get; set; }

        [Display(Name = "Rate Centre Abreviation")]
        public string Rate { get; set; }

        [Display(Name = "Assigned")]
        public int Assigned { get; set; }

        [Display(Name = "Aging")]
        public int Aging { get; set; }

        [Display(Name = "Ported-In")]
        public int PortedIn { get; set; }

        [Display(Name = "Notes")]
        public string Notes { get; set; }

        [Display(Name = "Available")]
        public int Available { get; set; }

        [Display(Name = "Utilization")]
        public string Utilization { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? createdDate { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? dateChanged { get; set; }
    }

    public partial class NrufFormF3a
    {
        public virtual NrufCompanyInfo NrufCompanyInfo { get; set; }

        [ScaffoldColumn(false)]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FormF3aId { get; set; }

        [ScaffoldColumn(false)]
        public int NrufCompanyInfoId { get; set; }

        [Display(Name = "NPA")]
        public string Npa { get; set; }

        [Display(Name = "Year 1")]
        public int Year1 { get; set; }

        [Display(Name = "Year 2")]
        public int Year2 { get; set; }

        [Display(Name = "Year 3")]
        public int Year3 { get; set; }

        [Display(Name = "Year 4")]
        public int Year4 { get; set; }

        [Display(Name = "Year 5")]
        public int Year5 { get; set; }

        [Display(Name = "Total NXX(s)")]
        public int Total { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? createdDate { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? dateChanged { get; set; }
    }



    // TODO: Add the default value for rates and the methods that do the calculation

    public partial class OrganizationResourceType
    {
        [ScaffoldColumn(false)]
        public int organizationResourceTypeId { get; set; }

        [Required]
        [Display(Name = "Organization")]
        public int organizationId { get; set; }

        [ScaffoldColumn(false)]
        public string organization { get; set; }

        [Display(Name = "Resource Type")]
        public int resourceTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string resourceType { get; set; }

        [Display(Name = "Resource Sub Type")]
        public int resourceSubTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string resourceSubType { get; set; }

        [Display(Name = "State")]
        public int linkedResourceStateId { get; set; }

        [ScaffoldColumn(false)]
        public string state { get; set; }

        public virtual Organization Organization { get; set; }
        public virtual LinkedResourceState LinkedResourceState { get; set; }
        public virtual ResourceType ResourceType { get; set; }
        public virtual ResourceSubType ResourceSubType { get; set; }
    }

    public partial class OrganizationServices
    {
        [ScaffoldColumn(false)]
        public int organizationServicesId { get; set; }

        [Required]
        [Display(Name = "Address")]
        public int AddressId { get; set; }

        [Required]
        [Display(Name = "Provider Service")]
        public int ProviderServiceId { get; set; }

        [Display(Name = "Organization")]
        public string organization { get; set; }

        [ScaffoldColumn(false)]
        public string providerService { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? createdDate { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? dateChanged { get; set; }

        public virtual Address Address { get; set; }
        public virtual ProviderService ProviderService { get; set; }
    }

    public partial class OrganizationState
    {

        public OrganizationState()
        {
            this.Organization = new HashSet<Organization>();
        }

        [ScaffoldColumn(false)]
        public int organizationStateId { get; set; }

        [Display(Name = "State")]
        public string name { get; set; }

        public virtual ICollection<Organization> Organization { get; set; }
    }

    public partial class OrganizationType
    {
        public OrganizationType()
        {
            this.Organization = new HashSet<Organization>();
            this.OrganizationUserType = new HashSet<OrganizationUserType>();
        }

        [ScaffoldColumn(false)]
        public int organizationTypeId { get; set; }

        [Display(Name = "Type of Organization")]
        public string name { get; set; }

        public virtual ICollection<Organization> Organization { get; set; }
        public virtual ICollection<OrganizationUserType> OrganizationUserType { get; set; }

    }

    public partial class OrganizationUserType
    {
        [ScaffoldColumn(false)]
        public int organizationUserTypeId { get; set; }

        [Required]
        [Display(Name = "Organization Type")]
        public int organizationTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string organizationType { get; set; }

        [Required]
        [Display(Name = "User Type")]
        public int userTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string userType { get; set; }

        public virtual OrganizationType OrganizationType { get; set; }
        public virtual UserType UserType { get; set; }
    }

    public partial class ProviderService
    {
        public ProviderService()
        {
            this.OrganizationServices = new HashSet<OrganizationServices>();
            this.ServiceProfile = new HashSet<ServiceProfile>();
            this.NrufCompanyInfo = new HashSet<NrufCompanyInfo>();
        }

        public virtual ICollection<OrganizationServices> OrganizationServices { get; set; }
        public virtual ICollection<ServiceProfile> ServiceProfile { get; set; }
        public virtual ICollection<NrufCompanyInfo> NrufCompanyInfo { get; set; }

        [ScaffoldColumn(false)]
        public int ProviderServiceId { get; set; }

        [Required]
        [Display(Name = "Provider Service")]
        public string name { get; set; }
    }

    public partial class Report
    {
        [ScaffoldColumn(false)]
        public int ReportId { get; set; }

        [Required]
        [Display(Name = "Name of Report")]
        public string name { get; set; }
    }

    public partial class Resource
    {

        public Resource()
        {
            this.ApplicationResource = new HashSet<ApplicationResource>();
            this.ResourceLink = new HashSet<ResourceLink>();

        }

        public virtual ICollection<ApplicationResource> ApplicationResource { get; set; }
        public virtual ICollection<ResourceLink> ResourceLink { get; set; }

        public virtual ResourceState ResourceState { get; set; }
        //public virtual ResourceType ResourceType { get; set; }
        //public virtual ResourceSubType ResourceSubType { get; set; }

        [ScaffoldColumn(false)]
        public int resourceId { get; set; }

        [Required]
        [Display(Name="Resource")]
        public string resource1 { get; set; }

       // [Required]
        [Display(Name = "Resource Type")]
        public int resourceTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string type { get; set; }

       // [Required]
        [Display(Name = "Resource Sub Type")]
        public int resourceSubTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string subtype { get; set; }

        [Display(Name = "Binary")]
        public string binary { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? createdDate { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? dateChanged { get; set; }

        [Display(Name = "State")]
        public int resourceLinkId { get; set; }

        [Display(Name = "Resource State")]
        public int resourceStateId { get; set; }

        [ScaffoldColumn(false)]
        public string state { get; set; }
    }

    public partial class ResourceLink
    {
        [ScaffoldColumn(false)]
        public int resourceLinkId { get; set; }

        [Display(Name = "Parent Resource")]
        public string parentResource { get; set; }

        [Display(Name = "Child Resource")]
        public string childResource { get; set; }

        [Display(Name = "State")]
        public int linkedResourceStateId { get; set; }

        [ScaffoldColumn(false)]
        public string state { get; set; }

        public virtual Resource Resource { get; set; }
        public virtual LinkedResourceState LinkedResourceState { get; set; }
    }

    public partial class ResourceState
    {
        public ResourceState()
        {

            this.Resource = new HashSet<Resource>();
        }

        public virtual ICollection<Resource> Resource { get; set; }

        [ScaffoldColumn(false)]
        public int resourceStateId { get; set; }

        [Required]
        [Display(Name = "State")]
        public string name { get; set; }
    }

    public partial class ResourceSubType
    {
        public ResourceSubType()
        {

            this.Application = new HashSet<Application>();
           // this.Resource = new HashSet<Resource>();
           // this.UserResourceType = new HashSet<UserResourceType>();
            this.ServiceProfile = new HashSet<ServiceProfile>();
            this.OrganizationResourceType = new HashSet<OrganizationResourceType>();
            this.NotificationDetail = new HashSet<NotificationDetail>();

        }

        public virtual ICollection<Application> Application { get; set; }
       // public virtual ICollection<Resource> Resource { get; set; }
       // public virtual ICollection<UserResourceType> UserResourceType { get; set; }
        public virtual ICollection<ServiceProfile> ServiceProfile { get; set; }
        public virtual ICollection<OrganizationResourceType> OrganizationResourceType { get; set; }
        public virtual ICollection<NotificationDetail> NotificationDetail { get; set; }

        [ScaffoldColumn(false)]
        public int resourceSubTypeId { get; set; }

        [Display(Name = "Resource Type")]
        public int resourceTypeId { get; set; }

        [Required]
        [Display(Name = "Resource Sub Type")]
        public string name { get; set; }

        [ScaffoldColumn(false)]
        public string subType { get; set; }

        [Required]
        [Display(Name = "Format")]
        public string format { get; set; }

        [Required]
        [Display(Name = "Regular Expression")]
        public string formatRegex { get; set; }

        public virtual ResourceType ResourceType { get; set; }
    }

    public partial class ResourceType
    {
        public ResourceType()
        {
            this.ResourceSubType = new HashSet<ResourceSubType>();
            this.Application = new HashSet<Application>();
           // this.Resource = new HashSet<Resource>();
           // this.UserResourceType = new HashSet<UserResourceType>();
            this.ServiceProfile = new HashSet<ServiceProfile>();
            this.OrganizationResourceType = new HashSet<OrganizationResourceType>();
            this.NotificationDetail = new HashSet<NotificationDetail>();
        }

        public virtual ICollection<ResourceSubType> ResourceSubType { get; set; }
        public virtual ICollection<Application> Application { get; set; }
       // public virtual ICollection<Resource> Resource { get; set; }
       // public virtual ICollection<UserResourceType> UserResourceType { get; set; }
        public virtual ICollection<ServiceProfile> ServiceProfile { get; set; }
        public virtual ICollection<OrganizationResourceType> OrganizationResourceType { get; set; }
        public virtual ICollection<NotificationDetail> NotificationDetail { get; set; }

        [ScaffoldColumn(false)]
        public int resourceTypeId { get; set; }

        [Required]
        [Display(Name = "Resource Type")]
        public string name { get; set; }

        [Required]
        [Display(Name = "Format")]
        public string format { get; set; }

        [Required]
        [Display(Name = "Regular Expression")]
        public string formatRegex { get; set; }


    }

    public partial class Service
    {
        public Service()
        {
           this.UserProfileDetail = new HashSet<UserProfileDetail>();
           this.ServiceProfile = new HashSet<ServiceProfile>();
        }

        public virtual ICollection<UserProfileDetail> UserProfileDetail { get; set; }
        public virtual ICollection<ServiceProfile> ServiceProfile { get; set; }

        [ScaffoldColumn(false)]
        public int serviceId { get; set; }

        [Required]
        [Display(Name = "Service Name")]
        public string name { get; set; }

        [Required]
        [Display(Name = "Application (Yes/ No)")]
        [DefaultValue(false)]
        public bool applicationYN { get; set; }
    }

    public partial class ServiceProfile
    {
        [ScaffoldColumn(false)]
        public int serviceProfileId { get; set; }

        [Display(Name = "Service")]
        public int serviceId { get; set; }

        [ScaffoldColumn(false)]
        public string service { get; set; }

        [Display(Name = "Resource Type")]
        public int resourceTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string resourceType { get; set; }

        [Display(Name = "Resource Sub Type")]
        public int resourceSubTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string resourceSubType { get; set; }

        [Required]
        [Display(Name = "Provider Service")]
        public int providerServiceId { get; set; }

        [ScaffoldColumn(false)]
        public string providerService { get; set; }

        [Required]
        [Display(Name = "Assigned to Exteral User (Yes/ No)")]
        //  [Range(typeof(bool), "true", "true", ErrorMessage = "Please indicate the primary contact")]
        [DefaultValue(false)]
        public bool assigned { get; set; }

        public virtual ProviderService ProviderService { get; set; }
        public virtual Service Service { get; set; }
        public virtual ResourceType ResourceType { get; set; }
        public virtual ResourceSubType ResourceSubType { get; set; }
    }

    public partial class SystemMessage
    {
        [ScaffoldColumn(false)]
        public long systemMessageId { get; set; }

        [Required]
        [Display(Name = "Title")]
        public string title { get; set; }

        [Required]
        [Display(Name = "Message")]
        public string message { get; set; }

        [Display(Name = "Expiration Date")]
        public DateTime? expiryDate { get; set; }

        [Display(Name = "Date Created")]
        public System.DateTime? createdDate { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? dateChanged { get; set; }
    }

    public partial class SystemParameter
    {
        [ScaffoldColumn(false)]
        public int systemParameterId { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string name { get; set; }

        [Required]
        [Display(Name = "Value")]
        public string value { get; set; }

        [ScaffoldColumn(false)]
        public string UserId { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? createdDate { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? dateChanged { get; set; }
    }

    public partial class UserNotification
    {
        [ScaffoldColumn(false)]
        public int userNotificationId { get; set; }

        [Required]
        [Display(Name = "User")]
        public string UserId { get; set; }

        [ScaffoldColumn(false)]
        public string user { get; set; }

        [Required]
        [Display(Name = "Type of Notification")]
        public int notificationId { get; set; }

        [ScaffoldColumn(false)]
        public string notification { get; set; }

        [Required]
        [Display(Name = "Assigned to User (Yes/ No)")]
        [DefaultValue(true)]
        public bool assigned { get; set; }

        public virtual User User { get; set; }
        public virtual Notification Notification { get; set; }
    }

    public partial class UserProfile
    {
        public UserProfile()
        {
            this.UserProfileDetail = new HashSet<UserProfileDetail>();
        }

        public virtual ICollection<UserProfileDetail> UserProfileDetail { get; set; }

        [ScaffoldColumn(false)]
        public int userProfileId { get; set; }

        [Required]
        [Display(Name = "User Profile Name")]
        public string name { get; set; }

        [Required]
        [Display(Name = "Default Profile (Yes/ No)")]
        [DefaultValue(false)]
        public bool defaultProfile { get; set; }
    }

    public partial class UserProfileDetail
    {
        [ScaffoldColumn(false)]
        public int userProfileDetailId { get; set; }

        [Required]
        [Display(Name = "User Profile Name")]
        public int userProfileId { get; set; }

        [ScaffoldColumn(false)]
        public string userProfile { get; set; }

        [Required]
        [Display(Name = "Type of Service")]
        public int serviceId { get; set; }

        [ScaffoldColumn(false)]
        public string service { get; set; }

        [Required]
        [Display(Name = "Type of User")]
        public int userTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string userType { get; set; }

        [Required]
        [Display(Name = "For External Users (Yes/ No)")]
        [DefaultValue(false)]
        public bool assigned { get; set; }

        public virtual UserType UserType { get; set; }
        public virtual UserProfile UserProfile { get; set; }
        public virtual Service Service { get; set; }
    }

    public partial class UserResourceType
    {
        [ScaffoldColumn(false)]
        public int userResourceTypeId { get; set; }

       // [Required]
        [Display(Name = "User Type")]
        public int userTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string userType { get; set; }

        [Display(Name = "Resource Type")]
        public int resourceTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string resourceType { get; set; }

        [Display(Name = "Resource Sub Type")]
        public int resourceSubTypeId { get; set; }

        [ScaffoldColumn(false)]
        public string resourceSubType { get; set; }

        [Display(Name = "State")]
        public int linkedResourceStateId { get; set; }

        [ScaffoldColumn(false)]
        public string state { get; set; }

       // public virtual UserType UserType { get; set; }
        //public virtual LinkedResourceState LinkedResourceState { get; set; }
        //public virtual ResourceType ResourceType { get; set; }
        //public virtual ResourceSubType ResourceSubType { get; set; }
    }

    public partial class UserState
    {
        public UserState()
        {
            this.User = new HashSet<User>();
        }

        public virtual ICollection<User> User { get; set; }

        [ScaffoldColumn(false)]
        public int userStateId { get; set; }

        [Display(Name = "Name")]
        public string name { get; set; }
    }

    public partial class UserType
    {
        public UserType()
        {
            this.User = new HashSet<User>();
            this.OrganizationUserType = new HashSet<OrganizationUserType>();
            this.UserProfileDetail = new HashSet<UserProfileDetail>();
        //  this.UserResourceType = new HashSet<UserResourceType>();
            this.NotificationDetail = new HashSet<NotificationDetail>();
        }

        public virtual ICollection<User> User { get; set; }
        public virtual ICollection<OrganizationUserType> OrganizationUserType { get; set; }
        public virtual ICollection<UserProfileDetail> UserProfileDetail { get; set; }
      // public virtual ICollection<UserResourceType> UserResourceType { get; set; }
        public virtual ICollection<NotificationDetail> NotificationDetail { get; set; }

        [ScaffoldColumn(false)]
        public int userTypeId { get; set; }

        [Display(Name = "Name")]
        public string name { get; set; }
    }

    public partial class ReportOnNXXAndNPACodes
    {

        public ReportOnNXXAndNPACodes()
        {

        }

        [ScaffoldColumn(false)]
        public int ReportOnNXXAndNPACodesId { get; set; }

        public int NPA { get; set; }

        [Display(Name = "NX\\X")]
        public int NX { get; set; }

        [Display(Name = "Service Allocation")]
        public string ServiceAllocation { get; set; }

        [Display(Name = "0")]
        public string X_0 { get; set; }

        [ScaffoldColumn(false)]
        public string X_0_CellStyle { get; set; }

        [Display(Name = "1")]
        public string X_1 { get; set; }

        [ScaffoldColumn(false)]
        public string X_1_CellStyle { get; set; }

        [Display(Name = "2")]
        public string X_2 { get; set; }

        [ScaffoldColumn(false)]
        public string X_2_CellStyle { get; set; }

        [Display(Name = "3")]
        public string X_3 { get; set; }

        [ScaffoldColumn(false)]
        public string X_3_CellStyle { get; set; }

        [Display(Name = "4")]
        public string X_4 { get; set; }

        [ScaffoldColumn(false)]
        public string X_4_CellStyle { get; set; }

        [Display(Name = "5")]
        public string X_5 { get; set; }

        [ScaffoldColumn(false)]
        public string X_5_CellStyle { get; set; }

        [Display(Name = "6")]
        public string X_6 { get; set; }

        [ScaffoldColumn(false)]
        public string X_6_CellStyle { get; set; }

        [Display(Name = "7")]
        public string X_7 { get; set; }

        [ScaffoldColumn(false)]
        public string X_7_CellStyle { get; set; }

        [Display(Name = "8")]
        public string X_8 { get; set; }

        [ScaffoldColumn(false)]
        public string X_8_CellStyle { get; set; }

        [Display(Name = "9")]
        public string X_9 { get; set; }

        [ScaffoldColumn(false)]
        public string X_9_CellStyle { get; set; }

        [Display(Name = "Date Created")]
        public System.DateTime? DateCreated { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? DateChanged { get; set; }

    }


    public partial class ReportOnNXXAndNPACodesKey
    {
        public ReportOnNXXAndNPACodesKey()
        {

        }

        [ScaffoldColumn(false)]
        public int ReportOnNXXAndNPACodesKeyId { get; set; }

        public string Key { get; set; }

        [Display(Name = "Text")]
        public string CellText { get; set; }

        [Display(Name = "Style")]
        public string CellStyle { get; set; }

        [Display(Name = "Date Created")]
        public System.DateTime? DateCreated { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? DateChanged { get; set; }
    }

    public partial class CentralOfficeCodeAssignments
    {
        public CentralOfficeCodeAssignments()
        {

        }

        [ScaffoldColumn(false)]
        public int CentralOfficeCodeAssignmentsId { get; set; }

        [Display(Name = "Name of Organization")]
        public int organizationId { get; set; }


        public string NPA { get; set; }

        public string NXX { get; set; }

        [Display(Name = "Date Created")]
        public System.DateTime? DateCreated { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? DateChanged { get; set; }

        public virtual Organization Organization1 { get; set; }

    }

    //public class ExceptionLogger
    //{
    //    [Key]
    //    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    //    public long Id { get; set; }
    //    public string UserId { get; set; }
    //    public string UserName { get; set; }
    //    public string ExceptionMessage { get; set; }
    //    public string ControllerName { get; set; }
    //    public string ExceptionStackTrace { get; set; }
    //    public DateTime? LogTime { get; set; }

    //}

    //public partial class Roles
    //{
    //    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
    //    public Roles()
    //    {
    //        this.Users = new HashSet<Users>();
    //    }

    //    public string RoleId { get; set; }
    //    public string Name { get; set; }

    //    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
    //    public virtual ICollection<Users> Users { get; set; }
    //}

    //public partial class UserClaims
    //{
    //    public int UserClaimId { get; set; }
    //    public string UserId { get; set; }
    //    public string ClaimType { get; set; }
    //    public string ClaimValue { get; set; }

    //    public virtual Users Users { get; set; }
    //}

    //public partial class UserLogins
    //{
    //    public string LoginProvider { get; set; }
    //    public string ProviderKey { get; set; }
    //    public string UserId { get; set; }

    //    public virtual Users Users { get; set; }
    //}

    //public partial class Users
    //{
    //    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
    //    public Users()
    //    {
    //        this.UserClaims = new HashSet<UserClaims>();
    //        this.UserLogins = new HashSet<UserLogins>();
    //        this.Roles = new HashSet<Roles>();
    //    }

    //    public string UserId { get; set; }
    //    public string Email { get; set; }
    //    public bool EmailConfirmed { get; set; }
    //    public string PasswordHash { get; set; }
    //    public string SecurityStamp { get; set; }
    //    public string PhoneNumber { get; set; }
    //    public bool PhoneNumberConfirmed { get; set; }
    //    public bool TwoFactorEnabled { get; set; }
    //    public Nullable<System.DateTime> LockoutEndDateUtc { get; set; }
    //    public bool LockoutEnabled { get; set; }
    //    public int AccessFailedCount { get; set; }
    //    public string UserName { get; set; }
    //    public string name { get; set; }
    //    public string organization { get; set; }
    //    public string userType { get; set; }
    //    public string userProfile { get; set; }
    //    public string securityQuestion { get; set; }
    //    public string securityAnswer { get; set; }
    //    public System.DateTime createdDate { get; set; }
    //    public System.DateTime dateChanged { get; set; }
    //    public Nullable<System.DateTime> lastConnectionDate { get; set; }
    //    public string state { get; set; }
    //    public int userNo { get; set; }
    //    public int organizationId { get; set; }
    //    public int userTypeId { get; set; }
    //    public int userProfileId { get; set; }
    //    public int userStateId { get; set; }

    //    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
    //    public virtual ICollection<UserClaims> UserClaims { get; set; }
    //    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
    //    public virtual ICollection<UserLogins> UserLogins { get; set; }
    //    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
    //    public virtual ICollection<Roles> Roles { get; set; }
    //}

    //public partial class UserRoles
    //{
    //    public string UserId { get; set; }
    //    public string RoleId { get; set; }
    //}


    public class ApplicationDbContext : IdentityDbContext<User>
    {
        public ApplicationDbContext()
            : base(Config.ConnectionStringName2, throwIfV1Schema: false)
        {
            Database.SetInitializer<ApplicationDbContext>(new jnasInitializer());
            InitializeDatabase();
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected virtual void InitializeDatabase()
        {
            if (!Database.Exists())
            {
                Database.Initialize(true);
            }
        }

        protected override void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Add(new FKConvention());
            base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<Department>().
            //  Property(d => d.Name).
            //  IsRequired().
            //  HasMaxLength(50);

            //modelBuilder.Entity<Department>().
            //  Property(d => d.DepartmentID).
            //  HasDatabaseGenerationOption(DatabaseGenerationOption.None);

            //modelBuilder.Entity<Department>().
            //  HasMany(d => d.Courses).
            //  WithRequired(c => c.Department).
            //  HasForeignKey(c => c.DepartmentID).
            //  WillCascadeOnDelete();

            //modelBuilder.Entity<Department>().
            //  Ignore(d => d.Administrator);

            //modelBuilder.Entity<Course>().
            //  Property(c => c.Title).
            //  IsRequired().
            //  HasColumnName("Name");

            modelBuilder.Entity<User>().ToTable("Users").Property(p => p.Id).HasColumnName("UserId");
            modelBuilder.Entity<IdentityRole>().ToTable("Roles").Property(p => p.Id).HasColumnName("RoleId");
            modelBuilder.Entity<IdentityUserRole>().ToTable("UserRoles");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UserLogins");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaims").Property(p => p.Id).HasColumnName("UserClaimId");

            //modelBuilder.Entity<Resource>().
            // HasRequired(c => c.ResourceType).
            // WithMany().
            // WillCascadeOnDelete(false);

            //modelBuilder.Entity<Resource>().
            //            HasRequired(c => c.ResourceSubType).
            //            WithMany().
            //            WillCascadeOnDelete(false);

            modelBuilder.Entity<Resource>().
                        HasRequired(c => c.ResourceState).
                        WithMany().
                        WillCascadeOnDelete(false);

            // modelBuilder.Entity<UserResourceType>().
            //HasRequired(c => c.ResourceType).
            //WithMany().
            //WillCascadeOnDelete(false);

            // modelBuilder.Entity<UserResourceType>().
            //             HasRequired(c => c.ResourceSubType).
            //             WithMany().
            //             WillCascadeOnDelete(false);

            //modelBuilder.Entity<UserResourceType>().
            //            HasRequired(c => c.LinkedResourceState).
            //            WithMany().
            //            WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationResource>().
                       HasRequired(c => c.Resource).
                       WithMany().
                       WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationResource>().
                      HasRequired(c => c.Application).
                      WithMany().
                      WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationResource>().
                     HasRequired(c => c.ApplicationResourceState).
                     WithMany().
                     WillCascadeOnDelete(false);

            modelBuilder.Entity<ResourceLink>().
                        HasRequired(c => c.Resource).
                        WithMany().
                        WillCascadeOnDelete(false);

            modelBuilder.Entity<ResourceLink>().
                        HasRequired(c => c.LinkedResourceState).
                        WithMany().
                        WillCascadeOnDelete(false);

            //modelBuilder.Entity<ResourceLink>()
            //   .Property(e => e.LinkedResourceState_linkedResourceStateId1)
            //   .IsOptional();

            //modelBuilder.Entity<ResourceLink>()
            //   .Property(e => e.Resource_resourceId)
            //   .IsOptional();

            //modelBuilder.Entity<Resource>()
            //   .Property(e => e.ResourceState_resourceStateId1)
            //   .IsOptional();

            //modelBuilder.Entity<Organization>().Property(x => x.createdDate).IsOptional();
            //modelBuilder.Entity<Organization>().Property(x => x.dateChanged).IsOptional();

            //modelBuilder.Entity<Application>().Property(x => x.createdDate).IsOptional();
            //modelBuilder.Entity<Application>().Property(x => x.dateChanged).IsOptional();

            //modelBuilder.Entity<ApplicationNotes>().Property(x => x.createdDate).IsOptional();
            //modelBuilder.Entity<ApplicationNotes>().Property(x => x.dateChanged).IsOptional();

            //modelBuilder.Entity<ApplicationResource>().Property(x => x.createdDate).IsOptional();
            //modelBuilder.Entity<ApplicationResource>().Property(x => x.dateChanged).IsOptional();

            //modelBuilder.Entity<Resource>().Property(x => x.createdDate).IsOptional();
            //modelBuilder.Entity<Resource>().Property(x => x.dateChanged).IsOptional();

            //modelBuilder.Entity<User>().Property(x => x.createdDate).IsOptional();
            //modelBuilder.Entity<User>().Property(x => x.dateChanged).IsOptional();

            //modelBuilder.Entity<OrganizationServices>().Property(x => x.createdDate).IsOptional();
            //modelBuilder.Entity<OrganizationServices>().Property(x => x.dateChanged).IsOptional();

            //modelBuilder.Entity<Mail>().Property(x => x.createdDate).IsOptional();
            //modelBuilder.Entity<Mail>().Property(x => x.dateChanged).IsOptional();

            //modelBuilder.Entity<SystemMessage>().Property(x => x.createdDate).IsOptional();
            //modelBuilder.Entity<SystemMessage>().Property(x => x.dateChanged).IsOptional();

            //modelBuilder.Entity<SystemParameter>().Property(x => x.createdDate).IsOptional();
            //modelBuilder.Entity<SystemParameter>().Property(x => x.dateChanged).IsOptional();

        }

        // 38 Tables 1 Migration Table

        public virtual DbSet<Address> Address { get; set; }
        public virtual DbSet<Application> Application { get; set; }
        public virtual DbSet<ApplicationNotes> ApplicationNotes { get; set; }
        public virtual DbSet<ApplicationResource> ApplicationResource { get; set; }
        public virtual DbSet<ApplicationResourceState> ApplicationResourceState { get; set; }
        public virtual DbSet<ApplicationState> ApplicationState { get; set; }
        public virtual DbSet<ApplicationType> ApplicationType { get; set; }
        public virtual DbSet<LinkedResourceState> LinkedResourceState { get; set; }
        public virtual DbSet<Mail> Mail { get; set; }
        public virtual DbSet<Notification> Notification { get; set; }
        public virtual DbSet<NotificationDetail> NotificationDetail { get; set; }
        public virtual DbSet<Organization> Organization { get; set; }
        public virtual DbSet<OrganizationResourceType> OrganizationResourceType { get; set; }
        public virtual DbSet<OrganizationServices> OrganizationServices { get; set; }
        public virtual DbSet<OrganizationState> OrganizationState { get; set; }
        public virtual DbSet<OrganizationType> OrganizationType { get; set; }
        public virtual DbSet<OrganizationUserType> OrganizationUserType { get; set; }
        public virtual DbSet<ProviderService> ProviderService { get; set; }
        public virtual DbSet<Report> Report { get; set; }
        public virtual DbSet<ReportOnNXXAndNPACodes> ReportOnNXXAndNPACodeses { get; set; }
        public virtual DbSet<ReportOnNXXAndNPACodesKey> ReportOnNXXAndNPACodesKeys { get; set; }
        public virtual DbSet<CentralOfficeCodeAssignments> CentralOfficeCodeAssignmentses { get; set; }
        public virtual DbSet<Resource> Resource { get; set; }
        public virtual DbSet<ResourceLink> ResourceLink { get; set; }
        public virtual DbSet<ResourceState> ResourceState { get; set; }
        public virtual DbSet<ResourceSubType> ResourceSubType { get; set; }
        public virtual DbSet<ResourceType> ResourceType { get; set; }
       // public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<Service> Service { get; set; }
        public virtual DbSet<ServiceProfile> ServiceProfile { get; set; }
        public virtual DbSet<SystemMessage> SystemMessage { get; set; }
        public virtual DbSet<SystemParameter> SystemParameter { get; set; }
        // public virtual DbSet<UserClaims> UserClaims { get; set; }
        // public virtual DbSet<UserLogins> UserLogins { get; set; }
        public virtual DbSet<UserNotification> UserNotification { get; set; }
        public virtual DbSet<UserProfile> UserProfile { get; set; }
        public virtual DbSet<UserProfileDetail> UserProfileDetail { get; set; }
        public virtual DbSet<UserResourceType> UserResourceType { get; set; }
       // public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<UserState> UserState { get; set; }
        public virtual DbSet<UserType> UserType { get; set; }
        public virtual DbSet<NrufCompanyInfo> NrufCompanyInfo { get; set; }
        public virtual DbSet<NrufFormU1> NrufFormU1 { get; set; }
        public virtual DbSet<NrufFormU2> NrufFormU2 { get; set; }
        public virtual DbSet<NrufFormU3> NrufFormU3 { get; set; }
        public virtual DbSet<NrufFormU4> NrufFormU4 { get; set; }
        public virtual DbSet<NrufFormF3a> NrufFormF3a { get; set; }
        public virtual DbSet<NrufFormF3b> NrufFormF3b { get; set; }
        // public virtual DbSet<ExceptionLogger> ExceptionLogger { get; set; }
    }

    public class FKConvention : IConceptualModelConvention<AssociationType>
    {
        public void Apply(AssociationType item, DbModel model)
        {
            if (item.IsForeignKey)
            {
                item.Constraint.FromRole.DeleteBehavior = OperationAction.None;
                item.Constraint.ToRole.DeleteBehavior = OperationAction.None;
            }
        }


    }
}