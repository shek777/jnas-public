﻿
using System.Web;
using System.Web.Optimization;

namespace jnas
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-1.12.1.js")); 


            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/vue").Include(
                       "~/Scripts/vue.js"));

            bundles.Add(new ScriptBundle("~/bundles/vue.min").Include(
                      "~/Scripts/vue.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            // DataTables script
            bundles.Add(new ScriptBundle("~/bundles/DataTablesjs").Include(
                     "~/Scripts/DataTables/jquery.dataTables.js",
                     "~/Scripts/DataTables/dataTables.bootstrap.js"));

            // DataTables jnas script
            bundles.Add(new ScriptBundle("~/bundles/jnasdatatablesjs").Include(
                    "~/Scripts/jnas/jnasdatatables1.js"));

            // select NPA
            bundles.Add(new ScriptBundle("~/bundles/jnasNPAselectjs").Include(
              "~/Scripts/jnas/jnasNPAselect.js"));

            // DataTables css
            bundles.Add(new StyleBundle("~/Content/DataTablescss").Include(
                    "~/Content/DataTables/css/jquery.dataTables.css",
                     "~/Content/DataTables/css/dataTables.bootstrap.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            // jquery-ui.css
            bundles.Add(new StyleBundle("~/Content/jqueryuicss").Include(
                      "~/Content/themes/base/jquery-ui.css"));

            //sb-admin css

            bundles.Add(new ScriptBundle("~/bundles/sb-admin-bootstrap").Include(
                      "~/Areas/Administration/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/sb-admin-css").Include(
                      "~/Areas/Administration/Content/bootstrap.css",
                      "~/Areas/Administration/Content/sb-admin.css"));

            bundles.Add(new StyleBundle("~/Content/font-awesome-css").Include(
                      "~/Areas/Administration/Content/font-awesome/css/font-awesome.min.css"));

            //external

            bundles.Add(new ScriptBundle("~/bundles/sb-admin-2-bootstrap").Include(
                      "~/Areas/External/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/metisMenu-js").Include(
                      "~/Areas/External/Scripts/metisMenu.js"));

            bundles.Add(new ScriptBundle("~/bundles/sb-admin-2-js").Include(
                     "~/Areas/External/Scripts/sb-admin-2.js"));

            bundles.Add(new StyleBundle("~/Content/bootstrap-2-css").Include(
                "~/Areas/External/Content/bootstrap.css"));

            bundles.Add(new StyleBundle("~/Content/sb-admin-2-css").Include(
                    "~/Areas/External/Content/sb-admin-2.css"));

            bundles.Add(new StyleBundle("~/Content/metisMenu-css").Include(
                      "~/Areas/External/Content/metisMenu.css"));

            bundles.Add(new StyleBundle("~/Content/font-awesome-2-css").Include(
                      "~/Areas/External/Content/font-awesome.min.css"));



        }
    }
}
