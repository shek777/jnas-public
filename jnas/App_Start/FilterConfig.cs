﻿using jnas.CustomFilter;
using System.Web;
using System.Web.Mvc;

namespace jnas
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
           // filters.Add(new ExceptionHandlerAttribute()); Basic Logger. Replaced by Elmah
        }
    }
}
